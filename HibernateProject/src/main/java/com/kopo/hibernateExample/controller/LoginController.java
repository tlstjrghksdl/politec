package com.kopo.hibernateExample.controller;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.kopo.hibernateExample.model.*;
import com.kopo.hibernateExample.server.LoginService;

@Controller
public class LoginController {

	@Autowired
	LoginService loginService;

	/*
	 * @RequestMapping(value = "/", method = RequestMethod.GET, headers =
	 * "Accept=application/json") public String getItems(Model model) { List<Login>
	 * listOfClients = loginService.getAllClients(); model.addAttribute("login", new
	 * Login()); model.addAttribute("listOfClients", listOfClients); return
	 * "itemsDetails"; }
	 */

	@RequestMapping("/")
	public String home(Model model) throws Exception {
		return "home";
	}

	@RequestMapping("resist")
	public String resist(Model model) throws Exception {
		return "resist";
	}

	@RequestMapping(value = "resist.do", method = RequestMethod.POST)
	public String insertClient(@ModelAttribute Login client, Model model, BindingResult br) throws Exception {

		List<Login> idchk = loginService.idchk(client.getIdentification());

		String passwd = client.getPasswd();
		MessageDigest md = null;

		try {
			md = MessageDigest.getInstance("SHA-256");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		md.update(passwd.getBytes());
		String hex = String.format("%064x", new BigInteger(1, md.digest()));
		System.out.println(hex);
		client.setPasswd(hex);

		// 아이디 정규식
		String idRegex = "^[a-zA-Z]{1}[a-zA-Z0-9_]{4,11}$";
		// 이름 정규식
		String nameRegex = "^[가-힣]{2,4}$";
		// 이메일 정규식
		String emailRegex = "^[_a-z0-9-]+(.[_a-z0-9-]+)*@(?:\\w+\\.)+\\w+$";

		// 유효성 검사
		if (idchk.size() == 0) {
			if (client.getIdentification().matches(idRegex)) {
				if (client.getName().matches(nameRegex)) {
					if (client.getEmail().matches(emailRegex)) {
						loginService.addClient(client);
						model.addAttribute("result", "회원가입완료");
						return "resistResult";
					} else {
						model.addAttribute("result", "이메일을 정확히 입력해주세요");
						return "resistResult";
					}
				} else {
					model.addAttribute("result", "이름을 정확히 입력해주세요");
					return "resistResult";
				}
			} else {
				model.addAttribute("result", "아이디형식에 맞지 않습니다. 다시 입력해주세요");
				return "resistResult";
			}
		} else {
			model.addAttribute("result", "이미 사용중인 ID입니다");
			return "resistResult";
		}

	}

	@RequestMapping(value = "login", method = RequestMethod.POST)
	public String loginClient(@ModelAttribute Login Login, HttpSession session, RedirectAttributes ra)
			throws Exception {
		String url = "";

		String passwd = Login.getPasswd();
		MessageDigest md = null;

		try {
			md = MessageDigest.getInstance("SHA-256");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		md.update(passwd.getBytes());
		String hex = String.format("%064x", new BigInteger(1, md.digest()));
		System.out.println(hex);
		Login.setPasswd(hex);

		List<Login> client2 = loginService.loginCheck(Login.getIdentification(), Login.getPasswd());

		if (client2.size() == 1) {
			if (client2.get(0).getGenre().equals("발라드")) {
				url = "https://www.melon.com/genre/song_list.htm?gnrCode=GN0100";
			} else if (client2.get(0).getGenre().equals("댄스")) {
				url = "https://www.melon.com/genre/song_list.htm?gnrCode=GN0200";
			} else if (client2.get(0).getGenre().equals("랩/힙합")) {
				url = "https://www.melon.com/genre/song_list.htm?gnrCode=GN0300";
			} else if (client2.get(0).getGenre().equals("R&B/Soul")) {
				url = "https://www.melon.com/genre/song_list.htm?gnrCode=GN0400";
			} else if (client2.get(0).getGenre().equals("락/메탈")) {
				url = "https://www.melon.com/genre/song_list.htm?gnrCode=GN0600";
			} else if (client2.get(0).getGenre().equals("트로트")) {
				url = "https://www.melon.com/genre/song_list.htm?gnrCode=GN0700";
			}

			Document doc = Jsoup.connect(url).userAgent("Mozilla").get();

			Elements titles = doc.select("div.ellipsis.rank01>span>a");

			List<String> songs = new ArrayList<String>();
			// 하단의 그림참조하여 적는다
			for (Element e : titles) {
				String song = e.text();
				songs.add(song);
			}
			session.setAttribute("client", client2.get(0));
			session.setAttribute("musics", songs);
		} else {
			session.setAttribute("client", null);
			ra.addFlashAttribute("msg", false);
		}

		return "redirect:/";
	}

	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public String logoutClient(HttpSession session) {
		session.invalidate();

		return "redirect:/";
	}

	@RequestMapping("crolling.do")
	public String result(@RequestParam String songName, Model model) throws IOException, InterruptedException {

		String url = "https://www.melon.com/search/song/index.htm?q=" + songName
				+ "&section=&searchGnbYn=Y&kkoSpl=N&kkoDpType=&ipath=srch_form";

		Document doc = Jsoup.connect(url).userAgent("Mozilla").get();

		Elements titles = doc.select("div.ellipsis>a.fc_gray");

		List<String> songs = new ArrayList<String>();
		// 하단의 그림참조하여 적는다
		for (Element e : titles) {
			String song = e.text();
			songs.add(song);
		}
		if (songs.size() != 0) {
			model.addAttribute("musics", songs);
			model.addAttribute("name", songName);
		} else {
			model.addAttribute("musics", songs);
			model.addAttribute("name", "검색결과없음");

		}
		return "result";
	}

}
