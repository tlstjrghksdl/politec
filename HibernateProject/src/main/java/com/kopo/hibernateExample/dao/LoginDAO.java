package com.kopo.hibernateExample.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kopo.hibernateExample.model.Login;

@Repository
public class LoginDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@SuppressWarnings("unchecked")
	public List<Login> getAllClients() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Login> ClientsList = session.createQuery("from Login").list();
		
		return ClientsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Login> idchk(String identification) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Login> idchk = session.createQuery("from Login where identification = '"+ identification + "'").list();
		return idchk;
	}
	
	@SuppressWarnings("unchecked")
	public List<Login> loginCheck(String identification, String passwd) {
		
		Session session = this.sessionFactory.getCurrentSession();
		List<Login> idchk = session.createQuery("from Login where identification = '"+ identification + "' and passwd = '" + passwd + "'").list();	
		return idchk;
	}

	public Login getClient(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Login Clients = (Login) session.get(Login.class, new Integer(id));
		return Clients;
	}

	public Login addClient(Login Clients) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(Clients);
		return Clients;
	}

	public void updateClient(Login Clients) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(Clients);
	}

	public Login loginCheck(Login Clients) {
		Session session = this.sessionFactory.getCurrentSession();
		Login clients = (Login) session.get(Login.class, new Integer(Clients.getId()));
		return clients;
	}
	
	public void deleteClient(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Login p = (Login) session.load(Login.class, new Integer(id));
		if (null != p) {
			session.delete(p);
		}
	}
	
}
