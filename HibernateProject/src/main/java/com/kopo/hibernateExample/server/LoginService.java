package com.kopo.hibernateExample.server;

import java.util.List;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kopo.hibernateExample.dao.LoginDAO;
import com.kopo.hibernateExample.model.*;

@Service("LoginService")
public class LoginService {

	@Autowired
	LoginDAO ClientsDao;
	
	@Transactional
	public List<Login> getAllClients() {
		return ClientsDao.getAllClients();
	}

	@Transactional
	public Login getClient(int id) {
		return ClientsDao.getClient(id);
	}

	@Transactional
	public void addClient(Login Clients) {
		ClientsDao.addClient(Clients);
	}

	@Transactional
	public void updateClient(Login Clients) {
		ClientsDao.updateClient(Clients);

	}

	@Transactional
	public void deleteClient(int id) {
		ClientsDao.deleteClient(id);
	}
	
	@Transactional
	public List<Login> loginCheck(String identification, String passwd)  {
		return ClientsDao.loginCheck(identification, passwd);
	}
	
	@Transactional
	public List<Login> idchk(String identification) {
		return ClientsDao.idchk(identification);
	}
}
