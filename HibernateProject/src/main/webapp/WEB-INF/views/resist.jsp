<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	request.setCharacterEncoding("utf-8");
%>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Welcome To Searching</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link
	href="https://fonts.googleapis.com/css2?family=Rowdies:wght@700&display=swap"
	rel="stylesheet">

<style type="text/css">
h1 {
	font-family: 'Rowdies', cursive;
}

table {
	color: white;
	background-color: black;
	width: 650px;
}

a:link {
	text-decoration: none;
	color: white;
}

a:visited {
	text-decoration: none;
	color: white;
}

a:active {
	text-decoration: none;
	color: white;
}

a:hover {
	text-decoration: none;
	color: white;
}

header {
	position: fixed;
	top: 0;
	height: 6%;
	width: 100%;
	padding: 20px;
	box-sizing: border-box;
	background: white;
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	font-weight: bold;
	display: flex;
	justify-content: space-between;
	align-items: center;
}

nav {
	display: flex;
	align-items: flex-end;
	justify-content: space-between;
	transition: align-items .2s;
}

.logo {
	font-size: 2rem;
	display: inline-block;
	padding: 20px 30px;
	background: #F35B66;
	color: #fff;
	margin: 50px 0 0 50px;
	transition: all .2s;
}

ul {
	display: flex;
	margin: 50px 50px 0 0;
	padding: 0;
	transition: margin .2s;
}

li:not(:last-child) {
	margin-right: 20px;
}

li a {
	display: block;
	padding: 10px 20px;
}

.toggle-menu {
	display: none;
	font-size: 2rem;
	color: #fff;
	margin: 10px 10px 0 0;
	transition: margin .2s;
}
input {
	text-align: left;
}
p, b {
	color: white;
}
</style>
<script type="text/javascript">
	function logout() {
		location.href = "logout";
	}

	function resist() {
		location.href = "resist";
	}
</script>
</head>
<body style="background-color: black; padding-top: 100px;">
	<div class="container">
		<div class="row">
			<div class="col">
				<header>
					<nav style="height: 200;">
						<h1 style="color: Black; position: fixed; top: 0; height: 100;">Resist</h1>
					</nav>
				</header>
				<form action="resist.do" method="post">
					<div><p style="color: white; font-size: 30px;">ID</p><input type="text" name="identification" class="btn btn-light" style="text-align: left" placeholder="영문, 숫자포함 4~11자"><br><br>
					<p style="font-size: 30px;">Password</p><input type="text" name="passwd" class="btn btn-light"><br><br>
					<p style="font-size: 30px;">Name</p><input type="text" name="name" class="btn btn-light"><br><br>
					<p style="font-size: 30px;">E-mail</p><input type="text" name="email" class="btn btn-light"><br><br><br>
					<p style="font-size: 30px;">Favorite Genre</p>
					<b>발라드&nbsp;</b><input type="radio" name="genre" value="발라드" class="btn btn-light">
					<b>&nbsp;&nbsp;&nbsp;&nbsp;댄스&nbsp;</b><input type="radio" name="genre" value="댄스" class="btn btn-light">
					<b>&nbsp;&nbsp;&nbsp;&nbsp;랩/힙합&nbsp;</b><input type="radio" name="genre" value="랩/힙합" class="btn btn-light">
					<b>&nbsp;&nbsp;&nbsp;&nbsp;R&B/Soul&nbsp;</b><input type="radio" name="genre" value="R&B/Soul" class="btn btn-light">
					<b>&nbsp;&nbsp;&nbsp;&nbsp;락/메탈&nbsp;</b><input type="radio" name="genre" value="락/메탈" class="btn btn-light">
					<b>&nbsp;&nbsp;&nbsp;&nbsp;트로트&nbsp;</b><input type="radio" name="genre" value="트로트" class="btn btn-light">
					<br><br><br>
					<button type="submit" class="btn btn-light">Resist</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>
</html>