<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Welcome To Searching</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link
	href="https://fonts.googleapis.com/css2?family=Rowdies:wght@700&display=swap"
	rel="stylesheet">

<style type="text/css">
h1 {
	font-family: 'Rowdies', cursive;
}

table {
	color: white;
	background-color: black;
	width: 650px;
}

a:link {
	text-decoration: none;
	color: white;
}

a:visited {
	text-decoration: none;
	color: white;
}

a:active {
	text-decoration: none;
	color: white;
}

a:hover {
	text-decoration: none;
	color: white;
}

header {
	position: fixed;
	top: 0;
	height: 6%;
	width: 100%;
	padding: 20px;
	box-sizing: border-box;
	background: white;
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	font-weight: bold;
	display: flex;
	justify-content: space-between;
	align-items: center;
}

nav {
	display: flex;
	align-items: flex-end;
	justify-content: space-between;
	transition: align-items .2s;
}

.logo {
	font-size: 2rem;
	display: inline-block;
	padding: 20px 30px;
	background: #F35B66;
	color: #fff;
	margin: 50px 0 0 50px;
	transition: all .2s;
}

ul {
	display: flex;
	margin: 50px 50px 0 0;
	padding: 0;
	transition: margin .2s;
}

li:not(:last-child) {
	margin-right: 20px;
}

li a {
	display: block;
	padding: 10px 20px;
}

.toggle-menu {
	display: none;
	font-size: 2rem;
	color: #fff;
	margin: 10px 10px 0 0;
	transition: margin .2s;
}
</style>
</head>
<body style="background-color: black; padding-top: 100px;">
	<div class="container">
		<div class="row">
			<div class="col">
				<header>
					<nav>
						<h1 style="color: Black; position: fixed; top: 0; height: 100;">Muzi
							Pointer </h1>
					</nav>
				</header>
				<h1 style="color: white; font-family: sans-serif;">검색결과 : '${name}'</h1>
				<table border="1">
					<colgroup>
						<col width="75%">
						<col width="25%">
					</colgroup>
					<c:forEach var="music" items="${musics}">
						<tr>
							<td><a
								href="https://www.youtube.com/results?search_query=${music}+${name}" target="_blank">${music}</a></td>
							<td>${name}</td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>
</html>
