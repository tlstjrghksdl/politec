<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>${result}</title>
</head>
<body>
<c:if test="${result == '회원가입완료'}">
<script type="text/javascript">
alert('${result}');
location.href="/";
</script>
</c:if>
<c:if test="${result != '회원가입완료'}">
<script type="text/javascript">
alert('${result}');
location.href="resist";
</script>
</c:if>


</body>
</html>