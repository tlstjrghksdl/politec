<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="com.example.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Insert title here</title>
<style type="text/css">
table {
	width: 500px;
}
</style>
</head>
<body>

	<form method="post">
		<input type="button" onclick="location.href='/'" value="후보등록" />
		<input type="button" onclick="location.href='vote'" value="투표" />
		<input type="button" style="background-color: yellow;"
			onclick="location.href='/voteResult'" value="개표결과" />
	</form>
	<table border="1">
		<colgroup>
			<col width="20%">
			<col width="80%">
		</colgroup>
		<c:out value="기호 ${id}번 득표성향 분석">기호 ${id}번 득표성향 분석</c:out>
		<c:forEach var="results" items="${results}" varStatus="status">
			<tr>
				<td>${status.count * 10}대</td>
				<td><input type="text" style="background-color: red; width: ${results * 20}px;" disabled/> ${results}</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>