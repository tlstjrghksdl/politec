<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="com.example.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>

<script type="text/javascript">
	var select = document.getElementById("selected");
	var value = select.options[select.selectedIndex].value;
	alert(value);
</script>
</head>
<body>
	<form>
		<input type="button" onclick="location.href='/'" value="후보등록" /> <input
			type="button" style="background-color: yellow;"
			onclick="location.href='vote'" value="투표" /> <input type="button"
			onclick="location.href='voteResult'" value="개표결과" />
	</form>
	<form method="post" action="voteResult">
		<table border="1">
			<tr>
				<td>
					<!-- select로 선택할 수 있는 form을 만들었다. 선택은 사용할 수 없게 하고 맨 처음 고정으로 해준다. -->
					<select name="id" size="1">
						<option value="none" selected="selected" disabled>=== 선택
							===</option>
						<c:forEach var="candidate" items="${candidates}">
							<option value="${candidate.id}">${candidate.id}번
								${candidate.name}</option>
						</c:forEach>
				</select>
				</td>
				<td><select name="age" size="1">
						<option value="none" selected="selected" disabled>=== 선택
							===</option>
						<c:forEach var="i" begin="1" end="9">
							<option value="${i * 10}">${i * 10}대</option>
						</c:forEach>
				</select></td>

				<td><input type="submit" value="투표하기" /></td>
			</tr>
		</table>
	</form>
</body>
</html>