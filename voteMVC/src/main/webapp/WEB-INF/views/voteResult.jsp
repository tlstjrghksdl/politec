<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="com.example.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<style type="text/css">
	table {
		width: 500px;
	}
</style>
</head>
<body>
	
	<form method="post">
		<input type="button" onclick="location.href='/'" value="후보등록" />
		 <input type="button" onclick="location.href='vote'" value="투표" /> 
		 <input type="button" style="background-color: yellow;" onclick="location.href='voteResult'" value="개표결과" />
	</form>
	<table border="1">
		<colgroup>
			<col width="20%">
			<col width="80%"> 
		</colgroup>
		<c:forEach var="candidate" items="${candidates}" varStatus="status">
			<tr>
			<td><a href="resultOnePerson/?id=${candidate.id}">${candidate.id} ${candidate.name}</a></td>
			<td>${votes[status.index]}</td>
		</tr>
		</c:forEach>
	</table>

</body>
</html>