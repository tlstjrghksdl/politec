<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="com.example.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>
<script type="text/javascript">

	/* 값을 가지고 add 혹은 delete 페이지로 이동할 function */
	function goInsert() {
		var del = document.forms['inputform'];
		del.action = '/insert';
		del.submit();
	}

	function goDelete() {
		var del = document.forms['inputform'];
		del.action = '/delete';
		del.submit();
	}
	
	function check() {
	    var stnName = document.getElementById("name").value;
	    
	    var blank = /\s+/g;
	    var specialCharacters = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
	    var number = /[0-9]/;
	    if (blank.test(stnName)) {
	       alert('이름에 공백을 제거해 주세요');
	       return false;            
	    } else if (specialCharacters.test(stnName)) {
	       alert('이름에 특수문자를 입력할 수 없습니다.');
	       return false;
	    } else if (stnName.length == 0){
	       alert('빈 칸을 입력해 주세요.');
	       return false;
	    } else if (number.test(stnName)) {
	       alert('이름에 숫자를 입력할 수 없습니다.');
	       return false;               
	    } else {
	    }
	    return true;
	}
	
</script>
</head>
<body>
	

	<!-- form을 통해 정보를 취합하여 전해주기 때문에 post사용 -->
	<form method="post"> 
	
		<!-- 각각의 버튼을 눌렀을 때 다른 jsp로 이동을 위해 onclick을 사용 -->
		<input type="button" style="background-color: yellow;" onclick="location.href='/'" value="후보등록" /> 
		<input	type="button"  onclick="location.href='vote'" value="투표" /> 
		<input type="button" onclick="location.href='voteResult'" value="개표결과" />
	</form>
	<form method="post" action="/insert" id ="name1" name="inputform" onsubmit="return check()">
		<table border="1">

			<c:forEach var="candidate" items="${candidates}">
			<tr>
				<td>기호번호 : <input type="text" value="${candidate.id}" name="id" disabled /></td>

				<td>후보명 : <input type="text" value="${candidate.name}" name="name" disabled /></td>
				
				<!-- 삭제버튼을 누르면 delete function이 실행되게 한다. -->
				<td><a href="delete/?id=${candidate.id}" ><input type="button" value="삭제" style="background: lime;"></a></td>
			</tr>
			</c:forEach>
			<!-- 후보자 추가 -->
			<tr>
				<td>기호번호 : <input type="number" value="${length + 1}" name="id" readonly></td>
				<td>후보명 : <input type="text" id="name" name="name"></td>
				<td><input style="background-color:lime;" type="submit"
					 value="추가" /></td>
			</tr>
		</table>
	</form>

</body>
</html>