package com.example.service;

import java.util.List;

import com.example.dto.CandidateVO;

public interface CandidateService {
	
	public List<CandidateVO> selectAll() throws Exception;
	public int insert(CandidateVO candidate) throws Exception;
	public int delete(String id) throws Exception;
}
