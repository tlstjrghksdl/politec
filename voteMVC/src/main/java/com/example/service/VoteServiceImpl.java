package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.VoteDAO;
import com.example.dto.VoteVO;

@Service
public class VoteServiceImpl implements VoteService{

	@Autowired
	VoteDAO dao;
	
	@Override
	public void insert(VoteVO vo) throws Exception {
		// TODO Auto-generated method stub
		System.out.println(vo.getAge());
		dao.insert(vo);
	}

	@Override
	public List<VoteVO> selectVote() throws Exception{
		// TODO Auto-generated method stub
		return dao.selectVote();
	}

	@Override
	public Integer resultNum(int id) throws Exception {
		// TODO Auto-generated method stub
		return dao.resultNum(id);
	}

	@Override
	public Integer resultOnePerson(VoteVO vo) throws Exception {
		// TODO Auto-generated method stub
		return dao.resultOnePerson(vo);
	}

}
