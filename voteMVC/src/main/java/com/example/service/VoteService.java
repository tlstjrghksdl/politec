package com.example.service;

import java.util.List;

import com.example.dto.VoteVO;

public interface VoteService {
	public void insert(VoteVO vo) throws Exception;
	public List<VoteVO> selectVote() throws Exception;
	public Integer resultNum(int id) throws Exception;
	public Integer resultOnePerson(VoteVO vo) throws Exception;
}
