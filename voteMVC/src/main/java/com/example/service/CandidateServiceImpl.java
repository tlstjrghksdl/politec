package com.example.service;

import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Service;

import com.example.dao.CandidateDAO;
import com.example.dto.CandidateVO;

@Service
public class CandidateServiceImpl implements CandidateService {

	@Inject
	private CandidateDAO dao;
	
	@Override
	public List<CandidateVO> selectAll() throws Exception {

		return dao.selectAll();
	}

	@Override
	public int insert(CandidateVO candidate) throws Exception {
		// TODO Auto-generated method stub
		return dao.insert(candidate);
	}

	@Override
	public int delete(String id) throws Exception {
		// TODO Auto-generated method stub
		return dao.delete(id);
	}

}
