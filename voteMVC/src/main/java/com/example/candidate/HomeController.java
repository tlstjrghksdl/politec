package com.example.candidate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.dto.CandidateVO;
import com.example.dto.VoteVO;
import com.example.service.CandidateService;
import com.example.service.VoteService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	@Autowired
	private CandidateService service;
	@Autowired
	private VoteService voteService;

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model) throws Exception {

		List<CandidateVO> candidates = service.selectAll();

		model.addAttribute("candidates", candidates);

		model.addAttribute("length", candidates.size());
		return "candidateView";
	}

	// 투표 UI로 이동
	@RequestMapping(value = "vote", method = RequestMethod.GET)
	public String vote(Model model) throws Exception {
		List<CandidateVO> candidates = service.selectAll();

		model.addAttribute("candidates", candidates);

		return "vote";
	}

	// 결과 UI로 이동
	@RequestMapping(value = "voteResult", method = RequestMethod.GET)
	public String voteResult(Model model) throws Exception {
		List<CandidateVO> candidates = service.selectAll();
		List<Integer> results = new ArrayList<Integer>();
		for (int i = 1; i < candidates.size() + 1; i++) {
			results.add(voteService.resultNum(i));
		}

		model.addAttribute("candidates", candidates);
		model.addAttribute("votes", results);
		return "voteResult";
	}
	
	//결과 한명 UI로 이동
	@RequestMapping(value = "resultOnePerson", method = RequestMethod.GET)
	public String resultOnePerson(@RequestParam int id, Model model) throws Exception {
		List<Integer> results = new ArrayList<Integer>();
		VoteVO vo = new VoteVO();
		for (int i = 10; i < 100; i = i + 10) {
			vo.setId(id);
			vo.setAge(i);
			results.add(voteService.resultOnePerson(vo));
		}
		model.addAttribute("id", id);
		model.addAttribute("results", results);
		return "resultOnePerson";
	}

	// 투표 진행 후 완료 팝업창 띄우고 main으로
	@RequestMapping(value = "voteResult", method = RequestMethod.POST)
	public String voteresult(@ModelAttribute VoteVO vo, Model model) throws Exception {

		voteService.insert(vo);

		model.addAttribute("candidateID", vo.getId());
		model.addAttribute("result", "투표");
		return "result";
	}

	// delete
	@RequestMapping(value = "delete", method = RequestMethod.GET)
	public String delete(@RequestParam("id") String id, Model model) throws Exception {

		service.delete(id);

		model.addAttribute("candidateID", id);
		model.addAttribute("result", "삭제");

		return "result";
	}

	// insert
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public String insert(@ModelAttribute CandidateVO candidate, Model model) throws Exception {
		service.insert(candidate);

		model.addAttribute("candidateID", candidate.getId());
		model.addAttribute("result", "추가");
		return "result";
	}

}
