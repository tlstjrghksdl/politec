package com.example.dao;

import java.util.List;

import com.example.dto.VoteVO;

public interface VoteDAO {
	public void insert(VoteVO vo) throws Exception;
	public List<VoteVO> selectVote() throws Exception;
	public Integer resultNum(int id) throws Exception;
	public Integer resultOnePerson(VoteVO vo) throws Exception;
}
