package com.example.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.dto.VoteVO;

@Repository
public class VoteDAOImpl implements VoteDAO{
	
	@Inject
	SqlSession sqlSession;
	private static final String Namespace = "com.example.mapper.candidateMapper";
	
	@Override
	public void insert(VoteVO vo) throws Exception {
		System.out.println(vo.getAge());
		// TODO Auto-generated method stub
		sqlSession.insert(Namespace + ".insertVote", vo);
	}

	@Override
	public List<VoteVO> selectVote() throws Exception {
		// TODO Auto-generated method stub
		return sqlSession.selectList(Namespace + ".selectVote");
	}

	@Override
	public Integer resultNum(int id) throws Exception {
		// TODO Auto-generated method stub
		return sqlSession.selectOne(Namespace + ".selectResult", id);
	}

	@Override
	public Integer resultOnePerson(VoteVO vo) throws Exception {
		// TODO Auto-generated method stub
		return sqlSession.selectOne(Namespace + ".resultOnePerson", vo);
	}

}
