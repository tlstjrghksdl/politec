package com.example.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.dto.CandidateVO;

@Repository
public class CandidateDAOImpl implements CandidateDAO {
	
	@Autowired
	private SqlSession sqlSession;
	private static final String Namespace = "com.example.mapper.candidateMapper";
	
	@Override
	public List<CandidateVO> selectAll() throws Exception {
		return sqlSession.selectList(Namespace+".selectAll");
	}

	@Override
	public int insert(CandidateVO candidate) throws Exception {
		
		// TODO Auto-generated method stub
		return sqlSession.insert(Namespace + ".insert", candidate);
	}

	@Override
	public int delete(String id) throws Exception {
		// TODO Auto-generated method stub
		return sqlSession.delete(Namespace + ".delete", id);
	}

}
