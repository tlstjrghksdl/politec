import time
from builtins import range

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait

from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.keys import Keys
import pyperclip
import pymysql

num = input("실행 횟수를 입력하세요.")

driver = webdriver.Firefox(executable_path="C:/Users/tjrgh/Desktop/기본 프로그램/geckodriver.exe")  # Firefox
driver.wait = WebDriverWait(driver, 2)
# 접속
driver.get('http://192.168.56.1:8081')
# 1초 쉬기
time.sleep(1)
rawData = driver.page_source
# 이름으로 넣을 값들. 10개 test해볼 것이기 때문에 배열에 10개 담음


def changeNumberToCharacter(number):
    try:
        firstDigit = int(number / 1000)
        secondDigit = int(int(number % 1000) / 100)
        thirdDigit = int(int(number % 100) / 10)
        forthDigit = int(number % 10)
        result = getOneCharacter(firstDigit) + getOneCharacter(secondDigit) + getOneCharacter(thirdDigit) + getOneCharacter(forthDigit)
        return result
    except:
        print('error changeCharacter!')

def getOneCharacter(number):
    try:
        if number == 0:
            return "영"
        elif number == 1:
            return "일"
        elif number == 2:
            return "이"
        elif number == 3:
            return "삼"
        elif number == 4:
            return "사"
        elif number == 5:
            return "오"
        elif number == 6:
            return "육"
        elif number == 7:
            return "칠"
        elif number == 8:
            return "팔"
        elif number == 9:
            return "구"
    except:
        print('error getOneCharacter!')


# for문으로 10번 돌리기
for i in range(0, int(num)):
    time.sleep(2)
    # 회원가입 버튼 클릭
    elem = driver.find_element_by_id("buttona").click()
    time.sleep(2)
    # 아이디 입력
    elem = driver.find_element_by_xpath("/html/body/div/div/div/form/div/input[1]")
    elem.send_keys('testss' + str(i))
    time.sleep(1)

    # 비밀번호 입력
    elem = driver.find_element_by_xpath("/html/body/div/div/div/form/div/input[2]")
    elem.send_keys('testss' + str(i))
    time.sleep(1)

    # 이름 입력
    elem = driver.find_element_by_xpath("/html/body/div/div/div/form/div/input[3]")
    elem.send_keys(changeNumberToCharacter(i))
    time.sleep(1)

    # 이메일 입력
    elem = driver.find_element_by_xpath("/html/body/div/div/div/form/div/input[4]")
    elem.send_keys("tjrghks3442@naver.com")
    time.sleep(1)

    # favorite 버튼 클릭
    elem = driver.find_element_by_xpath("/html/body/div/div/div/form/div/input[5]").click()
    time.sleep(2)

    # 가입버튼 클릭
    elem = driver.find_element_by_xpath("/html/body/div/div/div/form/div/button").click()
    time.sleep(2)
    # 팝업창 누르기
    da = Alert(driver)
    da.accept()

db = pymysql.connect(
    host='192.168.56.1',
    port=3306,
    user='root',
    passwd='SEOK09HWAN19!',
    db='login',
    charset='utf8'
)
cursor = db.cursor(pymysql.cursors.DictCursor)

sql = "select count(id) from login;"

cursor.execute(sql)

print(cursor.fetchall());
db.commit()
db.close()

