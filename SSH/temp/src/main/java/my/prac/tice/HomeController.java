package my.prac.tice;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	@Autowired
	public Member join;
	
	@Autowired
	
	@Value("30")
	public int num;

	@Value("temptemptemp")
	public String temp;

	@Value("true")
	public String flag1;
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/new", method = RequestMethod.GET)
	@ResponseBody
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);


		model.addAttribute("serverTime", join.register());
		System.out.println(join.register());
		System.out.println(num);
		System.out.println(temp);
		System.out.println(flag1);

		return "hello";
	}
	@RequestMapping("/")
	@ResponseBody
	public String doRgister() {
		return join.register();
	}

}

interface Member {
	public String register();
}

@Component
class join implements Member {
	public String register() {
		return "REGISTERED";
	}
}
