package com.kopo;

public class myDao {
	private Connection connection;
	
	public void check() {
		String connectionStatus = connection.checkConnection();
		System.out.println("Connection Status : " + connectionStatus);
	}
	
	public Connection getConnectionObj() {
		return connection;
	}
	
	public void setConnectionObj(Connection connection) {
		this.connection = connection;
	}
}
