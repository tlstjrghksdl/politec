package com.kopo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyDaoTest {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("myXml.xml");
		myDao dao = context.getBean("myDao", myDao.class);
		dao.check();
		myDao dao2 = context.getBean("myDao2", myDao.class);
		System.out.println("dao : " + dao);
		dao2.check();
		System.out.println("dao : " + dao2);
	}
}
