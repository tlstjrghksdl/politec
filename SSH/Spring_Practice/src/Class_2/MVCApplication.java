package Class_2;

public class MVCApplication {
	public static void main(String[] args) {
		ModelPeople model = retrievePeople();
		ViewPeople view = new ViewPeople();
		
		ControllerPeople controller = new ControllerPeople(model, view);
		controller.updateView();
		
		controller.setPeopleName("Shin");
		controller.setPeopleHobby("Playing Soccer");
		controller.updateView();
	}
	
	public static ModelPeople retrievePeople() {
		ModelPeople model = new ModelPeople();
		model.setName("Lee");
		model.setHobby("Watching Baseball Games");
		return model;
	}
}
