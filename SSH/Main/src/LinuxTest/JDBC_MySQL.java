package LinuxTest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBC_MySQL {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver"); 
		String url = "jdbc:mysql://192.168.23.105:33061/kopo36?serverTimezone=UTC"; // DB와 연결할 ip를 192.168.23.105로 내 ip설정. 포트포워딩을 33061로 설정해놨기 때문에 33061로 한다
		Connection conn = DriverManager.getConnection(url, "root", "kopo36"); // connection으로 DB와 연결 하는 것. 연결할 url과 DB에 만든 root권한자와 passwd를 넣는다
		Statement stmt = conn.createStatement(); // statement 는 SQL 질의문을 전달하는 역할을 한다. 
												 //Statement 인터페이스를 구현한 객체를 Connection 클래스의 createStatement()메소드를 호출함으로써 얻어진다
		ResultSet rset = stmt.executeQuery("select * from twice;"); 
		// ResultSet은 조회시 사용되는 인터페이스다.
		// 조회된 레코드를 얻을 수 있는 pointer를 가지고있다.
		// 조회 쿼리 실행 후 얻어진다.
		// ResultSet은 executeQuery() 메소드에서 실행된 select 문의 결과값을 가지고 있는 객체이다.
		// 그렇기 때문에 executeQuery안에 select 문을 넣는다.
		while (rset.next()) { // executeQuery() 메서드로 실행하는 Query결과를 얻어오는 select Query이다.
							  // 각 레코드가 연결되어 결과로 넘어오게 되는데, 각 결과물들은 next()메서드에 의해 다음 레코드로 이동하게 된다.
							  // 전체 결과를 출력해 보려면 next()메서드를 통해 다음 또는 그 다음으로 이동할 수 있다.
							  // next()는 다음 결과물이 존재한다면 true 값을 리턴한다. 그렇기에 while문에 넣어서 true일때는 계속해서 값을 출력하게 만드는 것이다.
			System.out.println("값 : " + rset.getString(1));
		}
		rset.close();
		stmt.close();
		conn.close();
	}
} 
 