package LinuxTest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBC_MySQL_Drop {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		String url = "jdbc:mysql://192.168.23.105:33061/kopo36?serverTimezone=UTC";
		// DB와 연결할 ip를 192.168.23.105로 내 ip설정. 포트포워딩을 33061로 설정해놨기 때문에 33061로 한다.
		// kopo36이라는 DB_Name에 접속하기위해 써준다.
		Connection conn = DriverManager.getConnection(url, "root", "kopo36"); // connection으로 DB와 연결 하는 것. 연결할 url과 DB에
																				// 만든 root권한자와 passwd를 넣는다
		Statement stmt = conn.createStatement(); // statement 는 SQL 질의문을 전달하는 역할을 한다.
													// Statement 인터페이스를 구현한 객체를 Connection 클래스의 createStatement()메소드를
													// 호출함으로써 얻어진다
		stmt.execute("drop table student_kopo36;");
		
		
		stmt.close(); // statement를 닫아주지 않으면 프로그램 죽는다.
		conn.close(); // connection을 닫아주지 않으면 프로그램 죽는다.^^
	}
}
