package LinuxTest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBC_MySQL_Connect {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		String url = "jdbc:mysql://192.168.56.1:33061/shin?serverTimezone=UTC";
		Connection conn = DriverManager.getConnection(url, "root", "kopo36");
		Statement stmt = conn.createStatement();
		stmt.execute("create table kopo36_freewifi("
				+ "kopo36_inst_place varchar(200), "
				+ "kopo36_inst_place_detail varchar(500), "
				+ "kopo36_inst_city varchar(200), "
				+ "kopo36_inst_country varchar(200), "
				+ "kopo36_inst_place_flag varchar(200), "
				+ "kopo36_service_provider varchar(200), "
				+ "kopo36_wifi_ssid varchar(50), "
				+ "kopo36_inst_date varchar(50), "
				+ "kopo36_place_addr_road varchar(200), "
				+ "kopo36_place_addr_land varchar(200), "
				+ "kopo36_manage_office varchar(50), "
				+ "kopo36_manage_office_phone varchar(50), "
				+ "kopo36_latitude varchar(50), "
				+ "kopo36_longitude varchar(50), "
				+ "kopo36_write_date date);");
		
		
		
		
		stmt.close();
		conn.close();
	}
}
