package LinuxTest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBC_Query {
	public static void useQuery (String Query) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		String url = "jdbc:mysql://192.168.23.105:33061/kopo36?serverTimezone=UTC";
		Connection conn = DriverManager.getConnection(url, "root", "kopo36");
		Statement stmt = conn.createStatement();
		
		
		stmt.execute(Query);
		
		
		
		stmt.close();
		conn.close();
	}
}
