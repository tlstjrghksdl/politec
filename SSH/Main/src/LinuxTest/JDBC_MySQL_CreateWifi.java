package LinuxTest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBC_MySQL_CreateWifi {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		
		Class.forName("com.mysql.cj.jdbc.Driver");
		String url = "jdbc:mysql://192.168.23.105:33061/kopo36?serverTimezone=UTC";
		// DB와 연결할 ip를 192.168.23.105로 내 ip설정. 포트포워딩을 33061로 설정해놨기 때문에 33061로 한다.
		// kopo36이라는 DB_Name에 접속하기위해 써준다.
		Connection conn = DriverManager.getConnection(url, "root", "kopo36"); // connection으로 DB와 연결 하는 것. 연결할 url과 DB에
																				// 만든 root권한자와 passwd를 넣는다
		Statement stmt = conn.createStatement(); // statement 는 SQL 질의문을 전달하는 역할을 한다.
													// Statement 인터페이스를 구현한 객체를 Connection 클래스의 createStatement()메소드를
													// 호출함으로써 얻어진다
		stmt.execute("create table kopo36wifi(" // kopo36_Wife 테이블을 만듬
				+ "inst_place varchar(200), " 	 // 설치 장소명
				+ "inst_place_detail varchar(500), " // 설치장소상세
				+ "inst_city varchar(200), "			// 설치 시.도 명
				+ "inst_country varchar(200), "		// 설치 시.군.구 명
				+ "inst_place_flag varchar(200), "	// 설치 시설 구분
				+ "service_provider varchar(200), "	// 서비스 제공자 명
				+ "wifi_ssid varchar(200), "			// wifi SSID
				+ "inst_date varchar(200), "			// 설치 년월 -> 정제할 것
				+ "place_addr_road varchar(200), "	// 소재지 도로명 주소
				+ "place_addr_land varchar(200), "	// 소재지 지번 주소
				+ "manage_office varchar(200), "		// 관리기관명
				+ "manage_office_phone varchar(200), "// 관리 기관 전화번호
				+ "latitude varchar(200), "				// 위도
				+ "longitude varchar(200), "				// 경도
				+ "write_date date);");				// 데이터 기준 일자
		
		
		stmt.close(); // statement를 닫아주지 않으면 프로그램 죽는다.
		conn.close(); // connection을 닫아주지 않으면 프로그램 죽는다.^^
	}
}
