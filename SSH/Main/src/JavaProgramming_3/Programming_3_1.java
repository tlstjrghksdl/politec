package JavaProgramming_3;

public class Programming_3_1 {
	public static void main(String[] args) {
		int kopo36_ii; // 정수형 변수 ii를 선언
		kopo36_ii = 1 + 2; //정수현 변수 ii는 1과 2를 더한 값이다. 입력
		System.out.printf("#1 - 1 : %d\n", kopo36_ii);
		// %d는 정수를 받아서 출력해준다. ii의 값이 3이기 때문에 %d자리에 3이 오게 되고 \n으로 개행처리를 해준다
		kopo36_ii = 1 + 2 * 3; 
		// 정수형 변수 ii를 다시 선언해준다. 1 + 2 * 3 = 7. ii는 7이 된다.
		// 위에서 3으로 선언했지만 다시 선언해주면 그 값으로 바뀐다
		System.out.printf("#1 - 2 : %d\n", kopo36_ii);
		// %d는 정수를 받아서 출력해준다. ii의 값이 3이기 때문에 %d자리에 3이 오게 되고 \n으로 개행처리를 해준다
	}
}
