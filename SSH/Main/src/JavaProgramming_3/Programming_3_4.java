package JavaProgramming_3;

public class Programming_3_4 {
	public static void main(String[] args) {
		int kopo36_ii;
		kopo36_ii = 12345; //ii변수를 12345로 초기화
		int kopo36_j = (kopo36_ii / 10) * 10;
		//일의 자리 숫자를 버리는 작업이다. 10으로 나누면 나눈 몫(1234)만 남는다.
		// 그리고 다시 10을 곱해주면 12340이 된다.
		System.out.printf("#4-1 : %d\n", kopo36_j);
		
		kopo36_ii = 12345; //ii변수를 12345로 초기화
		kopo36_j = ((kopo36_ii + 5) / 10) * 10; 
		// j변수를 ii에 5를 더하고 10을 나눈후 10을 곱한다. 
		// 일의 자리 숫자를 반올림하는 것이다. 5이상이라면 +5 처리가 되면서 10의 자리로 올라간다.
		System.out.printf("#4-1 : %d\n", kopo36_j);
		
		kopo36_ii = 12344; //ii변수를 12344로 초기화
		kopo36_j = ((kopo36_ii + 5) / 10) * 10;
		// j변수를 ii에 5를 더하고 10을 나눈후 10을 곱한다.
		// 일의 자리 숫자가 4이기 때문에 +5를 해줘도 10의 자리로 안 올라가기 때문에 반올림이 되지 않는다.
		System.out.printf("#4-1 : %d\n", kopo36_j);
		
	}
}
