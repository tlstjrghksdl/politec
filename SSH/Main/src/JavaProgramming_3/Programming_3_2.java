package JavaProgramming_3;

public class Programming_3_2 {
	public static void main(String[] args) {
		int kopo36_sum; // 합계를 넣을 sum변수 선언
		kopo36_sum = 0; // 누적 또는 루프에 사용하는 변수는 꼭 값을 0으로 초기화 해줘야한다.

		for (int kopo36_i = 1; kopo36_i < 101; kopo36_i++) {
			// for문을 이용하여 i가 1부터 실행되며 101보다 작은 100까지 for문이 실행된다.
			// i의 값은 1번 실핼될 때마다 1씩 증가한다.
			kopo36_sum = kopo36_sum + kopo36_i;
			// 첫 sum은 0이고 i가 1부터 100까지 돌아가면서 sum과 더해진다
			// sum값은 for문 안에서 계속 i값이 1~100까지 더해지면서 누적된다.
			// 밖에서 sum변수를 선언해놨기 때문에 sum변수는 전역변수로서 main메소드 안에서 사용 가능하다.
		}
		System.out.printf("#2 : %d\n", kopo36_sum);
		// %d는 정수를 받아서 출력해준다.
		// sum의 값이 정수이 때문에 %d자리에 다 더해진 sum이 오게 되고 \n으로 개행처리를 해준다
		System.out.printf("#2 - 2 : %d\n", kopo36_sum / 100);
		// 변수 sum을 100으로 나눈 것이다. java에서는 나누기 연산에 쓰는 특수문자가 두개가 있다
		// 하나는 / 이고 하나는 % 이다. / 는 나눈 몫을 %는 나눈고 남은 나머지를 뽑아준다

		int[] kopo36_v = { 1, 2, 3, 4, 5 };
		// 배열은 방(요소)을 만드는 것이다. 1~5까지 넣어주면 방이 5개가 생기고 방에 하나씩 들어간다
		// 배열의 방(요소)의 시작은 0번방(요소) 부터 시작하기 때문에 0~4까지 이다.
		int kopo36_vSum; // 정수형 변수 vSum을 선언하여 배열의 합을 구할 준비를 한다.
		kopo36_vSum = 0; // 누적시킬 변수는 항상 0으로 값을 초기화 하자
		for (int kopo36_i = 0; kopo36_i < kopo36_v.length; kopo36_i++) {
			// for문을 배열 v의 길이(요소의 개수)만큼 실행되게 하는 것이다.
			// 요소의 시작은 0이기 때문에 kopo36_i도 0부터 시작되게 한다
			kopo36_vSum = kopo36_vSum + kopo36_v[kopo36_i];
			// 앞에서 했던거와 같은 방법으로 합계를 누적 시키는 것이다. v[0]~v[4]까지 sum값에 차례대로 더해지면서 누적된다.
			// vSum변수는 전역 변수이기 때문에 for문 안에서 사라지지 않는다.
		}
		
		System.out.printf("2-3 : %d\n", kopo36_vSum);
		// %d는 정수를 받아서 출력해준다.
		// vSum의 값이 정수이 때문에 %d자리에 다 더해진 vSum이 오게 되고 \n으로 개행처리를 해준다
	} 
}
