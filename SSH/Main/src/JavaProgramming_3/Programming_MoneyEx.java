package JavaProgramming_3;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Programming_MoneyEx {
	public static void main(String[] args) {
		
		
		int kopo36_MyWon = 1000000; // 내가 환전 할 돈. 100만원
		double kopo36_MoneyEx = 1238.21; // 달러 환율
		double kopo36_commission = 0.003; // 환전 수수료 0.03%
		
		//신입사원 신석환은 네이버에서 환율을 보고 100만원 가져가면 807달러로 환전 받겠구나 하고 코드를 짠다
		
		int kopo36_USD = (int)(kopo36_MyWon / kopo36_MoneyEx); // 전체 금액을 환율로 나누고 정수형으로 형변환을 하면 버림 처리가 된다. ex)(int)(100만원 / 1200원)
		int kopo36_remain = (int) (kopo36_MyWon - kopo36_USD * kopo36_MoneyEx); 
		//남은돈. 받을 달러 * 환율을 곱한 값을 내가 가진 100원에서 빼면 된다. ex) (int)(100만원 - (807달러 * 1200원))
		
		System.out.printf("*******************************************************************\n");
		System.out.printf("*                         수수료없이 계산                         *\n");
		System.out.printf("총 한화환전금액 : %d원  =>  미화 : %d달러,  잔돈 : %d원\n", kopo36_MyWon, kopo36_USD, kopo36_remain);
		//%d에는 정수가 들어간다. \n은 개행
		System.out.printf("*******************************************************************\n");
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		double kopo36_ComPerOne = kopo36_MoneyEx * kopo36_commission; //달러당 수수료  ex) 1200dnjs * 0.003 = 달러당 수수료
		//달러당 은행이 가져가는 수수료가 있어야 은행도 먹고산다요~
		//소수점까지 나타내기 위한 double을 사용한다
		double kopo36_totalcom = kopo36_USD * kopo36_ComPerOne; // ex) 807달러 * 달러당 수수료  = 은행이 가져갈 총 수수료
		System.out.println("***************************************************************************");
		System.out.println("*                              수수료 계산                                *");
		System.out.printf("총 수수료 : %f원 => 미화 : %d달러,  달러당 수수료 : %f원\n", kopo36_totalcom, kopo36_USD, kopo36_ComPerOne);
		//%d에는 정수, %f에는 실수가 들어간다. \n은 개행
		
		//double로 했기 때문에 1원 아래 돈이 생긴다...
		System.out.println("***************************************************************************");
		
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 수수료는 받아야할 돈이기 때문에 소수점은 올림처리한다.
		int kopo36_i_totalcom; //소수점 올림처리하여 은행이 가져가는 총 환전 수수료
		if (kopo36_totalcom != (double)((int)kopo36_totalcom)) { 
		// 은행에서 가져갈 총 수수료가 기본 실수형과 (정수형으로 했다가(소수점을 버리기위한 작업) 실수형으로 한 값이 같지 않으면
		// 만약 1.1원이 은행이 가져갈 총 수수료라 하면 정수형을 바꾸고 실수형으로 다시 바꿔주면 1.0이 된다
		// 이말은 즉 소수점이 나오면 그냥 +1 하겠다는 말이다.
			kopo36_i_totalcom = (int) kopo36_totalcom + 1;
		} else {
		// 그외는 소수점이 안나와야하는 경우인데 이때는 그냥 같은 수이다.
			kopo36_i_totalcom = (int) kopo36_totalcom;
		}
		
		System.out.println("*************************************************************************************");
		System.out.println("*                                  수수료 적용 환율                                 *");
		System.out.printf("총 수수료 : %d원 => 미화 : %d달려, 달러당 수수료 : %f원\n", kopo36_i_totalcom, kopo36_USD, kopo36_ComPerOne);
		//%d에는 정수, %f에는 실수가 들어간다. \n은 개행
		
		kopo36_remain = (int) (kopo36_MyWon - (kopo36_USD * kopo36_MoneyEx) - kopo36_i_totalcom); // 잔돈 = 100만원 - (달러 * 달러 환율) - 은행이 가져갈 총 수수료
		System.out.printf("총 한화환전금액 : %d원 => 미화 : %d달러, 수수료청구 : %d원,  잔돈 : %d원\n", kopo36_MyWon, kopo36_USD, kopo36_i_totalcom, kopo36_remain);
		//%d에는 정수, %f에는 실수가 들어간다. \n은 개행
		
		System.out.println("*************************************************************************************");
	
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		kopo36_USD = (int) (kopo36_MyWon / (kopo36_MoneyEx + kopo36_ComPerOne)); //환전 달러
		// ex) 달러 환율이 1200원, 달러당 수수료가 3.7원 일때, ----->  100원 / 1203.7원
		kopo36_totalcom = kopo36_USD * kopo36_ComPerOne; //총 수수료
		// ex) 은행이 가져갈 총 수수료 = 807달러 * 달러당 수수료 
		
		
		//수수료 올림처리를 if문을 써서 하자
		if (kopo36_totalcom != (double) ((int) kopo36_totalcom)) {
		// 은행에서 가져갈 총 수수료가 기본 실수형과 (정수형으로 했다가(소수점을 버리기위한 작업) 실수형으로 한 값이 같지 않으면
		// 만약 1.1원이 은행이 가져갈 총 수수료라 하면 정수형을 바꾸고 실수형으로 다시 바꿔주면 1.0이 된다
		// 이말은 즉 소수점이 나오면 그냥 +1 하겠다는 말이다.
			kopo36_i_totalcom = (int) kopo36_totalcom + 1;
		} else {
		// 그외는 소수점이 안나와야하는 경우인데 이때는 그냥 같은 수이다.
			kopo36_i_totalcom = (int) kopo36_totalcom;
		}
		DecimalFormat kopo36_df = new DecimalFormat("###,###,###,###");
		// 돈에 콤마를 찍어주는 라이브러리인데 이렇게 되면 문자형으로 바뀌기 때문에 %d를 %s로 바꿔줘야한다
		
		Calendar kopo36_cal = Calendar.getInstance();
		// 날짜 형식을 표현할 때 쓰는 라이브러리이다.
		// 따로 Calendar라이브러리에 있는 메소드르 사용하지 않는다면 getTime메소드를 사용할때 현재 시간이 나온다 
		SimpleDateFormat kopo36_sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		//DateFormat을 정해주는 라이브러리이다. 괄호 안에 원하는 날짜 형식을 넣고 날짜를 불러오면 그런 형식으로 변형이 가능하다.
		System.out.println();
		System.out.println("**********************************************************************************");
		System.out.println("*                  (정확한) 수수료 적용 환전, 콤마찍기, 날짜 적용                *");
		System.out.printf("총 수수료 : %s원 => 미화 : %d달러, 달러당 수수료 : %f원\n", kopo36_df.format(kopo36_i_totalcom), kopo36_USD, kopo36_ComPerOne);
		//%d에는 정수, %f에는 실수가 들어간다. \n은 개행
		kopo36_remain = (int) (kopo36_MyWon - (kopo36_USD * kopo36_MoneyEx) - kopo36_i_totalcom); // 잔돈 = 100만원 - (달러 * 달러 환율) - 은행이 가져갈 총 수수료
		
		System.out.printf("총 한화환전금액 : %s원 => 미화 : %s달러, 수수료청구 : %s원, 잔돈 : %s원\n", kopo36_df.format(kopo36_MyWon),
						  kopo36_df.format(kopo36_USD), kopo36_df.format(kopo36_i_totalcom), kopo36_df.format(kopo36_remain));
		//DecimalFormat으로인해 숫자가 문자열로 바뀌어서 %s로 넣어줘야한다. %s는 문자열을 받는다. \n은 개행
		
		System.out.println("최종 실행 시간 : " + kopo36_sdf.format(kopo36_cal.getTime()));
		System.out.println("**********************************************************************************");
		
	}
}
