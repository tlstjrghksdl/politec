package JavaProgramming_3;

public class Programming_3_5 {
	public static void main(String[] args) {
		int kopo36_MyVal = 14 / 5;
		//정수형 변수이기 때문에 (14 / 5)를 했을때 소수점은 저장 되지 않는다. 2만 저장됨
		System.out.printf("#5-1 : %d\n", kopo36_MyVal);
		
		double kopo36_MyValF;
		// 실수형 변수 를 다시 선언
		kopo36_MyValF = 14 / 5; 
		//MyValF 가 실수형 변수이지만 나누는 숫자들이 정수이기 때문에 정수로 연산이 된다. 그래서 값은 2가 변수에 들어가게 된다.
		System.out.printf("#5-2 : %.2f\n", kopo36_MyValF);
		
		kopo36_MyValF = 14.0 / 5;
		// 여기서 14.0을 해줬기 때문에 실수형으로 연산이 된다. 연산을 할 때 하나라도 실수가 들어가 있따면 실수형으로 연산이 된다.
		System.out.printf("#5-3 : %.2f\n", kopo36_MyValF);
		
		kopo36_MyValF = 14.0 / 5 + 0.5;
		// 14.0 / 5 한 값 2.8에 0.5를 더한 값이 변수에 저장된다.
		System.out.printf("#5-4 : %.2f\n", kopo36_MyValF);
		
		kopo36_MyValF = (int) ((14.0) / 5 + 0.5);
		//위에서 나온 값에서 정수형으로 형변환을 해주는 것이다. 그렇게 되면 소수점은 버려지기 때문에 값이 3이 된다.
		System.out.printf("#5-5 : %.2f\n", kopo36_MyValF);
	}
}
