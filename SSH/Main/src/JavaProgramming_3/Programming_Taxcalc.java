package JavaProgramming_3;

class Taxcalc { //세금 구하는 공식
	static int taxcal(int kopo36_val, int kopo36_rate) {
		int kopo36_ret;
		// 
		if(((double) kopo36_val * (double) kopo36_rate / 100.0) == kopo36_val * kopo36_rate / 100) {
		// double로 연산했기 때문에 소수점 자리까지 나온다. 그렇기 때문에 double로 연산 안한것과 비교를 했을때 같으면
		// 소수점 자리가 0인 것이다. 이 것을 검출 하기 위한 if문
			kopo36_ret = kopo36_val * kopo36_rate / 100;
		} else {
			kopo36_ret = (kopo36_val * kopo36_rate / 100) + 1;
		}
		return kopo36_ret;
		//메소드가 int이기 때문에 return값을 int로 잡아줘야한다.
	}
}


public class Programming_Taxcalc {
	public static void main(String[] args) {
		int kopo36_val = 271; // 세전 물건 값
		int kopo36_rate = 3; // 세금 3퍼센트
		
		// 우리나라는 세금을 포함한 소비자 가격을 기입하는데, 일본가면 세전 가격을 소비자가라고 붙여놔서 햇갈린다.
		// 100엔이라고 편의점에서 사면 112엔을 받아서 잔돈이 생긴다.
		int kopo36_tax = Taxcalc.taxcal(kopo36_val, kopo36_rate);
		//세금
		System.out.printf("**************************************************\n");
		System.out.printf("*                     단순 세금 계산             *\n");
		System.out.printf("실제 세금계산 : %f\n", kopo36_val * kopo36_rate / 100.0);
		// 하나라도 double형식 연산을 하면 실수로 표시한다. 100.0을 해줬기 때문에 실수로 연산이 된다.
		System.out.printf("세전가격 : %d원  세금 : %d원  세포함가격 : %d\n", kopo36_val, kopo36_tax, kopo36_val + kopo36_tax);
		//%d에는 정수가 들어간다. \n은 개행
		System.out.printf("**************************************************\n");
		
		
		System.out.printf("\n\n\n");
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		int kopo36_custom = 280; // 소비자가
		int kopo36_taxrate = 3; // 세율
		
		int kopo36_taxval, kopo36_netval; //세금, 세전가

		kopo36_netval = (int)(kopo36_custom / (1 + kopo36_taxrate / 100.0));
		// 소비자가  / (1 + 0.03) ex) 1000 / 1.03
		kopo36_taxval = kopo36_custom - kopo36_netval;
		// 세금 = 소비자가 = 세전가
		
		System.out.printf("**********************************************************************\n");
		System.out.printf("*                       소비자가 중심 세금 계산                      *\n");
		
		System.out.printf("소비자가격 : %d원			세전가격 : %d원    세금 : %d원  \n", kopo36_custom, kopo36_netval, kopo36_taxval);
		//%d에는 정수가 들어간다. \n은 개행
		System.out.printf("**********************************************************************\n");
	}
}
