package JavaProgramming_3;

public class Programming_3_3 {
	public static void main(String[] args) {
		int kopo36_ii; //ii변수 선언
		kopo36_ii = 1000 / 3;
		// ii값은 1000 나누기 3한 값의 몫이다. 
		// "/"는 나눈 값만 나온다. 나머지는 안나온다.
		System.out.println("#3-1 : " + kopo36_ii);
		// 출력하여 무슨 값이 나오는지 확인하자!
		
		kopo36_ii = 1000 % 3;
		// ii값은 1000 나누기 3한 값의 나머지 이다. 
		// "%"는 나눈 나머지 값만 나온다. 나눈 값은 안나온다.
		System.out.println("#3-2 : " + kopo36_ii);
		// 출력하여 어떤 값이 나오는지 확인하자.
		
		
		//나머지 연산자의 응용
		for (int kopo36_i = 0; kopo36_i < 20; kopo36_i++) {
		// for문을 0부터 19까지 총 20번 실행시키는 것이다
			System.out.printf("#3-3 : %d   ", kopo36_i);
			// 한번 돌때마다 i변수가 1씩 증가하면서 출력될 것이다
			
			if (((kopo36_i + 1) % 5) == 0 ) {
			// i는 0부터 시작하기 때문에 1을 더해줘야 1부터 20까지 연산이 가능해진다
			// 5로 나눈 나머지 값이 0일때를 검출 해주는 if문이다
				System.out.println();
				//개행을 넣어줬다. 그렇게 되면 5번출력되고 개행하고 5번출력되고를 반복한다.
			}
			
		}
	}
}
