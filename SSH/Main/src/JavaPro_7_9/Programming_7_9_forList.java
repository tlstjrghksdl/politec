package JavaPro_7_9;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class Programming_7_9_forList {

	// 클래스를 담는 ArrayList를 인스턴스화한다.
	static ArrayList<kopo36_OneRecForList> kopo36_ArrayOneRec = new ArrayList<kopo36_OneRecForList>();

	// 현재페이지 합계와 평균에 사용할 변수
	static int kopo36_nowkor = 0;
	static int kopo36_noweng = 0;
	static int kopo36_nowmat = 0;
	static int kopo36_nowsum = 0;
	static double kopo36_nowavg = 0;

	// 누적페이지 합계와 평균에 사용할 변수
	static int kopo36_sumkor = 0;
	static int kopo36_sumeng = 0;
	static int kopo36_summat = 0;
	static int kopo36_sumsum = 0;
	static int kopo36_sumavg = 0;

	// 사람 수
	static final int kopo36_iPerson = 2500;

	// 데이터 만들기
	public static void kopo36_dataSet() {
		for (int kopo36_i = 0; kopo36_i < kopo36_iPerson; kopo36_i++) { // 사람수만큼 실행
			String kopo36_name = String.format("홍길%02d", kopo36_i + 1); // 이름에 숫자를 붙여준다. 순서대로
			int kopo36_kor = (int) (Math.random() * 100); // 숫자를 random함수로 불러온다. 곱하기 100을 하여 0~100까지 받는다
			int kopo36_eng = (int) (Math.random() * 100); // 숫자를 random함수로 불러온다. 곱하기 100을 하여 0~100까지 받는다
			int kopo36_mat = (int) (Math.random() * 100); // 숫자를 random함수로 불러온다. 곱하기 100을 하여 0~100까지 받는다
			kopo36_ArrayOneRec // list에 add로 얻은 값을 기반으로 생성자를 넣어준다.
					.add(new kopo36_OneRecForList(kopo36_i + 1, kopo36_name, kopo36_kor, kopo36_eng, kopo36_mat));
		}
	}

	// 헤더 인쇄
	public static void kopo36_HeaderPrint() {
		System.out.printf("========================================================\n");
		System.out.printf("%2s%5s%9s%6s%6s%6s%8s\n", "번호", "이름", "국어", "영어", "수학", "합계", "평균");
		System.out.printf("========================================================\n");
	}

	// 내용 인쇄
	public static void kopo36_itemPrint(int kopo36_i) {
		kopo36_OneRecForList kopo36_OF; // 출력할 생성자.

		kopo36_OF = kopo36_ArrayOneRec.get(kopo36_i); // 생성자를 정수 kopo36_i를 get에 넣어 몇번째 놈을 넣을지 정한다.

		// 이름의 길이가 5면, 즉 100명 이상일때 칸수와 아닐때의 칸수를 나눴다. 이름을 왼쪽으로 정렬하여 이쁘게 보이기 위해
		if (kopo36_OF.kopo36_name().length() == 5) {
			System.out.printf("%03d%9s%8d%8d%8d%8d%10.2f\n", kopo36_OF.kopo36_student_id(), kopo36_OF.kopo36_name(),
					kopo36_OF.kopo36_kor(), kopo36_OF.kopo36_eng(), kopo36_OF.kopo36_mat(), kopo36_OF.kopo36_sum(),
					(double) kopo36_OF.kopo36_avg());
		} else if (kopo36_OF.kopo36_name().length() < 5) {
			System.out.printf("%03d%8s%9d%8d%8d%8d%10.2f\n", kopo36_OF.kopo36_student_id(), kopo36_OF.kopo36_name(),
					kopo36_OF.kopo36_kor(), kopo36_OF.kopo36_eng(), kopo36_OF.kopo36_mat(), kopo36_OF.kopo36_sum(),
					(double) kopo36_OF.kopo36_avg());
		} else {
			System.out.printf("%03d%9s%7d%8d%8d%8d%10.2f\n", kopo36_OF.kopo36_student_id(), kopo36_OF.kopo36_name(),
					kopo36_OF.kopo36_kor(), kopo36_OF.kopo36_eng(), kopo36_OF.kopo36_mat(), kopo36_OF.kopo36_sum(),
					(double) kopo36_OF.kopo36_avg());
		}

		// 현재페이지의 합계이다. 한번 사용되고 다시 0으로 초기화 할것이기 때문에 누적을 하지 않는다.
		kopo36_nowkor = kopo36_OF.kopo36_kor(); // 현재페이지에 출력할 국어점수 합계
		kopo36_noweng = kopo36_OF.kopo36_eng(); // 현재페이지에 출력할 영어점수 합계
		kopo36_nowmat = kopo36_OF.kopo36_mat(); // 현재페이지에 출력할 수학점수 합계
		kopo36_nowsum = kopo36_OF.kopo36_sum(); // 현재페이지에 출력할 함계의 합계
		kopo36_nowavg = kopo36_OF.kopo36_avg(); // 현재페이지에 출력할 평균의 합계

		// 누적페이지의 합계이다. 계속해서 누적해야하기 때문에 +=을 사용했다
		kopo36_sumkor += kopo36_OF.kopo36_kor(); // 가져온 값들을 누적시킨다.
		kopo36_sumeng += kopo36_OF.kopo36_eng(); // 가져온 값들을 누적시킨다.
		kopo36_summat += kopo36_OF.kopo36_mat(); // 가져온 값들을 누적시킨다.
		kopo36_sumsum += kopo36_OF.kopo36_sum(); // 가져온 값들을 누적시킨다.
		kopo36_sumavg += kopo36_OF.kopo36_avg(); // 가져온 값들을 누적시킨다.
	}

	// 꼬리 인쇄
	public static void kopo36_TailPrint() {
		System.out.printf("국 어 합 계   %5d   국어평균 : %6.2f\n", kopo36_sumkor,
				kopo36_sumkor / (double) kopo36_ArrayOneRec.size());
		System.out.printf("영 어 합 계   %5d   영어평균 : %6.2f\n", kopo36_sumeng,
				kopo36_sumeng / (double) kopo36_ArrayOneRec.size());
		System.out.printf("수 학 합 계   %5d   수학평균 : %6.2f\n", kopo36_summat,
				kopo36_summat / (double) kopo36_ArrayOneRec.size());
		System.out.printf("========================================================\n");
		System.out.printf("반평균 합계   %5d   반 평 균 : %6.2f\n", kopo36_sumavg,
				kopo36_sumavg / (double) kopo36_ArrayOneRec.size());
	}

	// 누적페이지 double형의 변수를 받아서 그것으로 나눌 것이다.
	public static void kopo36_sumPage(double kopo36_avg) {
		System.out.printf("========================================================\n");
		System.out.printf("누적페이지\n");

		System.out.printf("합      계%12d%8d%8d%8d%10.2f\n", kopo36_sumkor, kopo36_sumeng, kopo36_summat,
				kopo36_sumkor + kopo36_sumeng + kopo36_summat, (kopo36_sumkor + kopo36_sumeng + kopo36_summat) / 3.0);

		// 누적된 값들의 평균을 나타내야 하기 때문에 나누는 값이 30씩 증가 해야한다.
		// 그렇다고 정수형 변수로 나누면 정수이기 때문에 받아온 double형으로 나눠야 소수점을 표시할 수 있는 실수형으로 나온다.
		System.out.printf("평      균%12.2f%8.2f%8.2f%8.2f%10.2f\n", kopo36_sumkor / kopo36_avg,
				kopo36_sumeng / kopo36_avg, kopo36_summat / kopo36_avg,
				((kopo36_sumkor + kopo36_sumeng + kopo36_summat) / 3.0) / kopo36_avg,
				((kopo36_sumkor + kopo36_sumeng + kopo36_summat) / 3.0) / kopo36_avg);
		System.out.printf("========================================================\n");
	}

	// 정렬
	public static void kopo36_dataSort() {
		// collections.sort를 사용하기 위해서는 인자값으로 List<>와 Comparator<>를 받는다.
		// 생성자가 들어간 List에서 두 요소 a2와 a1을 불러와 합계를 서로 빼준다.
		// return값으로 음수가 나오면 a1의 합계가 큰것이므로 뒤로 밀려난다. 즉 음수면 뒤로 밀려난다
		// 이러한 방식으로 여러차례 돌아가면서 정렬을 해주는 것 같다.
		Comparator<kopo36_OneRecForList> kopo36_BasicSortLine = new Comparator<kopo36_OneRecForList>() {
			public int compare(kopo36_OneRecForList kopo36_a1, kopo36_OneRecForList kopo36_a2) {
				return (kopo36_a2.kopo36_sum() - kopo36_a1.kopo36_sum()); //
			}
		};
		Collections.sort(Programming_7_9_forList.kopo36_ArrayOneRec, kopo36_BasicSortLine);
	}
}