package JavaPro_7_9;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Programming_7_9_Main {
	
	
	public static void main(String[] args) {
		// 페이지에 찍힐 현재 날짜와 시간의 포맷을 정해주는 SimpleDateFormat과
		// 날짜를 불러올 Calendar
		SimpleDateFormat kopo36_sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		Calendar kopo36_c = Calendar.getInstance();
		// 페이지수를 표시하려면 결국 누적하는방법이 좋다.
		int kopo36_pagenum = 1;
		// 한 페이지에 표시하고 싶은 아이템의 수
		int kopo36_itemnum = 30;
		// 변수 i가 한번 실행될때마다 30씩 증가하여 사람 수까지 실행하는 for문을 돌린다.
		// 앞서 했던 구구단 줄마다 3씩 더하여 만든것과 비슷한 원리
		// i는 0, 30, 60, 90... 가게 되고, 내부에 있는 for문도 i와 시작하는 수가 같아야 모든 학생들을 출력할 수 있다.
		// j는 0~29, 30~59, 60~89 이렇게 올라갈 것이다.
		for (int kopo36_i = 0; kopo36_i < Programming_7_9_forList.kopo36_iPerson; kopo36_i = kopo36_i
				+ kopo36_itemnum) {
			int kopo36_nowkor = 0; // 현재페이지 국어점수의 합계
			int kopo36_noweng = 0; // 현재페이지 영어점수의 합계
			int kopo36_nowmat = 0; // 현재페이지 수학점수의 합계
			int kopo36_nowsum = 0; // 현재페이지 합계의 합계
			double kopo36_nowavg = 0.0; // 현재페이지 평균의 합계
			System.out.printf("%28s\n\n", "성적집계표");
			System.out.printf("PAGE : %d%24s%s\n", kopo36_pagenum, "출력일자 : ", kopo36_sdf.format(kopo36_c.getTime()));
			// for문이 시작하고 바로 Page변수를 +1해준다. for문이 끝나면 page는 숫자가 누적되어 페이지를 알려줄 것이다.
			kopo36_pagenum++;
			Programming_7_9_forList.kopo36_dataSet(); // 데이터 만들기
//			Programming_7_9_forList.kopo36_dataSort(); // 합계로 정렬
			Programming_7_9_forList.kopo36_HeaderPrint(); // 헤더 출력
			// for문이 i부터 시작하게 된다. 그러면 0, 30, 60, ...에서 시작
			// i + 29까지 실행되게 하여 총 30개의 데이터를 순차적으로 출력할 수 있다.
			for (int kopo36_j = kopo36_i; kopo36_j < kopo36_i + kopo36_itemnum; kopo36_j++) {
				// 변수 j가 사람수와 같아지면 더이상 출력할 것이 없기 때문에 break로 for문을 끝낸다
				if (kopo36_j == Programming_7_9_forList.kopo36_iPerson) {
					break;
				} else {
					// 그게 아니면 item들을 출력한다.
					Programming_7_9_forList.kopo36_itemPrint(kopo36_j);
				}
				// for문이 한번 돌때마다 합계가 누적된다. 내부 for문에서 누적을 시킨다.
				kopo36_nowkor = kopo36_nowkor + Programming_7_9_forList.kopo36_nowkor;
				kopo36_noweng = kopo36_noweng + Programming_7_9_forList.kopo36_noweng;
				kopo36_nowmat = kopo36_nowmat + Programming_7_9_forList.kopo36_nowmat;
				kopo36_nowsum = kopo36_nowsum + Programming_7_9_forList.kopo36_nowsum;
				kopo36_nowavg = kopo36_nowavg + Programming_7_9_forList.kopo36_nowavg;
			}
			// 누적시킨 현재페이지 합계들을 사용한다. 사용하고나서 다시 for문이 돌때 0으로 초기화 시킨다.
			System.out.printf("========================================================\n");
			System.out.printf("현재페이지\n");
			// i + itemnum이 사람수보다 작거나 같으면 itemnum으로 나눈다. itemnum = 한페이지에 출력하고 싶은 사람 수
			if (kopo36_i + kopo36_itemnum <= Programming_7_9_forList.kopo36_iPerson) {
				System.out.printf("합      계%12d%8d%8d%8d%10.2f\n", kopo36_nowkor, kopo36_noweng, kopo36_nowmat, // 각각의 점수																												  
						kopo36_nowkor + kopo36_noweng + kopo36_nowmat, // 합계
						(kopo36_nowkor + kopo36_noweng + kopo36_nowmat) / 3.0); // 평균
				System.out.printf("평      균%12.2f%8.2f%8.2f%8.2f%10.2f\n", kopo36_nowkor / (double)kopo36_itemnum, kopo36_noweng / (double)kopo36_itemnum,
						kopo36_nowmat / (double)kopo36_itemnum, (kopo36_nowkor + kopo36_noweng + kopo36_nowmat) / (double)kopo36_itemnum,
						(kopo36_nowkor + kopo36_noweng + kopo36_nowmat) / (double)kopo36_itemnum);
			} else {
				// i + itemnum이 사람수보다 클때 사람수에서 i를 뺀값을 나눈다. 
				// 예를 들면 i가 240이고 사람수가 250이면 250 - 240 = 10.
				// 현재 페이지에 있는 사람들만의 평균을 구하는 것이기 때문에 10을 나눠야한다.
				int kopo36_division = Programming_7_9_forList.kopo36_iPerson - kopo36_i;
				System.out.printf("합      계%12d%8d%8d%8d%10.2f\n", kopo36_nowkor, kopo36_noweng, kopo36_nowmat, // 각각의 점수
						kopo36_nowkor + kopo36_noweng + kopo36_nowmat, // 합계
						(kopo36_nowkor + kopo36_noweng + kopo36_nowmat) / 3.0); // 평균
				System.out.printf("평      균%12.2f%8.2f%8.2f%8.2f%10.2f\n", kopo36_nowkor / (double)kopo36_division, 
						kopo36_noweng / (double)kopo36_division,
						kopo36_nowmat / (double)kopo36_division, (kopo36_nowkor + kopo36_noweng + kopo36_nowmat) / (double)kopo36_division,
						(kopo36_nowkor + kopo36_noweng + kopo36_nowmat) / (double)kopo36_division);
			}
			// 누적페이지 출력문
			// 합계도 누적되지만, 사람수도 누적되기 때문에 계속해서 변하는 사람 수 만큼 나눠줘야한다.
			// 그래서 한페이지에 30명씩 나오기 때문에 30씩 더하다가, 30을 더했을때 사람 수보다 많으면 사람수에 맞게 나누게 했다
			if (kopo36_i + 30 <= Programming_7_9_forList.kopo36_iPerson) {
				Programming_7_9_forList.kopo36_sumPage((double) kopo36_i + 30);
			} else {
				Programming_7_9_forList
						.kopo36_sumPage((double) kopo36_i + (Programming_7_9_forList.kopo36_iPerson - kopo36_i));
			}
			System.out.printf("\n\n\n");
		}
	}
	
	
}