package JavaPro_7_9;

public class kopo36_OneRecForList {
	// 학생의 id, 이름, 과목 점수를 선언
		private int kopo36_student_id;
		private String kopo36_name;
		private int kopo36_kor;
		private int	kopo36_eng;
		private int	kopo36_mat;
		
		// 생성자. 나중에 인스턴스화할때 값을 넣어주면 그 값을 가진 인스턴스가 된다
		public  kopo36_OneRecForList(int kopo36_student_id, String kopo36_name, int kor, int eng, int mat) {
			this.kopo36_student_id = kopo36_student_id;
			this.kopo36_name = kopo36_name;
			this.kopo36_kor = kor;
			this.kopo36_eng = eng;
			this.kopo36_mat = mat;
		}
		//getter
		public int kopo36_student_id() {
			return this.kopo36_student_id;
		}
		//getter
		public String kopo36_name() {
			return this.kopo36_name;
		}
		//getter
		public int kopo36_kor() {
			return this.kopo36_kor;
		}
		//getter
		public int kopo36_eng() {               
			return this.kopo36_eng;
		}
		//getter
		public int kopo36_mat() {               
			return this.kopo36_mat;
		}
		// 합계메소드. 각 점수를 더한 값
		public int kopo36_sum() {               
			return this.kopo36_kor + this.kopo36_eng + this.kopo36_mat;
		}
		
		// 평균메소드. 합계메소드를 3.0.으로 나눠서 소수점까지 표시
		public double kopo36_avg() {
			return this.kopo36_sum() / 3.0;
		}
}
