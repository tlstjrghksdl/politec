package JavaProgramming_4;

public class Programming_4_12 {
	public static void main(String[] args) {
		int kopo36_n, kopo36_m;
		// 정수형 변수 n, m을 선언
		
		kopo36_m = 20; // 변수 m을 20으로 초기화
		kopo36_n = 1;  // 변수 n을 1로 초기화
		
		while(true) { //while문으로 무한반복 루프 만듬
			for (int kopo36_i = 0; kopo36_i < kopo36_m; kopo36_i++) {
				System.out.printf(" "); //빈칸을 m의 수만큼 찍음
			}
			for (int kopo36_j = 0; kopo36_j < kopo36_n; kopo36_j++) {
				System.out.printf("*"); //별을 n의 수만큼 찍음 
			}
			System.out.printf("\n"); // 개행을 한다.
			
			kopo36_m = kopo36_m - 1; //m의 값은 for문이 끝나면 1씩 감소시킨다. 
			kopo36_n = kopo36_n + 2; //n의 값은 for문이 끝나면 2씩 증가시킨다. 
			// 이 두과정을 거치면 별의 개수가 2개씩 늘어나면서 찍히게 되고 빈칸은 1개씩 줄어들기 때문에 피라미드 형태가 된다.
			
			if(kopo36_m < 0) {
				break;
			// 변수 m의 값이 0보다 작아지면 while문을 멈춘다.
			}
		}
	}
} 
 