package JavaProgramming_4;

public class Programming_4_11 {
	public static void main(String[] args) {
		double kopo36_fSin;
		// double형 변수 fSin을 선언
		
		for (int kopo36_i = 0; kopo36_i < 360; kopo36_i++) {
		// for문을 0~359까지 실행한다. 원은 360도 이기 때문.
			kopo36_fSin = Math.sin(kopo36_i * 3.141592 / 180);
			// Math.sin 메소드에는 각도를 라디안단위를 받는다.으로 넣어줘야한다.
			System.out.printf("%d sin ==> %f\n", kopo36_i, kopo36_fSin);
			//for문이 돌면서 위에서 얻은 값을 출력한다. 
		}
		
		for (int kopo36_j = 0; kopo36_j < 360; kopo36_j++) {
		// for문을 0~359까지 실행한다. 원은 360도 이기 때문.	
			kopo36_fSin = Math.sin(kopo36_j * 3.141592 / 180);
			// Math.sin 메소드에는 각도를 라디안단위를 받는다.으로 넣어줘야한다.
			int kopo36_iSpace = (int) ((1.0 - kopo36_fSin) * 50);
			// 50을 쓴 이유는 그래프를 표시하기 위함이다.
			// 1 - fSin에 50을 곱하여 fSin값이 변할 때 마다 숫자가 변하게 한다.
			for (int kopo36_k = 0; kopo36_k < kopo36_iSpace; kopo36_k++) {
			//iSpace의 값에 따라 띄어 쓰기를 너주면 그래프가 완성 된다.
				System.out.printf(" ");
			}
			System.out.printf("*[%f][%d]\n", kopo36_fSin, kopo36_iSpace);
		}
	}
} 