package JavaProgramming_4;

public class Programming_4_2 {
	public static void main(String[] args) {
		int kopo36_iI; // 정수형 변수 선언
		double kopo36_iD; // 실수형 변수 선언
		
		kopo36_iI = 10 / 3; // 정수형 변수이기 때문에 몫값만 저장된다 (3)
		kopo36_iD = 10 / 3.0; // 실수형 변수이기 때문에 소수점까지 저장된다 (3.3333333333....)
		
		if(kopo36_iI == kopo36_iD) { // iI와 iD가 같으면
			System.out.printf("equal\n"); // equal을 출력. 하지만 같을 수가 없다
		} else {
			System.out.printf("Not equal[%f][%f]\n", (double)kopo36_iI, kopo36_iD); 
			// 다르면 Not equal출력 후 각각의 변수를 보여준다. iI를 double형으로 형변환을 해서 출력할 때 3.000000으로 나온다. %f는 실수를 출력해준다
		}
		
		if(kopo36_iD == 3.333333) { // iD와 3.333333이 같으면
			System.out.printf("equal\n"); 
			// equal을 출력해준다. 얼핏보면 같다고 생각할 수도 있다.
			// 하지만 10 / 3은 3.3333333333333333.... 이기 때문에 같지 않을 것이다.
		} else {
			System.out.printf("Not equal[3.333333][%f]\n", kopo36_iD);
			// 다르면 Not equal과 변수를 출력하게 한다. %f는 실수를 출력해준다.
		}
		
		kopo36_iD = (int) kopo36_iD; // 실수인 iD의 값을 정수형으로 형변환 한다.
		if(kopo36_iI == kopo36_iD) { // iD와 iI가 같으면
			System.out.printf("equal\n"); 
			// equal을 출력. 원래 3.333333.... 이었던 iD를 정수형으로 변환하면서 소수점을 버리고 3이 됐기 때문에 equal이 출력된다
		} else {
			System.out.printf("Not equal[%f][%f]", (double)kopo36_iI, kopo36_iD);
			// 다르면 Not equal과 함께 각각의 변수를 보여준다. 정수형 변수를 double형으로 형변환을 했기 때문에 3.000000으로 나온다.
		}
		
		
		System.out.printf("int로 인쇄[%d][%f]\n", kopo36_iI, kopo36_iD); 
		// 정수형 변수와 실수형 변수의 차이를 보여주기 위한 출력을 한다
		System.out.printf("double로 인쇄[%f][%f]\n", (double)kopo36_iI, kopo36_iD);
		// 정수형 변수를 double로 형변환 하면 어떻게 돼는지 실수형 변수와 비교하기 위한 출력을 한다
		
		char kopo36_a = 'c'; // byte와 같은 char은 문자 1개를 저장한다. 문자는 'c'로 변수 a에 저장한다.
		
		if (kopo36_a == 'b') { // 변수 a가 'b'라면
			System.out.printf("kopo36_a는 b이다\n"); // a는 b이다 라고 출력
		}
		
		if(kopo36_a == 'c') { // 변수 a가 'c'라면
			System.out.printf("kopo36_a는 c이다\n"); // a는 c이다 라고 출력
		}
		
		if(kopo36_a == 'd') { // 변수 a가 'd'라면
			System.out.printf("kopo36_a는 d이다\n"); // a는 d이다 라고 출력 
		}
		
		String kopo36_aa = "abcd"; // String은 변수의 역할도 하는 클래스이다.  aa라는 String변수에 "abcd"를 저장한다.
		
		if(kopo36_aa.equals("abcd")) { // aa변수가 가진 문자열이 "abcd"와 같다면 
			System.out.printf("kopo36_aa는 abcd이다\n"); // aa는 abcd이다 라고 출력
		} else {
			System.out.printf("kopo36_aa는 abcd이 아니다\n"); // 아니면 aa는 abcd이 아니다 라고 출력
		}
		
		boolean kopo36_bb = true; // boolean 변수는 true와 false를 저장하는 변수이다. bb라는 boolean변수에 true를 저장한다.
		
		if(!!kopo36_bb) { // ! 하나면 거짓의 의미이지만 ! 두개면 참의 의미이다. 그래서 변수 bb가 참이면  
			System.out.printf("bb가 아니고 아니면 참이다\n"); // bb가 아니고 아니면 참이다 를 출력한다
		} else {
			System.out.println("bb가 아니고 아니면 거짓이다\n");// 참이 아니면 bb가 아니고 아니면 거짓이다 를 출력한다.
		}
	}
}
