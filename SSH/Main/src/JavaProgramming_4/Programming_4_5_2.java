package JavaProgramming_4;

public class Programming_4_5_2 {
	public static void main(String[] args) {
		for (int kopo36_i = 1; kopo36_i < 13; kopo36_i++) { // for문을 1~12까지 실행한다. 1월~12월이 있기 때문
			System.out.printf(" %d월 ==>", kopo36_i);  // kopo36_i를 %d에 넣고 출력한다.
			for (int kopo36_j = 1; kopo36_j < 32; kopo36_j++) { // 이중 for문으로 kopo36_j를 1~31까지 실행한다. 1일~31일까지 있기 때문
				
				if(kopo36_i == 4 || kopo36_i == 6 || kopo36_i == 9 || kopo36_i == 11) {
					 // 30일까지만 있는 4, 6, 9, 11월만 모아서 조건을 걸었다.
					if(kopo36_j != 30) {
						//kopo36_j가 30이 아니면 "일, "를 출력
						System.out.printf("%d, ", kopo36_j);
					} else {
						// 31이면
						System.out.printf("%d\n", kopo36_j);
						// "일"만 출력한다
						break; // 그리고 for문 브레이크
					}
				} else if(kopo36_i == 1 || kopo36_i == 3 || kopo36_i == 5 || kopo36_i == 7 || kopo36_i == 8 || kopo36_i == 10 || kopo36_i == 12) {
						// 31일까지만 있는 1, 3, 5, 7, 8, 10, 12월만 모아서 조건을 걸었다.
					if(kopo36_j != 31) {
						//kopo36_j가 31이 아니면 "일, "를 출력
						System.out.printf("%d, ", kopo36_j);
					} else {
						// 31이면
						System.out.printf("%d\n", kopo36_j);
						// "일"만 출력한다
						break; // 그리고 for문 브레이크
					}
				} else {
					// 남은 2월은 28일 까지만 있다.
					if(kopo36_j != 28) {
						//kopo36_j가 28이 아니면 "일, "를 출력
						System.out.printf("%d, ", kopo36_j);
					} else {
						// 31이면
						System.out.printf("%d\n", kopo36_j);
						// "일"만 출력한다
						break; // 그리고 for문 브레이크
					}
				}
			}
		}
	}
}