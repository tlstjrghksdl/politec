package JavaProgramming_4;

public class Programming_4_4 {
	public static void main(String[] args) {
		for (int kopo36_i = 1; kopo36_i < 13; kopo36_i++) {
		// for문 안에 kopo36_i 변수를 1부터 12까지 실행 시키는 것인데 for문은 보통 0 부터 시작하지만
		// 우리가 하고자 하는 것은 달력의 날짜를 출력하는 것이기 때문에 1부터 12까지 실행되도록 하는 것이다.
			System.out.printf(" %d월 => ", kopo36_i);
			for (int kopo36_j = 1; kopo36_j < 32; kopo36_j++) {
			// for문을 1에서 31까지 실행시키는 것이다. 달력에서 일 수는 1~31 사이의 수이기 때문이다
				if(kopo36_i == 1 && kopo36_j != 31) {    // kopo_i가 1이고 kopo36_j가 31이 아닐때, 즉 1~30일때
					System.out.printf("%d, ", kopo36_j); // "kopo36_j, "출력
				} else if(kopo36_i == 1 && kopo36_j == 31){
					System.out.printf("%d", kopo36_j);   // kopo36_i가 1이고 kopo36_j가 31일때, kopo36_j만 출력
					break;
				}									     // 마지막 "일"에 콤마를 빼기위해..
				
				if(kopo36_i == 2 && kopo36_j != 28) {	 // kopo_i가 2이고 kopo36_j가 28이 아닐때, 즉 1~28일때 
					System.out.printf("%d, ", kopo36_j); // "kopo36_j, "출력                          
				} else if(kopo36_i == 2 && kopo36_j == 28) {                                                                            
					System.out.printf("%d", kopo36_j);   // kopo36_i가 2이고 kopo36_j가 28일때, kopo36_j만 
					break;
				}                                        // 마지막 "일"에 콤마를 빼기위해..                     
				
				if(kopo36_i == 3 && kopo36_j != 31) {	 // kopo_i가 3이고 kopo36_j가 31이 아닐때, 즉 1~30일때 
					System.out.printf("%d, ", kopo36_j); // "kopo36_j, "출력                          
				} else if(kopo36_i == 3 && kopo36_j == 31) {                                                                            
					System.out.printf("%d", kopo36_j);   // kopo36_i가 3이고 kopo36_j가 31일때, kopo36_j만 
					break;
				}                                        // 마지막 "일"에 콤마를 빼기위해..                     
				
				if(kopo36_i == 4 && kopo36_j != 30) {	 // kopo_i가 4이고 kopo36_j가 30이 아닐때, 즉 1~29일때 
					System.out.printf("%d, ", kopo36_j); // "kopo36_j, "출력                          
				} else if(kopo36_i == 4 && kopo36_j == 30) {                                                                            
					System.out.printf("%d", kopo36_j);   // kopo36_i가 4이고 kopo36_j가 30일때, kopo36_j만 
					break;
				}                                        // 마지막 "일"에 콤마를 빼기위해..                     
				
				if(kopo36_i == 5 && kopo36_j != 31) {	 // kopo_i가 5이고 kopo36_j가 31이 아닐때, 즉 1~30일때 
					System.out.printf("%d, ", kopo36_j); // "kopo36_j, "출력                          
				} else if(kopo36_i == 5 && kopo36_j == 31) {                                                                            
					System.out.printf("%d", kopo36_j);   // kopo36_i가 5이고 kopo36_j가 31일때, kopo36_j만 
					break;
				}                                        // 마지막 "일"에 콤마를 빼기위해..                     
				
				if(kopo36_i == 6 && kopo36_j != 30) {	 // kopo_i가 6이고 kopo36_j가 30이 아닐때, 즉 1~29일때 
					System.out.printf("%d, ", kopo36_j); // "kopo36_j, "출력                          
				} else if(kopo36_i == 6 && kopo36_j == 30) {                                                                            
					System.out.printf("%d", kopo36_j);
					break;								 // kopo36_i가 6이고 kopo36_j가 30일때, kopo36_j만 
				}                                        // 마지막 "일"에 콤마를 빼기위해..                     
				
				if(kopo36_i == 7 && kopo36_j != 31) {	 // kopo_i가 7이고 kopo36_j가 31이 아닐때, 즉 1~30일때 
					System.out.printf("%d, ", kopo36_j); // "kopo36_j, "출력                          
				} else if(kopo36_i == 7 && kopo36_j == 31) {                                                                            
					System.out.printf("%d", kopo36_j);   // kopo36_i가 7이고 kopo36_j가 31일때, kopo36_j만 
					break;
				}                                        // 마지막 "일"에 콤마를 빼기위해..                     
				
				if(kopo36_i == 8 && kopo36_j != 31) {	 // kopo_i가 8이고 kopo36_j가 31이 아닐때, 즉 1~30일때 
					System.out.printf("%d, ", kopo36_j); // "kopo36_j, "출력                          
				} else if(kopo36_i == 8 && kopo36_j == 31) {                                                                            
					System.out.printf("%d", kopo36_j);   // kopo36_i가 8이고 kopo36_j가 31일때, kopo36_j만 
					break;
				}                                        // 마지막 "일"에 콤마를 빼기위해..                     
				
				if(kopo36_i == 9 && kopo36_j != 30) {	 // kopo_i가 9이고 kopo36_j가 30이 아닐때, 즉 1~29일때 
					System.out.printf("%d, ", kopo36_j); // "kopo36_j, "출력                          
				} else if(kopo36_i == 9 && kopo36_j == 30) {                                                                            
					System.out.printf("%d", kopo36_j);   // kopo36_i가 9이고 kopo36_j가 30일때, kopo36_j만 
					break;
				}                                        // 마지막 "일"에 콤마를 빼기위해..                     
				
				if(kopo36_i == 10 && kopo36_j != 31) {	 // kopo_i가 10이고 kopo36_j가 31이 아닐때, 즉 1~30일때 
					System.out.printf("%d, ", kopo36_j); // "kopo36_j, "출력                          
				} else if(kopo36_i == 10 && kopo36_j == 31) {                                                                            
					System.out.printf("%d", kopo36_j);   // kopo36_i가 10이고 kopo36_j가 31일때, kopo36_j만 
					break;
				}                                        // 마지막 "일"에 콤마를 빼기위해..                     
				
				if(kopo36_i == 11 && kopo36_j != 30) {	 // kopo_i가 11이고 kopo36_j가 30이 아닐때, 즉 1~29일때 
					System.out.printf("%d, ", kopo36_j); // "kopo36_j, "출력                          
				} else if(kopo36_i == 11 && kopo36_j == 30) {                                                                            
					System.out.printf("%d", kopo36_j);   // kopo36_i가 11이고 kopo36_j가 30일때, kopo36_j만 
					break;
				}                                        // 마지막 "일"에 콤마를 빼기위해..                     
				
				if(kopo36_i == 12 && kopo36_j != 31) {	 // kopo_i가 12이고 kopo36_j가 31이 아닐때, 즉 1~30일때 
					System.out.printf("%d, ", kopo36_j); // "kopo36_j, "출력                          
				} else if(kopo36_i == 12 && kopo36_j == 31) {                                                                            
					System.out.printf("%d", kopo36_j);   // kopo36_i가 12이고 kopo36_j가 31일때, kopo36_j만 
					break;
				}                                        // 마지막 "일"에 콤마를 빼기위해..                     
				
			}
			System.out.printf("\n");
		} 
	}
}
