package JavaProgramming_4;

public class Programming_4_1 {
	public static void main(String[] args) {
		int kopo36_iA, kopo36_iB;
		 
		kopo36_iA = 0; //먼저 iA를 선언해준다
		while( true ) { //
			kopo36_iB = 0; //iB를 while문 안에서 선언해준다. while이 한번 돌아갈 때마다 0으로 초기화 시키기 위해
			while( true ) { // while 괄호에 true를 넣어서 계속 반복되게 한다
				System.out.printf("*"); //별(*)을 찍는다
				
				if(kopo36_iA == kopo36_iB) {
					break; //iA와 iB를 비교하여 같으면 while문을 멈춘다
				}
				kopo36_iB++; //iB를 증가시킨다. 
				// while문 안에 있기 때문에 iA와 iB가 같지 않으면 별(*)을 한번더 출력하고 iB를 증가시킨다
				// iA와 iB가 같으면 while문을 끝낸다
			}
			System.out.printf("\n"); //개행을 넣는다.
			kopo36_iA++; //iA는 전역변수이기 때문에 값이 증가한 값이 while문 밖에서도 적용된다.
			if( kopo36_iA == 30 ) {
				break; //iA가 30이 되면 while문을 끝낸다
			}
		}
	}
}
