package JavaProgramming_4;

public class Programming_4_8 {
	public static void main(String[] args) {
		String[] kopo36_units = { "영", "일", "이", "삼", "사", "오", "육", "칠", "팔", "구" };
		// kopo36_units 배열에 영~구 까지 문자를 넣어준다. 배열의 길이는 0~9까지 10개이다.
		for (int kopo36_i = 0; kopo36_i < 101; kopo36_i++) {
			// for문을 0부터 100까지 실행시킬것이다.
			if (0 <= kopo36_i && kopo36_i < 10) {
			// kopo36_i가 0~9사이일 때
				System.out.printf("일의 자리 : %s\n", kopo36_units[kopo36_i]);
				// "일의 자리 : %s\n"를 출력하는데 units배열의 요소를 kopo36_i번째를 출력한다.
			} else if (10 <= kopo36_i && kopo36_i < 100) {
				// kopo36_i가 10~99사이일 때
				int kopo36_i10, kopo36_i0; // 정수형 변수 kopo36_i10과 kopo36_i0를 선언한다. 
				kopo36_i10 = kopo36_i / 10; // 십의자리와 일의자리만판별하는 것이다. kopo36_i를 10으로 나눈 몫이 십의 자리 숫자이고
				kopo36_i0 = kopo36_i % 10; // kopo36_i를 10으로 나눈 나머지가 일의 자리 숫자이다.
				if (kopo36_i0 == 0) {
				// kopo36_i0 이 0일 때, 즉 나머지 값이 0일때, 
					System.out.printf("십의 자리 : %s십\n", kopo36_units[kopo36_i10]);
					// 일의 자리를 출력할 필요가 없기 때문에 십의 자리만 출력하게 한다.
				} else {
				// kopo36_i0이 0이 아니면, 즉 나머지 값이 0이 아닐때,
					System.out.printf("십의 자리 : %s십%s\n", kopo36_units[kopo36_i10], kopo36_units[kopo36_i0]);
					// 일의 자리도 출력해야 하기 때문에 %s십%s로 해준다.
				}
			} else {
				// kopo36_i 가 100일때 아래와 같이 출력한다.
				System.out.printf("==> %d\n", kopo36_i);
				
			}
		}
	}
} 