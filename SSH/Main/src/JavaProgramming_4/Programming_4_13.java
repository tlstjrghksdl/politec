package JavaProgramming_4;

import java.text.DecimalFormat;

public class Programming_4_13 {
	public static void main(String[] args) {
		String kopo36_item = "사과"; // String형의 item을 사과로 초기화
		int kopo36_unit_price = 5000; // int형 unit_price을 5000으로 초기화. 단가 5000원
		int kopo36_num = 500; // int형 num을 500으로 초기화. 개수 500개
		int kopo36_total = 0; // int total을 0으로 초기화

		DecimalFormat kopo36_df = new DecimalFormat("###,###,###,###,###");
		// DecimalFormat 라이브러리 사용으로 숫자에 콤마를 찍어준다. 돈이기 때문에
		System.out.printf("===========================================================\n");
		System.out.printf("%20.20s%8.8s%8.8s%8.8s\n", "품목", "단가", "수량", "합계");
		// %뒤에 숫자는 간격을 나타내는 것이다 간격을 정할때 숫자를 넣어가면서 간격을 맞춰보자
		System.out.printf("===========================================================\n"); 
		System.out.printf("%20.20s%10.10s%10.10s%10.10s\n",
				// %뒤에 숫자는 간격을 나타내는 것이다 간격을 정할때 숫자를 넣어가면서 간격을 맞춰보자
				kopo36_item, kopo36_df.format(kopo36_unit_price), kopo36_df.format(kopo36_num),
				kopo36_df.format(kopo36_unit_price * kopo36_num));
		System.out.printf("===========================================================\n");
	}
}
 