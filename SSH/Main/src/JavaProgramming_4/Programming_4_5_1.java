package JavaProgramming_4;

public class Programming_4_5_1 {
	public static void main(String[] args) {
		for (int kopo36_i = 1; kopo36_i < 13; kopo36_i++) { // for문을 1~12까지 실행한다. 1월~12월이 있기 때문
			System.out.printf(" %d월 => ", kopo36_i); // kopo36_i를 %d에 넣고 출력한다.
			for (int kopo36_j = 1; kopo36_j < 32; kopo36_j++) { // 이중 for문으로 kopo36_j를 1~31까지 실행한다. 1일~31일까지 있기 때문
				if((kopo36_i == 4 || kopo36_i == 6 || kopo36_i == 9 || kopo36_i == 11) && kopo36_j != 30) { 	  
					System.out.printf("%d, ", kopo36_j);	
					 // 30일까지만 있는 4, 6, 9, 11월만 모아서 조건을 걸었다. 마지막날인 30일을 제외한 나머지는  "일, "출력 되게 한다.
				} else if((kopo36_i == 4 || kopo36_i == 6 || kopo36_i == 9 || kopo36_i == 11) && kopo36_j == 30) { 
					System.out.printf("%d\n", kopo36_j);
					// 마지막 날인 30일에는 "일" 만 출력되게 한다.마지막 날이니 개행을 한다
					break; // 브레이크!
				} else {
				}
				
				if((kopo36_i == 1 || kopo36_i == 3 || kopo36_i == 5 || kopo36_i == 7 || kopo36_i == 8 || kopo36_i == 10 || kopo36_i == 12) && kopo36_j != 31) {                
					System.out.printf("%d, ", kopo36_j);              
					// 31일까지만 있는 1, 3, 5, 7, 8, 10, 12월만 모아서 조건을 걸었다. 마지막날인 31일을 제외한 나머지는  "일, "출력 되게 한다.
				} else if((kopo36_i == 1 || kopo36_i == 3 || kopo36_i == 5 || kopo36_i == 7 || kopo36_i == 8 || kopo36_i == 10 || kopo36_i == 12) && kopo36_j == 31) {                                              
					System.out.printf("%d\n", kopo36_j); 
					// 마지막 날인 31일에는 "일" 만 출력되게 한다. 마지막 날이니 개행을 한다
					break; // 브레이크!
				} else {
				}
				
				if(kopo36_i == 2 && kopo36_j != 28) {                       
					System.out.printf("%d, ", kopo36_j);               
					// 28일까지만 있는 2월만조건을 걸었다. 마지막날인 28일을 제외한 나머지는  "일, "출력 되게 한다.
				} else if(kopo36_i == 2 && kopo36_j == 28) {                                                            
					System.out.printf("%d\n", kopo36_j); 
					// 마지막 날인 30일에는 "일" 만 출력되게 한다. 마지막 날이니 개행을 한다
					break; // 브레이크!
				} else {
				}
			}
		}
	}
}
