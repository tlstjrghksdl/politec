package JavaProgramming_4;

public class Programming_4_10 {
	public static void main(String[] args) {
		for (int kopo36_i = 0; kopo36_i < 10; kopo36_i++) {
		// for문이 지역변수 i가 0~9까지 증가하면서 반복실행된다
			for (int kopo36_j = 0; kopo36_j < kopo36_i; kopo36_j++) {
			// for문이 지역변수 j가 0부터 i까지 증가하면서 반복실행된다.
				System.out.printf(" "); 
				// 띄어쓰기 출력
				// 즉 변수 i의 수만큼 띄어쓰기가 된다.
			}
			System.out.printf("%d\n", kopo36_i);
			// 변수 i값 출력
		}
	}
}
 