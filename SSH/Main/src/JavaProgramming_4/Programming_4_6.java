package JavaProgramming_4;

public class Programming_4_6 {
	public static void main(String[] args) {
		for (int kopo36_i = 1; kopo36_i < 13; kopo36_i++) {
			System.out.printf(" %d월 => ", kopo36_i);
		// for문을 이용해 1월~ 12월까지 반복실행된다
			LOOP : for (int kopo36_j = 1; kopo36_j < 32; kopo36_j++) {
			// 1~31까지 반복실행하는 이중 for문을 작성했다. 1일~31까지 있기 때문에
			// LOOP를 넣어준 이유는 switch문에서 break하고 다시 for문으로 돌아올 수 있도록 도와주기 위함이다.
			// 안쓰면 값이 요상하게 나온다...
				switch (kopo36_i) {
				// switch에 kopo36_i를 넣었을때를 조건으로 만든 것이다.
				case 4:case 6:case 9:case 11: // 4, 6, 9, 11월은 30일까지 있다
					if(kopo36_j != 30) { 
						System.out.printf("%d, ", kopo36_j);
						// kopo36_j가 30이 아닐때 "%d, "를 출력
					} else {
						System.out.printf("%d\n", kopo36_j);
						// kopo36_j가 30일때 "%d\n"출력. 마지막 날이기때문에 개행을 한다
						break LOOP; // 다 끝났으니까 LOOP로 돌아간다.
					} break; //스위치 문을 끝!
				case 1:case 3:case 5:case 7:case 8: case 10:case 12:
					// 1, 3, 5, 7, 8, 10, 11월은 31일까지 있다
					if(kopo36_j != 31) {
						System.out.printf("%d, ", kopo36_j);	
						// kopo36_j가 31이 아닐때 "%d, "를 출력
					} else {
						System.out.printf("%d\n", kopo36_j);
						// kopo36_j가 31일때 "%d\n"출력. 마지막 날이기때문에 개행을 한다
						break LOOP; // 다 끝났으니까 LOOP로 돌아간다.
					} break; //스위치 문을 끝!
				case 2: // 2월은 28일까지만 있다.
					if(kopo36_j != 28) {
						System.out.printf("%d, ", kopo36_j);	
						// kopo36_j가 28이 아닐때 "%d, "를 출력
					} else {
						System.out.printf("%d\n", kopo36_j);
						// kopo36_j가 28일때 "%d\n"출력. 마지막 날이기때문에 개행을 한다
						break LOOP; // 다 끝났으니까 LOOP로 돌아간다.
					} break; //스위치 문을 끝!
				}
			}
		}
	}
}
