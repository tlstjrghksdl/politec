package JavaProgramming_4;

public class Programming_4_7 {
	public static void main(String[] args) {
		int[] kopo36_iLMD = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		// 각 월별로 최대 일수를 넣은 배열을 만든다. 배열의 요소는 0부터 시작하기 때문에 0부터 11까지이다.
		
		for (int kopo36_i = 0; kopo36_i < kopo36_iLMD.length; kopo36_i++) {
		// for문을 이용해 kopo36_iLMD배열의 길이(크기)만큼 반복실행되게한다.
			System.out.printf(" %d월 => ", kopo36_i + 1);
			// 배열의 길이만큼 0부터 11까지 반복실행되는 for문이기 때문에 kopo36_i를 %d에 넣게 되면 0월부터 11월까지로 된다.
		 	// 그렇기 때문에 kopo36_i에 1을 더해서 넣는다.
			for (int kopo36_j = 1; kopo36_j < 32; kopo36_j++) {
				System.out.printf("%d", kopo36_j);
				//kopo36_j를 한번 출력한다.
				if(kopo36_iLMD[kopo36_i] == kopo36_j) {
					break;
				// iLMD배열에 [kopo36_i]번째 요소의 값이 kopo36_j와 같다면
				// for문을 멈춘다. ex) 2월을 보면 요소의 값이 28이다. for문이 31까지 돌아가기 때문에 멈춰야한다.
				}
				System.out.printf(", ");
				// 콤마를 나중에 사용해서 마지막날 뒤에 콤마를 출력하지 않게 한다.
			}
			System.out.printf("\n"); //안에 for문이 한번 끝나면 개행을 한다.
		}
	}
}
