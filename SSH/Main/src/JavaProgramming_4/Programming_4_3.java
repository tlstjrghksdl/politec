package JavaProgramming_4;

public class Programming_4_3 {
	public static void main(String[] args) {
		int kopo36_iVal; // 정수형 변수 kopo36_iVal을 선언한다
		for (int kopo36_i = 0; kopo36_i < 300; kopo36_i++) {
		// for문을 kopo36_i값이 0~299까지 증가하면서 300번 실행된다.
			kopo36_iVal = 5 * kopo36_i; // kopo36_iVal을 for문에서 얻은 kopo36_i값에 5를 곱한다.
			if(0 <= kopo36_iVal && kopo36_iVal < 10) { // kopo36_iVal의 값이 0이상 10미만일때
				System.out.printf("일 %d\n", kopo36_iVal); // "일 kopo36_iVal" 이 출력되게 한다. 
			} else if (10 <= kopo36_iVal && kopo36_iVal < 100) { // kopo36_iVal의 값이 10이상 100미만일때
				System.out.printf("십 %d\n", kopo36_iVal); // "십 kopo36_iVal" 이 출력되게 한다.
			} else if (100 <= kopo36_iVal && kopo36_iVal < 1000) { // kopo36_iVal의 값이 100이상 1000미만일때
				System.out.printf("백 %d\n", kopo36_iVal); // "백 kopo36_iVal" 이 출력되게 한다.
			} else {
				System.out.printf("천 %d\n", kopo36_iVal); // "위의 조건이 아닌 나머지는 천 kopo36_iVal" 이 출력되게 한다.
			}
		}
	}
}
