package JavaProgramming_4;

public class Programming_4_9 {
	public static void main(String[] args) {
		int kopo36_iNumVal = 1;
		// 정수형 변수를 선언한다. 1001034567로
		String kopo36_sNumVal = String.valueOf(kopo36_iNumVal); // 1001034567
		// valuesOf메소드는 정수형 변수를 String형으로 파씽해주는 것이다.
		String kopo36_sNumvoice = "";
		// String 변수 sNumvoice를 빈칸으로 초기화 해준다.
		System.out.printf("==> %s [%d자리]\n", kopo36_sNumVal, kopo36_sNumVal.length());
		// String형으로 바꾼 sNumVal이 몇자리인지 출력한다.
		int kopo36_i, kopo36_j;
		// 정수형 변수 i와 j를 선언한다.
		String[] kopo36_units = { "영", "일", "이", "삼", "사", "오", "육", "칠", "팔", "구" };
		// String 배열에 영~구 까지 넣어준다.
		String[] kopo36_unitX = { "", "십", "백", "천", "만", "십", "백", "천", "억", "십" };
		// String 배열에 "", 십, 백, 천, 만, 십, 백, 천, 억, 십 을 넣어준다. 숫자의 각 자리마다 표시하기 위해..
		kopo36_i = 0; //
		// 정수형 변수 i를 0으로 초기화한다.
		kopo36_j = kopo36_sNumVal.length() - 1; // 9
		// 정수형 변수 i를 sNumVal의 길이 - 1로 초기화한다. (9로 초기화됨)
		while (true) { // while 반복문을 이용하여
			if (kopo36_i >= kopo36_sNumVal.length()) {
				// 변수 i가 sNumVal의 자리수 즉 10보다 크거나 같으면 멈추게 조건문을 걸어준다.
				break;
			}
			System.out.printf("%s[%s]", kopo36_sNumVal.substring(kopo36_i, kopo36_i + 1), // 1001034567
					// sNumVal = "1001034567"
					// 문자열[문자열] 형식으로 출력을 할거다. sNumVal을 첫번째 글자만 사용할 수 있게 subString을 사용했다.
					// substring(0, 1)하면 첫번째 글자만 뽑아낼 수 있다. --> 1
					kopo36_units[Integer.parseInt(kopo36_sNumVal.substring(kopo36_i, kopo36_i + 1))] // units[1]
			);
			// units = { "영", "일", "이", "삼", "사", "오", "육", "칠", "팔", "구" }
			// substring으로 sNumVal 맨 앞글자를 자르고 그 글자를 다시 정수형으로 형변환한 후
			// units배열의 요소를 찾는데 쓴다. --> "일"
			// 변수 i만 1씩 증가시키면서 while문을 돌린다면 원하는 숫자에 원하는 글자를 넣을 수 있다.

			// 1[일]. while문으로 돌때 마지막에 kopo36_i의 값을 ++ 해주기 때문에 뒤에겄들도 하나씩 만들어진다.
			// 결국은 1[일]0[영]0[영]1[일]0[영]3[삼]4[사]5[오]6[육]7[칠]이 된다.

			if (kopo36_sNumVal.substring(kopo36_i, kopo36_i + 1).equals("0")) {
				// sNumVal을 subString을 이용해 kopo36_i번째 문자만 뽑아낸다.
				// substring(0, 1)하면 첫번째 글자만 뽑아낼 수 있다.
				// sNumVal에서 뽑아온 한 글자가 0이면서
				if (kopo36_unitX[kopo36_j].equals("억")) {
					// unitX배열의 j번째 요소가 "억"과 일치할 때
					if (kopo36_sNumVal.substring(kopo36_sNumVal.length() - 8, kopo36_sNumVal.length() - 4).equals("0000")) {
						// 만의 자리 수가 0000일 때
						kopo36_sNumvoice = kopo36_sNumvoice + "" + kopo36_unitX[kopo36_j];
						// 억을 넣는다.
					} else {
						kopo36_sNumvoice = kopo36_sNumvoice + "" + kopo36_unitX[kopo36_j];
					}

					// String 변수 sNumvoice는 sNumvoice에 unitX배열의 j번째 요소를 붙여 쓴다.
					// 결국 단위를 셀때 한 단위의 끝이 0이면 무시하고 세는 단위만 쓴다는 것
					// String kopo36_sNumvoice는 빈칸 처리 돼어 있기 때문에 "" + "" + "만" 또는 "억"이 출력
				} else if(kopo36_sNumVal.length() <= 8 && (kopo36_unitX[kopo36_j].equals("만") && kopo36_sNumVal.substring(kopo36_sNumVal.length() - 4, kopo36_sNumVal.length()).equals("0000"))) {
					// 만을 걸러주는 것으로 천만 단위 까지 이고, 천에 자리까지 0000일 때
					kopo36_sNumvoice = kopo36_sNumvoice + "" + kopo36_unitX[kopo36_j];
					//만을 출력하게 한다.
				} else {
				}
			} else {
				// sNumVal에서 뽑아온 한 글자가 0이 아닐 때
				kopo36_sNumvoice = // "일십"
						// kopo36_sNumvoice = "";
						kopo36_sNumvoice
								+ kopo36_units[Integer.parseInt(kopo36_sNumVal.substring(kopo36_i, kopo36_i + 1))]
								// Integer.parseInt(kopo36_sNumVal.substring(kopo36_i, kopo36_i + 1))는
								// 1001034567에서 한 글자만 뽑아서 정수로 형변환 시키는 것이다.
								// ex) kopo36_units[1] = 일
								+ kopo36_unitX[kopo36_j];
				// kopo36_j는 9부터 시작. kopo36_unitX[9] = "십"
				// substring으로 sNumVal 맨 앞글자를 자르고 그 글자를 다시 정수형으로 형변환한 후 units배열을 찾는데 사용한다. +
				// unitX배열의 j번째 요소를 더한 것이 sNumvoice와 같다
				// 0이 아니면 글자를 세는데 쓰는 배열을 사용하여 표시 해준다는 것이다.

				// 처음은 "일십"으로 시작하지만, 계속 누적되면서 "일십억일백삼만사천오백육십칠"이 된다.
			}

			kopo36_i++;
			// i값을 1씩 증가시킨다. 그렇게 하여 첫 번째 요소부터 움직이면서 숫자를 나타내는 배열을 모두 사용할 수 있다.
			kopo36_j--;
			// j값을 1씩 감소시킨다. 그렇게 하면 배열의 요소를 하나씩 움직이면서 숫자를 나타내는 배열을 모두 사용할 수 있다.
		}
		if( kopo36_sNumVal.length() == 1 && kopo36_sNumVal.substring(0, 1).equals("0")) {
			 kopo36_sNumvoice = "영"; // 자리수가 1이고 그 문자가 0일때 sNumvoice를 영으로 한다. 이렇게 하면 0만 입력했을때 0이 된다.
		}
		System.out.printf("\n %s[%s]\n", kopo36_sNumVal, kopo36_sNumvoice);
		// 위에 if문을 통해 만들어진 kopo36_sNumvoice이 사용된다.
	}
}