public class Notification {
	private String id;
	private String title;
	private String date;
	private String content;
	private String titlenum;
	private String relevel;
	private String recent;
	private String viewcnt;
	
	
	
	public String getTitlenum() {
		return titlenum;
	}
	public void setTitlenum(String titlenum) {
		this.titlenum = titlenum;
	}
	public String getRelevel() {
		return relevel;
	}
	public void setRelevel(String relevel) {
		this.relevel = relevel;
	}
	public String getRecent() {
		return recent;
	}
	public void setRecent(String recent) {
		this.recent = recent;
	}
	public String getViewcnt() {
		return viewcnt;
	}
	public void setViewcnt(String viewcnt) {
		this.viewcnt = viewcnt;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	
}
