package JavaPro_7_6;

import java.util.ArrayList;

public class Programming_7_6_OneRec {
	public static void main(String[] args) {
		// 명수를 정한다. 5명
		int kopo36_iPerson = 5;
		
		// 생성자를 인자로 받는 배열을 만든다.
		kopo36_OneRec[] kopo36_inData = new kopo36_OneRec[kopo36_iPerson];
		
		
		// 배열에 생성자를 하나씩 인스턴스화 해서 넣는다.
		kopo36_inData[0] = new kopo36_OneRec("홍길01", 100, 100, 90);
		kopo36_inData[1] = new kopo36_OneRec("홍길02", 90, 90, 90);
		kopo36_inData[2] = new kopo36_OneRec("홍길03", 80, 70, 90);
		kopo36_inData[3] = new kopo36_OneRec("홍길04", 70, 60, 90);
		kopo36_inData[4] = new kopo36_OneRec("홍길05", 60, 100, 90);
		
		// for문을 5번 돌린다.
		for (int kopo36_i = 0; kopo36_i < 5; kopo36_i++) {
			
			//잘 들어갔는지 확인해본다.
			System.out.println(kopo36_inData[kopo36_i].kopo36_name());
		}
		System.out.println("합계");
		System.out.println(kopo36_inData[kopo36_iPerson - 1].kopo36_sum());
	}
} 
