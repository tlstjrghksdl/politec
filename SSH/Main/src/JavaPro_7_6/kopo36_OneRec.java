package JavaPro_7_6;

public class kopo36_OneRec {
	// 클래스가 가지는 변수들 선언 (이름, 국어, 영어, 수학)
	private String kopo36_name;
	private int kopo36_kor;
	private int kopo36_eng;
	private int kopo36_mat;
	// 클래스가 가지는 변수들(이름, 국어, 영어, 수학)로 생성자메소드 만들기
	public kopo36_OneRec(String kopo36_name, int kopo36_kor, int kopo36_eng, int kopo36_mat) {
		this.kopo36_name = kopo36_name;
		this.kopo36_kor = kopo36_kor;
		this.kopo36_eng = kopo36_eng;
		this.kopo36_mat = kopo36_mat;
	}
	// getter
	public String kopo36_name() {
		return this.kopo36_name;
	}
	// getter
	public int kopo36_kor() {
		return this.kopo36_kor;
	}
	// getter
	public int kopo36_eng() {
		return this.kopo36_eng;
	}
	// 합계 구하는 메소드
	public int kopo36_sum() {
		return this.kopo36_kor + this.kopo36_eng + this.kopo36_mat;
	}
	// 평균 구하는 메소드
	public double kopo36_avg() {
		return this.kopo36_sum() / 3.0;
	}
	
}
