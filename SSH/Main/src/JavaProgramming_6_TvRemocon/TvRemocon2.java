package JavaProgramming_6_TvRemocon;

public class TvRemocon2 {
	int kopo36_limit_up_Chen;        // 마지막 채널 선언 
	int kopo36_limit_down_Chen;      // 맨 처음 채널 선언
	int kopo36_nowChen;              // 지금 채널 선언
	int kopo36_limit_up_Vol;         // 최대 볼륨 선언
	int kopo36_limit_down_Vol;       // 최소 볼륨 선언 
	int kopo36_nowVol;               // 지금 볼륨 선언 
	String kopo36_tellThem; 		 // 보여질 안내문을 선언
	
	
	// 생성자 1 
	TvRemocon2() {
		kopo36_limit_up_Chen = 10;    // 마지막 채널 초기화
		kopo36_limit_down_Chen = 0;   // 맨 처음 채널 초기화
		kopo36_nowChen = 1;           // 지금 채널 초기화
		kopo36_limit_up_Vol = 5;      // 최대 볼륨 초기화
		kopo36_limit_down_Vol = 0;    // 최소 볼륨 초기화
		kopo36_nowVol = 1;            // 지금 볼륨 초기화
		System.out.println("Tv 켜짐");
	}
	
	
	// int값을 받는 생성자 2
	TvRemocon2(int kopo36_a) {
		kopo36_limit_up_Chen = 10;     // 마지막 채널 초기화
		kopo36_limit_down_Chen = 0;    // 맨 처음 채널 초기화
		kopo36_nowChen = 1;            // 지금 채널 초기화
		kopo36_limit_up_Vol = 5;       // 최대 볼륨 초기화
		kopo36_limit_down_Vol = 0;     // 최소 볼륨 초기화
		kopo36_nowVol = 1;             // 지금 볼륨 초기화
		System.out.printf("Tv 켜짐[%d]\n", kopo36_a);
		
		if(kopo36_a > 0) {
		// 생성자가 받은 int값이 0보다 크면
			for (int kopo36_i = 0; kopo36_i < 10; kopo36_i++) {
				// for문을 통해 9까지 chennel up메소드를 실행시킨다.
				this.kopo36_ChenUP();
			}
		} else if (kopo36_a < 0) {
		// 생성자가 받은 int값이 0보다 작으면
			for (int kopo36_i = 0; kopo36_i < (kopo36_i*-1); kopo36_i++) {
				// for문을 통해 0까지 chennel up메소드를 실행시킨다. 즉 한번 실행 시킨 다는 것이다.
				this.kopo36_ChenDN();
				
			}
		}

	}
	
	// 채널을 올리는 메소드
	void kopo36_ChenUP() {
		if(kopo36_nowChen == kopo36_limit_up_Chen) { // 현재 채널과 마지막 채널이 같다면
			System.out.printf("마지막 채널입니다. [%d]\n", kopo36_nowChen);
			// 마지막 채널이라는 문구 출력과 몇채널인지 알려준다.
		} else { // 그것이 아니면
			kopo36_nowChen++;
			// 현재 채널을 1올려주고 몇채널로 변경됐는지 알려준다.
			System.out.printf("채널 변경 : [%d]\n", kopo36_nowChen);
		}
	}
	
	// 채널을 내리는 메소드
	void kopo36_ChenDN() {
		if(kopo36_nowChen == kopo36_limit_down_Chen) { // 현재 채널과 맨 처음 채널이 같다면
			System.out.printf("처음 채널입니다. [%d]\n", kopo36_nowChen);
			// 처음 채널이라는 문구 출력
		} else { // 맨 처음 채널이 아니면
			kopo36_nowChen--; // 현재 채널을 -1 하여 채널을 내린다.
			System.out.printf("채널 변경 : [%d]\n", kopo36_nowChen);
			// 채널이 몇채널로 변경됐는지 알려준다.
		}
	}
	
	// 볼륨을 올리는 메소드
	void kopo36_VolUP() { 
		if(kopo36_nowVol == kopo36_limit_up_Vol) { // 현재 볼륨과 최대볼륨이 같다면 
			System.out.printf("최대 볼륨입니다. [%d]\n", kopo36_nowVol);
			// 최대 볼륨이라고 출력
		} else { // 다르면
			kopo36_nowVol++; // 채널을 +1 해주고
			System.out.printf("볼륨 업 : [%d]\n", kopo36_nowVol);
			// 볼륨업 출력후 볼륨이 몇인지 알려준다.
		}
	}
	
	// 볼륨을 내리는 메소드
	void kopo36_VolDN() {
		if(kopo36_nowVol == kopo36_limit_down_Vol) { // 현재 볼륨이 최소볼륨과 같다면
			System.out.printf("최소 볼륨입니다. [%d]\n", kopo36_nowVol);
			// 최소 볼륨이라고 출력
		} else { // 다르면
			kopo36_nowVol--; // 현재 채널을 -1 해주고
			System.out.printf("볼륨 다운 : [%d]\n", kopo36_nowVol);
			// 볼륨 다운 출력후 현재 볼륨이 몇인지 알려준다.
		}
	}
	
}
 