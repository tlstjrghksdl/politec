package JavaProgramming_6_TvRemocon;

public class Programming_6_4_Remote {
	public static void main(String[] args) {
		
		// TvRemocon을 인스턴스화(복사)한다.
		TvRemocon kopo36_TR;
		kopo36_TR = new TvRemocon();
		
		// for문을 10번 실행하며 TvRemocon Class에 있는 채널을 올리는 메소드를 실행한다.
		// 채널은 최대 10개이기 때문에 처음시작하는 채널 1에서 10까지 올리기 위해
		for (int kopo36_i = 0; kopo36_i < 10; kopo36_i++) {
			kopo36_TR.kopo36_Chennel_up();
			
			System.out.println(kopo36_TR.kopo36_tellThem);
		}
		
		// for문을 10번 실행하며 TvRemocon Class에 있는 채널을 내리는 메소드를 실행한다.
		// 채널은 최저 1개이기 때문에 마지막 채널 10에서 11까지 내리기 위해
		for (int kopo36_i = 0; kopo36_i < 10; kopo36_i++) {
			kopo36_TR.kopo36_Chennel_down();
			
			System.out.println(kopo36_TR.kopo36_tellThem);
		}
		
		// for문을 5번 실행하며 TvRemocon Class에 있는 불륨을 올리는 메소드를 실행한다.
		// 볼륨은 최대 5개이기 때문에 처음시작하는 볼륨 1에서 5까지 올리기 위해
		for (int kopo36_i = 0; kopo36_i < 5; kopo36_i++) {
			kopo36_TR.kopo36_Vol_up();
			
			System.out.println(kopo36_TR.kopo36_tellThem);
		}
		
		// for문을 5번 실행하며 TvRemocon Class에 있는 볼륨을 내리는 메소드를 실행한다.
		// 볼륨은 최저 1개이기 때문에 최대볼륨 5에서 최저볼륨 1까지 내리기 위해
		for (int kopo36_i = 0; kopo36_i < 5; kopo36_i++) {
			kopo36_TR.kopo36_Vol_down();
			
			System.out.println(kopo36_TR.kopo36_tellThem);
		}
	}
}
