package JavaProgramming_6_TvRemocon;

public class TvRemocon {
	// 마지막 채널
	int kopo36_limit_Chen_up = 10;
	
	// 맨 처음 채널
	int kopo36_limit_Chen_down = 1;
	
	// 최대 볼륨
	int kopo36_limit_Vol_up = 5;
	
	// 최소 볼륨
	int kopo36_limit_Vol_down = 1;
	
	// 지금 채널
	int kopo36_nowChen = 1;
	
	// 지금 볼륨
	int kopo36_nowVol = 1;
	
	// 메소드 실행시마다 출력할 String값
	String kopo36_tellThem;
	
	
	// 채널을 올리는 메소드
	void kopo36_Chennel_up() {
		if(kopo36_nowChen == kopo36_limit_Chen_up) {
			kopo36_tellThem = "마지막 채널입니다.";
			// 현재 체널이 마지막 채널과 같으면 위와같이 출력한다.
		} else {
			kopo36_nowChen++;
			// 그렇지 않으면 현재 채널을 +1한다.
			kopo36_tellThem = String.format("현재 채널은 [%d] 입니다\n", kopo36_nowChen);
		}
	}
	
	
	// 채널을 내리는 메소드
	void kopo36_Chennel_down() {
		if(kopo36_nowChen == kopo36_limit_Chen_down) {
			kopo36_tellThem = "처음 채널입니다.";
			// 현재 체널이 처음 채널과 같으면 위와같이 출력한다.
		} else {
			kopo36_nowChen--;
			// 그렇지 않으면 현재 채널을 -1 한다.
			kopo36_tellThem = String.format("현재 채널은 [%d] 입니다\n", kopo36_nowChen);
		}
	}
	
	// 볼륨을 올리는 메소드
	void kopo36_Vol_up() {
		if(kopo36_nowVol == kopo36_limit_Vol_up) {
			kopo36_tellThem = "최대 볼륨입니다.";
		} else {
			kopo36_nowVol++;
			
			kopo36_tellThem = String.format("현재 볼륨 : [%d]\n", kopo36_nowVol);
		}
	}
	
	// 볼륨을 내리는 메소드
	void kopo36_Vol_down() {
		if(kopo36_nowVol == kopo36_limit_Vol_down) {
			kopo36_tellThem = "최저 볼륨입니다.";
		} else {
			kopo36_nowVol--;
			
			kopo36_tellThem = String.format("현재 볼륨 : [%d]\n", kopo36_nowVol);
		}
	}
 }
