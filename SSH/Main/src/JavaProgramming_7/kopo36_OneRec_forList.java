package JavaProgramming_7;

public class kopo36_OneRec_forList {
	private int kopo36_student_id;
	private String kopo36_name;
	private int kopo36_kor;
	private int	kopo36_eng;
	private int	kopo36_mat;
	
	
	public kopo36_OneRec_forList(int kopo36_student_id, String kopo36_name, int kor, int eng, int mat) {
		this.kopo36_student_id = kopo36_student_id;
		this.kopo36_name = kopo36_name;
		this.kopo36_kor = kor;
		this.kopo36_eng = eng;
		this.kopo36_mat = mat;
	}
	
	public int kopo36_student_id() {
		return this.kopo36_student_id;
	}
	
	public String kopo36_name() {
		return this.kopo36_name;
	}
	
	public int kopo36_kor() {
		return this.kopo36_kor;
	}
	
	public int kopo36_eng() {               
		return this.kopo36_eng;
	}
	
	public int kopo36_mat() {               
		return this.kopo36_mat;
	}
	
	public int kopo36_sum() {               
		return this.kopo36_kor + this.kopo36_eng + this.kopo36_mat;
	}
	
	public double kopo36_avg() {               
		return this.kopo36_sum() / 3.0;
	}
	
	

}
