package JavaProgramming_7;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;

public class Programming_7_2_real {
	public static void main(String[] args) throws UnsupportedEncodingException {
		// 가격에 콤마를 붙일것이다.
		DecimalFormat kopo36_df = new DecimalFormat("###,###,###");
		// emart에서 복사해온 상품들이다.
		String[] kopo36_OneRec = { "01                          1 99       100",
				"02* 나의               15,007  2    30,014", "03  동반자              2,980  1     2,980",
				"04* 영원한 나의         4,950  5    24,750", "05  동반자 내 생애 11,115,980  4     3,000",
				"06* 최고의 선물    11,084,800  222,169,600", "07  당신과 만남이  11,330,000  111,330,000",
				"08* 잘 살고 못 사   5,980,000  1 5,980,000", "09* 타고난 팔자지   2,980,000  1 2,980,000",
				"10  당신만을 사랑]      4,702  1    14,702", "11* 영원한 동반자         990  1       990",
				"12  아~당신은           1,440  2    32,880", "13* 못 믿을 사4람       4,250  1     4,250",
				"14* 아~당신은           2,750  1    62,750", "15* 철없는사람          9,800  1     9,800",
				"16* 아무리a             2,980  1    42,980", "17* 달래봐도              990  1       990",
				"18* 어쩔 순 없지만     10,130  1    10,130", "19* 마음 하나는         4,060  3    12,180",
				"20  괜찮은 사람         3,900  1     3,900", "21* 오늘은 들국화      12,400  2    24,800",
				"22* 또 내일은           5,000  1     5,000", "23* 장미꽃              5,000  1     5,000",
				"24* 치근~치근~치근      9,800  1     9,800", "25* 대다가 잠이         9,900  2    18,800",
				"26* 들겠지~            39,900  1    39,900", "27* 난 이제             5,180  2     9,360",
				"28* 지쳤어요              520  1       520", "29* 땡벌~               1,010  1     1,010",
				"30  땡벌~               1,020  1     1,020" };
		// for문을 배열의 길이만큼 실행
		for (int kopo36_i = 0; kopo36_i < kopo36_OneRec.length; kopo36_i++) {
			try {
				// 배열의 요소마다 byte로 잘라준다
				byte[] kopo36_b = kopo36_OneRec[kopo36_i].getBytes("EUC-KR");
				
				// 배열에서 0~2번째를 가져오면 상품의 순서가 나온다
				String kopo36_itemNum = new String(kopo36_b, 0, 2);
				
				// 배열에서 4번째에서 14바이트를 가져오면 상품명이 나온다
				String kopo36_itemName = new String(kopo36_b, 4, 14, "EUC-KR");

				// 배열에서 32번째에서 10바이트를 가져오면 상품의 순서가 나온다
				String kopo36_sum = new String(kopo36_b, 32, 10);
				// 가져온 String에는 콤마와 공백이 붙어 있기 때문에 공백과콤마를 없애준다. 그리고 int로 형변환
				int kopo36_intSum = Integer.parseInt(kopo36_sum.replace(",", "").trim());
				
				// 배열에서 29번째에서 3바이트를 가져오면 상품의 순서가 나온다
				String kopo36_num = new String(kopo36_b, 29, 3);
				// 가져온 String에는 콤마와 공백이 붙어 있기 때문에 공백과콤마를 없애준다. 그리고 int로 형변환
				int kopo36_intnum = Integer.parseInt(kopo36_num.replace(",", "").trim());
				
				// 배열에서 19번째를  10바이트가져오면 상품의 순서가 나온다
				String kopo36_price = new String(kopo36_b, 19, 10);
				// 가져온 String에는 콤마와 공백이 붙어 있기 때문에 공백과콤마를 없애준다. 그리고 int로 형변환
				int kopo36_intprice = Integer.parseInt(kopo36_price.replace(",", "").trim());
				
				// 그렇게 얻은 단가, 수량, 총액으로 if함수를 만든다. 단가 * 수량이 총액과 다를때 아래를 출력하게 한다.
				// 가격에는 다시 콤마를 넣어준다.
				if (kopo36_intprice * kopo36_intnum != kopo36_intSum) {
					System.out.println(
							"*************************************************************************************");
					System.out.printf("%s%-5s%s%20s%20s%20s]\n", "오류[", kopo36_itemNum, kopo36_itemName,
							kopo36_df.format(kopo36_intprice), kopo36_intnum, kopo36_df.format(kopo36_intSum));
					System.out.printf("%s%-5s%s%20s%20s%20s]\n", "수정[", kopo36_itemNum, kopo36_itemName,
							kopo36_df.format(kopo36_intprice), kopo36_intnum, kopo36_df.format(kopo36_intprice * kopo36_intnum));
					System.out.println(
							"*************************************************************************************");
				} else {
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
