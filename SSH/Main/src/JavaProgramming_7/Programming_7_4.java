package JavaProgramming_7;

import java.io.*;

import com.sun.imageio.stream.StreamCloser;

public class Programming_7_4 {
	public static void main(String[] args) throws Exception {
			// filewriter를 통해 만들 파일을 정한다.
			FileWriter kopo36_fw = new FileWriter("C:\\Users\\401-ST05\\Desktop\\name.txt", true);
			
			// bufferedwriter는 데이터를 하나하나 바로 처리하는 것이 아니라, 모아서 한번에 처리하는 것이다.
			// 그래서 bufferedwriter에 만들어서 저장할 파일을 가져온다.
			BufferedWriter kopo36_bw = new BufferedWriter(kopo36_fw);
			
			// 고아데이터가 남지 않도록 사용할 StringBuffer을 인스턴스화한다.
			StringBuffer kopo36_sb = new StringBuffer();
			
			
			// bufferedreader은 읽어올 녀석을 정하는 것이다. 
			// inputStreamreader은 바이트 스트림에서 문자 스트림으로 변환을 제공하는 입출력 스트림이다.
			BufferedReader kopo36_br = new BufferedReader(new InputStreamReader(System.in));
			StreamCloser kopo36_sc = new StreamCloser();
			//작성될 String 초기화
			String kopo36_str = "";
			
			// while이 s가 아닐때 while은 무한으로 돌아간다.
			// 문자열 str은 버퍼리드로 한줄씩 읽어온다.
			while(!(kopo36_str=kopo36_br.readLine()).startsWith("s")) {
				
				// 읽어온 str을 추가한다. 그리고 개행
				kopo36_sb.append(kopo36_str + "\n");
			}
			// 버퍼리드를 close
			kopo36_br.close();
			
			// StringBuffer 문자열로 바꿔주고, filewriter로 저장할 파일에 써준다.
			kopo36_fw.write(kopo36_sb.toString());
			
			// filewriter를 묶어준다.
			kopo36_fw.flush();
			
			// filewriter를 close한다.
			kopo36_fw.close();
			
			System.out.println("저장이 완료되었습니다.");
	}
}