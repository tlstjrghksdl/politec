package JavaProgramming_7;

import java.util.ArrayList;

public class Programming_7_7 {
	public static void main(String[] args) {
		
		// ArrayList에 String이 들어가게 인스턴스화 한다.
		ArrayList<String> kopo36_iAL = new ArrayList<String>();
		
		
		// add로 리스트 안에 추가한다. 그러면 요소가 하나씩 늘어난다.
		kopo36_iAL.add("abc");
		kopo36_iAL.add("abcd");
		kopo36_iAL.add("efga");
		kopo36_iAL.add("가나다0");
		kopo36_iAL.add("1234");
		kopo36_iAL.add("12rk34");
		
		System.out.printf("*****************************\n");
		System.out.printf(" 시작 ArraySize %d \n", kopo36_iAL.size());
		
		// for문을 list사이즈만큼 실행시키면서 출력문을 출력한다.
		for (int kopo36_i = 0; kopo36_i < kopo36_iAL.size(); kopo36_i++) {
			System.out.printf(" ArrayList %d = %s\n", kopo36_i, kopo36_iAL.get(kopo36_i));
		}
		
		// set메소드로 list 3번째 요소를 "가라라라"로 바꿔준다.
		kopo36_iAL.set(3, "가라라라");
		System.out.printf("*****************************\n");
		System.out.printf(" 내용변경 ArraySize %d \n", kopo36_iAL.size());
		
		// 그리고 전체를 출력해본다
		for (int kopo36_i = 0; kopo36_i < kopo36_iAL.size(); kopo36_i++) {
			System.out.printf(" ArrayList %d = %s\n", kopo36_i, kopo36_iAL.get(kopo36_i));
		}
		
		// 4번째 list값을 지워본다. remove
		kopo36_iAL.remove(4);
		System.out.printf("*****************************\n");
		System.out.printf(" 내용변경 ArraySize %d \n", kopo36_iAL.size());
		
		// 전체를 출력해본다.
		for (int kopo36_i = 0; kopo36_i < kopo36_iAL.size(); kopo36_i++) {
			System.out.printf(" ArrayList %d = %s\n", kopo36_i, kopo36_iAL.get(kopo36_i));
		}
		kopo36_iAL.sort(null); // sort메소드를 정렬한다. 순서는 숫자, 영어, 한글 순이다.
		System.out.printf("*****************************\n");
		System.out.printf(" 내용변경 ArraySize %d \n", kopo36_iAL.size());
		
		// 전체를 출력해본다.
		for (int kopo36_i = 0; kopo36_i < kopo36_iAL.size(); kopo36_i++) {
			System.out.printf(" ArrayList %d = %s\n", kopo36_i, kopo36_iAL.get(kopo36_i));
		}
		kopo36_iAL.clear();
		System.out.printf("*****************************\n");
		System.out.printf(" 전부삭제 ArraySize %d \n", kopo36_iAL.size());
		for (int kopo36_i = 0; kopo36_i < kopo36_iAL.size(); kopo36_i++) {
			System.out.printf(" ArrayList %d = %s\n", kopo36_i, kopo36_iAL.get(kopo36_i));
		}
	}
}
