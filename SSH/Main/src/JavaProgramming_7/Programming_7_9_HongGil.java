package JavaProgramming_7;

import java.util.ArrayList;

public class Programming_7_9_HongGil {
	
	// 클래스를 담는 ArrayList를 인스턴스화한다. 
	static ArrayList<kopo36_OneRec_forList> kopo36_ArrayOneRec = new ArrayList<kopo36_OneRec_forList>();
	static int kopo36_sumkor = 0;	// 누적할 점수
	static int kopo36_sumeng = 0;	// 누적할 점수
	static int kopo36_summat = 0;	// 누적할 점수
	static int kopo36_sumsum = 0;	// 누적할 점수
	static int kopo36_sumavg = 0;	// 누적할 점수
	static final int kopo36_iPerson = 30; // 사람수. final이 붙어서 가변의 변수가 됐다.

	// 데이터 만들기
	public static void kopo36_dataSet() {
		for (int kopo36_i = 0; kopo36_i < kopo36_iPerson; kopo36_i++) { // 사람수만큼 실행
			String kopo36_name = String.format("홍길%02d", kopo36_i + 1); // 이름에 숫자를 붙여준다. 순서대로
			int kopo36_kor = (int) (Math.random() * 100); // 숫자를 random함수로 불러온다. 곱하기 100을 하여 0~100까지 받는다
			int kopo36_eng = (int) (Math.random() * 100); // 숫자를 random함수로 불러온다. 곱하기 100을 하여 0~100까지 받는다
			int kopo36_mat = (int) (Math.random() * 100); // 숫자를 random함수로 불러온다. 곱하기 100을 하여 0~100까지 받는다
			kopo36_ArrayOneRec // list에 add로 얻은 값을 기반으로 생성자를 넣어준다.
					.add(new kopo36_OneRec_forList(kopo36_i + 1, kopo36_name, kopo36_kor, kopo36_eng, kopo36_mat));
		}
	}

	// 헤더 인쇄
	public static void kopo36_HeaderPrint() {
		System.out.printf("======================================================\n");
		System.out.printf("%2s%6s%6s%6s%6s%6s%8s\n", "번호", "이름", "국어", "영어", "수학", "합계", "평균");
		System.out.printf("======================================================\n");
	}

	// 내용 인쇄
	public static void kopo36_itemPrint(int kopo36_i) {
		kopo36_OneRec_forList kopo36_OF; // 출력할 생성자.

		kopo36_OF = kopo36_ArrayOneRec.get(kopo36_i); // 생성자를 정수 kopo36_i를 get에 넣어 몇번째 놈을 넣을지 정한다.
		System.out.printf("%4d%9s%5d%8d%8d%8d%10.2f\n", kopo36_OF.kopo36_student_id(), kopo36_OF.kopo36_name(),
				kopo36_OF.kopo36_kor(), kopo36_OF.kopo36_eng(), kopo36_OF.kopo36_mat(), kopo36_OF.kopo36_sum(),
				(double) kopo36_OF.kopo36_avg()); 

		kopo36_sumkor += kopo36_OF.kopo36_kor(); // 가져온 값들을 누적시킨다.
		kopo36_sumeng += kopo36_OF.kopo36_eng(); // 가져온 값들을 누적시킨다.
		kopo36_summat += kopo36_OF.kopo36_mat(); // 가져온 값들을 누적시킨다.
		kopo36_sumsum += kopo36_OF.kopo36_sum(); // 가져온 값들을 누적시킨다.
		kopo36_sumavg += kopo36_OF.kopo36_avg(); // 가져온 값들을 누적시킨다.
	}

	// 꼬리 인쇄
	public static void kopo36_TailPrint() {
		System.out.printf("======================================================\n");
		System.out.printf("국 어 합 계   %13d   국어평균 : %13.2f\n", kopo36_sumkor,
				kopo36_sumkor / (double) kopo36_ArrayOneRec.size());
		System.out.printf("영 어 합 계   %13d   영어평균 : %13.2f\n", kopo36_sumeng,
				kopo36_sumeng / (double) kopo36_ArrayOneRec.size());
		System.out.printf("수 학 합 계   %13d   수학평균 : %13.2f\n", kopo36_summat,
				kopo36_summat / (double) kopo36_ArrayOneRec.size());
		System.out.printf("======================================================\n");
		System.out.printf("반평균 합계   %13d   반 평 균 : %13.2f\n", kopo36_sumavg,
				kopo36_sumavg / (double) kopo36_ArrayOneRec.size());
	}

	public static void main(String[] args) {

		kopo36_dataSet(); // 데이터 생성
		kopo36_HeaderPrint(); // 헤더 출력을 해준다
		
		// for문을 사람수만큼 돌면서 itemPrint메소드를 출력한다. 
		for (int kopo36_i = 0; kopo36_i < kopo36_iPerson; kopo36_i++) {

			kopo36_itemPrint(kopo36_i);

		}
		kopo36_TailPrint(); //꼬리 부분을 출력한다. 누적된 값이 나온다.
	}
}
