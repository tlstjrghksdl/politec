package JavaProgramming_7;

public class Programming_7_5_Max {
	public static void main(String[] args) {
		
		//최대값을 구할 배열 만들기
		int[] kopo36_iArray = { 1, 7, 5, 3, 2, 1, 3, 4, 9, 12, 1, 212, 33, 11, 21, 11, 21, 11, 2121, 121, 11, 5, 6, 33 };
		
		// for문을 배열의 길이만큼 실행
		for (int kopo36_i = 0; kopo36_i < kopo36_iArray.length; kopo36_i++) {
			System.out.printf("iArray[%d] = %d\n", kopo36_i, kopo36_iArray[kopo36_i]);
		}
		
		// 구할 최대값을 먼저 배열의 첫번째 요소로 선언, 초기화
		int kopo36_iMax = kopo36_iArray[0];
		
		// 출력할 숫자
		int kopo36_iMaxPt = 0;
		
		// for문을 배열의 길이만큼 실행
		for (int kopo36_i = 0; kopo36_i < kopo36_iArray.length; kopo36_i++) {
			if(kopo36_iMax < kopo36_iArray[kopo36_i]) { // iMax의 값이 다음 배열 요소보다 작으면
				kopo36_iMax = kopo36_iArray[kopo36_i]; // 다음 배열로 iMax값을 변경.
				// 그렇게 되면 최대값이 될때까지 바뀌다가 최대값으로 바뀌면 안바뀐다
				kopo36_iMaxPt = kopo36_i;
				// 최대값이 몇번째 요소인지 바꾸는 것.
			}
		}
		
		System.out.printf("MAX : iArray[%d] = %d\n", kopo36_iMaxPt, kopo36_iMax);
		// 최대값을 나타내주는 출력문
	}
}
 