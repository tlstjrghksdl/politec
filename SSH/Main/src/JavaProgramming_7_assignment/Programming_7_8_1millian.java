package JavaProgramming_7_assignment;

import java.util.ArrayList;

public class Programming_7_8_1millian {
	public static void main(String[] args) {
		// 테스트할 숫자 100만개 초기화
		int kopo36_iTestMAX = 1000000;
		
		// 정수형 변수가 들어가는 List 인스턴스화
		ArrayList<Integer> kopo36_iAL = new ArrayList<>();
		
		// for문을 100만번 돌린다.
		// List에 0 ~ 100만 사이 아무 숫자나 add한다.
		for (int kopo36_i = 0; kopo36_i < kopo36_iTestMAX; kopo36_i++) {
			kopo36_iAL.add((int)(Math.random()*1000000));
		}
		
		// for문을 List의 사이즈(위에서 100만이 됨)만큼 돌린다.
		// List가 가진 값을 차례대로 출력
		for (int kopo36_i = 0; kopo36_i < kopo36_iAL.size(); kopo36_i++) {
			System.out.printf(" ArrayList %d = %d\n", kopo36_i, kopo36_iAL.get(kopo36_i));
		}
		
		System.out.printf("********************************\n");
		
		// 정렬!
		kopo36_iAL.sort(null); 
		
		// 다시 한번 List가 가진 값 출력해보기
		for (int kopo36_i = 0; kopo36_i < kopo36_iAL.size(); kopo36_i++) {
			System.out.printf(" ArrayList %d = %d\n", kopo36_i, kopo36_iAL.get(kopo36_i));
		}
	}
}
