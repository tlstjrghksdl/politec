package JavaProgramming_7_assignment;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;

public class Programming_7_2_useEmart {
//	static String kopo36_addBlank(String kopo36_a) {
//		int kopo36_byteCnt = 0;
//		int kopo36_korCnt = 0;
//		String addBlank = "";
//		int a = 0;
//		String[] kopo36_arr = kopo36_a.split("");
//		try {
//			for (int kopo36_i = 0; kopo36_i < kopo36_a.length(); kopo36_i++) {
//				if (kopo36_arr[kopo36_i].getBytes("EUC-KR").length == 2) {
//					kopo36_byteCnt = kopo36_byteCnt + 2;
//				} else {
//					kopo36_byteCnt++;
//				}
//			}
//			kopo36_korCnt = kopo36_byteCnt - kopo36_a.length();
//			a = 14 - kopo36_korCnt;
//			for (int kopo36_i = 0; kopo36_i < a; kopo36_i++) {
//				addBlank = addBlank + "";
//			}
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//		return addBlank;
//	}

	public static void main(String[] args) {
		// 가격에 콤마를 붙일것이다.
		DecimalFormat kopo36_df = new DecimalFormat("###,###,###");
		// emart에서 복사해온 상품들이다.
		String[] kopo36_OneRec = {

				"01                          1 99       100", "02* 나의               15,007  2    30,014",
				"03  동반자              2,980  1     2,980", "04* 영원한 나의         4,950  5    24,750",
				"05  동반자 내 생애 11,115,980  4     3,000", "06* 최고의 선물    11,084,800  222,169,600",
				"07  당신과 만남이  11,330,000  111,330,000", "08* 잘 살고 못 사   5,980,000  1 5,980,000",
				"09* 타고난 팔자지   2,980,000  1 2,980,000", "10  당신만을 사랑       4,702  1     4,702",
				"11* 영원한 동반자         990  1       990", "12  아~당신은           1,440  2     2,880",
				"13* 못 믿을 사4람       4,250  1     4,250", "14* 아~당신은           2,750  1     2,750",
				"15* 철없는사람          9,800  1     9,800", "16* 아무리              2,980  1     2,980",
				"17* 달래봐도              990  1       990", "18* 어쩔 순 없지만     10,130  1    10,130",
				"19* 마음 하나는         4,060  3    12,180", "20  괜찮은 사람         3,900  1     3,900",
				"21* 오늘은 들국화      12,400  2    24,800", "22* 또 내일은           5,000  1     5,000",
				"23* 장미꽃              5,000  1     5,000", "24* 치근~치근~치근      9,800  1     9,800",
				"25* 대다가 잠이         9,900  2    18,800", "26* 들겠지~            39,900  1    39,900",
				"27* 난 이제             5,180  2     9,360", "28* 지쳤어요              520  1       520",
				"29* 땡벌~               1,010  1     1,010", "30  땡벌~               1,020  1     1,020" 
				
		};
		 
		
		for (int kopo36_i = 0; kopo36_i < kopo36_OneRec.length; kopo36_i++) {
			// 총합액
			int kopo36_totalPrice = Integer.parseInt(kopo36_OneRec[kopo36_i]
					.substring(kopo36_OneRec[kopo36_i].length() - 10, kopo36_OneRec[kopo36_i].length()).replace(",", "")
					.trim());

			// 상품 구매 개수
			int kopo36_num = Integer.parseInt(kopo36_OneRec[kopo36_i]
					.substring(kopo36_OneRec[kopo36_i].length() - 13, kopo36_OneRec[kopo36_i].length() - 10).trim());

			// 상품의 단가
			int kopo36_price = Integer.parseInt(kopo36_OneRec[kopo36_i]
					.substring(kopo36_OneRec[kopo36_i].length() - 23, kopo36_OneRec[kopo36_i].length() - 13)
					.replace(",", "").trim());
			
			// 상품의 번호
			String kopo36_itemnum = kopo36_OneRec[kopo36_i].substring(0, 2);

			// 상품명
			String kopo36_itemname = kopo36_OneRec[kopo36_i].substring(4, kopo36_OneRec[kopo36_i].length() - 23);
			
		

			if (kopo36_price * kopo36_num != kopo36_totalPrice) {
				System.out.println("**************************************************************************************");
				System.out.printf("%s%-5s%s%20s%20s%20s]\n", "오류[",  kopo36_itemnum, kopo36_itemname, kopo36_df.format(kopo36_price), kopo36_num, kopo36_df.format(kopo36_totalPrice));
				System.out.printf("%s%-5s%s%20s%20s%20s]\n", "수정[",  kopo36_itemnum, kopo36_itemname, kopo36_df.format(kopo36_price), kopo36_num, kopo36_df.format(kopo36_price * kopo36_num));
				System.out.println("**************************************************************************************");
			} else {
 
			}
		}
	}
}
