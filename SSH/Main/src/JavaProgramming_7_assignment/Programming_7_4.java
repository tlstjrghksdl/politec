package JavaProgramming_7_assignment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStreamReader;

public class Programming_7_4 {
	public static void main(String[] args) throws Exception {
			// filewriter�� ���� ���� ������ ���Ѵ�.
			FileWriter kopo36_fw = new FileWriter("C:\\Users\\401-ST05\\Desktop\\Name.txt", true);
			
			// bufferedwriter�� �����͸� �ϳ��ϳ� �ٷ� ó���ϴ� ���� �ƴ϶�, ��Ƽ� �ѹ��� ó���ϴ� ���̴�.
			// �׷��� bufferedwriter�� ���� ������ ������ �����´�.
			BufferedWriter kopo36_bw = new BufferedWriter(kopo36_fw);
			
			// ��Ƶ����Ͱ� ���� �ʵ��� ����� StringBuffer�� �ν��Ͻ�ȭ�Ѵ�.
			StringBuffer kopo36_sb = new StringBuffer();
			
			
			// bufferedreader�� �о�� �༮�� ���ϴ� ���̴�. 
			// inputStreamreader�� ����Ʈ ��Ʈ������ ���� ��Ʈ������ ��ȯ�� �����ϴ� ����� ��Ʈ���̴�.
			BufferedReader kopo36_br = new BufferedReader(new InputStreamReader(System.in));
			
			//�ۼ��� String �ʱ�ȭ
			String kopo36_str = "";
			
			// while�� s�� �ƴҶ� while�� �������� ���ư���.
			// ���ڿ� str�� ���۸���� ���پ� �о�´�.
			while(!(kopo36_str=kopo36_br.readLine()).startsWith("s")) {
				
				// �о�� str�� �߰��Ѵ�. �׸��� ����
				kopo36_sb.append(kopo36_str + "\n");
			}
			//���۸��带 close
			kopo36_br.close();
			
			// StringBuffer ���ڿ��� �ٲ��ְ�, filewriter�� ������ ���Ͽ� ���ش�.
			kopo36_fw.write(kopo36_sb.toString());
			
			// filewriter�� �����ش�.
			kopo36_fw.flush();
			
			// filewriter�� close�Ѵ�.
			kopo36_fw.close();
			
			System.out.println("������ �Ϸ�Ǿ����ϴ�.");
	}
}