package JavaProgramming_7_assignment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

class Programming_7_10_class {
	// 클래스를 담는 ArrayList를 인스턴스화한다.
	static ArrayList<kopo36_OneRec_forList> kopo36_ArrayOneRec = new ArrayList<kopo36_OneRec_forList>();

	// 사람 수
	static final int kopo36_iPerson = 3000;
	// 한 페이지에 표시하고 싶은 아이템의 수
	static int kopo36_itemnum = 8;

	// 현재페이지 합계와 평균에 사용할 변수
	static int kopo36_nowkor = 0;
	static int kopo36_noweng = 0;
	static int kopo36_nowmat = 0;
	static int kopo36_nowsum = 0;
	static double kopo36_nowavg = 0;
	// 누적페이지 합계와 평균에 사용할 변수
	static int kopo36_sumkor = 0;
	static int kopo36_sumeng = 0;
	static int kopo36_summat = 0;
	static int kopo36_sumsum = 0;
	static int kopo36_sumavg = 0;
	// 데이터 만들기
	public static void kopo36_dataSet() {
		for (int kopo36_i = 0; kopo36_i < kopo36_iPerson; kopo36_i++) { // 사람수만큼 실행
			String kopo36_name = String.format("홍길%02d", kopo36_i + 1); // 이름에 숫자를 붙여준다. 순서대로
			int kopo36_kor = (int) (Math.random() * 100); // 숫자를 random함수로 불러온다. 곱하기 100을 하여 0~100까지 받는다
			int kopo36_eng = (int) (Math.random() * 100); // 숫자를 random함수로 불러온다. 곱하기 100을 하여 0~100까지 받는다
			int kopo36_mat = (int) (Math.random() * 100); // 숫자를 random함수로 불러온다. 곱하기 100을 하여 0~100까지 받는다
			kopo36_ArrayOneRec // list에 add로 얻은 값을 기반으로 생성자를 넣어준다.
					.add(new kopo36_OneRec_forList(kopo36_i + 1, kopo36_name, kopo36_kor, kopo36_eng, kopo36_mat));
		}
	}
	// 헤더 인쇄
	public static void kopo36_HeaderPrint(int kopo36_page) {
		// 페이지에 찍힐 현재 날짜와 시간의 포맷을 정해주는 SimpleDateFormat과
		// 날짜를 불러올 Calendar
		SimpleDateFormat kopo36_sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		Calendar kopo36_c = Calendar.getInstance();
		// 페이지수를 표시하려면 결국 누적하는방법이 좋다.
		System.out.printf("%28s\n\n", "성적집계표");
		if (0 < kopo36_page && kopo36_page <= 9) { // 페이지 수에 따라 칸이 밀리기 때문에 칸조절을 했다.
			System.out.printf("PAGE : %d%25s%s\n", kopo36_page, "출력일자 : ", kopo36_sdf.format(kopo36_c.getTime()));
			System.out.printf("========================================================\n");
			System.out.printf("%2s%5s%9s%6s%6s%6s%8s\n", "번호", "이름", "국어", "영어", "수학", "합계", "평균");
			System.out.printf("========================================================\n");
		} else if (9 < kopo36_page && kopo36_page <= 99) {
			System.out.printf("PAGE : %d%24s%s\n", kopo36_page, "출력일자 : ", kopo36_sdf.format(kopo36_c.getTime()));
			System.out.printf("========================================================\n");
			System.out.printf("%2s%5s%9s%6s%6s%6s%8s\n", "번호", "이름", "국어", "영어", "수학", "합계", "평균");
			System.out.printf("========================================================\n");
		} else if (99 < kopo36_page && kopo36_page <= 999) {
			System.out.printf("PAGE : %d%23s%s\n", kopo36_page, "출력일자 : ", kopo36_sdf.format(kopo36_c.getTime()));
			System.out.printf("========================================================\n");
			System.out.printf("%2s%5s%9s%6s%6s%6s%8s\n", "번호", "이름", "국어", "영어", "수학", "합계", "평균");
			System.out.printf("========================================================\n");
		}
	}
	// 내용 인쇄
	public static void kopo36_itemPrint(int kopo36_i) {
		kopo36_OneRec_forList kopo36_OF; // 출력할 생성자.

		kopo36_OF = kopo36_ArrayOneRec.get(kopo36_i); // 생성자를 정수 kopo36_i를 get에 넣어 몇번째 놈을 넣을지 정한다.

		// 이름의 길이가 5면, 즉 100명 이상일때 칸수와 아닐때의 칸수를 나눴다. 이름을 왼쪽으로 정렬하여 이쁘게 보이기 위해
		if (kopo36_OF.kopo36_name().length() == 5) {
			System.out.printf("%03d%9s%8d%8d%8d%8d%10.2f\n", kopo36_OF.kopo36_student_id(), kopo36_OF.kopo36_name(),
					kopo36_OF.kopo36_kor(), kopo36_OF.kopo36_eng(), kopo36_OF.kopo36_mat(), kopo36_OF.kopo36_sum(),
					(double) (kopo36_OF.kopo36_avg()));
		} else if (kopo36_OF.kopo36_name().length() < 5) {
			System.out.printf("%03d%8s%9d%8d%8d%8d%10.2f\n", kopo36_OF.kopo36_student_id(), kopo36_OF.kopo36_name(),
					kopo36_OF.kopo36_kor(), kopo36_OF.kopo36_eng(), kopo36_OF.kopo36_mat(), kopo36_OF.kopo36_sum(),
					(double) (kopo36_OF.kopo36_avg()));
		} else {
			System.out.printf("%03d%9s%7d%8d%8d%8d%10.2f\n", kopo36_OF.kopo36_student_id(), kopo36_OF.kopo36_name(),
					kopo36_OF.kopo36_kor(), kopo36_OF.kopo36_eng(), kopo36_OF.kopo36_mat(), kopo36_OF.kopo36_sum(),
					(double) (kopo36_OF.kopo36_avg()));
		}
		// 현재페이지의 합계이다. 한번 사용되고 다시 0으로 초기화 할것이기 때문에 누적을 하지 않는다.
		kopo36_nowkor = kopo36_OF.kopo36_kor(); // 현재페이지에 출력할 국어점수 합계
		kopo36_noweng = kopo36_OF.kopo36_eng(); // 현재페이지에 출력할 영어점수 합계
		kopo36_nowmat = kopo36_OF.kopo36_mat(); // 현재페이지에 출력할 수학점수 합계
		kopo36_nowsum = kopo36_OF.kopo36_sum(); // 현재페이지에 출력할 함계의 합계
		kopo36_nowavg = kopo36_OF.kopo36_avg(); // 현재페이지에 출력할 평균의 합계

		// 누적페이지의 합계이다. 계속해서 누적해야하기 때문에 +=을 사용했다
		kopo36_sumkor += kopo36_OF.kopo36_kor(); // 가져온 값들을 누적시킨다.
		kopo36_sumeng += kopo36_OF.kopo36_eng(); // 가져온 값들을 누적시킨다.
		kopo36_summat += kopo36_OF.kopo36_mat(); // 가져온 값들을 누적시킨다.
		kopo36_sumsum += kopo36_OF.kopo36_sum(); // 가져온 값들을 누적시킨다.
		kopo36_sumavg += kopo36_OF.kopo36_avg(); // 가져온 값들을 누적시킨다.
	}

	// 꼬리 인쇄
	public static void kopo36_TailPrint() {
		System.out.printf("국 어 합 계   %5d   국어평균 : %6.2f\n", kopo36_sumkor,
				kopo36_sumkor / (double) kopo36_ArrayOneRec.size());
		System.out.printf("영 어 합 계   %5d   영어평균 : %6.2f\n", kopo36_sumeng,
				kopo36_sumeng / (double) kopo36_ArrayOneRec.size());
		System.out.printf("수 학 합 계   %5d   수학평균 : %6.2f\n", kopo36_summat,
				kopo36_summat / (double) kopo36_ArrayOneRec.size());
		System.out.printf("========================================================\n");
		System.out.printf("반평균 합계   %5d   반 평 균 : %6.2f\n", kopo36_sumavg,
				kopo36_sumavg / (double) kopo36_ArrayOneRec.size());
	} 
 
	// 누적페이지 double형의 변수를 받아서 그것으로 나눌 것이다.
	public static void kopo36_sumPage(double kopo36_avg) {
									// 100
		System.out.printf("========================================================\n");
		System.out.printf("누적페이지\n");

		System.out.printf("합      계%12d%8d%8d%8d%10.2f\n", kopo36_sumkor,
				kopo36_sumeng,
				kopo36_summat,
				kopo36_sumkor + kopo36_sumeng + kopo36_summat, (kopo36_sumkor + kopo36_sumeng + kopo36_summat) / 3.0);

		// 누적된 값들의 평균을 나타내야 하기 때문에 나누는 값이 30씩 증가 해야한다.
		// 그렇다고 정수형 변수로 나누면 정수이기 때문에 받아온 double형으로 나눠야 소수점을 표시할 수 있는 실수형으로 나온다.
		System.out.printf("평      균%12.2f%8.2f%8.2f%8.2f%10.2f\n", kopo36_sumkor / kopo36_avg, // 100
				kopo36_sumeng / kopo36_avg, // 100
				kopo36_summat / kopo36_avg, // 100
				((kopo36_sumkor + kopo36_sumeng + kopo36_summat) / kopo36_avg), // 누적 합계에서 여태까지 나온 사람의 수를 나눠줘야지..
				((kopo36_sumkor + kopo36_sumeng + kopo36_summat) / 3.0) / kopo36_avg); // 
		System.out.printf("========================================================\n");
	}

	// 정렬
	public static void kopo36_dataSort() {
		// collections.sort를 사용하기 위해서는 인자값으로 List<>와 Comparator<>를 받는다.
		// 생성자가 들어간 List에서 두 요소 a2와 a1을 불러와 합계를 서로 빼준다. 
		// return값으로 음수가 나오면 a1의 합계가 큰것이므로 뒤로 밀려난다. 즉 음수면 뒤로 밀려난다
		// 이러한 방식으로 여러차례 돌아가면서 정렬을 해주는 것 같다.
		Comparator<kopo36_OneRec_forList> kopo36_BasicSortLine = new Comparator<kopo36_OneRec_forList>() {
			public int compare(kopo36_OneRec_forList kopo36_a1, kopo36_OneRec_forList kopo36_a2) {
				return (kopo36_a2.kopo36_sum() - kopo36_a1.kopo36_sum()); //
			}
		};
		Collections.sort(Programming_7_10_class.kopo36_ArrayOneRec, kopo36_BasicSortLine);
	}
}

public class Programming_7_10_PageHongGil {
	public static void main(String[] args) {
		// 변수 i가 한번 실행될때마다 30씩 증가하여 사람 수까지 실행하는 for문을 돌린다.
		// 앞서 했던 구구단 줄마다 3씩 더하여 만든것과 비슷한 원리
		// i는 0, 30, 60, 90... 가게 되고, 내부에 있는 for문도 i와 시작하는 수가 같아야 모든 학생들을 출력할 수 있다.
		// j는 0~29, 30~59, 60~89 이렇게 올라갈 것이다.
		int kopo36_pagenum = 1;
		for (int kopo36_i = 0; kopo36_i < Programming_7_10_class.kopo36_iPerson; kopo36_i = kopo36_i
				+ Programming_7_10_class.kopo36_itemnum) {
			int kopo36_nowkor = 0; // 현재페이지 국어점수의 합계
			int kopo36_noweng = 0; // 현재페이지 영어점수의 합계
			int kopo36_nowmat = 0; // 현재페이지 수학점수의 합계
			int kopo36_nowsum = 0; // 현재페이지 합계의 합계
			double kopo36_nowavg = 0.0; // 현재페이지 평균의 합계
			// for문이 시작하고 바로 Page변수를 +1해준다. for문이 끝나면 page는 숫자가 누적되어 페이지를 알려줄 것이다.
			Programming_7_10_class.kopo36_dataSet(); // 데이터 만들기
//			Programming_7_10_class.kopo36_dataSort(); // 합계로 정렬
			Programming_7_10_class.kopo36_HeaderPrint(kopo36_pagenum); // 헤더 출력
			kopo36_pagenum++;
			// for문이 i부터 시작하게 된다. 그러면 0, 30, 60, ...에서 시작
			// i + 29까지 실행되게 하여 총 30개의 데이터를 순차적으로 출력할 수 있다.

			for (int kopo36_j = kopo36_i; kopo36_j < kopo36_i + Programming_7_10_class.kopo36_itemnum; kopo36_j++) {
				// 변수 j가 사람수와 같아지면 더이상 출력할 것이 없기 때문에 break로 for문을 끝낸다
				if (kopo36_j == Programming_7_10_class.kopo36_iPerson) {
					break;
				} else {
					// 그게 아니면 item들을 출력한다.
					Programming_7_10_class.kopo36_itemPrint(kopo36_j);
				}
				// for문이 한번 돌때마다 합계가 누적된다. 내부 for문에서 누적을 시킨다.
				kopo36_nowkor = kopo36_nowkor + Programming_7_10_class.kopo36_nowkor;
				kopo36_noweng = kopo36_noweng + Programming_7_10_class.kopo36_noweng;
				kopo36_nowmat = kopo36_nowmat + Programming_7_10_class.kopo36_nowmat;
				kopo36_nowsum = kopo36_nowsum + Programming_7_10_class.kopo36_nowsum;
				kopo36_nowavg = kopo36_nowavg + Programming_7_10_class.kopo36_nowavg;
			}
			// 누적시킨 현재페이지 합계들을 사용한다. 사용하고나서 다시 for문이 돌때 0으로 초기화 시킨다.
			System.out.printf("========================================================\n");
			System.out.printf("현재페이지\n");
			// i + itemnum이 사람수보다 작거나 같으면 itemnum으로 나눈다. itemnum = 한페이지에 출력하고 싶은 사람 수
			if (kopo36_i + Programming_7_10_class.kopo36_itemnum <= Programming_7_10_class.kopo36_iPerson) {
				System.out.printf("합      계%12d%8d%8d%8d%10.2f\n", kopo36_nowkor, kopo36_noweng, kopo36_nowmat, // 각각의
																												// 점수
						kopo36_nowkor + kopo36_noweng + kopo36_nowmat, // 합계
						((double)(kopo36_nowkor + kopo36_noweng + kopo36_nowmat) / 3.0)); // 평균
				System.out.printf("평      균%12.2f%8.2f%8.2f%8.2f%10.2f\n",
						kopo36_nowkor / (double) Programming_7_10_class.kopo36_itemnum,
						kopo36_noweng / (double) Programming_7_10_class.kopo36_itemnum,
						kopo36_nowmat / (double) Programming_7_10_class.kopo36_itemnum,
						(kopo36_nowkor + kopo36_noweng + kopo36_nowmat)
								/ (double) Programming_7_10_class.kopo36_itemnum,
						((double)(kopo36_nowkor + kopo36_noweng + kopo36_nowmat) / Programming_7_10_class.kopo36_itemnum) / 3.0);
			} else {
				// i + itemnum이 사람수보다 클때 사람수에서 i를 뺀값을 나눈다.
				// 예를 들면 i가 240이고 사람수가 250이면 250 - 240 = 10.
				// 현재 페이지에 있는 사람들만의 평균을 구하는 것이기 때문에 10을 나눠야한다.
				int kopo36_division = Programming_7_10_class.kopo36_iPerson - kopo36_i;
				System.out.printf("합      계%12d%8d%8d%8d%10.2f\n", kopo36_nowkor, kopo36_noweng, kopo36_nowmat, // 각각의
																												// 점수
						kopo36_nowkor + kopo36_noweng + kopo36_nowmat, // 합계
						(kopo36_nowkor + kopo36_noweng + kopo36_nowmat) / 3.0); // 평균
				System.out.printf("평      균%12.2f%8.2f%8.2f%8.2f%10.2f\n",
						kopo36_nowkor / (double) kopo36_division,
						kopo36_noweng / (double) kopo36_division,
						kopo36_nowmat / (double) kopo36_division,
						(kopo36_nowkor + kopo36_noweng + kopo36_nowmat) / (double) kopo36_division,
						((kopo36_nowkor + kopo36_noweng + kopo36_nowmat) / kopo36_division / 3.0));
			}
			// 누적페이지 출력문
			// 합계도 누적되지만, 사람수도 누적되기 때문에 계속해서 변하는 사람 수 만큼 나눠줘야한다.
			// 그래서 한페이지에 30명씩 나오기 때문에 30씩 더하다가, 30을 더했을때 사람 수보다 많으면 사람수에 맞게 나누게 했다
			if (kopo36_i + Programming_7_10_class.kopo36_itemnum <= Programming_7_10_class.kopo36_iPerson) {
				Programming_7_10_class.kopo36_sumPage((double) kopo36_i + Programming_7_10_class.kopo36_itemnum);
			} else {
				Programming_7_10_class
						.kopo36_sumPage((double) kopo36_i + (Programming_7_10_class.kopo36_iPerson - kopo36_i));
			}
			System.out.printf("\n\n\n");
		}
	}
}

class kopo36_OneRec_forList {

	// 학생의 id, 이름, 과목 점수를 선언
	private int kopo36_student_id;
	private String kopo36_name;
	private int kopo36_kor;
	private int kopo36_eng;
	private int kopo36_mat;

	// 생성자. 나중에 인스턴스화할때 값을 넣어주면 그 값을 가진 인스턴스가 된다
	public kopo36_OneRec_forList(int kopo36_student_id, String kopo36_name, 
			int kor, int eng, int mat) {
		this.kopo36_student_id = kopo36_student_id;
		this.kopo36_name = kopo36_name;
		this.kopo36_kor = kor;
		this.kopo36_eng = eng;
		this.kopo36_mat = mat;
	}

	// getter
	public int kopo36_student_id() {
		return this.kopo36_student_id;
	}

	// getter
	public String kopo36_name() {
		return this.kopo36_name;
	}

	// getter
	public int kopo36_kor() {
		return this.kopo36_kor;
	}

	// getter
	public int kopo36_eng() {
		return this.kopo36_eng;
	}

	// getter
	public int kopo36_mat() {
		return this.kopo36_mat;
	}

	// 합계메소드. 각 점수를 더한 값
	public int kopo36_sum() {
		return this.kopo36_kor + this.kopo36_eng + this.kopo36_mat;
	}

	// 평균메소드. 합계메소드를 3.0.으로 나눠서 소수점까지 표시
	public double kopo36_avg() {
		return (double)(this.kopo36_sum()) / 3.0;
	}
}
