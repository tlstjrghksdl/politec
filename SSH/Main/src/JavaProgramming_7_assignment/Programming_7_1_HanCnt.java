package JavaProgramming_7_assignment;

import java.io.UnsupportedEncodingException;

public class Programming_7_1_HanCnt {
	
	// 한글 개수를 구하는 메소드
	static int kopo36_HanCount(String kopo36_a) {
		// 바이트 카운트
		int kopo36_byteCnt = 0;
		
		// 한글 카운트
		int kopo36_korCnt = 0;
		
		// 받은 받아온 kopo36_a를 한글자씩 잘라서 배열에 넣는다.
		String[] kopo36_arr = kopo36_a.split("");
		try {
			
			// for문을 받아온 String값의 길이만큼 실행시킨다.
			for (int kopo36_i = 0; kopo36_i < kopo36_a.length(); kopo36_i++) {
				
				// arr배열의 요소에 한글이 들어가 있으면 바이트 카운트를 2 증가시킨다
				if (kopo36_arr[kopo36_i].getBytes("EUC-KR").length == 2) {
					kopo36_byteCnt = kopo36_byteCnt + 2;
				} else {
					// 한글이 아니면 1 증가시킨다.
					kopo36_byteCnt++;
				}
			}
			// 바이트 카운트에서 받아온 String값의 길이를 빼면 한글의 개수이다.
			kopo36_korCnt = kopo36_byteCnt - kopo36_a.length();

		} catch (Exception e) {
			// TODO: handle exception
		}
		
		// return할 한글개수
		return kopo36_korCnt;
	}

	
	// 앞에 붙일 공백 메소드
	static String kopo36_HanBlankForeword(String kopo36_a, int kopo36_b) {
		// 바이트 카운트
		int kopo36_byteCnt = 0;
		
		// 한글 카운트
		int kopo36_korCnt = 0;
		
		// 공백 개수
		int kopo36_blank = 0;
		
		// 추가할 공백을 누적시키는 String
		String kopo36_addBlank = "";
		
		//받아온 a를 한글자씩 자른다.
		String[] kopo36_arr = kopo36_a.split("");
		
		// return할 값
		String kopo36_ret = "";
		
		// for문을 a의 길이만큼 실행
		for (int kopo36_i = 0; kopo36_i < kopo36_a.length(); kopo36_i++) {
			try {
				
				// 배열의 요소에 한글이 들어있으면 2증가, 아니면 1증가
				if (kopo36_arr[kopo36_i].getBytes("EUC-KR").length == 2) {
					kopo36_byteCnt = kopo36_byteCnt + 2;
				} else {
					kopo36_byteCnt++;
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		// 한글 카운트는 바이트 - 문자열 길이
		kopo36_korCnt = kopo36_byteCnt - kopo36_a.length();
		
		// 블랭크 개수는 정수 b에서 바이트 카운트를 뺀다.
		kopo36_blank = kopo36_b - kopo36_byteCnt;
		
		// 블랭크 개수만큼 for문을 돌린다.
		for (int kopo36_i = 0; kopo36_i < kopo36_blank; kopo36_i++) {
			// 돌리면서 추가할 블랭크를 누적시킨다.
			kopo36_addBlank = kopo36_addBlank + " ";
		}
		
		//리턴값은 블랭크를 앞에 붙인 문자열 a이다.
		kopo36_ret = kopo36_addBlank + kopo36_a;

		return kopo36_ret;
	}
	
	// 뒤에 붙일 공백 메소드.
	// 앞에 붙일 공백 메소드랑 똑같고 return값에 넣어줄 공백을 뒤에 붙여주면 끝!
	static String kopo36_HanBlankBackword(String kopo36_a, int kopo36_b) {
		// 바이트 카운트
		int kopo36_byteCnt = 0;
		// 한글 카운트
		int kopo36_korCnt = 0;
		// 공백 개수
		int kopo36_blank = 0;
		String kopo36_addBlank = "";
		String[] kopo36_Byte = kopo36_a.split("");
		String kopo36_ret = "";
		
		for (int kopo36_i = 0; kopo36_i < kopo36_a.length(); kopo36_i++) {
			try {
				if (kopo36_Byte[kopo36_i].getBytes("EUC-KR").length == 2) {
					kopo36_byteCnt = kopo36_byteCnt + 2;
				} else {
					kopo36_byteCnt++;
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		kopo36_korCnt = kopo36_byteCnt - kopo36_a.length();
		// 블랭크 개수는 정수 b에서 바이트 카운트를 뺀다.
		kopo36_blank = kopo36_b - kopo36_byteCnt;

		for (int kopo36_i = 0; kopo36_i < kopo36_blank; kopo36_i++) {
			// 돌리면서 추가할 블랭크를 누적시킨다.
			kopo36_addBlank = kopo36_addBlank + " ";
		}
		kopo36_ret = kopo36_a + kopo36_addBlank;

		return kopo36_ret;
	}

	public static void main(String[] args) throws UnsupportedEncodingException {
		System.out.printf("HanBlankForeword[%s]\n", kopo36_HanBlankForeword("한글asdf", 15));   //만든 메소드를 사용
		System.out.printf("HanBlankForeword[%s]\n", kopo36_HanBlankForeword("한글한글aa", 15)); //만든 메소드를 사용
		System.out.printf("HanBlankForeword[%s]\n", kopo36_HanBlankBackword("한글aa", 15));     //만든 메소드를 사용
		System.out.printf("HanBlankForeword[%s]\n", kopo36_HanBlankBackword("한글한글aa", 15)); //만든 메소드를 사용
		System.out.printf("한글은 [%d]개\n", kopo36_HanCount("한글한글aa")); //만든 메소드를 사용
	}
}
