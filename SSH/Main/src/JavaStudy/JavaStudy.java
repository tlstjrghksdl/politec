package JavaStudy;

import java.util.Random;

public class JavaStudy {
	public static void main(String[] args) {
		double x = new Random().nextDouble();
		System.out.println(x);
	}
	
	public static int[] makeArray(int size) {
		int[] array = new int[size];
		for (int i = 0; i < array.length; i++) {
			array[i] = i;
		}
		return array;
	}  
}
 