package seokhwam;

public class tests {
	private String letters;

	
	public boolean isVowel(int i) {
		String[] a = letters.split("");
		if(
				a[i].equals("a") || a[i].equals("e") || a[i].equals("i") || a[i].equals("o") || a[i].equals("u")
				) {
			return true;
		} else {
			return false;
		}
	}
	
	
	
	
	public boolean isConsonant(int i) {
		return  !"aeiouAEIOU".contains(letters.substring(i, i + 1));
	}
	

	public String getLetters() {
		return letters;
	}

	public void setLetters(String letters) {
		this.letters = letters;
	}
}
