package JavaProgramming_6_Overload_Score;

public class Programming_6_5_Score {
	public static void main(String[] args) {
		
		// 학생의 이름 문자열로 선언 후 초기화
		String kopo36_name;
		kopo36_name = "폴리텍";
		
		int kopo36_kor = 100; // 국어 성적을 선언과 초기화
		int kopo36_eng = 100; // 영어 성적을 선언과 초기화
		int kopo36_mat = 100; // 수학 성적을 선언과 초기화
		int kopo36_sci = 100; // 과학 성적을 선언과 초기화
		int kopo36_soc = 100; // 사회 성적을 선언과 초기화
		
		// 성적의 합계와 평균이 있는 클래스를 인스턴스화
		scoreCalc kopo36_SC = new scoreCalc();
		
		// 3월달 성적의 종합
		int kopo36_sum3 = kopo36_SC.kopo36_sum(kopo36_kor, kopo36_eng, kopo36_mat);
		
		// 4월달 성적의 종합
		int kopo36_sum4 = kopo36_SC.kopo36_sum(kopo36_kor, kopo36_eng, kopo36_mat, kopo36_sci);
		
		// 5월달 성적의 종합
		int kopo36_sum5 = kopo36_SC.kopo36_sum(kopo36_kor, kopo36_eng, kopo36_mat, kopo36_sci, kopo36_soc);
		
		// 3월달 성적의 평균
		int kopo36_avg3 = kopo36_SC.kopo36_avg(kopo36_kor, kopo36_eng, kopo36_mat);
		
		// 4월달 성적의 평균
		double kopo36_avg4 = kopo36_SC.kopo36_avg(kopo36_kor, kopo36_eng, kopo36_mat, kopo36_sci);
		
		// 5월달 성적의 평균
		double kopo36_avg5 = kopo36_SC.kopo36_avg(kopo36_kor, kopo36_eng, kopo36_mat, kopo36_sci, kopo36_soc);
		 
		
		// 3, 4, 5월달의 성적별 점수와 총점, 평균을 출력
		System.out.println("3월 성적표");
		System.out.println("============================================================");
		System.out.printf("%2s%6s%6s%6s%6s%6s\n", "이름", "국어", "영어", "수학", "총점", "평균");
		System.out.println("============================================================");
		System.out.printf("%-6s%3d%8d%8d%8d%8d\n", kopo36_name, kopo36_kor, kopo36_eng,
				kopo36_mat, kopo36_sum3, kopo36_avg3);
		System.out.printf("\n\n");
		
		System.out.println("4월 성적표");
		System.out.println("============================================================");
		System.out.printf("%2s%6s%6s%6s%6s%6s%6s\n", "이름", "국어", "영어", "수학", "과학", "총점", "평균");
		System.out.println("============================================================");
		System.out.printf("%-6s%3d%8d%8d%8d%8d%8.1f\n", kopo36_name, kopo36_kor, kopo36_eng,
				kopo36_mat, kopo36_sci, kopo36_sum4, kopo36_avg4);
		System.out.printf("\n\n");
		
		System.out.println("5월 성적표");
		System.out.println("============================================================");
		System.out.printf("%2s%6s%6s%6s%6s%6s%6s%6s\n", "이름", "국어", "영어", "수학", "과학", "사회", "총점", "평균");
		System.out.println("============================================================");
		System.out.printf("%-6s%3d%8d%8d%8d%8d%8d%8.1f\n", kopo36_name, kopo36_kor, kopo36_eng,
				kopo36_mat, kopo36_sci, kopo36_soc, kopo36_sum5, kopo36_avg5);
	
	}
}
