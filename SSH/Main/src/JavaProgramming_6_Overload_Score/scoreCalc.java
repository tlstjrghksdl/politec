package JavaProgramming_6_Overload_Score;

public class scoreCalc {

	// 세 개의 int형을 더하는 메소드
	public int kopo36_sum(int kopo36_a, int kopo36_b, int kopo36_c) {
		return kopo36_a + kopo36_b + kopo36_c;
	}

	// 네 개의 int형을 더하는 메소드
	public int kopo36_sum(int kopo36_a, int kopo36_b, int kopo36_c, int kopo36_d) {
		return kopo36_a + kopo36_b + kopo36_c + kopo36_d;
	}

	// 다섯 개의 int형을 더하는 메소드
	public int kopo36_sum(int kopo36_a, int kopo36_b, int kopo36_c, int kopo36_d, int kopo36_e) {
		return kopo36_a + kopo36_b + kopo36_c + kopo36_d + kopo36_e;
	}

	// 세 개의 int형의 평균을 구하는 메소드
	public int kopo36_avg(int kopo36_a, int kopo36_b, int kopo36_c) {
		return (kopo36_a + kopo36_b + kopo36_c) / 3;
	}

	// 네 개의 int형을 더하여 평균을 구하는 반환 값이 double인 메소드
	public double kopo36_avg(int kopo36_a, int kopo36_b, int kopo36_c, int kopo36_d) {
		return (kopo36_a + kopo36_b + kopo36_c + kopo36_d) / 4;
	}

	// 다섯 개의 int형을 더하여 평균을 구하는 반환 값이 double인 메소드
	public double kopo36_avg(int kopo36_a, int kopo36_b, int kopo36_c, int kopo36_d, int kopo36_e) {
		return (kopo36_a + kopo36_b + kopo36_c + kopo36_d + kopo36_e) / 5;
	}

	/*
	 * 메소드 오버라이딩 : 메소드 명이 같아서 못쓸거 같지만, 인자 값의 갯수를 다르게 하거나 변수의 형태를 다르게 하면 가능하다. 위와같이
	 * 메소드 이름은 같지만 들어가는 인자 값이 3개, 4개, 5개이다.
	 */
}
 