package StudyingJava_9;

public class Main {
	public static void main(String[] args) {
		String s = "abc.def:ghi,jkl";
		String[] words = s.split("[.:,]");
		
		for (String word : words) {
			System.out.print(word + "->");
		}
	}
}
