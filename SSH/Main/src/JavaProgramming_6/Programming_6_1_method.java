package JavaProgramming_6;

public class Programming_6_1_method {
	
	// 怨듦났�옄�썝�씤 static int蹂��닔瑜� �꽑�뼵
	static int kopo36_iStatic;
	
	// 異붽� 硫붿냼�뱶
	public static void kopo36_add(int kopo36_i) {
		kopo36_iStatic++; // ++�� 1 �뜑�븯�뒗 寃�
		kopo36_i++; // ++�� 1 �뜑�븯�뒗 寃�
		System.out.printf("add硫붿냼�뱶�뿉�꽌->kopo36_iStatic=[%d]\n", kopo36_iStatic);
		System.out.printf("add硫붿냼�뱶�뿉�꽌->kopo36_i=[%d]\n", kopo36_i);
	}
	
	// 異붽� 硫붿냼�뱶2
	public static int kopo36_add2(int kopo36_i) {
		kopo36_iStatic++; // ++�� 1 �뜑�븯�뒗 寃�
		kopo36_i++; // ++�� 1 �뜑�븯�뒗 寃�
		System.out.printf("add硫붿냼�뱶�뿉�꽌 -> kopo36_iStatic=[%d]\n", kopo36_iStatic);
		System.out.printf("add硫붿냼�뱶�뿉�꽌 ->    kopo36_i   =[%d]\n", kopo36_i);
		return kopo36_i; // return(諛섑솚)�븷 int媛�.
		// 硫붿냼�뱶 諛섑솚媛믪쓣 �뼱�뼡 蹂��닔濡� �젙�븯�뒓�깘�뿉 �뵲�씪 諛섑솚媛믪쓣 �젙�빐以섏빞�븳�떎.
	}
	
	
	// 硫붿씤 硫붿냼�뱶
	public static void main(String[] args) {
		
		int kopo36_iMain;
		
		// �쟾�뿭蹂��닔 iMain�쓣 珥덇린�솕�빐以��떎.
		kopo36_iMain = 1;
		// 怨듦났�옄�썝�씤 iStatic�쓣 珥덇린�솕�빐以��떎.
		kopo36_iStatic = 1;
		
		
		System.out.printf("*********************************************\n");
		System.out.printf("硫붿냼�뱶�샇異쒖쟾 硫붿씤�뿉�꽌 -> kopo36_iStatic = [%d]\n", kopo36_iStatic);
		System.out.printf("硫붿냼�뱶�샇異쒖쟾 硫붿씤�뿉�꽌 -> kopo36_iMain   = [%d]\n", kopo36_iMain);
		System.out.printf("*********************************************\n");
		
		// 硫붿냼�뱶1 �샇異�
		kopo36_add(kopo36_iMain); // 硫붿냼�뱶瑜� �넻�빐 媛� int蹂��닔瑜� ++ �빐以��떎
		
		
		System.out.printf("********************************************************\n");
		System.out.printf("硫붿냼�뱶 kopo36_add�샇異쒗썑 硫붿씤�뿉�꽌 -> kopo36_iStatic = [%d]\n", kopo36_iStatic);
		System.out.printf("硫붿냼�뱶 kopo36_add�샇異쒗썑 硫붿씤�뿉�꽌 -> kopo36_iMain   = [%d]\n", kopo36_iMain);
		System.out.printf("********************************************************\n");
		
		// 硫붿냼�뱶2 �샇異�
		kopo36_iMain = kopo36_add2(kopo36_iMain); // 硫붿냼�뱶�뿉�꽌 諛섑솚�맂 int媛믪쓣 諛쏅뒗�떎.
		// 硫붿냼�뱶�뿉�꽌 諛섑솚�릺�뒗 int媛믪쓣 iMain�뿉 �쟻�슜�떆耳� iMain蹂��닔�쓽 媛믪쓣 諛붽퓭以��떎.
		
		System.out.printf("*********************************************************\n");
		System.out.printf("硫붿냼�뱶 kopo36_add2�샇異쒗썑 硫붿씤�뿉�꽌 -> kopo36_iStatic = [%d]\n", kopo36_iStatic);
		System.out.printf("硫붿냼�뱶 kopo36_add2�샇異쒗썑 硫붿씤�뿉�꽌 -> kopo36_iMain   = [%d]\n", kopo36_iMain);
		System.out.printf("*********************************************************\n");
	}
}
