package JavaProgramming_6;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Programming_6_2_methodEmart {

	// 상품 갯수
	static int kopo36_itemCnt = 30;

	// 제품명. 제품을 30개로 잡아서 배열의 길이는 30 (0~29까지)
	static String[] kopo36_itemname = { "당신은", "나의", "동반자", "영원한 나의", "동반자 내 생애", "최고의 선물", "당신과 만남이었어", "잘 살고 못 사는 건",
			"타고난 팔자지만", "당신만을 사랑해요", "영원한 동반자여", "아~당신은", "못 믿을 사람", "아~당신은", "철없는사람", "아무리", "달래봐도", "어쩔 순 없지만",
			"마음 하나는", "괜찮은 사람", "오늘은 들국화", "또 내일은", "장미꽃", "치근~치근~치근", "대다가 잠이", "들겠지~", "난 이제", "지쳤어요", "땡벌~", "땡벌~" };

	// 단가
	static int[] kopo36_price = { 3780, 15007, 2980, 4950, 5980, 10848000, 13300000, 59800000, 29800000, 4702, 990,
			1440, 4250, 2750, 9800, 2980, 990, 10130, 4060, 3900, 12400, 5000, 5000, 9800, 9900, 39900, 5180, 520, 1010,
			1020 };

	// 수량
	static int[] kopo36_num = { 1, 2, 1, 5, 4, 2, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 3, 1, 2, 1, 1, 1, 2, 1, 2, 1, 1,
			1 };

	// 면세 유무
	static boolean[] kopo36_taxfree = { false, true, false, true, false, true, false, true, true, false, true, false,
			true, true, true, true, true, true, true, false, true, true, true, true, true, true, true, true, true,
			false };

	// 세율 : 10퍼센트
	static int kopo36_taxrate = 10;

	// 면세 물품의 가격합, 과세 물품의 가격합
	static int kopo36_sumTaxFreePrice = 0;
	static int kopo36_sumNoFreePrice = 0;

	// 세전가
	static int kopo36_beforPlusTax;

	// 총 부가세
	static int kopo36_taxPrice;
	static boolean kopo36_cnt = true; // boolean변수 count를 선언한다.

	// 사용할 Decimal, SimpleDateFormat, Calendar
	static DecimalFormat kopo36_df = new DecimalFormat("###,###,###,###"); // decimal포멧으로 가격이 될 숫자를 콤마 처리 해준다.
	static SimpleDateFormat kopo36_sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm"); // 출력해줄 날짜의 형식을 정해주는
																					// simpledateforamt을 인스턴스화한다
	static Calendar kopo36_c = Calendar.getInstance(); // 날짜를 받아와야하기 때문에 calendar설정을 해준다.

	// 세전가 구하기
	static int kopo36_FreeTaxPrice(int kopo36_sumPrice, int kopo36_taxrate) {
		int kopo36_ret; // 리턴할 정수형 변수 리턴변수를 선언한다.
		if (kopo36_sumPrice
				/ (1 + (kopo36_taxrate / 100.0)) == (double) (kopo36_sumPrice / (1 + (kopo36_taxrate / 100.0)))) {
			// 여태까지 많이 사용해 왔던 소수점 버리기이다.
			// if문으로 double형으로 형변환한 값과 아닌 값을 비교했을때 같으면 둘다 소수점이 없는 것이기 때문에 소수점을 올림처리할 필요가 없다
			kopo36_ret = (int) (kopo36_sumPrice / (1 + (kopo36_taxrate / 100.0)));
		} else {
			// 반면 double형으로 형변환한 계산값이 소수점을 가진다면 1을 소수점을 빼고 1을 올린다. (소수점올림)
			kopo36_ret = 1 + (int) (kopo36_sumPrice / (1 + (kopo36_taxrate / 100.0)));
		}
		return kopo36_ret; // int형 반환
	}

	// 비과세, 과세 물품 가격의 합
	static void kopo36_sumPriceEach() {
		for (int kopo36_i = 0; kopo36_i < kopo36_itemname.length; kopo36_i++) {
			if (kopo36_taxfree[kopo36_i] == true) { // taxxfree 배열에서 true값인 요소들을 검출하는 if문
				kopo36_sumTaxFreePrice = kopo36_sumTaxFreePrice + (kopo36_price[kopo36_i] * kopo36_num[kopo36_i]);
				// 정수형 변수인 비과세 물품 가격의 합에 값을 누적시키면서 합을 구한다.
			} else {
				kopo36_sumNoFreePrice = kopo36_sumNoFreePrice + (kopo36_price[kopo36_i] * kopo36_num[kopo36_i]);
				// 정수형 변수인 과세 물품 가격의 합에 값을 누적시키면서 합을 구한다.
			}
		}
	}

	// 과세 물품의 세전가
	static void kopo36_beforeAddTax() {
		kopo36_beforPlusTax = kopo36_FreeTaxPrice(kopo36_sumNoFreePrice, kopo36_taxrate);

		kopo36_taxPrice = kopo36_sumNoFreePrice - kopo36_beforPlusTax;
		// 비과세는 부가세가 붙지 않기 때문에 구할 필요 없다.
		// 부가세는 과세물품 총액에서 과세 전 총액을 빼면 부가세가 나온다.
	}

	// 14바이트에서 잘르는 메소드
	public static String kopo36_cutting(String kopo36_input) {
		// 누적시킬 문자열 변수 (나중에 리턴)
		String kopo36_ret = "";

		// 14바이트까지 누적시킬 int변수
		int kopo36_Cnt = 0;

		// 받아온 String변수를 한글자씩 잘라서 넣는다.
		String[] kopo36_arr = kopo36_input.split("");
		try { // 바이트 계산에 생기는 Exception을 잡아주는 try{} catch{}문

			// for문을 받아온 문자열의 길이만큼 실행시킨다.
			// 확인하면서 배열로 나눈 값을 다시 합쳐주는 작업을 한다.
			// 14바이트, 15바이트에서 for문이 끝나게 하고, 나온 값을 return값으로 해준다
			for (int kopo36_i = 0; kopo36_i < kopo36_input.length(); kopo36_i++) {

				// 잘라서 배열에 넣은 값이 몇 Byte인지 확인한다
				if (kopo36_arr[kopo36_i].getBytes("EUC-KR").length == 2) {
					kopo36_Cnt = kopo36_Cnt + 2; // 2 Byte이면 카운트를 2씩 올려준다.
					kopo36_ret = kopo36_ret + kopo36_arr[kopo36_i];
					// 검사가 완료되면 리턴시킬 String 변수에 누적시킨다.
				} else { // 1 Byte이면 카운트를 1씩 올려준다.
					kopo36_Cnt = kopo36_Cnt + 1;
					kopo36_ret = kopo36_ret + kopo36_arr[kopo36_i];
					// 마찬가지로 검사가 완료되면 리턴시킬 String변수에 누적시킨다.
				}

				// 몇 Byte인지 확인하는 if문이다. byte를 주적시킬때 13바이트에서 한글이 한글자 추가되면 15바이트가 되기때문에 확인을 해줘야한다.
				if (kopo36_Cnt == 14) {
					break; // 14바이트일때는 확인 할 필요 없다. 이미 끝
				} else if (kopo36_Cnt == 15) { // 15바이트일때는(13바이트에서 한글이 추가된 경우) if문으로 검출해서
					kopo36_ret = kopo36_ret.substring(0, kopo36_ret.length() - 1) + " ";
					// 마지막 한글자를 지워주면 13바이트가 된다. 14바이트를 맞추기위해 띄어쓰기 하나 넣는다.
					break; // 그리고 break
				} else {
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.getStackTrace();
		}
		return kopo36_ret;
	}
	
	
	// 바이트를 14로 잘라주고, 잘라준 것을 배열로 다시 초기화 해주는 메소드
	public static void kopo36_matchLength() {
		for (int kopo36_i = 0; kopo36_i < kopo36_itemname.length; kopo36_i++) {
			kopo36_itemname[kopo36_i] = kopo36_cutting(kopo36_itemname[kopo36_i]);
		}
	}

	// 머리말 메소드
	public static void kopo36_header() {

		System.out.printf("%34s\n", "이마트 죽전점(031)888-1234");
		System.out.printf("%9s%21s\n", "emart", "206-86-50913 이갑수");
		System.out.printf("%30s\n", "용인시 수지구 포은대로 552");
		System.out.printf("%s\n", "영수증 미지참시 교환/환불 불가(30일내)");
		System.out.printf("%s\n", "교환/환불 구매점에서 가능(결제카드지참)");
		System.out.printf("%s\n", "체크카드/신용카드 청구취소 반영은");
		System.out.printf("%s\n", "최대 3~5일 소요 (주말,공휴일제외)");
		System.out.printf("\n");
		System.out.printf("%4s%15s%10s%s\n", "[구매]", kopo36_sdf.format(kopo36_c.getTime()), "POS:", "0009-2418");
		// 출력 시간이 나와야하기 때문에 미리 만들어둔 SimpleDateFormat과 Calender로 현재 시간을 뽑아낸다.
	}

	
	// 타이틀 메소드
	public static void kopo36_title() {
		System.out.printf("-----------------------------------------\n");
		System.out.printf("%10s%10s%4s%8s\n", "상 품 명", "단  가", "수량", "금  액");
		System.out.printf("-----------------------------------------\n");
	}
	
	
	// 상품 가격 메소드
	public static void kopo36_mainPrint() {
		try {
			for (int kopo36_i = 0; kopo36_i < kopo36_itemCnt; kopo36_i++) {

				// 한글이 몇글자 있는지 나타내는 length
				int kopo36_length = 0;

				// Byte수가 8이고, String수가 6이면, 한글이 2글자 있는 것이다. 한글은 2Byte를 차지하고 영어, 빈칸은 1Byte를
				// 차지한다.
				kopo36_length = kopo36_itemname[kopo36_i].getBytes("EUC-KR").length // 몇바이트?
						- kopo36_itemname[kopo36_i].length(); // String몇?
				kopo36_cnt = kopo36_taxfree[kopo36_i]; // for문 안에서 taxfree배열의 요소가 순차적으로 변하면서 true혹은 false로 바뀌게 된다.

				// true인지 false인지 검출해서 별(*)을 붙일지 말지 정하는 if문
				if (kopo36_cnt == true) {
					// count변수가 true일 때는 물품의 순서에 *을 붙인다
					System.out.printf("%02d* ", kopo36_i + 1);
				} else {
					// 아닐경우에는 *을 붙이지 않는다.
					System.out.printf("%02d  ", kopo36_i + 1);
				}

				// 한글의 갯수마다 간격을 다르게 정해줘야 하기 때문에 갯수마다 다른 출력문을 넣어줬다.

				// 한글이 0개일때, 14바이트를 다 차지해도 상관없다.
				if (kopo36_length == 0) {
					// 상품명, 단가, 수령, 금액을 출력하는 printf문
					System.out.printf("%-14.14s%10.10s%3s%10.10s\n", kopo36_itemname[kopo36_i],
							kopo36_df.format(kopo36_price[kopo36_i]), kopo36_df.format(kopo36_num[kopo36_i]),
							kopo36_df.format(kopo36_price[kopo36_i] * kopo36_num[kopo36_i]));

					// 한글이 1개일때, 한글은 한자리에 2바이트를 차지하기 때문에 한칸씩 밀려나게된다. 그렇기 때문에 13을 넣어준다.
					// 이렇게 한글의 갯수가 7개일때까지 출력겂을 다르게 매겨줘서 잘 출력되게 한다.
				} else if (kopo36_length == 1) {
					// 상품명, 단가, 수령, 금액을 출력하는 printf문
					System.out.printf("%-13.13s%10.10s%3s%10.10s\n", kopo36_itemname[kopo36_i],
							kopo36_df.format(kopo36_price[kopo36_i]), kopo36_df.format(kopo36_num[kopo36_i]),
							kopo36_df.format(kopo36_price[kopo36_i] * kopo36_num[kopo36_i]));
					// 금액에는 콤마를 넣어줘야하기 때문에 Decimal포멧을 사용한다
				} else if (kopo36_length == 2) {
					// 상품명, 단가, 수령, 금액을 출력하는 printf문
					System.out.printf("%-12.12s%10.10s%3s%10.10s\n", kopo36_itemname[kopo36_i],
							kopo36_df.format(kopo36_price[kopo36_i]), kopo36_df.format(kopo36_num[kopo36_i]),
							kopo36_df.format(kopo36_price[kopo36_i] * kopo36_num[kopo36_i]));
					// 금액에는 콤마를 넣어줘야하기 때문에 Decimal포멧을 사용한다
				} else if (kopo36_length == 3) {
					// 상품명, 단가, 수령, 금액을 출력하는 printf문
					System.out.printf("%-11.11s%10.10s%3s%10.10s\n", kopo36_itemname[kopo36_i],
							kopo36_df.format(kopo36_price[kopo36_i]), kopo36_df.format(kopo36_num[kopo36_i]),
							kopo36_df.format(kopo36_price[kopo36_i] * kopo36_num[kopo36_i]));
					// 금액에는 콤마를 넣어줘야하기 때문에 Decimal포멧을 사용한다
				} else if (kopo36_length == 4) {
					// 상품명, 단가, 수령, 금액을 출력하는 printf문
					System.out.printf("%-10.10s%10.10s%3s%10.10s\n", kopo36_itemname[kopo36_i],
							kopo36_df.format(kopo36_price[kopo36_i]), kopo36_df.format(kopo36_num[kopo36_i]),
							kopo36_df.format(kopo36_price[kopo36_i] * kopo36_num[kopo36_i]));
					// 금액에는 콤마를 넣어줘야하기 때문에 Decimal포멧을 사용한다
				} else if (kopo36_length == 5) {
					// 상품명, 단가, 수령, 금액을 출력하는 printf문
					System.out.printf("%-9.9s%10.10s%3s%10.10s\n", kopo36_itemname[kopo36_i],
							kopo36_df.format(kopo36_price[kopo36_i]), kopo36_df.format(kopo36_num[kopo36_i]),
							kopo36_df.format(kopo36_price[kopo36_i] * kopo36_num[kopo36_i]));
					// 금액에는 콤마를 넣어줘야하기 때문에 Decimal포멧을 사용한다
				} else if (kopo36_length == 6) {
					// 상품명, 단가, 수령, 금액을 출력하는 printf문
					System.out.printf("%-8.8s%10.10s%3s%10.10s\n", kopo36_itemname[kopo36_i],
							kopo36_df.format(kopo36_price[kopo36_i]), kopo36_df.format(kopo36_num[kopo36_i]),
							kopo36_df.format(kopo36_price[kopo36_i] * kopo36_num[kopo36_i]));
					// 금액에는 콤마를 넣어줘야하기 때문에 Decimal포멧을 사용한다
				} else if (kopo36_length == 7) {
					// 상품명, 단가, 수령, 금액을 출력하는 printf문
					System.out.printf("%-8.8s%10.10s%3s%10.10s\n", kopo36_itemname[kopo36_i],
							kopo36_df.format(kopo36_price[kopo36_i]), kopo36_df.format(kopo36_num[kopo36_i]),
							kopo36_df.format(kopo36_price[kopo36_i] * kopo36_num[kopo36_i]));
					// 금액에는 콤마를 넣어줘야하기 때문에 Decimal포멧을 사용한다
				} else {
					// 7개 이상은 나올 수가 없다. 이미 위에 메소드를 통해 긴 문자를 다 잘라줬기 때문에..
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.getStackTrace();
		}
	}

	// 상품에 대한 총 합등 출력 메소드
	public static void kopo36_totalPrice() {
		System.out.printf("%20s%17s\n", "(*)면 세  물 품", kopo36_df.format(kopo36_sumTaxFreePrice));
		// 금액에는 콤마를 넣어줘야하기 때문에 Decimal포멧을 사용한다
		System.out.printf("%20s%17s\n", "과 세  물 품", kopo36_df.format(kopo36_beforPlusTax));
		// 금액에는 콤마를 넣어줘야하기 때문에 Decimal포멧을 사용한다
		System.out.printf("%21s%17s\n", "부   가   세", kopo36_df.format(kopo36_taxPrice));
		// 금액에는 콤마를 넣어줘야하기 때문에 Decimal포멧을 사용한다
		System.out.printf("%22s%17s\n", "합        계", kopo36_df.format(kopo36_sumNoFreePrice + kopo36_sumTaxFreePrice));
		// 금액에는 콤마를 넣어줘야하기 때문에 Decimal포멧을 사용한다
		System.out.printf("%10s%24s\n", "결 제 대 상 금 액",
				kopo36_df.format(kopo36_sumNoFreePrice + kopo36_sumTaxFreePrice));
		// 금액에는 콤마를 넣어줘야하기 때문에 Decimal포멧을 사용한다
	}

	
	// 지불 방법 출력 메소드
	public static void kopo36_howToPay() {
		System.out.printf("-----------------------------------------\n");
		// 금액에는 콤마를 넣어줘야하기 때문에 Decimal포멧을 사용한다
		System.out.printf("%6s%30s\n", "0024 하  나", "5417%%8890/07850246");
		// 금액에는 콤마를 넣어줘야하기 때문에 Decimal포멧을 사용한다
		System.out.printf("%-13s%21s\n", "카드결제",
				"일시불 / " + kopo36_df.format(kopo36_sumNoFreePrice + kopo36_sumTaxFreePrice));
		// 금액에는 콤마를 넣어줘야하기 때문에 Decimal포멧을 사용한다
		System.out.printf("-----------------------------------------\n");
		System.out.printf("%s %s\n", "신*환", "고객님의 포인트 현황입니다.");
	}

	public static void main(String[] args) {
		kopo36_matchLength(); // 길이를 맞춰주는 메소드 실행
		kopo36_sumPriceEach(); // 세전가, 부과세 미포함 상품들 각각의 합
		kopo36_beforeAddTax(); // 과세물품의 세전가
		kopo36_header();
		kopo36_title();
		kopo36_mainPrint();
		kopo36_totalPrice();
		kopo36_howToPay(); 
	}
}
