package Chulsuk;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;

public class chulsuk {
	static Scanner sc = new Scanner(System.in);
	static informChulsuk IC = new informChulsuk();
	static viewForTeacher AD = new viewForTeacher();
	static DateFormat DF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	static Calendar cal = Calendar.getInstance();
	static LocalDateTime LDT = LocalDateTime.now();
	static int countForStudent = 0;
	static int countForTeacher = 0;

	public static void main(String[] args) {
		
		while (true) {
			informChulsuk.start(); 
			
			String input = sc.next();

			if (input.equals("1")) {

				informChulsuk.goodLuckForStudent();

			} else if (input.equals("2")) {
				
				informChulsuk.attendance();

			} else {
				System.out.println("Error");
				System.out.println();
			}
		}
	}
}
 