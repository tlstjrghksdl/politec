package JavaProgramming_6_Elevater;



public class Programming_6_3_Elev{
	private static int inval;
	
	public static void up() {
		inval++;
		System.out.printf("난 그냥 메소드[%d]\n", inval);
	}
	
	public static void main(String[] args) {
		inval = 0;
		Elevater elev;
		
		elev = new Elevater();
		
		up();
		for (int kopo36_i = 0; kopo36_i < 10; kopo36_i++) {
			elev.up();
			System.out.printf("MSG[%s]\n", elev.help);
		}
		
		for (int kopo36_i = 0; kopo36_i < 11; kopo36_i++) {
			elev.down();
			System.out.printf("MSG[%s]\n", elev.help);
		}
	}
}
