package JavaProgramming_6_Elevater;

public class Programming_6_3_Elev2 {
	public static void main(String[] args) {
		Elevater2 elev;
		elev = new Elevater2();
		for (int i = 0; i < 10; i++) {
			elev.up();
			
			System.out.printf("MGS[%s]\n", elev.help);
		}
		
		for (int i = 0; i < 10; i++) {
			elev.down();
			
			System.out.printf("MSG[%s]\n", elev.help);
		}
		
		Elevater2 elevUP = new Elevater2(5);
		System.out.printf("MSG elevUP[%s]\n", elevUP.help);
		
		
		Elevater2 elevDN = new Elevater2(-5);
		System.out.printf("MSG elevDN[%s]\n", elevDN.help);
	}
}
