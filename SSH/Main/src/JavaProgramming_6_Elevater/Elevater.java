package JavaProgramming_6_Elevater;

public class Elevater {
	int limit_up_floor = 10; // 최상위
	int limit_down_floor = 0; // 최하위
	int now_floor = 1; // 현재 층
	
	// 안내문구 출력하도록 해주는 String변수 선언
	String help;
	
	// 층수가 올라가는 메소드
	void up() {
		if (now_floor == limit_up_floor) {
			help = "마지막 층입니다.";
		// 마지막 층일때는 문구만 출력
			
		} else {
			now_floor++;
			help = String.format("%d층입니다", now_floor);
		// 마지막 층이 아닐때는 1층씩 올라가도록 ++
		
		}
	}
	
	// 층수가 내려가는 메소드
	void down() {
		if (now_floor == limit_down_floor) {
			help = "처음층 입니다.";
		// 가장 아래 층일때는 문구만 출력
			
		} else {
			now_floor--;
			help = String.format("%d층입니다", now_floor);
		// 가장 아래 층이 아닐때는 1층씩 내려가도록 --
			
		}
	}
}
