package JavaProgramming_6_Elevater;

public class Elevater2 {
	int limit_up_floor;
	int limit_down_floor;
	int nowfloor;
	String help;

	Elevater2() {
		limit_up_floor = 10;
		limit_down_floor = 0;
		nowfloor = 1;
		System.out.printf("엘레베이터 준공완료\n");
	}

	Elevater2(int a) {
		limit_up_floor = 10;
		limit_down_floor = 0;
		nowfloor = 1;
		System.out.printf("엘레베이터 준공완료[%d]\n", a);

		if (a > 0) {
			for (int i = 0; i < a; i++) {
				this.up();
			}
		} else if (a < 0) {
			for (int i = 0; i < -a; i++) {
				this.down();
			}
		}
	}

	// 층수가 올라가는 메소드
	void up() {
		if (nowfloor == limit_up_floor) {
			help = "마지막 층입니다.";
			// 마지막 층일때는 문구만 출력

		} else {
			nowfloor++;
			help = String.format("%d층입니다", nowfloor);
			// 마지막 층이 아닐때는 1층씩 올라가도록 ++

		}
	}

	// 층수가 내려가는 메소드
	void down() {
		if (nowfloor == limit_down_floor) {
			help = "처음층 입니다.";
			// 가장 아래 층일때는 문구만 출력

		} else {
			nowfloor--;
			help = String.format("%d층입니다", nowfloor);
			// 가장 아래 층이 아닐때는 1층씩 내려가도록 --

		}
	}

}
 