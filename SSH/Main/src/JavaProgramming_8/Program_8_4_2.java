package JavaProgramming_8;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Program_8_4_2 {
	public static void main(String[] args) {
		SimpleDateFormat k36_sdf = new SimpleDateFormat("yyy/MM/dd-HH:mm:ss");
		try {
			// 전 실습에서 만든 csv파일을 지정한다. 그리고 bufferedreader를 인스턴스화 하고 인자값으로 파일을 넣어준다.
			File k36_f = new File("C:\\Users\\401-ST05\\Desktop\\StockDailyPrice.csv");
			BufferedReader k36_br = new BufferedReader(new FileReader(k36_f));
			// 새로 저장할 파일의 경로와 이름을 정한다. 그리고 bufferedwriter를 인스턴스화 하고 새로 만들 파일을 인자로 넣어준다.
			File k36_f2 = new File("C:\\Users\\401-ST05\\Desktop\\A005930.csv");
			BufferedWriter k36_bw = new BufferedWriter(new FileWriter(k36_f2));
			
			// 읽을 문자열을 선언
			String k36_readtxt;
			
			// 적은 카운트와 그냥 카운트를 선언한다.
			int k36_cnt = 0;
			int k36_wcnt = 0;
			
			//시작전 시간 한번 찍어준다.
			System.out.print("시작 : ");
			System.out.println(k36_sdf.format(System.currentTimeMillis()));
			// while문으로 더이상 읽을 파일이 없으면 끝나는 로직을 만든다.
			while((k36_readtxt = k36_br.readLine()) != null) {
				// StringBuffer를 인스턴스화해준다.
				StringBuffer k36_sb = new StringBuffer();
				// 읽어온 파일이 csv파일이기 때문에 구분자는 콤마로 돼있다.
				String[] k36_field = k36_readtxt.split(",", 0);
				// 구분자로 나눠 배열에 담았을때 길이가 2보다 클때(즉 A라는 종목코드가 있는 배열일때)
				// 그리고, 배열의 2번째 요소에 붙어있는 ^를 없애주는 replace를 쓰고, 빈칸을 없애는 trim을 써준후, 첫번째 글자가 "A005930"와 일치할때
				if(k36_field.length > 2 && k36_field[2].replace("^", "").trim().substring(0, 7).equals("A005930")) {
					// StringBuffer에 첫번째 값을 추가 해준다. 그리고 for문을 돌린다! (아까 구분자로 있떤 ^를 모두 지워준다)
					k36_sb.append(k36_field[0].trim());
					// 그 후에 값들을 for문을 통해 넣어준다.(아까 구분자로 있떤 ^를 모두 지워준다)
					for (int i = 1; i < k36_field.length; i++) {
						k36_sb.append("," + k36_field[i].replace("^", "").trim());
					}
					//for문이 한번 돌때마다 한 줄이 완성이 된다. 그것을 아까 생성하려고 지정한 파일에 써준다.
					k36_bw.write(k36_sb.toString());
					// 그리고 개행
					k36_bw.newLine();  
					// 써졌으면 써진 카운트를 올려준다.
					k36_wcnt++;
				}
				// 그리고 그냥 카운트를 올려준다.
				k36_cnt++;
			} 
			// while이 끝나면 read와 write를 모두 끝낸다.
			k36_br.close();
			k36_bw.close();
			// 총 몇개에서 몇개를 record했다 라고 출력한다.
			System.out.printf("Program End[%d][%d]records\n", k36_cnt, k36_wcnt);
			System.out.println(k36_sdf.format(System.currentTimeMillis()));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
