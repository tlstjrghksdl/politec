package JavaProgramming_8;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Program_8_4_4 {
	public static void main(String[] args) {
		// 찍어줄 날짜를 위해 날짜의 포멧을 정해준다.
		SimpleDateFormat k36_sdf = new SimpleDateFormat("yyy/MM/dd-HH:mm:ss");
		try {// 불러올 파일을 정해준다. 빠른 처리를 위해 bufferedreader로 불러온 파일을 읽을 준비를 한다.
			File k36_f = new File("C:\\Users\\401-ST05\\Desktop\\StockDailyPrice.csv");
			BufferedReader k36_br = new BufferedReader(new FileReader(k36_f));
			// 읽어서 만들 파일을 정해준다.파일을 읽을때도 buffered를 사용하면 한줄을 읽어 주는 것이기 때문에 
			File k36_f2 = new File("C:\\Users\\401-ST05\\Desktop\\MaxAndMin.csv");
			BufferedWriter k36_bw = new BufferedWriter(new FileWriter(k36_f2));
			// 읽어올 String변수를 선언한다.
			String k36_readtxt;
			// 적은 카운트와 그냥 카운트를 선언한다.
			int k36_cnt = 0;
			int k36_wcnt = 0;

			int k36_a = 0; // 최대값
			int k36_b = 100000000; // 최소값
			System.out.print("시작 : ");
			System.out.println(k36_sdf.format(System.currentTimeMillis()));
			// while문으로 더이상 읽을 파일이 없으면 끝나는 로직을 만든다.
			while ((k36_readtxt = k36_br.readLine()) != null) {
				// StringBuffer를 인스턴스화해준다.
				String[] k36_field = k36_readtxt.split(",");
				
				//2015년의 삼성주가의 종가를 이용해서 최대값 최소값을 구한다.
				if ((k36_field.length > 4 && k36_field[2].replace("^", "").trim().substring(0, 7).equals("A005930"))
						&& k36_field[1].replace("^", "").trim().substring(0, 4).equals("2015")) {
					// 만약 종가가 이전 종가와 비교했을 때 크면 a값을 그 값으로 바꿔준다.
					if (k36_a < Integer.parseInt(k36_field[3].replace("^", "").trim())) {
						k36_a = Integer.parseInt(k36_field[3].replace("^", "").trim());
					} else {
					}
					// 만약 종가가 이전 종가와 비교했을 때 크면 b값을 그 값으로 바꿔준다.
					if (k36_b > Integer.parseInt(k36_field[3].replace("^", "").trim())) {
						k36_b = Integer.parseInt(k36_field[3].replace("^", "").trim());
					} else {
					}
				}
			}
			// 최대값 최소값을 출력한다.
			System.out.println(k36_a);
			System.out.println(k36_b);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
