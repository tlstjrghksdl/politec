package JavaProgramming_8;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class Program_8_1 {
	public static void main(String[] args) {
		try {
			// 생성할 파일의 경로를 정한다.
			File k36_f = new File("C:\\Users\\401-ST05\\Desktop\\Name.txt");
			
			// FileWriter로 생성한 파일에 써준다.
			FileWriter k36_fw = new FileWriter(k36_f);
			
			// hello라고 쓴다
			k36_fw.write("Hello");
			
			// my name is Juan이라고 쓴다.
			k36_fw.write("my name is Juan");
			
			// filewriter를 다 사용했으면 닫아준다. 
			k36_fw.close();
			
			// 써서 저장한 파일을 읽는다.
			FileReader k36_fr = new FileReader(k36_f);
			
			// 카운트 n을 -1로 초기화 한다.
			int k36_n = -1;
			
			// char 배열을 생성한다.
			char[] k36_ch;
			
			//while이 true일 때 
			while(true) {
				
				// char배열의 길이를 100으로 한다.
				k36_ch = new char[100];
				
				// FileReader에 있는 read매소드로 한글자씩 읽어온다.
				k36_n = k36_fr.read(k36_ch);
				
				// -1은 read가 읽을 것이 없으면 -1을 반환한다. 그렇기 때문에 읽을것이 없으면 while문을 끝낸다.
				if(k36_n == -1) {
					break;
				}
				
				// 읽은 글자 하나하나를 출력한다.
				for (int k36_i = 0; k36_i < k36_n; k36_i++) {
					System.out.printf("%c", k36_ch[k36_i]);
				}
			}
			// FileReader를 종료한다.
			k36_fr.close();
			System.out.printf("\n FILE READ END ");
		} catch (Exception e) {
			System.out.printf("*******[%s]\n", e);
		}
	}
}
