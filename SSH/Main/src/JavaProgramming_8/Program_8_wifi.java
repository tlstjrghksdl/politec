package JavaProgramming_8;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Program_8_wifi {
	public static void main(String[] args) throws IOException {
		// 불러올 파일, 버퍼로 한줄씩읽을거기 때문에 파일을 인자로 버퍼를 인스턴스화
		File k36_f = new File("C:\\Users\\401-ST05\\Desktop\\JUAN\\교안\\BIZ프로그래밍기초\\실습데이터\\전국무료와이파이표준데이터.csv");
		BufferedReader k36_br = new BufferedReader(new FileReader(k36_f));

		// 만들 파일들
		File k36_f1 = new File("C:\\Users\\401-ST05\\Desktop\\전국무료와이파이표준데이터_SKT.txt");
		File k36_f2 = new File("C:\\Users\\401-ST05\\Desktop\\전국무료와이파이표준데이터_KT.txt");
		File k36_f3 = new File("C:\\Users\\401-ST05\\Desktop\\전국무료와이파이표준데이터_LGU.txt");

		// 만들 파일들을 한줄씩 써야하기 때문에, 버퍼에 넣는다.
		BufferedWriter k36_bw1 = new BufferedWriter(new FileWriter(k36_f1));
		BufferedWriter k36_bw2 = new BufferedWriter(new FileWriter(k36_f2));
		BufferedWriter k36_bw3 = new BufferedWriter(new FileWriter(k36_f3));

		// 받을 데이터 한줄을 먼저 선언
		String k36_readtxt;
		

		// while을 데이터를 받을때까지 실행하는 것
		while ((k36_readtxt = k36_br.readLine()) != null) {
			// 행 안에 콤마 가 있따면 그걸 제외하고 콤마를 구분자로하여 배열에 넣는다.
			String[] k36_field = k36_readtxt.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
			// 통신사에 특수문자를 지워준다.
			k36_field[5] = k36_field[5].replace("\"", "");
			String inputString = "";
			
			// 통신사별로 모두 검토하여 stk가 포함되어 있는 열은 sk로 바꾼후 써준다.
			if (k36_field[5].trim().equals("SK") || k36_field[5].trim().equals("SKT")
					|| k36_field[5].trim().equals("서울특별시청-SKT") || k36_field[5].trim().equals("SK텔레콤")
					|| k36_field[5].trim().equals("sk텔레콤") || k36_field[5].trim().equals("㈜SK텔레콤")
					|| k36_field[5].trim().equals("SKT,경기도 광주시청")) {
				inputString = changeField(k36_readtxt, "SK");
				k36_bw1.write(inputString);
				k36_bw1.newLine();
				// kt가 포함되어있는 애들은 kt로 바꾼후 써준다.
			} else if (k36_field[5].trim().equals("KT, 하남시") || k36_field[5].trim().equals("KT")
					|| k36_field[5].trim().equals("㈜KT") || k36_field[5].trim().equals("㈜케이티")
					|| k36_field[5].trim().equals("시흥시청,㈜케이티") || k36_field[5].trim().equals("서울특별시청-KT")
					|| k36_field[5].trim().equals("강원도 고성군청, KT") || k36_field[5].trim().equals("KT올레")) {
				inputString = changeField(k36_readtxt, "KT");
				k36_bw2.write(inputString);
				k36_bw2.newLine();
				// kt가 포함되어있는 애들은 kt로 바꾼후 써준다.
			} else if (k36_field[5].trim().equals("LG") || k36_field[5].trim().equals("LG U+")
					|| k36_field[5].trim().equals("LGT") || k36_field[5].trim().equals("LGU+")
					|| k36_field[5].trim().equals("Lgu+") || k36_field[5].trim().equals("U+")
					|| k36_field[5].trim().equals("(주)엘지유플러스") || k36_field[5].trim().equals("LG U +")
					|| k36_field[5].trim().equals("LGU") || k36_field[5].trim().equals("U+")
					|| k36_field[5].trim().equals("강원도 고성군청, LGU+") || k36_field[5].trim().equals("서울특별시청-LGU+")
					|| k36_field[5].trim().equals("") || k36_field[5].trim().equals("")
					|| k36_field[5].trim().equals("") || k36_field[5].trim().equals("")
					|| k36_field[5].trim().equals("") || k36_field[5].trim().equals("")
					|| k36_field[5].trim().equals("") || k36_field[5].trim().equals("")) {
				inputString = changeField(k36_readtxt, "LG");
				k36_bw3.write(inputString);
				k36_bw3.newLine();
				// skt와 kt가 포함되어있는 애들은 각각 이름을 바꾼후 써준다.
			} else if (k36_field[5].trim().equals("KT,SKT") || k36_field[5].trim().equals("SKT, KT")
					|| k36_field[5].trim().equals("KT, SKT")) {
				inputString = changeField(k36_readtxt, "SK");
				k36_bw1.write(inputString);
				k36_bw1.newLine();
				inputString = changeField(k36_readtxt, "KT");
				k36_bw2.write(inputString);
				k36_bw2.newLine();
				// skt와 lg가 포함되어있는 애들은 각각 이름을 바꾼후 써준다.
			} else if (k36_field[5].trim().equals("LGU+,SKT") || k36_field[5].trim().equals("SKT, LG")
					|| k36_field[5].trim().equals("SKT,LGU+,경기도 광주시청") || k36_field[5].trim().equals("SKT,U+")
					|| k36_field[5].trim().equals("SKT/LGU+") || k36_field[5].trim().equals("SKT, LGU+")) {
				inputString = changeField(k36_readtxt, "SK");
				k36_bw1.write(inputString);
				k36_bw1.newLine();
				inputString = changeField(k36_readtxt, "LG");
				k36_bw3.write(inputString);
				k36_bw3.newLine();

				// kt와 lg가 포함되어있는 애들은 각각 이름을 바꾼후 써준다.
			} else if (k36_field[5].trim().equals("KT, LGU+") || k36_field[5].trim().equals("KT,LG")
					|| k36_field[5].trim().equals("KT,U+") || k36_field[5].trim().equals("KT/LGU+")
					|| k36_field[5].trim().equals("LGU+, KT") || k36_field[5].trim().equals("LGU+,KT")) {
				inputString = changeField(k36_readtxt, "KT");
				k36_bw2.write(inputString);
				k36_bw2.newLine();
				inputString = changeField(k36_readtxt, "LG");
				k36_bw3.write(inputString);
				k36_bw3.newLine();
				// 모두다 포함되어있는 애들은 각각 이름을 바꾼후 써준다.
			} else if (k36_field[5].trim().equals("KT, SKT, LGU+") || k36_field[5].trim().equals("SK, KT, LG U+")
					|| k36_field[5].trim().equals("SK, KT, LGU+") || k36_field[5].trim().equals("SK,KT,LG")
					|| k36_field[5].trim().equals("SKT, KT, LGU+") || k36_field[5].trim().equals("SKT,KT,LGU+")
					|| k36_field[5].trim().equals("SKT,KT,U+")
					|| k36_field[5].trim().equals("미래창조과학부(SKT, KT, LGU+)")) {
				inputString = changeField(k36_readtxt, "SK");
				k36_bw1.write(inputString);
				k36_bw1.newLine();
				inputString = changeField(k36_readtxt, "KT");
				k36_bw2.write(inputString);
				k36_bw2.newLine();
				inputString = changeField(k36_readtxt, "LG");
				k36_bw3.write(inputString);
				k36_bw3.newLine();
			} else {
			}
			

		}
		// while이 끝나면 모두 닫아준다.
		k36_br.close();
		k36_bw1.close();
		k36_bw2.close();
		k36_bw3.close();

	}
	
	// 값을 바꿔주는 메소드를 만든다. 
	public static String changeField(String readtxt, String telecom) {
		// 인자로 받은 readtxt를 콤마로 구분자 하여 field배열에 넣는다.
		// 그리고 인자로 통신사의 이름을 넣으면 그것으로 바꿔준다.
		String[] field = readtxt.split(",");
		if (telecom.equals("SK")) {
			field[5] = "SK";
		} else if (telecom.equals("KT")) {
			field[5] = "KT";
		} else {
			field[5] = "LG";
		}
		
		// readtxt를 field배열 0번째 요소로 바꿔준다.
		readtxt = field[0];
		
		// csv파일이기때문에 중간에 쉼표를 모두 넣어준다.
		for (int i = 0; i < 13; i++) {
			readtxt = readtxt + "," + field[i + 1];
		}
		return readtxt;
	}
}