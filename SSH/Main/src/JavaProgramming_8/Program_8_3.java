package JavaProgramming_8;

import java.io.*;

public class Program_8_3 {
	public static void main(String[] args) {
		try {
			// 만들어질 파일의 경로와 이름을 정한다. stockDailyPrice.csv
			File k36_f = new File("C:\\Users\\401-ST05\\Desktop\\JUAN\\교안\\BIZ프로그래밍기초\\실습데이터\\day_data\\THTSKS010H00.dat");
			
			// BufferedReader로 파일을 읽을 것이다.
			BufferedReader k36_br = new BufferedReader(new FileReader(k36_f));
			
			// 읽어올 파일을 readtxt를 선언
			String k36_readtxt;
			// 라인을 카운팅하는 int변수를 선언한다.
			int k36_LineCnt = 0;
			
			// while이 끝날 것을 정한다.
			int k36_n = -1;
			
			// StringBuffer를 인스턴스화한다.
			StringBuffer k36_s = new StringBuffer();
			 
			
			// while을 만들고 LineCnt를 1씩 증가시키면서 30이 되면 멈추게한다.
			while(true) {
				
				// char배열의 길이를 1000으로 해서 인스턴스화한다.
				char[] k36_ch = new char[1000];
				
				// read메소드로 파일을 읽는다.
				k36_n = k36_br.read(k36_ch);
				
				// n이 -1이 되면, 즉 더이상 읽을 글자가 없으면 while을 끝낸다.
				if(k36_n == -1) {
					break;
				}
				// foreach문으로 c에 읽어온 글자를 받는다.
				for (char k36_c : k36_ch) {
					// 받아온 c의 값이 개행이면 
					if(k36_c == '\n') {
						// StringBuffer로 받은 값을 스트링으로 변환 후 출력한다.
						System.out.printf("[%s]***\n", k36_s.toString());
						// 그리고 초기화 해준다.
						k36_s.delete(0, k36_s.length());
					} else {
						//그게 아닌경우엔 Stringbuffer에 값을 추가해 준다.
						k36_s.append(k36_c);
					}
				}
				// 한번 끝나면 라인을 세는 변수를 1씩 증가시킨다.
				k36_LineCnt++;
			}
			System.out.printf("[%s]***\n", k36_s.toString());
		} catch (Exception e) {
			// TODO: handle exception
		} 
	}
}
