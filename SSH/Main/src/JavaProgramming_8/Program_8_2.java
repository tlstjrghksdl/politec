package JavaProgramming_8;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class Program_8_2 {


	public static void k36_FileWrite() {
		try {
			// file을 만든다.
			File k36_f = new File("C:\\Users\\401-ST05\\Desktop\\Name.txt");
			
			// bufferedwriter로 한줄씩 쓴다.
			BufferedWriter k36_bw = new BufferedWriter(new FileWriter(k36_f));
			
			// hello를 쓴다
			k36_bw.write("Hello");
			//새로운 라인을 만든다
			k36_bw.newLine();
			// my name is Juan을 쓴다.
			k36_bw.write("my name is Juan");
			//새로운 라인을 만든다
			k36_bw.newLine();
			
			//bufferedWriter를 닫는다.
			k36_bw.close();
		} catch (Exception e) {
			System.out.printf("******[%s]\n", e);
		}
	}
	
	public static void k36_FileReader() {
		try {
			// 파일을 지정하는 것이다. 파일이 없다면 만들어준다.
			File k36_f = new File("C:\\Users\\401-ST05\\Desktop\\Name.txt");
			
			// bufferedReader로 파일을 읽을 것이다.
			BufferedReader k36_br = new BufferedReader(new FileReader(k36_f));
			
			// 읽을 String값을 미리 선언한다.
			String k36_readtxt;
			
			// readLine메소드가 끝날 때 까지 돌아가는 while문이 끝나게 한다.
			while((k36_readtxt = k36_br.readLine()) != null) {
				System.out.printf("%s\n", k36_readtxt);
			}
			// while문이 끝난 것은 더이상 읽을 것이 없다는 것이기 때문에 
			// bufferedReader를 끝낸다.
			k36_br.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public static void main(String[] args) {
		// 메인 메소드에서 먼저 filewrite로 쓰고,
		// 써서 만들어진 파일을 filereader로 읽는다.
		k36_FileWrite();
		k36_FileReader();
	}
}
