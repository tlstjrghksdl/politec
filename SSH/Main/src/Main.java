import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Main {
	static Connection conn = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	static PreparedStatement pstmt = null;
	public static void connect() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

			String url = "jdbc:mysql://192.168.23.32:33061/notification?"
					+ "serverTimezone=UTC&characterEncoding=UTF-8&allowPublicKeyRetrieval=true&useSSL=false";

			conn = DriverManager.getConnection(url, "root", "kopo36");
			stmt = conn.createStatement();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	// 연결중지 메소드
	public static void disconnect() {
		try {
			stmt.close();
			conn.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		
		List<Notification> notices = new ArrayList<>();
		connect();
		try {

			rs = stmt.executeQuery("select * from notice order by id;");
			while (rs.next()) {
				Notification notice = new Notification();
				notice.setId(rs.getString(1));
				notice.setTitle(rs.getString(2));
				notice.setDate(rs.getString(3));
				notice.setContent(rs.getString(4));
				notice.setTitlenum(rs.getString(5));
				notice.setRelevel(rs.getString(6));
				notice.setRecent(rs.getString(7));
				notice.setViewcnt(rs.getString(8));
				notices.add(notice);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();
		
		System.out.println(notices.get(0).getViewcnt());
	}
}
