package Basic_Training;

import java.io.*;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class aaaaa {
    public static class k27_JDBC {
        private static Connection k27_conn;
        // Connection 클래스 형 conn 객체에 대한 싱클톤 패턴(Singleton pattern)을 적용하여 접속이 유지된 상태면 다시 접속을 하지 않고
        // 접속이 이뤄지지 않은 상태(객체 생성전)면 conn 객체에 DriverManger.getConnection으로 접속한다.
        // Connection 메소드를 각 쿼리 도입부에 삽입하고 쿼리를 재실행하여도 접속이 유지되도록 한다.
        public static synchronized Connection k27_getInstance() {
            // static 변수 conn의 상태를 확인하고
            // null(접속 해제 상태)이면 접속을 진행한다
            // null이 아니면(접속 상태) 이 구문을 지나친다. (접속 유지)
            if (k27_conn == null) {
                final String k27_id = "root";
                final String k27_password = "kopo36";
                final String k27_url = "jdbc:mysql://192.168.23.105:33061/kopo36?rewriteBatchedStatements=true&useUnicode=true&characterEncoding=utf8&serverTimezone=Asia/Seoul&allowPublicKeyRetrieval=true&useSSL=false";
                try {
                    k27_conn = DriverManager.getConnection(k27_url, k27_id, k27_password);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
            // 접속 정보가 담긴 인스턴스 conn을 리턴한다.
            return k27_conn;
        }
        public static int k27_insertStockList(ArrayList<k27_Stock> k27_stockArrayList, int k27_wcnt) throws SQLException {
            // Connection 클래스의 k27_conn 인스턴스를 선언.
            // getInstance 메서드를 통해 접속 정보를 설정한다.
            Connection k27_conn = k27_getInstance();
            PreparedStatement k27_pstmt;
            String sql = "INSERT ignore INTO stock(stnd_iscd, bsop_date, shrn_iscd, stck_prpr, stck_oprc, stck_hgpr, stck_lwpr, prdy_vrss_sign, prdy_vrss, prdy_ctrt, prdy_vol, acml_vol, acml_tr_pbmn, askp1, bidp1, total_askp_rsqn, total_bidp_rsqn, seln_cntg_smtn, shnu_cntg_smtn, seln_tr_pbmn, shnu_tr_pbmn, seln_cntg_csnu, shnu_cntg_csnu, w52_hgpr, w52_lwpr, w52_hgpr_date, w52_lwpr_date, ovtm_untp_bsop_hour, ovtm_untp_prpr, ovtm_untp_prdy_vrss, ovtm_untp_prdy_vrss_sign, ovtm_untp_askp1, ovtm_untp_bidp1, ovtm_untp_vol, ovtm_untp_tr_pbmn, ovtm_untp_oprc, ovtm_untp_hgpr, ovtm_untp_lwpr, mkob_otcp_vol, mkob_otcp_tr_pbmn, mkfa_otcp_vol, mkfa_otcp_tr_pbmn, mrkt_div_cls_code, pstc_dvdn_amt, lstn_stcn, stck_sdpr, stck_fcam, wghn_avrg_stck_prc, issu_limt_rate, frgn_limt_qty, oder_able_qty, frgn_limt_exhs_cls_code, frgn_hldn_qty, frgn_hldn_rate, hts_frgn_ehrt, itmt_last_nav, prdy_last_nav, trc_errt, dprt, ssts_cntg_qty, ssts_tr_pbmn, frgn_ntby_qty, flng_cls_code, prtt_rate, acml_prtt_rate, stdv, beta_cfcn, crlt_cfcn, bull_beta, bear_beta, bull_dvtn, bear_dvtn, bull_crlt, bear_crlt, stck_mxpr, stck_llam, icic_cls_code, itmt_vol, itmt_tr_pbmn, fcam_mod_cls_code, revl_issu_reas_code, orgn_ntby_qty, adj_prpr, fn_oprc, fn_hgpr, fn_lwpr, fn_prpr, fn_acml_vol, fn_acml_tr_pbmn, fn_prtt_rate, fn_flng_cls_code, buyin_nor_prpr, buyin_nor_prdy_vrss, buyin_nor_vol, buyin_nor_tr_pbmn, buyin_tod_prpr, buyin_tod_prdy_vrss, buyin_tod_vol, buyin_tod_tr_pbmn) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?)";
            // SimpleDateFormat으로 날짜 포맷 설정한다.
            SimpleDateFormat k27_sdfYMD = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // setAutoCommit 메서드로 오토 커미션을 해제한다.
            // 오토 커미션을 해제해야 배치 작업을 수행할 수 있다.
            k27_conn.setAutoCommit(false);
            try {
                // pstmt에 sql 구문을 올린다.
                k27_pstmt = k27_conn.prepareStatement(sql);
                for (int i = 0; i < k27_stockArrayList.size(); i++) {
                    k27_Stock stock = k27_stockArrayList.get(i);
                    k27_pstmt.setString(1, stock.stnd_iscd);
                    k27_pstmt.setInt(2, stock.bsop_date);
                    k27_pstmt.setString(3, stock.shrn_iscd);
                    k27_pstmt.setInt(4, stock.stck_prpr);
                    k27_pstmt.setInt(5, stock.stck_oprc);
                    k27_pstmt.setInt(6, stock.stck_hgpr);
                    k27_pstmt.setInt(7, stock.stck_lwpr);
                    k27_pstmt.setString(8, stock.prdy_vrss_sign);
                    k27_pstmt.setInt(9, stock.prdy_vrss);
                    k27_pstmt.setFloat(10, stock.prdy_ctrt);
                    k27_pstmt.setLong(11, stock.prdy_vol);
                    k27_pstmt.setLong(12, stock.acml_vol);
                    k27_pstmt.setLong(13, stock.acml_tr_pbmn);
                    k27_pstmt.setInt(14, stock.askp1);
                    k27_pstmt.setInt(15, stock.bidp1);
                    k27_pstmt.setLong(16, stock.total_askp_rsqn);
                    k27_pstmt.setLong(17, stock.total_bidp_rsqn);
                    k27_pstmt.setLong(18, stock.seln_cntg_smtn);
                    k27_pstmt.setLong(19, stock.shnu_cntg_smtn);
                    k27_pstmt.setLong(20, stock.seln_tr_pbmn);
                    k27_pstmt.setLong(21, stock.shnu_tr_pbmn);
                    k27_pstmt.setInt(22, stock.seln_cntg_csnu);
                    k27_pstmt.setInt(23, stock.shnu_cntg_csnu);
                    k27_pstmt.setInt(24, stock.w52_hgpr);
                    k27_pstmt.setInt(25, stock.w52_lwpr);
                    k27_pstmt.setInt(26, stock.w52_hgpr_date);
                    k27_pstmt.setInt(27, stock.w52_lwpr_date);
                    k27_pstmt.setInt(28, stock.ovtm_untp_bsop_hour);
                    k27_pstmt.setInt(29, stock.ovtm_untp_prpr);
                    k27_pstmt.setInt(30, stock.ovtm_untp_prdy_vrss);
                    k27_pstmt.setString(31, stock.ovtm_untp_prdy_vrss_sign);
                    k27_pstmt.setInt(32, stock.ovtm_untp_askp1);
                    k27_pstmt.setInt(33, stock.ovtm_untp_bidp1);
                    k27_pstmt.setLong(34, stock.ovtm_untp_vol);
                    k27_pstmt.setLong(35, stock.ovtm_untp_tr_pbmn);
                    k27_pstmt.setInt(36, stock.ovtm_untp_oprc);
                    k27_pstmt.setInt(37, stock.ovtm_untp_hgpr);
                    k27_pstmt.setInt(38, stock.ovtm_untp_lwpr);
                    k27_pstmt.setLong(39, stock.mkob_otcp_vol);
                    k27_pstmt.setLong(40, stock.mkob_otcp_tr_pbmn);
                    k27_pstmt.setLong(41, stock.mkfa_otcp_vol);
                    k27_pstmt.setLong(42, stock.mkfa_otcp_tr_pbmn);
                    k27_pstmt.setString(43, stock.mrkt_div_cls_code);
                    k27_pstmt.setLong(44, stock.pstc_dvdn_amt);
                    k27_pstmt.setLong(45, stock.lstn_stcn);
                    k27_pstmt.setInt(46, stock.stck_sdpr);
                    k27_pstmt.setFloat(47, stock.stck_fcam);
                    k27_pstmt.setFloat(48, stock.wghn_avrg_stck_prc);
                    k27_pstmt.setFloat(49, stock.issu_limt_rate);
                    k27_pstmt.setLong(50, stock.frgn_limt_qty);
                    k27_pstmt.setLong(51, stock.oder_able_qty);
                    k27_pstmt.setString(52, stock.frgn_limt_exhs_cls_code);
                    k27_pstmt.setLong(53, stock.frgn_hldn_qty);
                    k27_pstmt.setFloat(54, stock.frgn_hldn_rate);
                    k27_pstmt.setFloat(55, stock.hts_frgn_ehrt);
                    k27_pstmt.setFloat(56, stock.itmt_last_nav);
                    k27_pstmt.setFloat(57, stock.prdy_last_nav);
                    k27_pstmt.setFloat(58, stock.trc_errt);
                    k27_pstmt.setFloat(59, stock.dprt);
                    k27_pstmt.setLong(60, stock.ssts_cntg_qty);
                    k27_pstmt.setLong(61, stock.ssts_tr_pbmn);
                    k27_pstmt.setLong(62, stock.frgn_ntby_qty);
                    k27_pstmt.setString(63, stock.flng_cls_code);
                    k27_pstmt.setFloat(64, stock.prtt_rate);
                    k27_pstmt.setFloat(65, stock.acml_prtt_rate);
                    k27_pstmt.setFloat(66, stock.stdv);
                    k27_pstmt.setFloat(67, stock.beta_cfcn);
                    k27_pstmt.setFloat(68, stock.crlt_cfcn);
                    k27_pstmt.setFloat(69, stock.bull_beta);
                    k27_pstmt.setFloat(70, stock.bear_beta);
                    k27_pstmt.setFloat(71, stock.bull_dvtn);
                    k27_pstmt.setFloat(72, stock.bear_dvtn);
                    k27_pstmt.setFloat(73, stock.bull_crlt);
                    k27_pstmt.setFloat(74, stock.bear_crlt);
                    k27_pstmt.setInt(75, stock.stck_mxpr);
                    k27_pstmt.setInt(76, stock.stck_llam);
                    k27_pstmt.setString(77, stock.icic_cls_code);
                    k27_pstmt.setLong(78, stock.itmt_vol);
                    k27_pstmt.setLong(79, stock.itmt_tr_pbmn);
                    k27_pstmt.setString(80, stock.fcam_mod_cls_code);
                    k27_pstmt.setString(81, stock.revl_issu_reas_code);
                    k27_pstmt.setLong(82, stock.orgn_ntby_qty);
                    k27_pstmt.setInt(83, stock.adj_prpr);
                    k27_pstmt.setInt(84, stock.fn_oprc);
                    k27_pstmt.setInt(85, stock.fn_hgpr);
                    k27_pstmt.setInt(86, stock.fn_lwpr);
                    k27_pstmt.setInt(87, stock.fn_prpr);
                    k27_pstmt.setLong(88, stock.fn_acml_vol);
                    k27_pstmt.setLong(89, stock.fn_acml_tr_pbmn);
                    k27_pstmt.setFloat(90, stock.fn_prtt_rate);
                    k27_pstmt.setString(91, stock.fn_flng_cls_code);
                    k27_pstmt.setInt(92, stock.buyin_nor_prpr);
                    k27_pstmt.setInt(93, stock.buyin_nor_prdy_vrss);
                    k27_pstmt.setLong(94, stock.buyin_nor_vol);
                    k27_pstmt.setLong(95, stock.buyin_nor_tr_pbmn);
                    k27_pstmt.setInt(96, stock.buyin_tod_prpr);
                    k27_pstmt.setInt(97, stock.buyin_tod_prdy_vrss);
                    k27_pstmt.setLong(98, stock.buyin_tod_vol);
                    k27_pstmt.setLong(99, stock.buyin_tod_tr_pbmn);
                    // 여태까지 세팅한 칼럼들을 배치에 추가한다.
                    // 여기까지 add한 것이 한 쿼리문이 되고 이것들을 모아서 배치처리한다.
                    k27_pstmt.addBatch();
                    // 카운트 변수 wcnt를 증가시킨다.
                    k27_wcnt++;
                    // 카운트 변수가 10000으로 나눠떨어지면 배치를 진행한다.
                    // 1만 단위로 배치를 묶음으로 하여 진행한다.
                    // 하지만 실제로 10000이나 1000이나 크게 차이가 나지 않는 게 MySQL에서는
                    // String으로 배치를 만들어 처리하기 때문에 제한적인 배치만 적용 된다,
                    if ((k27_wcnt % 10000) == 0) {
                        // Batch Cycle 날짜를 표시할 Calendar 인스턴스를 생성한다.
                        Calendar k27_startTime = Calendar.getInstance();
                        System.out.printf("k27_Batch Cycle: %d\n", k27_wcnt);
                        System.out.println(k27_sdfYMD.format(k27_startTime.getTime()));
                        // executeBatch를 실행하여 메모리에 올라가있는 배치 쿼리들을 전송한다.
                        k27_pstmt.executeBatch();
                        // 배치 작업 목록을 초기화한다.
                        k27_pstmt.clearBatch();
                        // 커밋을 수행한다. 최종승인
                        k27_conn.commit();
                    }
                }
                // executeBatch를 실행하여 메모리에 올라가있는 배치 쿼리들을 전송한다.
                k27_pstmt.executeBatch();
                // 커밋을 수행한다. 최종승인
                k27_conn.commit();

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            return k27_wcnt;
        }
    }

    public static void main(String[] args) throws IOException, ParseException, SQLException {
        // SimpleDateFormat으로 지정한 날짜 포맷을 설정한다.
        SimpleDateFormat k27_sdfYMD = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat k27_sdfMS = new SimpleDateFormat("SSS");
        SimpleDateFormat k27_sdfHMS = new SimpleDateFormat("HH:mm:ss");
        // File 클래스 인스턴스 f를 생성한다.
        File k27_f = new File("C:\\Users\\401-ST05\\Desktop\\JUAN\\교안\\BIZ프로그래밍기초\\실습데이터\\day_data\\THTSKS010H00.dat");
        //File k27_f = new File("StockTest.dat");
        BufferedReader k27_br = new BufferedReader(new FileReader(k27_f));
        // 라인을 저장할 k27_readtxt String 변수 선언
        String k27_readtxt;
        // 읽어들인 카운트
        int k27_cnt = 0;
        // 쓴 카운트
        int k27_wcnt = 0;

        // 시작 시간을 위한 k27_startTime 생성성
        Calendar k27_startTime = Calendar.getInstance();
        System.out.printf("k27_Program Start\n");
        System.out.println(k27_sdfYMD.format(k27_startTime.getTime()));
        System.out.printf("\n");

        // 배치 처리를 보낼 stockArraylist 선언
        // Null Data 처리를 위한 k27_refineField 배열 선언
        ArrayList<k27_Stock> stockInfo = new ArrayList<>();
        String[] k27_refineField = new String[99];
        // readLine 메서드를 통해 한줄씩 내용을 불러오고 이를 readtxt에 저장한다.
        // readtxt의 null을 체크해서 아닐 경우 계속 반복한다.
        while ((k27_readtxt = k27_br.readLine()) != null) {
            StringBuffer k27_s = new StringBuffer();
            String[] k27_field = k27_readtxt.split("%_%");
            // 첫 필드는 뒤에있는 ^ 기호만 떼어내고 append한다.
            if (k27_field.length > 2 && k27_field[2].replace("^", "").trim()
                    .substring(0, 1).equals("A")) {
                // 두 번째 필드는 좌우에 있는 ^ 기호를 떼어내고 append한다.
                k27_s.append(k27_field[0].replace("^", "").trim());
                for (int j = 1; j < k27_field.length; j++) {
                    k27_s.append("," + k27_field[j].replace("^", "").trim());
                }
                // 최초로 컴마 기준으로 가른 데이터는 k27_splitField 배열에 저장한다.
                String[] k27_splitField = k27_s.toString().split(",");
                // null 처리 구문
                // 데이터의 길이가 뒤죽박죽이기 때문에 길이가 99인 k27_refineField 배열을 선언하고
                // k27_refineField 배열에 k27_splitField 배열을 복사한다.
                for (int i = 0; i < k27_refineField.length; i++) {
                    // k27_splitField 길이 이내일때는 splitField의 복사를 진행한다.
                    if (i < k27_splitField.length) {
                        // 빈공간 값이면 0으로 초기화
                        if (k27_splitField[i].matches("")) {
                            k27_refineField[i] = "0";
                            // 그게 아니면 splitField를 refineField에 복사
                        } else {
                            k27_refineField[i] = k27_splitField[i];
                        }
                        // splitField길이 이상일때 refineField가 0이면
                        // refineField를 0으로 초기화한다.
                    } else {
                        if (k27_refineField[i] == null) {
                            k27_refineField[i] = "0";
                        }
                    }
                }
                // 배치를 보낼 arrayList에 add로 데이터를 삽입한다.
                stockInfo.add(new k27_Stock(k27_refineField[0], Integer.parseInt(k27_refineField[1]), k27_refineField[2], Integer.parseInt(k27_refineField[3]), Integer.parseInt(k27_refineField[4]), Integer.parseInt(k27_refineField[5]), Integer.parseInt(k27_refineField[6]), k27_refineField[7], Integer.parseInt(k27_refineField[8]), Float.parseFloat(k27_refineField[9]), Long.parseLong(k27_refineField[10]), Long.parseLong(k27_refineField[11]), Long.parseLong(k27_refineField[12]), Integer.parseInt(k27_refineField[13]), Integer.parseInt(k27_refineField[14]), Long.parseLong(k27_refineField[15]), Long.parseLong(k27_refineField[16]), Long.parseLong(k27_refineField[17]), Long.parseLong(k27_refineField[18]), Long.parseLong(k27_refineField[19]), Long.parseLong(k27_refineField[20]), Integer.parseInt(k27_refineField[21]), Integer.parseInt(k27_refineField[22]), Integer.parseInt(k27_refineField[23]), Integer.parseInt(k27_refineField[24]), Integer.parseInt(k27_refineField[25]), Integer.parseInt(k27_refineField[26]), Integer.parseInt(k27_refineField[27]), Integer.parseInt(k27_refineField[28]), Integer.parseInt(k27_refineField[29]), k27_refineField[30], Integer.parseInt(k27_refineField[31]), Integer.parseInt(k27_refineField[32]), Long.parseLong(k27_refineField[33]), Long.parseLong(k27_refineField[34]), Integer.parseInt(k27_refineField[35]), Integer.parseInt(k27_refineField[36]), Integer.parseInt(k27_refineField[37]), Long.parseLong(k27_refineField[38]), Long.parseLong(k27_refineField[39]), Long.parseLong(k27_refineField[40]), Long.parseLong(k27_refineField[41]), k27_refineField[42], Long.parseLong(k27_refineField[43]), Long.parseLong(k27_refineField[44]), Integer.parseInt(k27_refineField[45]), Float.parseFloat(k27_refineField[46]), Float.parseFloat(k27_refineField[47]), Float.parseFloat(k27_refineField[48]), Long.parseLong(k27_refineField[49]), Long.parseLong(k27_refineField[50]), k27_refineField[51], Long.parseLong(k27_refineField[52]), Float.parseFloat(k27_refineField[53]), Float.parseFloat(k27_refineField[54]), Float.parseFloat(k27_refineField[55]), Float.parseFloat(k27_refineField[56]), Float.parseFloat(k27_refineField[57]), Float.parseFloat(k27_refineField[58]), Long.parseLong(k27_refineField[59]), Long.parseLong(k27_refineField[60]), Long.parseLong(k27_refineField[61]), k27_refineField[62], Float.parseFloat(k27_refineField[63]), Float.parseFloat(k27_refineField[64]), Float.parseFloat(k27_refineField[65]), Float.parseFloat(k27_refineField[66]), Float.parseFloat(k27_refineField[67]), Float.parseFloat(k27_refineField[68]), Float.parseFloat(k27_refineField[69]), Float.parseFloat(k27_refineField[70]), Float.parseFloat(k27_refineField[71]), Float.parseFloat(k27_refineField[72]), Float.parseFloat(k27_refineField[73]), Integer.parseInt(k27_refineField[74]), Integer.parseInt(k27_refineField[75]), k27_refineField[76], Long.parseLong(k27_refineField[77]), Long.parseLong(k27_refineField[78]), k27_refineField[79], k27_refineField[80], Long.parseLong(k27_refineField[81]), Integer.parseInt(k27_refineField[82]), Integer.parseInt(k27_refineField[83]), Integer.parseInt(k27_refineField[84]), Integer.parseInt(k27_refineField[85]), Integer.parseInt(k27_refineField[86]), Long.parseLong(k27_refineField[87]), Long.parseLong(k27_refineField[88]), Float.parseFloat(k27_refineField[89]), k27_refineField[90], Integer.parseInt(k27_refineField[91]), Integer.parseInt(k27_refineField[92]), Long.parseLong(k27_refineField[93]), Long.parseLong(k27_refineField[94]), Integer.parseInt(k27_refineField[95]), Integer.parseInt(k27_refineField[96]), Long.parseLong(k27_refineField[97]), Long.parseLong(k27_refineField[98])));

                // 데이터는 만건 단위로 처리가 된다.
                if (k27_cnt % 10000 == 0) {
                    // insertStockList 메서드로 배치를 처리하고 k27_wcnt 값을 반환한다.
                    k27_wcnt = k27_JDBC.k27_insertStockList(stockInfo, k27_wcnt);
                    // 한번 배치가 끝나면 stockArrayList를 초기화한다.
                    stockInfo.clear();
                }
                k27_cnt++;
            }
        }
        // 10000으로 나눠지지 않는 나머지에 대해서 배치처리를 진행한다.
        k27_wcnt = k27_JDBC.k27_insertStockList(stockInfo, k27_wcnt);
        stockInfo.clear();

        k27_br.close();

        // 처리 회수 및 삽입 라인 수 출력
        System.out.printf("\nk27_Program End [%d] [%d] records\n", k27_cnt, k27_wcnt);
        // 종료 타임 calendar 객체 생성
        Calendar k27_endTime = Calendar.getInstance();
        // 종료 타임 스탬프 출력
        System.out.println(k27_sdfYMD.format(k27_endTime.getTime()));
        System.out.printf("\n");
        // 진행 시간을 측정하고 출력한다.
        long time = k27_endTime.getTimeInMillis() - k27_startTime.getTimeInMillis();
        Date k27_procTime = k27_sdfMS.parse(String.valueOf(time));

        System.out.printf("k27_Processing Time: %s\n", k27_sdfHMS.format(k27_procTime));
    }

    // 주식 정보가 저장되는 클래스 생성
    public static class k27_Stock {
        // DB 파일에서 각 필드값들을 structure.txt 파일의 자료형을 기준으로 동일하게 생성한다.
        // 아래 값들은 MySQL DB에 PreparedStatement와 set 메서드와 연동되어 쿼리값에 직접 들어간다.
        private String stnd_iscd;
        private int bsop_date;
        private String shrn_iscd;
        private int stck_prpr;
        private int stck_oprc;
        private int stck_hgpr;
        private int stck_lwpr;
        private String prdy_vrss_sign;
        private int prdy_vrss;
        private float prdy_ctrt;
        private long prdy_vol;
        private long acml_vol;
        private long acml_tr_pbmn;
        private int askp1;
        private int bidp1;
        private long total_askp_rsqn;
        private long total_bidp_rsqn;
        private long seln_cntg_smtn;
        private long shnu_cntg_smtn;
        private long seln_tr_pbmn;
        private long shnu_tr_pbmn;
        private int seln_cntg_csnu;
        private int shnu_cntg_csnu;
        private int w52_hgpr;
        private int w52_lwpr;
        private int w52_hgpr_date;
        private int w52_lwpr_date;
        private int ovtm_untp_bsop_hour;
        private int ovtm_untp_prpr;
        private int ovtm_untp_prdy_vrss;
        private String ovtm_untp_prdy_vrss_sign;
        private int ovtm_untp_askp1;
        private int ovtm_untp_bidp1;
        private long ovtm_untp_vol;
        private long ovtm_untp_tr_pbmn;
        private int ovtm_untp_oprc;
        private int ovtm_untp_hgpr;
        private int ovtm_untp_lwpr;
        private long mkob_otcp_vol;
        private long mkob_otcp_tr_pbmn;
        private long mkfa_otcp_vol;
        private long mkfa_otcp_tr_pbmn;
        private String mrkt_div_cls_code;
        private long pstc_dvdn_amt;
        private long lstn_stcn;
        private int stck_sdpr;
        private float stck_fcam;
        private float wghn_avrg_stck_prc;
        private float issu_limt_rate;
        private long frgn_limt_qty;
        private long oder_able_qty;
        private String frgn_limt_exhs_cls_code;
        private long frgn_hldn_qty;
        private float frgn_hldn_rate;
        private float hts_frgn_ehrt;
        private float itmt_last_nav;
        private float prdy_last_nav;
        private float trc_errt;
        private float dprt;
        private long ssts_cntg_qty;
        private long ssts_tr_pbmn;
        private long frgn_ntby_qty;
        private String flng_cls_code;
        private float prtt_rate;
        private float acml_prtt_rate;
        private float stdv;
        private float beta_cfcn;
        private float crlt_cfcn;
        private float bull_beta;
        private float bear_beta;
        private float bull_dvtn;
        private float bear_dvtn;
        private float bull_crlt;
        private float bear_crlt;
        private int stck_mxpr;
        private int stck_llam;
        private String icic_cls_code;
        private long itmt_vol;
        private long itmt_tr_pbmn;
        private String fcam_mod_cls_code;
        private String revl_issu_reas_code;
        private long orgn_ntby_qty;
        private int adj_prpr;
        private int fn_oprc;
        private int fn_hgpr;
        private int fn_lwpr;
        private int fn_prpr;
        private long fn_acml_vol;
        private long fn_acml_tr_pbmn;
        private float fn_prtt_rate;
        private String fn_flng_cls_code;
        private int buyin_nor_prpr;
        private int buyin_nor_prdy_vrss;
        private long buyin_nor_vol;
        private long buyin_nor_tr_pbmn;
        private int buyin_tod_prpr;
        private int buyin_tod_prdy_vrss;
        private long buyin_tod_vol;
        private long buyin_tod_tr_pbmn;
        // 배치 리스트를 구성할 때 필요한 생성자 정의한다.
        public k27_Stock(String stnd_iscd, int bsop_date, String shrn_iscd, int stck_prpr, int stck_oprc, int stck_hgpr, int stck_lwpr, String prdy_vrss_sign, int prdy_vrss, float prdy_ctrt, long prdy_vol, long acml_vol, long acml_tr_pbmn, int askp1, int bidp1, long total_askp_rsqn, long total_bidp_rsqn, long seln_cntg_smtn, long shnu_cntg_smtn, long seln_tr_pbmn, long shnu_tr_pbmn, int seln_cntg_csnu, int shnu_cntg_csnu, int w52_hgpr, int w52_lwpr, int w52_hgpr_date, int w52_lwpr_date, int ovtm_untp_bsop_hour, int ovtm_untp_prpr, int ovtm_untp_prdy_vrss, String ovtm_untp_prdy_vrss_sign, int ovtm_untp_askp1, int ovtm_untp_bidp1, long ovtm_untp_vol, long ovtm_untp_tr_pbmn, int ovtm_untp_oprc, int ovtm_untp_hgpr, int ovtm_untp_lwpr, long mkob_otcp_vol, long mkob_otcp_tr_pbmn, long mkfa_otcp_vol, long mkfa_otcp_tr_pbmn, String mrkt_div_cls_code, long pstc_dvdn_amt, long lstn_stcn, int stck_sdpr, float stck_fcam, float wghn_avrg_stck_prc, float issu_limt_rate, long frgn_limt_qty, long oder_able_qty, String frgn_limt_exhs_cls_code, long frgn_hldn_qty, float frgn_hldn_rate, float hts_frgn_ehrt, float itmt_last_nav, float prdy_last_nav, float trc_errt, float dprt, long ssts_cntg_qty, long ssts_tr_pbmn, long frgn_ntby_qty, String flng_cls_code, float prtt_rate, float acml_prtt_rate, float stdv, float beta_cfcn, float crlt_cfcn, float bull_beta, float bear_beta, float bull_dvtn, float bear_dvtn, float bull_crlt, float bear_crlt, int stck_mxpr, int stck_llam, String icic_cls_code, long itmt_vol, long itmt_tr_pbmn, String fcam_mod_cls_code, String revl_issu_reas_code, long orgn_ntby_qty, int adj_prpr, int fn_oprc, int fn_hgpr, int fn_lwpr, int fn_prpr, long fn_acml_vol, long fn_acml_tr_pbmn, float fn_prtt_rate, String fn_flng_cls_code, int buyin_nor_prpr, int buyin_nor_prdy_vrss, long buyin_nor_vol, long buyin_nor_tr_pbmn, int buyin_tod_prpr, int buyin_tod_prdy_vrss, long buyin_tod_vol, long buyin_tod_tr_pbmn) {
            this.stnd_iscd = stnd_iscd;
            this.bsop_date = bsop_date;
            this.shrn_iscd = shrn_iscd;
            this.stck_prpr = stck_prpr;
            this.stck_oprc = stck_oprc;
            this.stck_hgpr = stck_hgpr;
            this.stck_lwpr = stck_lwpr;
            this.prdy_vrss_sign = prdy_vrss_sign;
            this.prdy_vrss = prdy_vrss;
            this.prdy_ctrt = prdy_ctrt;
            this.prdy_vol = prdy_vol;
            this.acml_vol = acml_vol;
            this.acml_tr_pbmn = acml_tr_pbmn;
            this.askp1 = askp1;
            this.bidp1 = bidp1;
            this.total_askp_rsqn = total_askp_rsqn;
            this.total_bidp_rsqn = total_bidp_rsqn;
            this.seln_cntg_smtn = seln_cntg_smtn;
            this.shnu_cntg_smtn = shnu_cntg_smtn;
            this.seln_tr_pbmn = seln_tr_pbmn;
            this.shnu_tr_pbmn = shnu_tr_pbmn;
            this.seln_cntg_csnu = seln_cntg_csnu;
            this.shnu_cntg_csnu = shnu_cntg_csnu;
            this.w52_hgpr = w52_hgpr;
            this.w52_lwpr = w52_lwpr;
            this.w52_hgpr_date = w52_hgpr_date;
            this.w52_lwpr_date = w52_lwpr_date;
            this.ovtm_untp_bsop_hour = ovtm_untp_bsop_hour;
            this.ovtm_untp_prpr = ovtm_untp_prpr;
            this.ovtm_untp_prdy_vrss = ovtm_untp_prdy_vrss;
            this.ovtm_untp_prdy_vrss_sign = ovtm_untp_prdy_vrss_sign;
            this.ovtm_untp_askp1 = ovtm_untp_askp1;
            this.ovtm_untp_bidp1 = ovtm_untp_bidp1;
            this.ovtm_untp_vol = ovtm_untp_vol;
            this.ovtm_untp_tr_pbmn = ovtm_untp_tr_pbmn;
            this.ovtm_untp_oprc = ovtm_untp_oprc;
            this.ovtm_untp_hgpr = ovtm_untp_hgpr;
            this.ovtm_untp_lwpr = ovtm_untp_lwpr;
            this.mkob_otcp_vol = mkob_otcp_vol;
            this.mkob_otcp_tr_pbmn = mkob_otcp_tr_pbmn;
            this.mkfa_otcp_vol = mkfa_otcp_vol;
            this.mkfa_otcp_tr_pbmn = mkfa_otcp_tr_pbmn;
            this.mrkt_div_cls_code = mrkt_div_cls_code;
            this.pstc_dvdn_amt = pstc_dvdn_amt;
            this.lstn_stcn = lstn_stcn;
            this.stck_sdpr = stck_sdpr;
            this.stck_fcam = stck_fcam;
            this.wghn_avrg_stck_prc = wghn_avrg_stck_prc;
            this.issu_limt_rate = issu_limt_rate;
            this.frgn_limt_qty = frgn_limt_qty;
            this.oder_able_qty = oder_able_qty;
            this.frgn_limt_exhs_cls_code = frgn_limt_exhs_cls_code;
            this.frgn_hldn_qty = frgn_hldn_qty;
            this.frgn_hldn_rate = frgn_hldn_rate;
            this.hts_frgn_ehrt = hts_frgn_ehrt;
            this.itmt_last_nav = itmt_last_nav;
            this.prdy_last_nav = prdy_last_nav;
            this.trc_errt = trc_errt;
            this.dprt = dprt;
            this.ssts_cntg_qty = ssts_cntg_qty;
            this.ssts_tr_pbmn = ssts_tr_pbmn;
            this.frgn_ntby_qty = frgn_ntby_qty;
            this.flng_cls_code = flng_cls_code;
            this.prtt_rate = prtt_rate;
            this.acml_prtt_rate = acml_prtt_rate;
            this.stdv = stdv;
            this.beta_cfcn = beta_cfcn;
            this.crlt_cfcn = crlt_cfcn;
            this.bull_beta = bull_beta;
            this.bear_beta = bear_beta;
            this.bull_dvtn = bull_dvtn;
            this.bear_dvtn = bear_dvtn;
            this.bull_crlt = bull_crlt;
            this.bear_crlt = bear_crlt;
            this.stck_mxpr = stck_mxpr;
            this.stck_llam = stck_llam;
            this.icic_cls_code = icic_cls_code;
            this.itmt_vol = itmt_vol;
            this.itmt_tr_pbmn = itmt_tr_pbmn;
            this.fcam_mod_cls_code = fcam_mod_cls_code;
            this.revl_issu_reas_code = revl_issu_reas_code;
            this.orgn_ntby_qty = orgn_ntby_qty;
            this.adj_prpr = adj_prpr;
            this.fn_oprc = fn_oprc;
            this.fn_hgpr = fn_hgpr;
            this.fn_lwpr = fn_lwpr;
            this.fn_prpr = fn_prpr;
            this.fn_acml_vol = fn_acml_vol;
            this.fn_acml_tr_pbmn = fn_acml_tr_pbmn;
            this.fn_prtt_rate = fn_prtt_rate;
            this.fn_flng_cls_code = fn_flng_cls_code;
            this.buyin_nor_prpr = buyin_nor_prpr;
            this.buyin_nor_prdy_vrss = buyin_nor_prdy_vrss;
            this.buyin_nor_vol = buyin_nor_vol;
            this.buyin_nor_tr_pbmn = buyin_nor_tr_pbmn;
            this.buyin_tod_prpr = buyin_tod_prpr;
            this.buyin_tod_prdy_vrss = buyin_tod_prdy_vrss;
            this.buyin_tod_vol = buyin_tod_vol;
            this.buyin_tod_tr_pbmn = buyin_tod_tr_pbmn;
        }
    }
}
