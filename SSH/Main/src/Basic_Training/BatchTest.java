package Basic_Training;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;

public class BatchTest {
	public static void main(String[] args) {
		try {
			SimpleDateFormat k36_sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			String url = "jdbc:mysql://192.168.23.105:33061/kopo36?rewriteBatchedStatements = true";
			Connection conn = DriverManager.getConnection(url, "root", "kopo36");

			BufferedReader bReader = new BufferedReader(
					new FileReader("C:\\Users\\401-ST05\\Desktop\\StockDailyPrice.csv"));

			String insertQuery = "insert into stockInfo values(?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?,"
					+ "?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?,"
					+ "?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?);";
			PreparedStatement ps = conn.prepareStatement(insertQuery);
			int batchTime = 0;
			int inputcount = 0;
			String line = "";
			System.out.println(k36_sdf.format(System.currentTimeMillis()));
			while ((line = bReader.readLine()) != null) {

				String[] array = line.split(",");
//				System.out.println(array[0]);
				for (int i = 0; i < array.length; i++) {
					ps.setString(i + 1, array[i]);
		
				}
				batchTime++;
				ps.addBatch();
				if (batchTime == 100000) {
				ps.executeBatch();
				ps.clearBatch();
				batchTime = 0;
				System.out.println(k36_sdf.format(System.currentTimeMillis()));
				}
			}

			bReader.close();
			

			ps.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
