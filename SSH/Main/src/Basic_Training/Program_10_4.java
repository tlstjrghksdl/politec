package Basic_Training;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Program_10_4 {
	public static void main(String[] args) {
		try {
			// ������ �ּ�
			String k36_url = "jdbc:mysql://192.168.23.105:33061/kopo36?serverTimezone=UTC";

			// connection���� DB�� ����
			Connection k36_conn = DriverManager.getConnection(k36_url, "root", "kopo36");

			// ������ ������ ����ϴ� Statement
			Statement k36_statement1 = null;

			// stockInfo ���̺��� �����ϴ� ������
			String k36_firstQuery = "CREATE TABLE stockInfo(" + "stnd_iscd VARCHAR(20)," + "bsop_date  VARCHAR(20),"
					+ "shrn_iscd VARCHAR(20)," + "stck_prpr  VARCHAR(20)," + "stck_oprc  VARCHAR(20),"
					+ "stck_hgpr  VARCHAR(20)," + "stck_lwpr  VARCHAR(20)," + "prdy_vrss_sign VARCHAR(20),"
					+ "prdy_vrss  VARCHAR(20)," + "prdy_ctrt  VARCHAR(20)," + "prdy_vol  VARCHAR(20),"
					+ "acml_vol  VARCHAR(20)," + "acml_tr_pbmn  VARCHAR(20)," + "askp1  VARCHAR(20),"
					+ "bidp1  VARCHAR(20)," + "total_askp_rsqn  VARCHAR(20)," + "total_bidp_rsqn  VARCHAR(20),"
					+ "seln_cntg_smtn  VARCHAR(20)," + "shnu_cntg_smtn  VARCHAR(20)," + "seln_tr_pbmn  VARCHAR(20),"
					+ "shnu_tr_pbmn  VARCHAR(20)," + "seln_cntg_csnu  VARCHAR(20)," + "shnu_cntg_csnu  VARCHAR(20),"
					+ "w52_hgpr  VARCHAR(20)," + "w52_lwpr  VARCHAR(20)," + "w52_hgpr_date  VARCHAR(20),"
					+ "w52_lwpr_date  VARCHAR(20)," + "ovtm_untp_bsop_hour  VARCHAR(20),"
					+ "ovtm_untp_prpr  VARCHAR(20)," + "ovtm_untp_prdy_vrss  VARCHAR(20),"
					+ "ovtm_untp_prdy_vrss_sign VARCHAR(20)," + "ovtm_untp_askp1  VARCHAR(20),"
					+ "ovtm_untp_bidp1  VARCHAR(20)," + "ovtm_untp_vol  VARCHAR(20),"
					+ "ovtm_untp_tr_pbmn  VARCHAR(20)," + "ovtm_untp_oprc  VARCHAR(20),"
					+ "ovtm_untp_hgpr  VARCHAR(20)," + "ovtm_untp_lwpr  VARCHAR(20)," + "mkob_otcp_vol  VARCHAR(20),"
					+ "mkob_otcp_tr_pbmn  VARCHAR(20)," + "mkfa_otcp_vol  VARCHAR(20),"
					+ "mkfa_otcp_tr_pbmn  VARCHAR(20)," + "mrkt_div_cls_code VARCHAR(20),"
					+ "pstc_dvdn_amt  VARCHAR(20)," + "lstn_stcn  VARCHAR(20)," + "stck_sdpr  VARCHAR(20),"
					+ "stck_fcam  VARCHAR(20)," + "wghn_avrg_stck_prc  VARCHAR(20)," + "issu_limt_rate  VARCHAR(20),"
					+ "frgn_limt_qty  VARCHAR(20)," + "oder_able_qty  VARCHAR(20),"
					+ "frgn_limt_exhs_cls_code VARCHAR(20)," + "frgn_hldn_qty  VARCHAR(20),"
					+ "frgn_hldn_rate  VARCHAR(20)," + "hts_frgn_ehrt  VARCHAR(20)," + "itmt_last_nav  VARCHAR(20),"
					+ "prdy_last_nav  VARCHAR(20)," + "trc_errt  VARCHAR(20)," + "dprt  VARCHAR(20),"
					+ "ssts_cntg_qty  VARCHAR(20)," + "ssts_tr_pbmn  VARCHAR(20)," + "frgn_ntby_qty  VARCHAR(20),"
					+ "flng_cls_code VARCHAR(20)," + "prtt_rate  VARCHAR(20)," + "acml_prtt_rate  VARCHAR(20),"
					+ "stdv  VARCHAR(20)," + "beta_cfcn  VARCHAR(20)," + "crlt_cfcn  VARCHAR(20),"
					+ "bull_beta  VARCHAR(20)," + "bear_beta  VARCHAR(20)," + "bull_dvtn  VARCHAR(20),"
					+ "bear_dvtn  VARCHAR(20)," + "bull_crlt  VARCHAR(20)," + "bear_crlt  VARCHAR(20),"
					+ "stck_mxpr  VARCHAR(20)," + "stck_llam  VARCHAR(20)," + "icic_cls_code VARCHAR(20),"
					+ "itmt_vol  VARCHAR(20)," + "itmt_tr_pbmn  VARCHAR(20)," + "fcam_mod_cls_code VARCHAR(20),"
					+ "revl_issu_reas_code VARCHAR(20)," + "orgn_ntby_qty  VARCHAR(20)," + "adj_prpr  VARCHAR(20),"
					+ "fn_oprc  VARCHAR(20)," + "fn_hgpr  VARCHAR(20)," + "fn_lwpr  VARCHAR(20),"
					+ "fn_prpr  VARCHAR(20)," + "fn_acml_vol  VARCHAR(20)," + "fn_acml_tr_pbmn  VARCHAR(20),"
					+ "fn_prtt_rate  VARCHAR(20)," + "fn_flng_cls_code VARCHAR(20)," + "buyin_nor_prpr  VARCHAR(20),"
					+ "buyin_nor_prdy_vrss  VARCHAR(20)," + "buyin_nor_vol  VARCHAR(20),"
					+ "buyin_nor_tr_pbmn  VARCHAR(20)," + "buyin_tod_prpr  VARCHAR(20),"
					+ "buyin_tod_prdy_vrss  VARCHAR(20)," + "buyin_tod_vol  VARCHAR(20),"
					+ "buyin_tod_tr_pbmn  VARCHAR(20));";
			// statement�� �����.
			k36_statement1 = k36_conn.createStatement();
			// ���� �������� execute�Ͽ� DB���� ����ǰ� �Ѵ�.
			k36_statement1.execute(k36_firstQuery);
			// statement�� �ݴ´�.
			k36_statement1.close();
			// ������ �����Ѵ�.
			k36_conn.close();
		} catch (NullPointerException E) {
			E.printStackTrace();
		} catch (Exception E) {
			E.printStackTrace();

		}

	}
}
