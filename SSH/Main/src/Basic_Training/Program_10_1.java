package Basic_Training;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Program_10_1 {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
//		k36_MakeAndDel("create table student_score ("
//				+ "student_id int primary key, name varchar(20), kor int, eng int, mat int"
//				+ ");");
		k36_MakeAndDel("drop table student_score;");
	}
	
	// 쿼리문을 넣으면 DB에 연결되서 mysql 쿼리를 수행하는 메소드
	public static void k36_MakeAndDel(String k36_Quary) {
		try {	
			
			// 연결할 ip주소와 포트포워딩 주소, 그리고 Database를 적어준다.
			String k36_url = "jdbc:mysql://192.168.56.1:33061/shin?serverTimezone=UTC";
			
			//connection을 통해 연결한다. 앞에 있는 url주소와, 아이디, 비밀번호가 필요하다.
			Connection k36_conn = DriverManager.getConnection(k36_url, "root", "kopo36");
			
			// connection에 있는 createStatement를 통해 쿼리문을 받을 준비를 한다.
			Statement k36_stmt = k36_conn.createStatement();
			
			// 메소드가 받은 인자값을 execute메소드에 넣어주면 그것이 DB에서 수행한다.
			k36_stmt.execute(k36_Quary); 

			// statement와 connection을 꺼준다.
			k36_stmt.close();
			k36_conn.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}

} 