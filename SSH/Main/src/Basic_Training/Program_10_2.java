package Basic_Training;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

class Programming_7_10_class {
	

	// 시간찍을때 쓰는 포맷
	static SimpleDateFormat k36_sdf = new SimpleDateFormat("HH:mm:ss");
	// 사람수, 한페이지에 출력할 사람수
	static int kopo36_iPerson = 1000;
	static int kopo36_itemnum = 30;



	// 데이터를 만드는 메소드 ArrayList를 인자로 받는다. 
	public static void kopo36_dataSet(ArrayList<kopo36_OneRec_forList> kopo36_inputArray) {
		// for문을 돌리면서 내가 만들고 싶은 사람수만큼 데이터를 만들어 준다.
		for (int kopo36_i = 0; kopo36_i < kopo36_iPerson; kopo36_i++) { 
			//학생의 이름은 홍길에 숫자를 붙인다.
			String kopo36_name = String.format("홍길%02d", kopo36_i + 1);
			// 국어, 영어, 수학점수는 랜덤함수를 돌려서 0<= 점수 < 100 사이의 값이 나온 것으로 초기화 한다.
			int kopo36_kor = (int) (Math.random() * 100); 
			int kopo36_eng = (int) (Math.random() * 100); 
			int kopo36_mat = (int) (Math.random() * 100); 
			// 위에서 얻은 값을 배열에 넣어서 초기화 해준다. 그렇게 되면 리스트 안에 1000개의 요소가 생기게 된다.
			kopo36_inputArray 
					.add(new kopo36_OneRec_forList(kopo36_i + 1, kopo36_name, kopo36_kor, kopo36_eng, kopo36_mat));
		}
	}

	// make메소드는 List를 인자로 받는다.
	// List안에 인스턴스를 담아 학생별로 한 요소에 들어갈 수 있게 한다.
	public static void k36_Make(ArrayList<kopo36_OneRec_forList> kopo36_Array) {
		try {
			// 연결하는 주소
			String k36_url = "jdbc:mysql://192.168.23.105:33061/kopo36?serverTimezone=UTC";
			
			//connection을 통해 연결한다. 앞에 있는 url주소와, 아이디, 비밀번호가 필요하다.
			Connection k36_conn = DriverManager.getConnection(k36_url, "root", "kopo36");
			
			// connection에 있는 createStatement를 통해 쿼리문을 받을 준비를 한다.
			Statement k36_stmt = k36_conn.createStatement();
			
			
			// 쿼리문에 들어갈 String값을 초기화 해준다.
			String input = "";
			
			// 데이터를 만드는 메소드를 불러온다.
			Programming_7_10_class.kopo36_dataSet(kopo36_Array);
			
			// for문을 List의 요소의 사이즈만큼 돌린다
			for (int k36_i = 0; k36_i < kopo36_Array.size(); k36_i++) {
			
				// 들어갈 쿼리문을 String.format으로 정해주고 거기가 들어갈 값들을 다 입력해준다. 
				input = String.format("insert into student_score values(%s, '%s', %s, %s, %s);",
						kopo36_Array.get(k36_i).kopo36_student_id(), kopo36_Array.get(k36_i).kopo36_name(),
						kopo36_Array.get(k36_i).kopo36_kor(), kopo36_Array.get(k36_i).kopo36_eng(),
						kopo36_Array.get(k36_i).kopo36_mat());
				// fop문이 돌면서 input할 쿼리문이 완성이 되면 execute로 DB에 넎어준다.
				k36_stmt.execute(input);
			}
			// for문이 끝나면 stmt와 conn을 꺼준다.
			k36_stmt.close();
			k36_conn.close();
//			System.out.println(sdf.format(System.currentTimeMillis()));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}

public class Program_10_2 {

	public static void main(String[] args) {
		
		// 메인 메소드에서 List의 한 요소에 한 학생이 들어갈 수 있도록 인스턴스화 해준다.
		ArrayList<kopo36_OneRec_forList> kopo36_arr = new ArrayList<kopo36_OneRec_forList>();
		
		// 시작시간을 체크한다.
		System.out.print("시작 : ");
		System.out.println(Programming_7_10_class.k36_sdf.format(System.currentTimeMillis()));
		
		// make메소드를 실행하고 만들어둔 List를 인자로 넣어준다.데이터를 만들고 바로 insert해준다.
		Programming_7_10_class.k36_Make(kopo36_arr);
		
		// 끝나는 시간을 체크한다.
		System.out.print("끝  :  ");
		System.out.println(Programming_7_10_class.k36_sdf.format(System.currentTimeMillis()));
	}
}

class kopo36_OneRec_forList {

	// 학생의 개인 정보를 모두 초기화 해준다.
	private int kopo36_student_id;
	private String kopo36_name;
	private int kopo36_kor;
	private int kopo36_eng;
	private int kopo36_mat;

	// constructor을 만들어준다. 
	public kopo36_OneRec_forList(int kopo36_student_id, String kopo36_name, int kor, int eng, int mat) {
		this.kopo36_student_id = kopo36_student_id;
		this.kopo36_name = kopo36_name;
		this.kopo36_kor = kor;
		this.kopo36_eng = eng;
		this.kopo36_mat = mat;
	}

	// getter
	public int kopo36_student_id() {
		return this.kopo36_student_id;
	}

	// getter
	public String kopo36_name() {
		return this.kopo36_name;
	}

	// getter
	public int kopo36_kor() {
		return this.kopo36_kor;
	}

	// getter
	public int kopo36_eng() {
		return this.kopo36_eng;
	}

	// getter
	public int kopo36_mat() {
		return this.kopo36_mat;
	}

}