package Basic_Training;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Program_10_3 {
	// ������ �������� ���ؼ� ����ؾ��� ��ŭ �����.
	// ���� ������, ����������, ���������� �� ����� select�� ��
	static Statement k36_statement2 = null;
	static Statement k36_statement3 = null;
	static Statement k36_statement4 = null;
	static Statement k36_statement5 = null;
	static Statement k36_statement1 = null;
	static ResultSet k36_rsNowsum = null;
	static ResultSet k36_rsNowavg = null;
	static ResultSet k36_rsAllsum = null;
	static ResultSet k36_rsAllavg = null;
	static ResultSet k36_rsQuery = null;
	
	// ������ DB
	static String k36_url = "jdbc:mysql://192.168.23.105:33061/kopo36?serverTimezone=UTC";
	
	// ���ῡ �ʿ��� Connection�� �ʱ�ȭ �صд�.
	static Connection k36_connection = null;
	
	//�������� ����ؾ��ϱ� ������ 1�� �ʱ�ȭ
	static int k36_Page = 1;
	public static void main(String[] args) {

		try {
			// connection�� ����� �ʱ�ȭ���ش�.
			k36_connection = DriverManager.getConnection(k36_url, "root", "kopo36");

			
			// ��� ���� �� �������� ��Ÿ�� ��� ���� ���س������̴�.
			int kopo36_iPerson = 1000;
			int kopo36_itemnum = 30;
			
			// �����ڰ� ��ҷ� ���� ArrayList�� �ν��Ͻ�ȭ �Ѵ�.
			ArrayList<kopo36_OneRec_forList> kopo36_Array = new ArrayList<kopo36_OneRec_forList>();
			
			// for���� ���������� �� �������ŭ �����Ͽ� �ο�����ŭ �����ϰ� �ߴ�.
			for (int kopo36_i = 0; kopo36_i < kopo36_iPerson; kopo36_i = kopo36_i + kopo36_itemnum) {
				// ������ ���� ����� �ʱ�ȭ ���ش�.
				String k36_Query = "";
				String k36_nowsum = "";
				String k36_allsum = "";
				String k36_nowavg = "";
				String k36_allavg = "";
				// ������ ǥ���� ��.
				System.out.println("Page : " + k36_Page);
				k36_Page++;
				
				// for���� ������ ���������� ����� for���� �����.
				for (int kopo36_j = kopo36_i; kopo36_j < kopo36_i + kopo36_itemnum; kopo36_j++) {
					// j���� �л����� �������� ���̻� for���� ������ �ʿ䰡 ���� ������ ���߰� �Ѵ�.
					if (kopo36_j == kopo36_iPerson) {
						break;
					} else { 
						// �װ� �ƴҶ��� select*���� String������ ��´�.
						k36_Query = String.format(
								"select student_id, name, kor, eng, mat, kor+eng+mat, "
								+ "kor/3+eng/3+mat/3 from student_score where student_id > %d and student_id <= %d;",
								kopo36_j, kopo36_j + 1);

					}
					
					// �׸��� ��� �޼ҵ忡 �������� �־ ����ǰ� �Ѵ�.
					k36_print(k36_Query); 
				}

				
				// ���������� �����
				int k36_allperson = kopo36_i + kopo36_itemnum;
				
				// ���� �����. ������ �������� ���� ������� ����ϱ� ������
				int k36_restOfPerson = kopo36_iPerson - kopo36_i;
				
				// ���� ������ �����
				int k36_InPagePerson = kopo36_itemnum; 
				
				// 1000�� �����ϱ� ������
				if (kopo36_i + kopo36_itemnum <= kopo36_iPerson) {
					// ���� �������� �հ迡 �ش��ϴ� select ������ �����.
					k36_nowsum = String.format(
							"select sum(kor), sum(eng), sum(mat), sum(kor)+sum(eng)+sum(mat),"
							+ " sum(kor)+sum(eng)+sum(mat)/3 from student_score where student_id > %d and student_id <= %d;",
							kopo36_i, k36_allperson);
					// ���� �������� ��տ� �ش��ϴ� select ������ �����.
					k36_nowavg = String.format(
							"select sum(kor)/%d, sum(eng)/%d, sum(mat)/%d, sum(kor)/%d+sum(eng)/%d+sum(mat)/%d,"
							+ " sum(kor)/%d+sum(eng)/%d+sum(mat)/%d from student_score where student_id > %d and student_id <= %d;",
							k36_InPagePerson, k36_InPagePerson, k36_InPagePerson, k36_InPagePerson, k36_InPagePerson, k36_InPagePerson,
							(k36_InPagePerson * 3), (k36_InPagePerson * 3), (k36_InPagePerson * 3), kopo36_i, kopo36_i + k36_InPagePerson);
				} else { 
					// ������ �������� �����ϸ� ���� �л����� ������ŭ�� �հ�� ����� ���Ѵ�.
					// ���� �������� �հ迡 �ش��ϴ� select ������ �����.
					k36_nowsum = String.format(
							"select sum(kor), sum(eng), sum(mat), sum(kor)+sum(eng)+sum(mat),"
							+ " sum(kor)/%d+sum(eng)/%d+sum(mat)/%d from student_score where student_id > %d and student_id <= %d;",
							k36_restOfPerson, k36_restOfPerson, k36_restOfPerson, kopo36_i, kopo36_iPerson);
					// ���� �������� ��տ� �ش��ϴ� select ������ �����.
					k36_nowavg = String.format(
							"select sum(kor)/%d, " + "sum(eng)/%d," + " sum(mat)/%d,"
									+ " sum(kor)/%d+sum(eng)/%d+sum(mat)/%d," + " sum(kor)/%d+sum(eng)/%d+sum(mat)/%d "
									+ "from student_score where student_id > %d and student_id <= %d;",
							k36_restOfPerson, k36_restOfPerson, k36_restOfPerson, k36_restOfPerson, k36_restOfPerson, k36_restOfPerson,
							k36_restOfPerson * 3, k36_restOfPerson * 3, k36_restOfPerson * 3, kopo36_i, kopo36_iPerson);

				}

				// 1000�� �����ϱ� ������
				if (kopo36_i + kopo36_itemnum <= kopo36_iPerson) {
					// ���� �������� �հ迡 �ش��ϴ� select ������ �����. ������������ �׻� ó������ �� ���Ѱ��̱⶧���� 0������ �հ踦 �����.
					k36_allsum = String.format(
							"select sum(kor), sum(eng), sum(mat), sum(kor)+sum(eng)+sum(mat),"
							+ " sum(kor)/3+sum(eng)/3+sum(mat)/3 from student_score where student_id > %d and student_id <= %d;",
							0, k36_allperson);

					// ���� �������� �հ迡 �ش��ϴ� select ������ �����.
					// ������������ �׻� ó������ �� ���Ѱ��� ����̱⶧���� 0������ �հ踦 �������� ��µ� ��� ����ŭ ����� ���Ͽ� �����.
					k36_allavg = String.format(
							"select sum(kor)/%d, " + "sum(eng)/%d," + " sum(mat)/%d,"
									+ " sum(kor)/%d+sum(eng)/%d+sum(mat)/%d," + " sum(kor)/%d+sum(eng)/%d+sum(mat)/%d "
									+ "from student_score where student_id > %d and student_id <= %d;",
							 k36_allperson, k36_allperson, k36_allperson, k36_allperson, k36_allperson, k36_allperson, (k36_allperson * 3),
							(k36_allperson * 3), (k36_allperson * 3), 0, k36_allperson);

					// 1000�� �����ϸ�
				} else {
					// 1000���� ���� �����ִ� select ������ �����.
					k36_allsum = String.format(
							"select sum(kor), sum(eng), sum(mat), sum(kor)+sum(eng)+sum(mat),"
							+ " sum(kor)/3+sum(eng)/3+sum(mat)/3 from student_score where student_id > %d and student_id <= %d;",
							0, k36_allperson);
					// ���� �������� 1000���հ迡 �ش��ϴ� select ������ �����.
					// ������������ �׻� ó������ �� ���Ѱ��� ����̱⶧���� 0������ �հ踦 �������� ��µ� ��� ����ŭ ����� ���Ͽ� �����.
					k36_allavg = String.format(
							"select sum(kor)/%d, sum(eng)/%d, sum(mat)/%d, sum(kor)/%d+sum(eng)/%d+sum(mat)/%d, "
							+ "sum(kor)/%d+sum(eng)/%d+sum(mat)/%d from student_score where student_id > %d and student_id <= %d;",
							kopo36_iPerson, kopo36_iPerson, kopo36_iPerson, kopo36_iPerson, kopo36_iPerson,
							kopo36_iPerson, kopo36_iPerson * 3, kopo36_iPerson * 3, kopo36_iPerson * 3, 0,
							kopo36_iPerson);
				}
				//���� �������� ������������ ����� �޼ҵ带 �ҷ��� ���ڰ��� �־��ش�.
			k36_tail(k36_nowsum, k36_nowavg, k36_allsum, k36_allavg);
			}
			
			// ������ �־��� statement�� ��� �ݾ��ش�. 
			k36_statement1.close();
			k36_statement2.close();
			k36_statement3.close();
			k36_statement4.close();
			k36_statement5.close();
			k36_connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	// ���� �������� ������������ ����ϴ� �޼ҵ�
	public static void k36_tail(String b, String c, String d, String e) {
		// �÷� ������
		int k36_columnSize = 0;
		// �޾ƿ� �����հ�
		String k36_QueryAllsum = d;
		// �޾ƿ� �������
		String k36_QueryAllavg = e;
		// �޾ƿ� ���� ������ ���
		String k36_QueryNowsum = b;
		// �޾ƿ� ���� ������ ���
		String k36_QueryNowavg = c;
		try {
			//statement�� ������ش�.
			k36_statement2 = k36_connection.createStatement();
			// �޾ƿ� ���ڰ��� statement.execute�޼ҵ带 ���� �־��ش�.
			if (k36_statement2.execute(k36_QueryNowsum)) {
				k36_rsNowsum = k36_statement2.getResultSet();
				// java�� ����ؾ� �ϱ� ������ resultset�� �ν��Ͻ�ȭ ���ش�.
				ResultSetMetaData k36_rsmd = (ResultSetMetaData) k36_rsNowsum.getMetaData();
				// �÷��� ����� ī��Ʈ ���ش�.
				k36_columnSize = k36_rsmd.getColumnCount();
			}
			// �����͸� �޾ƿ� ���� ���� ������ while�� �����Ѵ�.
			while (k36_rsNowsum != null && k36_rsNowsum.next()) {
				// �÷� �����ŭ ���� for��
				for (int i = 1; i <= k36_columnSize; i++) {
					// str�� �������������� ���� String������ �ʱ�ȭ�Ѵ�.
					String k36_str = k36_rsNowsum.getString(i);
					// �װ��� ���
					System.out.printf(k36_str + "  ");
				}
				// �׸��� ����
				System.out.println();
			}
			
			//statement�� ������ش�.
			k36_statement3 = k36_connection.createStatement();
			// �޾ƿ� ���ڰ��� statement.execute�޼ҵ带 ���� �־��ش�.
			if (k36_statement3.execute(k36_QueryNowavg)) {
				k36_rsNowavg = k36_statement3.getResultSet();
				// java�� ����ؾ� �ϱ� ������ resultset�� �ν��Ͻ�ȭ ���ش�.
				ResultSetMetaData k36_rsmd = (ResultSetMetaData) k36_rsNowavg.getMetaData();
				// �÷��� ����� ī��Ʈ ���ش�.
				k36_columnSize = k36_rsmd.getColumnCount();
			}
			// �����͸� �޾ƿ� ���� ���� ������ while�� �����Ѵ�.
			while (k36_rsNowavg != null && k36_rsNowavg.next()) {
				// �÷� �����ŭ ���� for��
				for (int i = 1; i <= k36_columnSize; i++) {
					// str�� �������������� ���� String������ �ʱ�ȭ�Ѵ�.
					String k36_str = k36_rsNowavg.getString(i);
					// �װ��� ���
					System.out.printf(k36_str + "  ");
				}
				System.out.println();
			}
			//statement�� ������ش�.
			k36_statement4 = k36_connection.createStatement();
			// �޾ƿ� ���ڰ��� statement.execute�޼ҵ带 ���� �־��ش�.
			if (k36_statement4.execute(k36_QueryAllsum)) {
				k36_rsAllsum = k36_statement4.getResultSet();
				// java�� ����ؾ� �ϱ� ������ resultset�� �ν��Ͻ�ȭ ���ش�.
				ResultSetMetaData k36_rsmd = (ResultSetMetaData) k36_rsAllsum.getMetaData();
				// �÷��� ����� ī��Ʈ ���ش�.
				k36_columnSize = k36_rsmd.getColumnCount();
			}
			// �����͸� �޾ƿ� ���� ���� ������ while�� �����Ѵ�.
			while (k36_rsAllsum != null && k36_rsAllsum.next()) {
				// �÷� �����ŭ ���� for��
				for (int i = 1; i <= k36_columnSize; i++) {
					// str�� �������������� ���� String������ �ʱ�ȭ�Ѵ�.
					String k36_str = k36_rsAllsum.getString(i);
					// �װ��� ���
					System.out.printf(k36_str + "  ");
				}
				System.out.println();
			}
			
			//statement�� ������ش�.
			k36_statement5 = k36_connection.createStatement();
			// �޾ƿ� ���ڰ��� statement.execute�޼ҵ带 ���� �־��ش�.
			if (k36_statement5.execute(k36_QueryAllavg)) {
				k36_rsAllavg = k36_statement5.getResultSet();
				// java�� ����ؾ� �ϱ� ������ resultset�� �ν��Ͻ�ȭ ���ش�.
				ResultSetMetaData k36_rsmd = (ResultSetMetaData) k36_rsAllavg.getMetaData();
				// �÷��� ����� ī��Ʈ ���ش�.
				k36_columnSize = k36_rsmd.getColumnCount();

			}
			// �����͸� �޾ƿ� ���� ���� ������ while�� �����Ѵ�.
			while (k36_rsAllavg != null && k36_rsAllavg.next()) {
				// �÷� �����ŭ ���� for��
				for (int i = 1; i <= k36_columnSize; i++) {
					// str�� �������������� ���� String������ �ʱ�ȭ�Ѵ�.
					String k36_str = k36_rsAllavg.getString(i);
					// �װ��� ���
					System.out.printf(k36_str + "  ");
				}
				System.out.printf("\n\n\n\n");
			}
			
			
		} catch (NullPointerException E) {
			E.printStackTrace();
		} catch (Exception E) {
			E.printStackTrace();

		}
	}

	public static void k36_print(String a) {
		// �÷�������
		int k36_columnSize = 0;
		
		// ������������ ����� String��
		String k36_firstQuery = a;

		try {
			//statement�� ������ش�.
			k36_statement1 = k36_connection.createStatement();
			// �޾ƿ� ���ڰ��� statement.execute�޼ҵ带 ���� �־��ش�.
			if (k36_statement1.execute(k36_firstQuery)) {
				k36_rsQuery = k36_statement1.getResultSet();
				// java�� ����ؾ� �ϱ� ������ resultset�� �ν��Ͻ�ȭ ���ش�.
				ResultSetMetaData rsmd = (ResultSetMetaData) k36_rsQuery.getMetaData();
				// �÷��� ����� ī��Ʈ ���ش�.
				k36_columnSize = rsmd.getColumnCount();

			}
			// �����͸� �޾ƿ� ���� ���� ������ while�� �����Ѵ�.
			while (k36_rsQuery != null && k36_rsQuery.next()) {
				// �÷� �����ŭ ���� for��
				for (int i = 1; i <= k36_columnSize; i++) {
					// str�� �������������� ���� String������ �ʱ�ȭ�Ѵ�.
					String k36_str = k36_rsQuery.getString(i);
					// �װ��� ���
					System.out.printf(k36_str + "  ");
				}
				// ����
				System.out.println();
			}

		} catch (NullPointerException E) {
			E.printStackTrace();
		} catch (Exception E) {
			E.printStackTrace();

		}
	}
}