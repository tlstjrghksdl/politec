package Java_class;

import java.util.HashSet;
import java.util.Set;

public class instancePractice implements Comparable<instancePractice>, Cloneable{
	private String name;
	private int hp;
	private int mp;
	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "My name is " + name + " and Hp is " + hp + " and Mp is " + mp;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + hp;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		instancePractice other = (instancePractice) obj;
		if (hp != other.hp)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(instancePractice o) {
		if(this.mp == o.mp) {
			return 0;	
		} else if(this.mp > o.mp) {
			return 1;
		} else {
			return -1;
		}
	}
	

	@Override
	protected instancePractice clone() throws CloneNotSupportedException {
		instancePractice result = new instancePractice();
		result.name = this.name;
		result.hp = this.hp;
		
		return result;
	}


	public static void main(String[] args) throws CloneNotSupportedException {
		Set<instancePractice> IPSet = new HashSet<>();
		instancePractice IP = new instancePractice();
		
		IP.name = "shin";
		IP.hp = 40;
		IP.mp = 30;
		instancePractice IP2 = IP.clone();
		
		System.out.println(IPSet.size());
		
		System.out.println(IP);
		System.out.println(IP2); 
	}
}
