package Java_class;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class MainForJava {
	public static final String HERO_TABLE_NAME = "hero";
	public static final String HERO_COLUMN_NAME = "name";
	public static final String HERO_COLUMN_HP = "hp";

	public static void connectionClose(Connection con) {
		// 3.����
		try {
			if (con != null)
				con.close();
		} catch (SQLException e) {
		}
	}

	public static Connection connect() {
		Connection con = null;

		String server = "localhost"; // MySQL ���� �ּ� 127.0.0.1
		String database = "test_db"; // MySQL DATABASE �̸�
		String user_name = "root"; // MySQL ���� ���̵�
		String password = "password"; // MySQL ���� ��й�ȣ

		// 2.����
		try {
			con = DriverManager.getConnection(
					"jdbc:mysql://" + server + "/" + database
							+ "?allowPublicKeyRetrieval=true&characterEncoding=UTF-8&serverTimezone=UTC&useSSL=false",
					user_name, password);
//			System.out.println("���������� ����Ǿ����ϴ�.");
		} catch (SQLException e) {
			System.err.println("con ����:" + e.getMessage());
			e.printStackTrace();
		}

		return con;
	}

	public static void main(String args[]) throws Exception {
		// ����
		Hero spiderMan = new Hero("spider man", 200);
		if (insert(spiderMan) > 0) {
			System.out.println("���� ó��");
		} else {
			System.out.println("����");
		}

		// �˻�
		List<Hero> heroList = query(10);
		for (Hero hero : heroList) {
			System.out.println(hero);
		}

		// ����
		delete(spiderMan);

		// ������Ʈ
		Hero firstHero = heroList.get(0);
		firstHero.setHp(1000);
		int result = update(firstHero);
		if (result > 0) {
			System.out.println(result + "�� ������Ʈ ��");
		} else {
			System.out.println("�ƹ��͵� �� ��");
		}

		// �˻�
		heroList = query(10);
		for (Hero hero : heroList) {
			System.out.println(hero);
		}

		// Ʈ����� ó��
		Connection con = connect();
		try {
			con.setAutoCommit(false);

			insert(spiderMan);
			spiderMan.setHp(2000);
			update(spiderMan);

			con.commit();
		} catch (Exception e) {
			con.rollback();
		}

		// 3.����
		connectionClose(con);
	}

	public static int delete(Hero hero) throws SQLException {
		Connection con = connect();

		// DELETE FROM hero WHERE name = 'spider man';
		PreparedStatement pstmt = con.prepareStatement("DELETE FROM hero WHERE name = ?");
		pstmt.setString(1, hero.getName());
		int result = pstmt.executeUpdate();

		// ó��
//		System.out.println(result + " �� ���Ե�");

		pstmt.close();

		connectionClose(con);
		return result;
	}

	public static int update(Hero hero) throws SQLException {
		Connection con = connect();

		// UPDATE hero SET hp = 50 WHERE name = 'super man'
		PreparedStatement pstmt = con.prepareStatement("UPDATE hero SET hp = ? WHERE name = ?");
		pstmt.setInt(1, hero.getHp());
		pstmt.setString(2, hero.getName());
		int result = pstmt.executeUpdate();

		// ó��
//		System.out.println(result + " �� ���Ե�");

		pstmt.close();

		connectionClose(con);
		return result;
	}

	public static int insert(Hero hero) throws SQLException {
		Connection con = connect();

		PreparedStatement pstmt = con.prepareStatement("INSERT INTO " + HERO_TABLE_NAME + " VALUES (?, ?)");
		pstmt.setString(1, hero.getName());
		pstmt.setInt(2, hero.getHp());
		int result = pstmt.executeUpdate();

		// ó��
//		System.out.println(result + " �� ���Ե�");

		pstmt.close();

		connectionClose(con);

		return result;
	}

	public static List<Hero> query(int minHp) throws SQLException {
		Connection con = connect();

		PreparedStatement pstmt = con
				.prepareStatement("SELECT * FROM " + HERO_TABLE_NAME + " WHERE " + HERO_COLUMN_HP + " >= ?");
		pstmt.setInt(1, minHp);
		ResultSet result = pstmt.executeQuery();

		// ó��
		List<Hero> heroList = new ArrayList<Hero>();
		while (result.next()) {
			String name = result.getString(HERO_COLUMN_NAME);
			int hp = result.getInt(HERO_COLUMN_HP);
//			System.out.println(name + ": " + hp);

			Hero hero = new Hero(name, hp);
			heroList.add(hero);
		}

		result.close();
		pstmt.close();

		connectionClose(con);

		return heroList;

	}
}
