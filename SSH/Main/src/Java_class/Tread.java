package Java_class;

public class Tread {
	public static void main(String[] args) throws InterruptedException {
		// 메인 스레드
//		for (int i = 0; i < 10; i++) {
//			System.out.println(i);
//			Thread.sleep(500);
//		}

		// 별도 스레드
		Thread T = new MyThread();
		T.start();

		// 별도 스레드
		Thread T2 = new MyThread();
		T2.start();

		// 별도 스레드
		Thread T3 = new MyThread();
		T3.start();

		// 별도 스레드
		Thread T4 = new Thread(new MyThread2());
		T4.start();

		// 별도 스레드
		Thread T5 = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				for (int i = 0; i < 10; i++) {
					System.out.print(i);
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		T5.start();
		
		Thread T6 = new Thread(() -> {
			for (int i = 0; i < 10; i++) {
				System.out.print(i);
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		T6.start();
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				for (int i = 0; i < 10; i++) {
					System.out.print(i);
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}	
			}
		}).start();
	}
}

class MyThread2 implements Runnable {

	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (int i = 0; i < 10; i++) {
			System.out.print(i);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}

class MyThread extends Thread {

	@Override
	public void run() {
		// TODO Auto-generated method stub
		something();
	}

	private void something() {
		for (int i = 0; i < 10; i++) {
			System.out.print(i);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}