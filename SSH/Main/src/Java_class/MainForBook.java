package Java_class;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class MainForBook {
	public static void main(String[] args) throws ParseException {
		List<Book> Book = new ArrayList<>();
		
		Calendar cal = Calendar.getInstance();
		cal.set(2011, Calendar.MAY, 2);
		Date date = new Date(cal.getTimeInMillis());
		Book book1 = new Book("shin", date, "asdf");
		Book.add(book1);
		
		Calendar cal2 = Calendar.getInstance();
		cal2.set(2013, Calendar.MAY, 2);
		Date date2 = new Date(cal2.getTimeInMillis());
		Book book2 = new Book("shin", date2, "asf");
		Book.add(book2);
		
		Calendar cal3 = Calendar.getInstance();
		cal3.set(2014, Calendar.JUNE, 2);
		Date date3 = new Date(cal3.getTimeInMillis());
		Book book3 = new Book("shin", date3, "asf");
		Book.add(book3);
		
		Collections.sort(Book);
		 
		System.out.println(book1.equals(book2)); //1��
		
		for (int i = 0; i < 3; i++) {
			System.out.println(Book.get(i)); //2��
			
		Book book4 = book1.clone(); //3��
		
		System.out.println(book4); //3��
		}
	}
}
