package Java_class;

public class hamsu {
	public static void main(String[] args) {
		// 1��
		Func1 F1 = n -> n % 2 == 1;
		// 2��
		Func2 F2 = (male, name1) -> {
			if(male == true) {
				return "Mr." + name1;
			}
			return "Ms." + name1;
		};
		
		System.out.println(F1.isOdd(5));
		System.out.println(F2.addNamePrefix(true, "shin"));
	}

	


	
	
}

// 1��
interface Func1 {
	public abstract boolean isOdd(int n);
}

// 2��
interface Func2 {
	public abstract String addNamePrefix(boolean male, String name1);
}
