package Java_class;

public class Hero implements Comparable<Hero>, Cloneable{
	String name;
	int hp;
	

	public Hero(String name, int hp) {
		this.name = name;
		this.hp = hp;
	}
	
	
	
	
	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}




	public int getHp() {
		return hp;
	}




	public void setHp(int hp) {
		this.hp = hp;
	}






	@Override
	public String toString() {
		return "Hero [name=" + name + ", hp=" + hp + "]";
	}




	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + hp;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}




	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Hero other = (Hero) obj;
		if (hp != other.hp)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}




	@Override
	public int compareTo(Hero o) {
		if(hp < o.hp) {
			return -1;
		} else if(hp > o.hp) {
		return 1;
		}
		return 0;
	}


	@Override
	protected Object clone() throws CloneNotSupportedException {
		Hero hero = new Hero(name, hp);
		return hero;
	}
	
}