package Java_class;

import java.io.Serializable;

public class Department implements Serializable {
	private String departmentName;
	private Employee leader;
	
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public Employee getLeader() {
		return leader;
	}
	public void setLeader(Employee leader) {
		this.leader = leader;
	}
	
	
	
	
	
}
