package Java_class;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class URL1 {
	public static void main(String[] args) throws MalformedURLException {
		URL url = new URL("https://alimipro.com/favicon.ico");
//		InputStream is = url.openStream();
		try (BufferedInputStream bis = new BufferedInputStream(url.openStream());
				BufferedOutputStream bos = new BufferedOutputStream(
						new FileOutputStream("C:\\Users\\401-ST05\\eclipse-workspace\\Main\\icon.ico"));) {
			int in = 0;
			while ((in = bis.read()) != -1) {
				bos.write(in);
			} // fos라이트로 isr을 read한것을 넣어요
			bis.close();
			bos.close();

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
//		InputStreamReader isr = new InputStreamReader(is); //인풋스트림리더로 is를 읽었다. 그러면 아웃풋으로 파일을 생성해야지 멍청아 
//		FileOutputStream fos = new FileOutputStream("C:\\Users\\401-ST05\\eclipse-workspace\\Main\\icon.ico");
		// 그래서 아웃풋 스트림으로 파일명을 정해서 만들거야 라고 표시하고~
	}
}
