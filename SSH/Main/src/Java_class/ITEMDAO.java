package Java_class;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ITEMDAO {
	public static final String ITEM_TABLE_NAME = "item";
	public static final String ITEM_COLUMN_NAME = "name";
	public static final String ITEM_COLUMN_PRICE = "price";
	public static final String ITEM_COLUMN_WEIGHT = "weight";

	private String name;
	private int price;
	private int weight;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getPrice() {
		return price;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public int getWeight() {
		return weight;
	}
	
	public void setWeight(int weight) {
		this.weight = weight;
	}
	

	public static ArrayList<ITEM> findByMinimumPrice(int Minimumprice) throws SQLException {
		Connection con = MainForItem.connect();

		PreparedStatement pstmt = con
				.prepareStatement("SELECT * FROM " + ITEM_TABLE_NAME + " WHERE " + ITEM_COLUMN_PRICE + " >= ?"); //SQL에 입력할 것.
		pstmt.setInt(1, Minimumprice); //
		ResultSet result = pstmt.executeQuery();
		
		ArrayList<ITEM> itemList = new ArrayList<ITEM>();
		while (result.next()) {
			String name = result.getString(ITEM_COLUMN_NAME);
			int price = result.getInt(ITEM_COLUMN_PRICE);
			int weight = result.getInt(ITEM_COLUMN_WEIGHT);
			ITEM IT = new ITEM(name, price, weight);
			itemList.add(IT);
		}

		result.close();
		pstmt.close();

		MainForItem.connectionClose(con);

		return  itemList;

	}
}
