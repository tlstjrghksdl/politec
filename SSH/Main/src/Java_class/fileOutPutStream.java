package Java_class;

import java.io.FileOutputStream;
import java.io.IOException;

public class fileOutPutStream {
	public static void main(String[] args) throws IOException {
		FileOutputStream fos = new FileOutputStream("save.dat", true);
		
		fos.write(65);
		fos.write(66);
		fos.write(67);
		fos.flush();
		fos.close();
	}
}
