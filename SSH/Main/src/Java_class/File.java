package Java_class;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class File {
	public static void main(String[] args) throws IOException {
		File.copy("save.dat", "save_copy3.dat");
		System.out.println("Done");
	}
	
	public static void copy(String source, String dest) throws IOException {

	
			FileInputStream fis = new FileInputStream(source);
			FileOutputStream fos = new FileOutputStream(dest);

			byte[] buf = new byte[1024];
			int read;
			while ((read = fis.read(buf)) != -1) 
				fos.write(buf, 0, read);
				fis.close();
				fos.close();
	}
	

} 
