package Java_class;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class fileInAndOutPutStream {
	public static void main(String[] args) throws IOException {
		FileInputStream fis = null;
		FileOutputStream fos = null;

		try {
			fis = new FileInputStream("save.dat");
			fos = new FileOutputStream("save_copy1.dat");

			byte[] buffer = new byte[1024];
			int readcount = 0;

			while ((readcount = fis.read(buffer)) != -1) {
				fos.write(buffer, 0, readcount);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			fis.close();
			fos.close();
		}
	}
}
