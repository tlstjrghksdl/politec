package Java_class;

import java.io.Serializable;

public class Employee extends Department implements Serializable{
	private String name;
	private int age;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return  super.getDepartmentName() + " ���� " + name + "(" + age + ")";
	}
	@Override
	public String getDepartmentName() {
		// TODO Auto-generated method stub
		return super.getDepartmentName();
	}
	@Override
	public void setDepartmentName(String departmentName) {
		// TODO Auto-generated method stub
		super.setDepartmentName(departmentName);
	}
	@Override
	public Employee getLeader() {
		// TODO Auto-generated method stub
		return super.getLeader();
	}
	@Override
	public void setLeader(Employee leader) {
		// TODO Auto-generated method stub
		super.setLeader(leader);
	}
	
	
}
