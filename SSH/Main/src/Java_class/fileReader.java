package Java_class;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class fileReader {

	public static void main(String[] args) throws IOException {
		fileReader.zipExample();
		
		
	}
	
	
	
	public static void zipExample() {
		FileInputStream fis = null;
		BufferedInputStream bis = null;
		GZIPOutputStream gos = null;
		try {
//			fis = new FileInputStream("C:\\Users\\401-ST05\\eclipse-workspace\\Main\\save_copy.dat"); //불러올 파일
			bis = new BufferedInputStream( new FileInputStream("C:\\Users\\401-ST05\\eclipse-workspace\\Main\\save_copy.dat")); //fis  버퍼를 할거야
			gos = new GZIPOutputStream(new FileOutputStream("C:\\Users\\401-ST05\\eclipse-workspace\\Main\\myhouse.zip"));
			// 저장할 위치 output으로 받아 gz로 저장
			
			int i;
			while((i = bis.read()) > -1 ) {
				gos.write(i);
			}
			gos.flush();
		
		} catch (Exception e) {
			System.out.println("Error!");
			// TODO: handle exception
		} finally {
			try {
				if(bis != null) {
					bis.close();
				}
				if(gos != null) {
					gos.close();
				}
				
			} catch (FileNotFoundException e) {
				System.out.println(e.getMessage());
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
    }
}
	
		//file.renameTo
//		File file = new File("save.dat");
//		
//		file.renameTo(new File("C:\\Users\\401-ST05", "shin.dat"));
		
		
		//StringReader
//		String msg = "Hello World";
//
//		StringReader reader = new StringReader(msg);
//
//		char ch1 = (char) reader.read();
//		char ch2 = (char) reader.read();
//
//		System.out.println(ch1);
//		System.out.println(ch2);
		
		
		//ByteArrayOutputStream
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//		baos.write(65);
//		baos.write(66);
//		
//		byte[] data = baos.toByteArray();
//		
//		System.out.println((char) data[1]);
//		System.out.println((char) data[0]);
		
		
		
	
//		FileReader FR = new FileReader("save.dat");
//		
//		int i = FR.read();
//		
//		while (i != -1) {
//			char ch = (char) i;
//			System.out.print(ch);
//			i = FR.read();
//		}
//		FR.close();

//		BufferedReader br = new BufferedReader(new FileReader("save.txt"));
//		String line = null;
//		
//		while((line = br.readLine()) != null) {
//			System.out.println(line);

//		File file = new File("save.dat");
//		file.delete();
