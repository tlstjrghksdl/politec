package Java_class;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;


public class MainForItem {
	public static final String ITEM_TABLE_NAME = "item";
	public static final String ITEM_COLUMN_NAME = "name";
	public static final String ITEM_COLUMN_PRICE = "price";
	public static final String ITEM_COLUMN_WEIGHT = "weight";

	public static void main(String[] args) throws SQLException {
//		ITEM a = new ITEM("b", 5, 3);
//		if (insert(a) > 0) {
//			System.out.println("정상처리");
//		} else {
//			System.out.println("에러");
//		}


		ArrayList<ITEM> items = ITEMDAO.findByMinimumPrice(6);
		for (ITEM item : items) {
			System.out.println(item.getName() + ", " + item.getPrice() + ", " + item.getWeight());
		}

	}

	public static int insert(ITEM item) throws SQLException {
		Connection con = connect();

		PreparedStatement pstmt = con.prepareStatement("INSERT INTO " + ITEM_TABLE_NAME + " VALUES (?, ?, ?)");
		pstmt.setString(1, item.getName());
		pstmt.setInt(2, item.getPrice());
		pstmt.setInt(3, item.getWeight()); 
		int result = pstmt.executeUpdate();

		// ó��
//		System.out.println(result + " �� ���Ե�");

		pstmt.close();

		connectionClose(con);

		return result;
	}

	public static Connection connect() {
		Connection con = null;

		String server = "localhost";
		String database = "item";
		String user_name = "root";
		String password = "SEOK09HWAN19!";

		// 2.����
		try {
			con = DriverManager.getConnection(
					"jdbc:mysql://" + server + "/" + database
							+ "?allowPublicKeyRetrieval=true&characterEncoding=UTF-8&serverTimezone=UTC&useSSL=false",
					user_name, password);
//			����ƴ�
		} catch (SQLException e) {
			System.err.println("con ����:" + e.getMessage());
			e.printStackTrace();
			
			//����
		} 
//		try {
//			// ���� Ŀ������ ����
//			con.setAutoCommit(false);
//			
//			// SQL �۽� ó��
//			con.commit(); //Ŀ��
//		} catch (SQLException e) {
//			try {
//				con.rollback();
//			} catch (SQLException e2) {
//				// �ѹ� ���� ó��
//				e2.printStackTrace();
//			} finally {
//				if(con != null) {
//					try {
//						con.close();
//					} catch (SQLException e3) {
//						e3.printStackTrace();
//					}
//				}
//			}
//		}

		return con;
	}

	public static void connectionClose(Connection con) {
		// 3.����
		try {
			if (con != null)
				con.close();
		} catch (SQLException e) {
		}
	}
}

//	public static List<ITEM> query(int price) throws SQLException {
//		Connection con = connect();
//
//		PreparedStatement pstmt = con
//				.prepareStatement("SELECT * FROM " + ITEM_TABLE_NAME + " WHERE " + ITEM_COLUMN_PRICE + " >= ?");
//		pstmt.setInt(1, minHp);
//		ResultSet result = pstmt.executeQuery();
//
//		// ó��
//		List<ITEM> heroList = new ArrayList<ITEM>();
//		while (result.next()) {
//			String name = result.getString(ITEM_COLUMN_NAME);
//			int price = result.getInt(ITEM_COLUMN_PRICE);
//			int weight = result.getInt(ITEM_COLUMN_WEIGHT);
////			System.out.println(name + ": " + hp);
//
//			ITEM item = new ITEM(name, price, weight);
//			heroList.add(item);
//		}
//
//		result.close();
//		pstmt.close();
//
//		connectionClose(con);
//
//		return heroList;

//	public static ArrayList<Hero> findHerosByMinHp(Connection connection, int minHP) {
//		String sql = "select * from " + HERO_TABLE_NAME + " where hp > ?";
//		List<Hero> heroList = new ArrayList<Hero>();
//		try{
//			PreparedStatement pstmt = connection.prepareStatement(sql);
//			pstmt.setInt(1,  minHP);
//			ResultSet result = pstmt.executeQuery();
//			
//			while(result.next()) {
//				long id = result.getLong("id");
//				String name = result.getString("name");
//				int hp = result.getInt("hp");
//			}
//			
//			result.close();
//			pstmt.close();
//			
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
