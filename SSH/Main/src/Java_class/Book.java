package Java_class;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Book implements Comparable<Book>, Cloneable{
	private String title;
	private Date PublishDate;
	private String comment;
	
	
	public Book(String title, Date PublishDate, String comment) {
		this.title = title;
		this.PublishDate = PublishDate;
		this.comment = comment;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getPublishDate() {
		return PublishDate;
	}

	public void setPublishDate(Date publishDate) {
		PublishDate = publishDate;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	

	@Override
	public String toString() {
		SimpleDateFormat SDF = new SimpleDateFormat("yyyy/ MM/ dd");
		String s = SDF.format(PublishDate);
		return "Book [title= " + title + ", PublishDate= " + s + ", comment= " + comment + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((PublishDate == null) ? 0 : PublishDate.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (PublishDate.toString() == null) {
			if (other.PublishDate.toString() != null)
				return false;
		} else if (!PublishDate.toString().equals(other.PublishDate.toString()))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	
	@Override
	public int compareTo(Book o) {
		return PublishDate.compareTo(PublishDate);
	}

	@Override
	protected Book clone() {
		Book result = new Book(title, PublishDate, comment);
		result.title = this.title;
		result.PublishDate = (Date) this.PublishDate.clone();
		result.comment = this.comment;
		return result;
	}
	
	
}
