package Java_class;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Human implements Serializable{
	private static final long serialVersionUID = 1L;
	private String name;
	private int hp;
	
	public void human(String name, int hp) {
		this.name = name;
		this.hp = hp;
	}

	@Override
	public String toString() {
		return "Human [name=" + name + ", hp=" + hp + "]";
	}
}
