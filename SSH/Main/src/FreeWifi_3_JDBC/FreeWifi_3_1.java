package FreeWifi_3_JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

public class FreeWifi_3_1 {
//	public static void main(String[] args) throws ClassNotFoundException, SQLException {
//		Class.forName("com.mysql.cj.jdbc.Driver"); 
//		String kopo36_url = "jdbc:mysql://192.168.23.105:33061/kopo36?serverTimezone=UTC"; // DB�� �뿰寃고븷 ip瑜� 192.168.23.105濡� �궡 ip�꽕�젙. �룷�듃�룷�썙�뵫�쓣 33061濡� �꽕�젙�빐�넧湲� �븣臾몄뿉 33061濡� �븳�떎
//		Connection kopo36_conn = DriverManager.getConnection(kopo36_url, "root", "kopo36"); // connection�쑝濡� DB�� �뿰寃� �븯�뒗 寃�. �뿰寃고븷 url怨� DB�뿉 留뚮뱺 root沅뚰븳�옄�� passwd瑜� �꽔�뒗�떎
//		Statement kopo36_stmt = kopo36_conn.createStatement(); // statement �뒗 SQL 吏덉쓽臾몄쓣 �쟾�떖�븯�뒗 �뿭�븷�쓣 �븳�떎. 
//												 //Statement �씤�꽣�럹�씠�뒪瑜� 援ы쁽�븳 媛앹껜瑜� Connection �겢�옒�뒪�쓽 createStatement()硫붿냼�뱶瑜� �샇異쒗븿�쑝濡쒖뜥 �뼸�뼱吏꾨떎
//		
//		double kopo36_lat = 37.3860521; //�궡 �쐞�룄 蹂��닔瑜� �꽑�뼵�븳�떎.
//		double kopo36_lng = 127.1214038; //�궡 寃쎈룄 蹂��닔瑜� �꽑�뼵�븳�떎. 利� �궡�쐞移�.
//		
//		String kopo36_QueryTxt = null;
//		kopo36_QueryTxt = String.format("select * from kopo36_freewifi where "
//				+ "sqrt( pow( latitude-%f,2) + pow( longitude-%f,2)) = " // 嫄곕━ 援ы븯�뒗 怨듭떇�쓣 �쟻�슜�븳�떎. 
//				+ "(select min( sqrt( power( latitude-%f,2) + power( longitude-%f,2))) from kopo36_freewifi);",  // 理쒖냼媛믪씠 嫄곕━ 援ы븳 怨듭떇怨� �씪移섑븯硫� 異쒕젰�맂�떎.
//				+ kopo36_lat, kopo36_lng, kopo36_lat, kopo36_lng);
////		kopo36_QueryTxt = "select * from kopo36_freewifi"; //�쟾泥대�� �떎 異쒕젰�븯�뒗 select荑쇰━
////		kopo36_QueryTxt = "select * from kopo36_freewifi where service_provider = 'SKT'"; //�꽌鍮꾩뒪 �젣怨듭옄媛� 'SKT'�씤 �뻾�쓣 李얠븘�꽌 異쒕젰�븳�떎.
////		kopo36_QueryTxt = "select * from kopo36_freewifi where inst_country = '�닔�썝�떆'"; //�꽕移섏떆. 援�. 援� �뿉�꽌 '�닔�썝�떆'�씤 �뻾�쓣 李얠븘�꽌 異쒕젰�븳�떎.
//		
//		ResultSet kopo36_rset = kopo36_stmt.executeQuery(kopo36_QueryTxt); //�굹�삩 媛믪쓣 executeQuery臾몄뿉 �꽔�뒗�떎
//		int kopo36_Cnt = 0;
//		while (kopo36_rset.next()) { // ResultSe�쓽 next()硫붿냼�뱶瑜� �씠�슜�븯�뿬, 媛믪씠  紐⑤몢 異쒕젰�맆 �븣源뚯� 硫덉텛吏� �븡�뒗 while臾몄쓣 留뚮뱺�떎.
//			System.out.printf("*(%d)*********************************************\n", kopo36_Cnt++);
//			System.out.printf("�꽕移� �옣�냼紐�           : %s\n",kopo36_rset.getString(1)); //�뼸��媛� 1�뿴 ~ 15�뿴源뚯� 異쒕젰�릺�룄濡� �븳�떎.
//			System.out.printf("�꽕移섏옣�냼�긽�꽭         :	%s\n",kopo36_rset.getString(2));
//			System.out.printf("�꽕移� �떆.�룄 紐�        :	%s\n",kopo36_rset.getString(3));
//			System.out.printf("�꽕移� �떆.援�.援� 紐�    : %s\n",kopo36_rset.getString(4));
//			System.out.printf("�꽕移� �떆�꽕 援щ텇        :	%s\n",kopo36_rset.getString(5));
//			System.out.printf("�꽌鍮꾩뒪 �젣怨듭옄 紐�     :	%s\n",kopo36_rset.getString(6));
//			System.out.printf("wifi SSID     :	%s\n",kopo36_rset.getString(7)); 
//			System.out.printf("�꽕移� �뀈�썡              :	%s\n",kopo36_rset.getString(8));
//			System.out.printf("�냼�옱吏� �룄濡쒕챸 二쇱냼   :	%s\n",kopo36_rset.getString(9));  
//			System.out.printf("�냼�옱吏� 吏�踰� 二쇱냼     :	%s\n",kopo36_rset.getString(10));
//			System.out.printf("愿�由ш린愿�紐�            :	%s\n",kopo36_rset.getString(11));
//			System.out.printf("愿�由� 湲곌� �쟾�솕踰덊샇   :	%s\n",kopo36_rset.getString(12));
//			System.out.printf("�쐞�룄                    :	%s\n",kopo36_rset.getFloat(13));
//			System.out.printf("寃쎈룄                    :	%s\n",kopo36_rset.getFloat(14));
//			System.out.printf("�뜲�씠�꽣 湲곗� �씪�옄      :	%s\n",kopo36_rset.getDate(15));
//			System.out.printf("***********************************************\n");
//		}                               
//		kopo36_rset.close();
//		kopo36_stmt.close();
//		kopo36_conn.close();
//	}
}
