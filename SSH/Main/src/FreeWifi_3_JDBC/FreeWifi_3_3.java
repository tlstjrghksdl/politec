package FreeWifi_3_JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class FreeWifi_3_3 {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver"); 
		String kopo36_url = "jdbc:mysql://192.168.23.105:33061/kopo36?serverTimezone=UTC"; // DB와 연결할 ip를 192.168.23.105로 내 ip설정. 포트포워딩을 33061로 설정해놨기 때문에 33061로 한다
		Connection kopo36_conn = DriverManager.getConnection(kopo36_url, "root", "kopo36"); // connection으로 DB와 연결 하는 것. 연결할 url과 DB에 만든 root권한자와 passwd를 넣는다
		Statement kopo36_stmt = kopo36_conn.createStatement(); // statement 는 SQL 질의문을 전달하는 역할을 한다. 
												 //Statement 인터페이스를 구현한 객체를 Connection 클래스의 createStatement()메소드를 호출함으로써 얻어진다
		
		double kopo36_lat = 37.3860521; //내 위도 변수를 선언한다.
		double kopo36_lng = 127.1214038; //내 경도 변수를 선언한다. 즉 내위치.
		
		String kopo36_QueryTxt = null;
//		kopo36_QueryTxt = String.format("select * from kopo36_freewifi where "
//				+ "sqrt( pow( latitude-%f,2) + pow( longitude-%f,2)) = " // 거리 구하는 공식을 적용한다. 
//				+ "(select min( sqrt( power( latitude-%f,2) + power( longitude-%f,2))) from kopo36_freewifi);",  // 최소값이 거리 구한 공식과 일치하면 출력된다.
//				+ kopo36_lat, kopo36_lng, kopo36_lat, kopo36_lng);
//		kopo36_QueryTxt = "select * from kopo36_freewifi"; //전체를 다 출력하는 select쿼리
		kopo36_QueryTxt = "select * from kopo36_freewifi where service_provider = 'SKT'"; //서비스 제공자가 'SKT'인 행을 찾아서 출력한다.
//		kopo36_QueryTxt = "select * from kopo36_freewifi where inst_country = '수원시'"; //설치시. 군. 구 에서 '수원시'인 행을 찾아서 출력한다.
		
		ResultSet kopo36_rset = kopo36_stmt.executeQuery(kopo36_QueryTxt); //나온 값을 executeQuery문에 넣는다
		int kopo36_Cnt = 0;
		while (kopo36_rset.next()) { // ResultSe의 next()메소드를 이용하여, 값이  모두 출력될 때까지 멈추지 않는 while문을 만든다.
			System.out.printf("*(%d)*********************************************\n", kopo36_Cnt++);
			System.out.printf("설치 장소명           : %s\n",kopo36_rset.getString(1)); //얻은값 1열 ~ 15열까지 출력되도록 한다.
			System.out.printf("설치장소상세         :	%s\n",kopo36_rset.getString(2));
			System.out.printf("설치 시.도 명        :	%s\n",kopo36_rset.getString(3));
			System.out.printf("설치 시.군.구 명    : %s\n",kopo36_rset.getString(4));
			System.out.printf("설치 시설 구분        :	%s\n",kopo36_rset.getString(5));
			System.out.printf("서비스 제공자 명     :	%s\n",kopo36_rset.getString(6));
			System.out.printf("wifi SSID     :	%s\n",kopo36_rset.getString(7)); 
			System.out.printf("설치 년월              :	%s\n",kopo36_rset.getString(8));
			System.out.printf("소재지 도로명 주소   :	%s\n",kopo36_rset.getString(9));  
			System.out.printf("소재지 지번 주소     :	%s\n",kopo36_rset.getString(10));
			System.out.printf("관리기관명            :	%s\n",kopo36_rset.getString(11));
			System.out.printf("관리 기관 전화번호   :	%s\n",kopo36_rset.getString(12));
			System.out.printf("위도                    :	%s\n",kopo36_rset.getFloat(13));
			System.out.printf("경도                    :	%s\n",kopo36_rset.getFloat(14));
			System.out.printf("데이터 기준 일자      :	%s\n",kopo36_rset.getDate(15));
			System.out.printf("***********************************************\n");
		}                               
		kopo36_rset.close();
		kopo36_stmt.close();
		kopo36_conn.close();
	}
}
