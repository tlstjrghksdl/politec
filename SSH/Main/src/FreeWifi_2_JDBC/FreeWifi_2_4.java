package FreeWifi_2_JDBC;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FreeWifi_2_4 {
	public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException, ParseException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		String kopo36_url = "jdbc:mysql://192.168.23.105:33061/kopo36?serverTimezone=UTC";
		Connection kopo36_conn = DriverManager.getConnection(kopo36_url, "root", "kopo36");
		Statement kopo36_stmt = kopo36_conn.createStatement();
		File kopo36_f = new File("C:\\Users\\401-ST05\\Desktop\\�쟾援�臾대즺���씠�뙆�씠�몴以��뜲�씠�꽣.txt");
		// �뙆�씪�쓣 遺덈윭�삤�뒗 �옉�뾽. �뙆�씪 寃쎈줈�� �뙆�씪 �씠由꾩쓣 �쟻�뼱以��떎.
		BufferedReader kopo36_br = new BufferedReader(new FileReader(kopo36_f));
		// �뙆�씪�쓣 �씫�뒗 �옉�뾽. FileReader蹂대떎 醫뗫떎.
		String kopo36_readtxt;
		// �씫�뼱�삱 �뀓�뒪�듃瑜� String�쑝濡� �븳�떎.
		if((kopo36_readtxt = kopo36_br.readLine()) == null) { 
			//readLine 硫붿냼�뱶�뒗 �뀓�뒪�듃瑜� �븳以꾩뵫 諛쏆븘�삤�뒗 硫붿냼�뱶�씠�떎. 諛쏆븘�삤�뒗 媛믪씠 �뾾�쑝硫�(null�씠硫�) 鍮덊뙆�씪 泥섎━.
			System.out.printf("鍮� �뙆�씪�엯�땲�떎\n");
			return;
		}
		
		
		SimpleDateFormat kopo36_sdf_ddMMM = new SimpleDateFormat("yy-MMM", Locale.ENGLISH);
		//	yy-MMM�삎�떇�쑝濡� 諛쏆븘�삤�뒗 媛믪쓣 SimpleDateFormat�쑝濡� �삎�떇�쓣 諛붽퓭二쇰뒗 怨쇱젙�씠�떎.
		//	MMM�쑝濡� �뱾�뼱�삤�뒗 �쁺�뼱濡쒕맂 '�썡' 媛믪� ENGLISH濡� 濡쒖뺄 蹂�寃쎌쓣 �빐以섏빞�븳�떎.
		SimpleDateFormat kopo36_sdf_yyyyMM = new SimpleDateFormat("yyyyMM");
		SimpleDateFormat kopo36_sdf_yyyy = new SimpleDateFormat("yyyy"); 
		SimpleDateFormat kopo36_sdf_yyyy_MM = new SimpleDateFormat("yyyy.MM");
		SimpleDateFormat kopo36_sdf_yyyyMMM = new SimpleDateFormat("yyyy-MMM", Locale.ENGLISH);
		//	�삎�깭媛� �떎瑜� �궇吏쒕뱾�쓣 醫낅쪟蹂꾨줈 李얠븘�꽌 �삎�떇�쓣 紐⑤몢 kopo36_original�쓽 媛믪쿂�읆 諛붽퓭二쇨린 �쐞�븳 寃껋씠�떎.
		//	txt�뙆�씪�뿉�꽌 �뱾�뼱�삩 媛믪쓣 �뜲�씠�듃 �룷留룹쓣 �꽕�젙 �빐以��떎�쓬 洹� 媛믪쓣 DB�뿉 �뱾�뼱媛� �닔 �엳�뒗 �삎�떇�쑝濡� 諛붽씔�떎.
		
		SimpleDateFormat kopo36_original = new SimpleDateFormat("yyyy-MM-dd");
		//	java�쓽 date�삎�떇�쑝濡� 蹂��솚�맂 媛믪쓣 DB�뿉 �꽔�쓣 �삎�떇�쑝濡� 諛붽퓭二쇰뒗 �옉�뾽�씠�떎. 
		// (ex : Fri May 01 00:00:00 KST 2015 -> 2015-05-01)
	
		// String[] kopo36_field_name = kopo36_readtxt.split("\t");
		int kopo36_Linecnt=0; 
		//紐뉕컻�쓽 �씪�씤�씠 �엳�뒗吏� 移댁슫�듃 �븯湲� �쐞�븳 �옉�뾽 
		while ((kopo36_readtxt=kopo36_br.readLine()) != null) {
			//readLine�씠 �븳以꾩뵫 �씫�뼱�삤�뒗�뜲 �씫�뼱�삱寃껋씠 �뾾�떎硫�, 利� null�씠�씪硫� while臾몄씠 硫덉텛寃� �븳�떎.
			String[] kopo36_field = kopo36_readtxt.split("\t");
			// field�씪�뒗 String諛곗뿴�쓣 留뚮뱾怨� readLine�쑝濡� �씫�뼱�삩 �뀓�뒪�듃 �븳 以꾩쓣 tab�쓣 援щ텇�옄濡� �굹�늿�떎.
			String kopo36_QueryTxt;
			// QueryTxt�씪�뒗 String蹂��닔瑜� �꽑�뼵.
			// QueryTxt�뿉�뒗 DB�뿉 留뚮뱺 table�뿉 �뱾�뼱媛� 紐낅졊臾몄쓣 �옉�꽦�븳�떎.
			
			if (kopo36_field[7].matches("[0-9]{2}-[A-Z][a-z]{2}")) { //yyMMM
				// 泥ル쾲吏� �삎�떇�� �뀈(�닽�옄2媛�)-�썡(�쁺�뼱)�씠湲� �븣臾몄뿉 �젙洹� �몴�쁽�떇�쑝濡� �씠�젃寃� �릺�뼱�엳�뒗 �삎�떇�씠 �엳�떎硫� 李얠븘以��떎
				Date kopo36_first = kopo36_sdf_ddMMM.parse(kopo36_field[7]);
				// 李얠� 媛믪쓣 �쐞�뿉 �씤�뒪�꽩�뒪�솕�븳 SimpleDateFormat�쓣 �씠�슜�븯�뿬 java�쓽 date�삎�떇�쑝濡� 蹂��솚�븳�떎 
				// (ex : 15-May -> Fri May 01 00:00:00 KST 2015 �뿬湲곗꽌 諛쏆븘�삤吏� �븡�� '�씪' 媛믪� �옄�룞�쑝濡� 1�씪�씠 �맂�떎)
				String kopo36_realFirst = kopo36_original.format(kopo36_first);
				// java�쓽 date�삎�떇�쑝濡� 蹂��솚�맂 媛믪쓣 DB�뿉 �꽔�쓣 �삎�떇�쑝濡� 諛붽퓭二쇰뒗 �옉�뾽�씠�떎.
				// (ex : Fri May 01 00:00:00 KST 2015 -> 2015-05-01)
				kopo36_field[7] = kopo36_realFirst;
				// DB�뿉 �뱾�뼱媛� 以�鍮꾧� �맂 �궇吏� �삎�떇�쓣 �떎�떆 kopo36_field[7]�뿉 �꽔�뼱以��떎
			} else if (kopo36_field[7].matches("[0-9]{6}")) { //yyyyMM
				// �몢踰덉㎏ �삎�떇�� yyyyMM�쑝濡� 6�옄由� 紐⑤몢 �닽�옄�씠湲� �븣臾몄뿉 �젙洹쒗몴�쁽�떇�쑝濡� 6�옄由� 紐⑤몢 �닽�옄媛� �삱 寃쎌슦瑜� 寃�異� �빐二쇰뒗 怨쇱젙�씠�떎
				Date kopo36_Second = kopo36_sdf_yyyyMM.parse(kopo36_field[7]);
				// (ex : 201505 -> Fri May 01 00:00:00 KST 2015 �뿬湲곗꽌 諛쏆븘�삤吏� �븡�� '�씪' 媛믪� �옄�룞�쑝濡� 1�씪�씠 �맂�떎)
				String kopo36_realSecond = kopo36_original.format(kopo36_Second);
				// (ex : Fri May 01 00:00:00 KST 2015 -> 2015-05-01)
				kopo36_field[7] = kopo36_realSecond;
			} else if(kopo36_field[7].matches("[0-9]{4}")) { //yyyy
				Date kopo36_Third = kopo36_sdf_yyyy.parse(kopo36_field[7]);
				// (ex : yyyy -> Fri Jan 01 00:00:00 KST 2015 �뿬湲곗꽌 諛쏆븘�삤吏� �븡�� '�썡, �씪' 媛믪� �옄�룞�쑝濡� 1�씪�씠 �맂�떎)
				String kopo36_realThird = kopo36_original.format(kopo36_Third);
				// (ex : Fri May 01 00:00:00 KST 2015 -> 2015-05-01)
				kopo36_field[7] = kopo36_realThird;
			} else if (kopo36_field[7].matches("[0-9]{4}.[0-9]{2}")) { //yyyy.MM
				Date kopo36_Fourth = kopo36_sdf_yyyy_MM.parse(kopo36_field[7]);
				// (ex : 2015.05 -> Fri May 01 00:00:00 KST 2015 �뿬湲곗꽌 諛쏆븘�삤吏� �븡�� '�씪' 媛믪� �옄�룞�쑝濡� 1�씪�씠 �맂�떎)
				String kopo36_realFourth = kopo36_original.format(kopo36_Fourth);
				// (ex : Fri May 01 00:00:00 KST 2015 -> 2015-05-01)
				
				kopo36_field[7] = kopo36_realFourth;
			} else if (kopo36_field[7].matches("[0-9]{4}-[A-Z][a-z]{2}")) { // yyyy-MMM
				Date kopo36_Fifth = kopo36_sdf_yyyyMMM.parse(kopo36_field[7]);
				// (ex : 2015-May -> Fri May 01 00:00:00 KST 2015 �뿬湲곗꽌 諛쏆븘�삤吏� �븡�� '�씪' 媛믪� �옄�룞�쑝濡� 1�씪�씠 �맂�떎)
				String kopo36_realFifth = kopo36_original.format(kopo36_Fifth);
				// (ex : Fri May 01 00:00:00 KST 2015 -> 2015-05-01)
				kopo36_field[7] = kopo36_realFifth;
			} else if (kopo36_field[7].matches("")) { // empty
				kopo36_field[7] = "";
				//鍮꾩뼱�엳�뒗 移쇰읆移몄� 0-01-01濡� �젙�빐以��떎. �븵�꽌 �떎�뒿�븳 怨쇱젙�뿉�꽌 紐⑤뱺 移쇰읆�뿉 primary key瑜� �쟻�슜�떆耳곌린 �븣臾몄뿉 null媛믪씠 �뱾�뼱媛� �닔 �뾾�떎.
			} else {
				kopo36_field[7] = kopo36_field[7];
				// �씠誘� DB�뿉 �뱾�뼱媛� �삎�떇�쑝濡� 諛쏆븘�삩 媛믪� 洹몃깷 洹몃�濡쒖씠�떎.
			}
			
			kopo36_QueryTxt = String.format("insert ignore into kopo36_freewifi values ("
					+ "'%s', '%s', '%s', '%s', '%s', "
					+ "'%s', '%s', '%s', '%s', '%s', "
					+ "'%s', '%s', %s, %s, '%s');", 
					kopo36_field[0], kopo36_field[1], kopo36_field[2], kopo36_field[3], kopo36_field[4], 
					kopo36_field[5], kopo36_field[6], kopo36_field[7], kopo36_field[8], kopo36_field[9], 
					kopo36_field[10], kopo36_field[11], kopo36_field[12], kopo36_field[13], kopo36_field[14]);
			kopo36_stmt.execute(kopo36_QueryTxt);
			// execute�븯�뿬 QueryTxt瑜� DB濡� �뿰寃고빐�꽌 紐낅졊�뼱 �떎�뻾�쓣 �떆�궓�떎.
			System.out.printf("%d踰덉㎏ �빆紐� insert OK [%s]\n", kopo36_Linecnt, kopo36_QueryTxt);
			// 紐� 踰덉㎏源뚯� �꽦怨듯뻽�뒗吏� �굹���궡湲� �쐞�빐 異쒕젰媛믪쓣 �꽔�뼱以��떎.
			kopo36_Linecnt++;
			// count瑜� �꽦怨듯븷�븣留덈떎 1�뵫 利앷��떆耳� 珥� 紐뉗쨪�씠 �꽦怨듯뻽�뒗吏� �븣 �닔 �엳寃� �븳�떎.
		}
		
		kopo36_br.close();
		// �븞�떕�븘二쇰㈃ �봽濡쒓렇�옩 二쎈뒗�떎
		kopo36_stmt.close();
		// �븞�떕�븘二쇰㈃ �봽濡쒓렇�옩 二쎈뒗�떎
		kopo36_conn.close();
		// �븞�떕�븘二쇰㈃ �봽濡쒓렇�옩 二쎈뒗�떎
	}
}
