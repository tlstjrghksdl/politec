package FreeWifi_2_JDBC;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FreeWifi_2_2 {
	public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException, ParseException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		String kopo36_url = "jdbc:mysql://192.168.23.105:33061/kopo36?serverTimezone=UTC";
		Connection kopo36_conn = DriverManager.getConnection(kopo36_url, "root", "kopo36");
		Statement kopo36_stmt = kopo36_conn.createStatement();
		File kopo36_f = new File("C:\\Users\\401-ST05\\Desktop\\전국무료와이파이표준데이터.txt");
		// 파일을 불러오는 작업. 파일 경로와 파일 이름을 적어준다.
		BufferedReader kopo36_br = new BufferedReader(new FileReader(kopo36_f));
		// 파일을 읽는 작업. FileReader보다 좋다.
		String kopo36_readtxt;
		// 읽어올 텍스트를 String으로 한다.
		if((kopo36_readtxt = kopo36_br.readLine()) == null) { 
			//readLine 메소드는 텍스트를 한줄씩 받아오는 메소드이다. 받아오는 값이 없으면(null이면) 빈파일 처리.
			System.out.printf("빈 파일입니다\n");
		}
		
		 
		
		
//		String[] kopo36_field_name = kopo36_readtxt.split("\t");
		int kopo36_Linecnt=0; 
		//몇개의 라인이 있는지 카운트 하기 위한 작업 
		while ((kopo36_readtxt=kopo36_br.readLine()) != null) {
			//readLine이 한줄씩 읽어오는데 읽어올것이 없다면, 즉 null이라면 while문이 멈추게 한다.
			String[] kopo36_field = kopo36_readtxt.split("\t");
			// field라는 String배열을 만들고 readLine으로 읽어온 텍스트 한 줄을 tab을 구분자로 나눈다.
			String kopo36_QueryTxt;
			// QueryTxt라는 String변수를 선언.
			// QueryTxt에는 DB에 만든 table에 들어갈 명령문을 작성한다.
			
			
//			kopo36_QueryTxt = String.format("insert ignore into kopo36_freewifi values ("
//					+ "'%s', '%s', '%s', '%s', '%s', "
//					+ "'%s', '%s', '%s', '%s', '%s', "
//					+ "'%s', '%s', %s, %s, '%s');", 
//					kopo36_field[0], kopo36_field[1], kopo36_field[2], kopo36_field[3], kopo36_field[4], 
//					kopo36_field[5], kopo36_field[6], kopo36_field[7], kopo36_field[8], kopo36_field[9], 
//					kopo36_field[10], kopo36_field[11], kopo36_field[12], kopo36_field[13], kopo36_field[14]);
//			kopo36_stmt.execute(kopo36_QueryTxt);
//			// execute하여 QueryTxt를 DB로 연결해서 명령어 실행을 시킨다.
//			System.out.printf("%d번째 항목 insert OK [%s]\n", kopo36_Linecnt, kopo36_QueryTxt);
//			// 몇 번째까지 성공했는지 나타내기 위해 출력값을 넣어준다.
//			kopo36_Linecnt++;
//			// count를 성공할때마다 1씩 증가시켜 총 몇줄이 성공했는지 알 수 있게 한다.
		}
		
		kopo36_br.close();
		// 안닫아주면 프로그램 죽는다
		kopo36_stmt.close();
		// 안닫아주면 프로그램 죽는다
		kopo36_conn.close();
		// 안닫아주면 프로그램 죽는다
	}
}
