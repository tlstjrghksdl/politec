package FreeWifi_2_JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class FreeWifi_2_1 {
	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			String url = "jdbc:mysql://192.168.23.105:33061/kopo36?serverTimezone=UTC";
			Connection conn = DriverManager.getConnection(url, "root", "kopo36");
			Statement stmt = conn.createStatement();      // 호출함으로써 얻어진다            
			stmt.execute("create table kopo36_freewifi("  // kopo36_Wife 테이블을 만듬    
					+ "inst_place varchar(100), "         // 설치 장소명                 
					+ "inst_place_detail varchar(200), "  // 설치장소상세                 
					+ "inst_city varchar(8), "            // 설치 시.도 명                    
					+ "inst_country varchar(10), "        // 설치 시.군.구 명                  
					+ "inst_place_flag varchar(10), "     // 설치 시설 구분                    
					+ "service_provider varchar(20), "    // 서비스 제공자 명                   
					+ "wifi_ssid varchar(50), "           // wifi SSID                   
					+ "inst_date varchar(10), "           // 설치 년월 -> 정제할 것              
					+ "place_addr_road varchar(80), "     // 소재지 도로명 주소                  
					+ "place_addr_land varchar(50), "     // 소재지 지번 주소                   
					+ "manage_office varchar(50), "       // 관리기관명                       
					+ "manage_office_phone varchar(15), " // 관리 기관 전화번호                  
					+ "latitude double, "                 // 위도                          
					+ "longitude double, "                // 경도                          
					+ "write_date date);"); 			  // 데이터 기준 일자
			
				
			stmt.close();
			conn.close();	
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
}
