package webtoons;

import java.util.Scanner;

public class start {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		System.out.println("Welcome to Juan's Webtoon!");
		System.out.println();
		System.out.println("1. watch toons");
		System.out.println("2. watch likeRank");

		int watch = sc.nextInt();
		if (watch == 1) {
			select();
		} else if (watch == 2) {
			rankWebtoons.likeRank();
		} else {
			System.out.println("Error");
		}
	}

	public static void select() {
		while (true) {
			System.out.println("Select day!");
			System.out.println();
			System.out.println("1. Monday");
			System.out.println("2. Tuesday");
			System.out.println("3. Wednesday");
			System.out.println("4. Thursday");
			System.out.println("5. Friday");
			System.out.println("6. Saturday");
			System.out.println("7. Sunday");
			System.out.println("0. End");
			int selectDay = sc.nextInt();

			if (selectDay == 1) {
				Monday.Mon();
			} else if (selectDay == 2) {
				Tuesday.Tue();
			} else if (selectDay == 3) {
				Wednesday.Wed();
			} else if (selectDay == 4) {
				Thursday.Tur();
			} else if (selectDay == 5) {
				Friday.Fri();
			} else if (selectDay == 6) {
				Saturday.Sat();
			} else if (selectDay == 7) {
				Sunday.Sun();
			} else if (selectDay == 0) {
				System.out.println("See ya!");
				break;
			} else {
				System.out.println("Error!!");

			}
		}
	}
}
