package Databases;

import java.util.Scanner;

import Chulsuk.inputSQL;

public class Homework1 {
	static inputSQL mysql = new inputSQL();

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("########################################");
		System.out.println("############# MY DATABASE ##############");
		System.out.println("########################################");
		System.out.println("[1] LIST OF DATABASE");
		System.out.println("[2] USE DATABASE");
		System.out.println("[3] LIST OF TABLES");
		System.out.println("[4] SELECT * FROM A TABLE");
		System.out.print("Input number : ");
		int a = sc.nextInt();
		if (a == 1) {
			inputSQL.getsql("SHOW DATABASES;");
		} else if (a == 2) {
			System.out.println("Enter Database");
			String b = sc.next();
			HOMEWORK.getsql2(b, b);
		} else if (a == 3) {
			System.out.println("Enter Database");
			String b = sc.next();
			inputSQL.getsql3(b);
		} else if (a == 4) {
			System.out.println("Enter Database and Table Name");
			String DB = sc.next();
			String TN = sc.next();
			inputSQL.getsql4(DB, TN);
		}
	}

}