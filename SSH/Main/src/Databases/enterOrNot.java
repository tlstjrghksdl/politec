package Databases;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class enterOrNot {

	public static void main(String[] args) {
		//
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		String id = "root";
		String password = "SEOK09HWAN19!";
		String useDB = "schoolmanagement";
		StringBuilder sb = new StringBuilder();

		sb.append("create table enterOrNot(").append("NO INT," + "classroom_1 int, classroom_2 int, classroom_3 int, "
				+ "FOREIGN KEY(NO) REFERENCES Students(NO) ON UPDATE CASCADE);");

		String dbQuery = sb.toString();

		try {
			connection = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/schoolmanagement?useUnicode=true&characterEncoding=utf8&serverTimezone=Asia/Seoul&useSSL=false",
					id, password);
			statement = connection.createStatement();

			if (statement.execute(dbQuery)) {
				resultSet = statement.getResultSet();
			}

			while (resultSet != null && resultSet.next()) {
				String str = resultSet.getString(1);

				System.out.println(str);
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}