
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class passwd {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String raw = "1234";
		System.out.println("before : " + raw);
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		md.update(raw.getBytes());
		String hex = String.format("%064x", new BigInteger(1, md.digest()));
		System.out.println("after : " + hex);
	}

}
