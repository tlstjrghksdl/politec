package Algorism;

import java.util.Scanner;

public class D156 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		int b = 2;
		for (int i = 0; i < a - 1; i++) {
			b = b*2;
		}
		
		System.out.println(b);
	}
}
