package JavaProgramming_5;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

class Taxcalc {
	
	//세금 구하기
	static int kopo36_taxcal(int kopo36_realPrice, int kopo36_tax) {
		int kopo36_return;
		// 리턴할 int변수를 미리 선언 해둔다.
		if(((double)kopo36_realPrice * (double) kopo36_tax / 100) == kopo36_realPrice * kopo36_tax / 100) {
		// 앞에서 실습해했던 것 처럼, 세전가와 세율을 100으로 나눈 값을 곱하면 물건의 세금 값이 나온다.
		// 그것이 소수점이 없을 때 즉, 정수로 나왔을 때 그냥 그대로 사용하면 된다.
			kopo36_return =  kopo36_realPrice * kopo36_tax / 100;
		} else {
			kopo36_return =  kopo36_realPrice * kopo36_tax / 100 + 1;
		// 소수점이 나왔을 때는 소수점도 세금이기 때문에 버리면 기업이 손해를 보기 때문에 1을 더한 정수로 리턴 값을 초기화해준다.
		}
		return kopo36_return;
		//메소드에서 나온 int값을 내보낸다.
	}
}
  
public class Programming_5_4_recipt {
	public static void main(String[] args) {
		DecimalFormat kopo36_df = new DecimalFormat("###,###,###,###");
		// 정수에 콤마를 찍을 수 있게 형식을 정하는 라이브러리이다.
		// 돈의 형식과 맞게 콤마를 3자리당 한번씩 찍게 해준다.
		SimpleDateFormat kopo36_sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat kopo36_sdf2 = new SimpleDateFormat("yyyyMMdd");
		
		// 날짜 출력 형식을 정할 수 있는 라이브러리이다. 영수증과 같게 한다. MM은 월, mm은 분이다.
		Calendar kopo36_c = Calendar.getInstance(); // 켈린더는 날짜를 받아올 수 있는 라이브러리이다.
		int kopo36_realPrice = 4231; // 세전 물건 값
		int kopo36_tax = 10; // 세율 (10퍼센트)
		
		int kopo36_taxPrice = Taxcalc.kopo36_taxcal(kopo36_realPrice, kopo36_tax);
		// 세금을 구하는 메소드를 불러와서 세전 물건 값과 세율을 넣어 계산하게 한다.
		
		System.out.printf("%22s\n", "영 수 증"); // 앞에 20을 붙여 글자를 포함해 20개의 자리수를 차지 하는 것이다.
		// ex) (         영수증(여기까지20개))           
		System.out.printf("-----------------------------------------\n");
		System.out.printf("%-15s%s\n", "종로상회(분당서현점)", "129-17-77924");
		System.out.printf("이상철 031 781 1596\n"); 
		System.out.printf("성남시 분당구 서현동 269-3\n");
		System.out.printf("테이블명 : 12\n");
		System.out.printf("%6s%s%9s\n","주문번호 : ", kopo36_sdf2.format(kopo36_c.getTime()), "01 00041" );
		System.out.printf("-----------------------------------------\n");
		System.out.printf("%-8s%29s\n", "주문합계", kopo36_df.format(kopo36_realPrice + kopo36_taxPrice));
		// DecimalFormat 을 사용하여 정수에 콤마를 입력하게 하는 것이다. 그렇게 되면 정수가 아닌 문자열이 되기 때문에 %s를 써준다.
		System.out.printf("%-8s%29s\n", "할인금액", 0);
		System.out.printf("%-8s%29s\n", "받을금액", kopo36_df.format(kopo36_realPrice + kopo36_taxPrice));
		// DecimalFormat 을 사용하여 정수에 콤마를 입력하게 하는 것이다. 그렇게 되면 정수가 아닌 문자열이 되기 때문에 %s를 써준다.

		
		System.out.printf("-----------------------------------------\n");
		System.out.printf("%-6s%11s%6s%14s\n", "현  금", "0", "과  세", kopo36_df.format(kopo36_realPrice));
		// DecimalFormat 을 사용하여 정수에 콤마를 입력하게 하는 것이다. 그렇게 되면 정수가 아닌 문자열이 되기 때문에 %s를 써준다.
		System.out.printf("%-6s%11s%6s%14s\n", "카  드", kopo36_df.format(kopo36_realPrice + kopo36_taxPrice), "세  액", kopo36_df.format(kopo36_taxPrice));
		// DecimalFormat 을 사용하여 정수에 콤마를 입력하게 하는 것이다. 그렇게 되면 정수가 아닌 문자열이 되기 때문에 %s를 써준다.
		System.out.printf("%-5s%11s%6s%14s\n", "포인트", "0", "면  세", "0");
		System.out.printf("%25s%12s\n", "영수금액", kopo36_df.format(kopo36_realPrice + kopo36_taxPrice));
		// DecimalFormat 을 사용하여 정수에 콤마를 입력하게 하는 것이다. 그렇게 되면 정수가 아닌 문자열이 되기 때문에 %s를 써준다.
		System.out.printf("-----------------------------------------\n");
		System.out.printf("%23s\n", "[우리카드 신용 승인]");
		System.out.printf("%-10s%5s\n", "승 인 일 시 : " , kopo36_sdf.format(kopo36_c.getTime()));
		// SimpleDateFormat으로 바꿔준 날짜 형식을 넣기 위해 format메소드를 사용하고 날짜를 받아오기 위해 켈린더에 getTime메소드를 받아온다.
		System.out.printf("%-10s%5s\n", "카 드 번 호 : ", "55222059****2021");
		System.out.printf("%-10s%5s%9s%02d\n", "승 인 번 호 : ", "79753574", "할부개월 : ", 0);
		System.out.printf("%-10s%5s\n", "승 인 금 액 : ", kopo36_df.format(kopo36_realPrice + kopo36_taxPrice));
		// DecimalFormat 을 사용하여 정수에 콤마를 입력하게 하는 것이다. 그렇게 되면 정수가 아닌 문자열이 되기 때문에 %s를 써준다.
		// 승인 금액은 세금 + 세전액 이기 때문에 더한다.
		System.out.printf("%-10s%5s%5s\n", "가 맹 번 호 : ", "730461774 / ", "비씨카드사");
		System.out.printf("%-9s%4s\n", "사업자 번호 : ", "129-17-77924");
		System.out.printf("-----------------------------------------\n");
		System.out.printf("%-21s%10s%8s\n", kopo36_sdf.format(kopo36_c.getTime()), "CASHIER : ", "직원");
		// SimpleDateFormat으로 바꿔준 날짜 형식을 넣기 위해 format메소드를 사용하고 날짜를 받아오기 위해 켈린더에 getTime메소드를 받아온다.
		System.out.printf("-----------------------------------------\n");
		System.out.printf("%s", "감사 합니다.");
	}
}
