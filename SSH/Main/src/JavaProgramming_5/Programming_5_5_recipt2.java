package JavaProgramming_5;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

class Taxcalc2 { // 세전가 구하는 공식
	static int kopo36_taxcal2(int kopo36_Price, int kopo36_taxrate) {
		int kopo36_re;
		if (kopo36_Price != 10) {
			if((int)(kopo36_Price / (1 + kopo36_taxrate / 100.0)) == (double)(kopo36_Price / (1 + kopo36_taxrate / 100.0))) {
				// int형과 double형이 같으면, 즉 소수점이 없으면, 
				kopo36_re = (int)(kopo36_Price / (1 + kopo36_taxrate / 100.0));
				// 그냥 그 값 그대로 사용한다.
			} else {
				kopo36_re = 1 + (int)((kopo36_Price / (1 + kopo36_taxrate / 100.0)));
				// 세전가가 소수점이 나오면 + 1 해준다
			}
		} else {
			kopo36_re = (int)(kopo36_Price / (1 + kopo36_taxrate / 100.0));
			
		}
		return kopo36_re;
	}
}
 
public class Programming_5_5_recipt2 {
	public static void main(String[] args) {
		Calendar kopo36_c = Calendar.getInstance(); // calendar라이브러리를 이횽해서 날짜를 받아온다.
		DecimalFormat kopo36_df = new DecimalFormat("###,###,###,###"); // deciaml포맷으로 돈에 콤마를 찍을 것이다.
		SimpleDateFormat kopo36_sdf = new SimpleDateFormat("yyyy/MM/dd-"); // 계산기에 날짜 포멧이 총 3개이다. 그것을 다 인스턴스화 해줬다.
		SimpleDateFormat kopo36_sdf2 = new SimpleDateFormat("yyyyMMdd HHmmss"); // 계산기에 날짜 포멧이 총 3개이다. 그것을 다 인스턴스화 해줬다.
		SimpleDateFormat kopo36_sdf3 = new SimpleDateFormat("yyyyMMddHHmmss"); // 계산기에 날짜 포멧이 총 3개이다. 그것을 다 인스턴스화 해줬다.
		
		String kopo36_itemname1, kopo36_itemname2; // 두개의 아이템을 String형으로 선언
		String kopo36_itemcode1, kopo36_itemcode2; // 두개 아이템의 코드를 String형으로 선언
		
		
		
		int kopo36_price1, kopo36_price2; //두 상품의 가격을 int로 선언
		int kopo36_sumPrice; //두 상품가격의 합
		
		
		
		int kopo36_num1, kopo36_num2; // 두 상품의 구매 개수를 int로 선언
		
		int kopo36_rate = 10; // 세율 10퍼
		int kopo36_taxFreePrice1; // 상품1의 세전가
		int kopo36_taxFreePrice2; // 상품2의세전가
		int kopo36_taxFreeSum; // 상품들의 세전가의 합
		
		
		int kopo36_taxPrice1; // 세금1
		int kopo36_taxPrice2; // 세금2
		int kopo36_taxSum; // 두 세금의 합
		
		
		kopo36_itemname1 = "볼펜"; // 상품명 초기화
		kopo36_itemcode1 = "8809169718205"; // 상품 코드 초기화
		kopo36_price1 = 1002320; // 소비자가
		kopo36_num1 = 1; // 개수
		
		kopo36_itemname2 = "공책"; // 상품명 초기화
		kopo36_itemcode2 = "8809169213444"; // 상품 코드 초기화
		kopo36_price2 = 1230; // 소비자가
		kopo36_num2 = 5; // 개수
		
		// 모든 상품의 총 금액
		int kopo36_allSum = (kopo36_price1 * kopo36_num1) + (kopo36_price2 * kopo36_num2);
		
		// 두 물품의 세전 합계
		kopo36_taxFreeSum = Taxcalc2.kopo36_taxcal2(kopo36_allSum, kopo36_rate); 

		
		// 세전가 구하는 공식1 
		if((int)(kopo36_price1 / (1 + kopo36_rate / 100.0)) == (double)(kopo36_price1 / (1 + kopo36_rate / 100.0))) { 
			kopo36_taxFreePrice1 = (int)(kopo36_price1 / (1 + kopo36_rate / 100.0));
			// int로 구한 세전가와 double로 구한 세전가가 같으면 그대로 사용
		} else {
			kopo36_taxFreePrice1 = (int)(kopo36_price1 / (1 + kopo36_rate / 100.0)) + 1; 
			// 소수점이 있다면 +1 하기
		}
		
		
		// 세전가 구하는 공식2 
		if((int)(kopo36_price2 / (1 + kopo36_rate / 100.0)) == (double)(kopo36_price2 / (1 + kopo36_rate / 100.0))) {
			kopo36_taxFreePrice2= (int)(kopo36_price2 / (1 + kopo36_rate / 100.0)); // 세전가
			// int로 구한 세전가와 double로 구한 세전가가 같으면 그대로 사용
		} else {
			kopo36_taxFreePrice2 = (int)(kopo36_price2 / (1 + kopo36_rate / 100.0)) + 1; // 세전가
			// 소수점이 있다면 +1 하기
		}
		
		
		// 세금 구하기
		kopo36_taxPrice1 = kopo36_price1 - kopo36_taxFreePrice1; // 세금 구하는 공식은 소비자가 - 세전가 하면 된다
		kopo36_taxPrice2 = kopo36_price2 - kopo36_taxFreePrice2; // 세금 구하는 공식은 소비자가 - 세전가 하면 된다

		
		
		// 세금의 합
		if (kopo36_taxFreeSum % 10 != 0) { // 상품들의 세전가의 합을 10으로 나누고 나머지값이 0이 아니라면
			kopo36_taxSum = kopo36_allSum - kopo36_taxFreeSum + 1; 
			kopo36_taxFreeSum = kopo36_allSum - kopo36_taxSum; 
		} else {
			// 상품들의 세전가의 합을 10으로 나누고 나머지값이 0이라면
			kopo36_taxSum = kopo36_allSum - kopo36_taxFreeSum; // 세금의 합
			kopo36_taxFreeSum = kopo36_allSum - kopo36_taxSum;
		}
		
		System.out.printf("%s\n", "충주(양평)휴게소");
		System.out.printf("%s\n", "충북충주시가금면용전리380-4");
		System.out.printf("%s%2s\n", "최병권 677-85-00239", "TEL:043-857-9229");
		System.out.printf("\n");
		System.out.printf("\n");
//		System.out.printf("\n");
		System.out.printf("%-6s%15s%9s%4s\n", "[정상등록]", kopo36_sdf2.format(kopo36_c.getTime()), "POS번호: ", "0002");
		// 날짜를 찍어야하기 때문에 만들어둔 sdf2날짜 변경형식으로 바꿔준다.
		System.out.printf("----------------------------------------\n");
		System.out.printf("%-13s%5s%3s%9s\n", "품목코드", "단가", "수량", "금액");
		System.out.printf("----------------------------------------\n");
		System.out.printf("%-14s\n", kopo36_itemname1);
		System.out.printf("%-14s%10.10s%5.5s%11.11s\n", kopo36_itemcode1,kopo36_df.format(kopo36_price1), 
				kopo36_df.format(kopo36_num1), kopo36_df.format(kopo36_price1 * kopo36_num1));
		// 돈과 수량은 데시말 포멧으로 콤마를 찍어줘야하기 때문에 데시말 포멧을 씌워준다
		System.out.printf("%-14s\n", kopo36_itemname2);
		System.out.printf("%-14s%10.10s%5.5s%11.11s\n", kopo36_itemcode2,kopo36_df.format(kopo36_price2),
				kopo36_df.format(kopo36_num2), kopo36_df.format(kopo36_price2 * kopo36_num2));
		// 돈과 수량은 데시말 포멧으로 콤마를 찍어줘야하기 때문에 데시말 포멧을 씌워준다
		System.out.printf("\n");
		System.out.printf("\n");
		System.out.printf("%-19s%15s\n", "과세 물품 합계", kopo36_df.format(kopo36_taxFreeSum));
		// 돈과 수량은 데시말 포멧으로 콤마를 찍어줘야하기 때문에 데시말 포멧을 씌워준다
		System.out.printf("%-19s%18s\n", "부    가    세", kopo36_df.format(kopo36_taxSum));
		// 돈과 수량은 데시말 포멧으로 콤마를 찍어줘야하기 때문에 데시말 포멧을 씌워준다
		System.out.printf("%-19s%19s\n", "합          계", kopo36_df.format(kopo36_allSum));
		// 돈과 수량은 데시말 포멧으로 콤마를 찍어줘야하기 때문에 데시말 포멧을 씌워준다
		System.out.printf("%-16s%19s\n", "026-비씨카드사", "00/00A");
		System.out.printf("%-15s%21s\n", "카  드  번  호 : ", "5522-20**-****-BADD");
		System.out.printf("%-15s%21s\n", "카  드  매  출 : ", kopo36_df.format(kopo36_allSum));
		// 돈과 수량은 데시말 포멧으로 콤마를 찍어줘야하기 때문에 데시말 포멧을 씌워준다
		System.out.printf("%-10s%s%16s\n", "승  인  번  호 : ", "04-KICC", "75549250");
		System.out.printf("%-19s%19s\n", "가 맹 점 번 호 : ", "");
		System.out.printf("%-15s%21s\n", "받  은  금  액 : ", kopo36_df.format(kopo36_allSum));
		// 돈과 수량은 데시말 포멧으로 콤마를 찍어줘야하기 때문에 데시말 포멧을 씌워준다
		System.out.printf("%-15s%22s\n", "거    스    름 : ", "0");
		System.out.printf("----------------------------------------\n");
		System.out.printf("%-12s%s\n" , "주문번호:", "0920");
		System.out.printf("----------------------------------------\n");
		System.out.printf("%6s%1s%5s\n", "판매원 : ", "000002", "편의점2");
		System.out.printf("%6s%s\n", kopo36_sdf.format(kopo36_c.getTime()), "0002-0922");
		// 날짜를 찍어야하기 때문에 만들어둔 sdf날짜 변경형식으로 바꿔준다.
		System.out.printf("%s[%s%15s]", "연동모듈:", "00138705", kopo36_sdf3.format(kopo36_c.getTime()));
		// 날짜를 찍어야하기 때문에 만들어둔 sdf3날짜 변경형식으로 바꿔준다.
	} 
}
