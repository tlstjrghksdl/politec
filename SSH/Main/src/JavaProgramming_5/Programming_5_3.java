package JavaProgramming_5;

public class Programming_5_3 {
	public static void main(String[] args) {
		int kopo36_weekDays = 3;
		// 2020년 1월 1일은 수요일이다. 그렇기 때문에 3으로 잡아 준다.
		int[] kopo36_endMonth = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		// 월별로 마지막 날을 배열에 담는다. 나중에 for문에서 이 배열요소의 길이만큼 실행시킬것이다.
		for (int kopo36_i = 1; kopo36_i < 13; kopo36_i++) {
		// for문을 1부터 12까지 돌린다.
			System.out.printf("\n\n%11d\n", kopo36_i);
			//월별로 나눌 것이기 때문에 월을 먼저 출력한다
			System.out.printf("=====================\n");
			System.out.printf("%2s%2s%2s%2s%2s%2s %2s", "일", "월", "화", "수", "목", "금", "토\n");
			// title과 header을 출력하는 과정...
			 
			
			if(kopo36_weekDays == 1) {         // weekDays가 1이면 그 달은 월요일부터 시작하는 달이다.
				System.out.printf("%3s", ""); // 그렇기 때문에 빈칸을 한개 앞에 넣어준다 
			} else if (kopo36_weekDays == 2) {         // weekDays가 2이면 그 달은 화요일부터 시작하는 달이다.
				System.out.printf("%3s%3s", "", "");  // 그렇기 때문에 빈칸을 두 개 앞에 넣어준다             
			} else if (kopo36_weekDays == 3) {                // weekDays가 3이면 그 달은 수요일부터 시작하는 달이다.
				System.out.printf("%3s%3s%3s", "", "", "");  // 그렇기 때문에 빈칸을 세 개 앞에 넣어준다             
			} else if (kopo36_weekDays == 4) {                       // weekDays가 4이면 그 달은 목요일부터 시작하는 달이다.
				System.out.printf("%3s%3s%3s%3s", "", "", "", "");  // 그렇기 때문에 빈칸을 네 개 앞에 넣어준다             
			} else if (kopo36_weekDays == 5) {                             // weekDays가 5이면 그 달은 금요일부터 시작하는 달이다.
				System.out.printf("%3s%3s%3s%3s%3s", "", "", "", "", ""); // 그렇기 때문에 빈칸을 다섯 개 앞에 넣어준다             
			} else if (kopo36_weekDays == 6) {                                    // weekDays가 6이면 그 달은 토요일부터 시작하는 달이다.
				System.out.printf("%3s%3s%3s%3s%3s%3s", "", "", "", "", "", ""); // 그렇기 때문에 빈칸을 한개 앞에 넣어준다             
			} else { // 그외는 아무 것도 하지 않는다.
			}
			
			for (int kopo36_j = 1; kopo36_j <= kopo36_endMonth[kopo36_i - 1]; kopo36_j++) {
			// for문을 배열 요소의 값만큼 실행 시킬 것이다. 1월이면 31까지, 2월이면 28까지...
				if (kopo36_weekDays == 7) { // weekDays 가 7이면, 즉 6까지 입력이 되고 +1되면,
					kopo36_weekDays = 0;    // 값을 0으로 바꾸고
					System.out.printf("\n");// 개행을 한다.
				} else {
				}
				System.out.printf("%3d", kopo36_j); //for문을 돌면서 한번씩 출력한다.
				kopo36_weekDays++; // weekDays를 for문이 돌때마다 증가시킨다. 

			}
		}
	}
} 