package JavaProgramming_5;

public class Programming_5_1 {
	public static void main(String[] args) {
		for (int kopo36_i = 1; kopo36_i < 10; kopo36_i = kopo36_i + 3) {
		// for문을 이용한다 1부터 9까지 돌아가고(구구단이기때문), kopo36_i의 값이 3씩 증가하게 한다.
		// 그렇게 하면 개행시 1, 4, 7이 된다. 10보다 클 수 없기 때문에 10은 안된다.
			System.out.printf("***************\t***************\t***************\n");
			// tab으로 구분한다
			System.out.printf(" 구구단 %2d  단\t 구구단 %2d  단\t 구구단 %2d  단\n", kopo36_i, kopo36_i + 1, kopo36_i + 2);
			// 한 줄이 1단 2단 3단 이렇게 3단씩 나와야 하기 때문에 %d에 넣을 값을 1씩 더한다. 총 3개
			System.out.printf("***************\t***************\t***************\n");
			for (int kopo36_j = 1; kopo36_j < 10; kopo36_j++) {
			// for문을 이용해 곱해질 값 1~9까지 실행시킨다.
				System.out.printf("%2d  * %2d  =  %2d\t", kopo36_i, kopo36_j, kopo36_j * kopo36_i);
				// "숫자a * 숫자b = 두 숫자의 곱" 의 형식으로 출려을 해야한다. 그렇기에 변수 i값이 1일때 변수 j값이 1~9까지 곱해지는 과정이다.
				System.out.printf("%2d  * %2d  =  %2d\t", kopo36_i + 1, kopo36_j, kopo36_j * (kopo36_i + 1));
				// 그 다음 칸에 들어갈 출력 값이다. 첫 칸에 1이었다면 2단을 출력하기 위해 변수 i값에 +1을 해줬다.
				System.out.printf("%2d  * %2d  =  %2d\t\n", kopo36_i + 2, kopo36_j, kopo36_j * (kopo36_i + 2));
				// 그~ 다음 칸에 들어갈 출력 값이다. 마찬가지로 처음 이 1단이었다면 3단을 출력해야 하기 때문에 변수 i에 + 2를 했다.
			}
		}  
	}
} 
