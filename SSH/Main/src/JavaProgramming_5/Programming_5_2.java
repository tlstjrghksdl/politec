package JavaProgramming_5;

public class Programming_5_2 {
	public static void main(String[] args) {
		for (int kopo36_i = 1; kopo36_i < 4; kopo36_i++) {
			// for문을 이용한다 1부터 3까지 돌아가고
			// 그렇게 하면 개행시 1, 2, 3이 된다. 
			System.out.printf("***************\t***************\t***************\n");
			System.out.printf(" 구구단 %2d  단\t 구구단 %2d  단\t 구구단 %2d  단  \n", kopo36_i, kopo36_i + 3, kopo36_i + 6);
			// 옆칸에 3씩 더해서 (1, 4, 7단), (2, 5, 8단), (3, 6, 9단)이렇게 출력
			System.out.printf("***************\t***************\t***************\n");
			for (int kopo36_j = 1; kopo36_j < 10; kopo36_j++) {
			// for문을 이용해 곱해질 값 1~9까지 실행시킨다.
				System.out.printf("%2d  * %2d  =  %2d\t", kopo36_i, kopo36_j, kopo36_j * kopo36_i);
				// "숫자a * 숫자b = 두 숫자의 곱" 의 형식으로 출려을 해야한다. 그렇기에 변수 i값이 1일때 변수 j값이 1~9까지 곱해지는 과정이다.
				System.out.printf("%2d  * %2d  =  %2d\t", kopo36_i + 3, kopo36_j, kopo36_j * (kopo36_i + 3));
				// 그 다음 칸에 들어갈 출력 값이다. 첫 칸에 1이었다면 4단을 출력하기 위해 변수 i값에 +3을 해줬다.
				System.out.printf("%2d  * %2d  =  %2d\t\n", kopo36_i + 6, kopo36_j, kopo36_j * (kopo36_i + 6));
				// 그~ 다음 칸에 들어갈 출력 값이다. 마찬가지로 처음 이 1단이었다면 7단을 출력해야 하기 때문에 변수 i에 +6를 했다.
			}
		} 
	}  
}
  