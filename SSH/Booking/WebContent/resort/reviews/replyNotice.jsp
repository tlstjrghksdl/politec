<%@page import="DAO.ReviewDAO"%>
<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@page import="DTO.Notif"%>
<%@page import="DAO.NotifDAO"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>

<%
	request.setCharacterEncoding("utf-8");
%>


<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
	
</script>
<link
	href="https://fonts.googleapis.com/css2?family=Caveat:wght@700&display=swap"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@900&display=swap"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../style2.css">
<script type="text/javascript" src="../script.js"></script>


<script type="text/javascript">
	function join() {

		var title = document.fr.title.value;

		var newtitle = title.replace(/(\s*)/g, "");
		if (newtitle.length == 0) {
			alert("공백입니다. 입력해주세요.")
			document.fr.title.focus();
			return false;
		}
	}
</script>
<style>
table {
	width: 1000px;
	border-top: 1px solid #444444;
	border-bottom: 1px solid #444444;
	border-collapse: collapse;
	margin-bottom: 10px;
}
</style>
</head>
<body>
<header id="header"></header>
	<%
		String uploadPath = "C:/Users/401-ST05/eclipse-workspace/Booking/WebContent/upload"; // 업로드 경로
	int maxFileSize = 1024 * 1024 * 2; // 업로드 제한 용량 = 2MB
	String encoding = "utf-8"; // 인코딩

	MultipartRequest multi = new MultipartRequest(request, uploadPath, maxFileSize, encoding,
			new DefaultFileRenamePolicy());

	String id = multi.getParameter("id");
	String writer = multi.getParameter("writer");
	String title = multi.getParameter("title");
	String date = multi.getParameter("date");
	String content = multi.getParameter("content");
	String rootid = multi.getParameter("rootid");
	String relevel = multi.getParameter("relevel");
	String recnt = multi.getParameter("recnt");
	String viewcnt = multi.getParameter("viewcnt");
	int inputRelevel = Integer.parseInt(relevel) + 1;
	int recnts = ReviewDAO.getRecnt(rootid, Integer.toString(inputRelevel)) + 1;

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	Calendar calendar = Calendar.getInstance();
	int getNum = ReviewDAO.getNum() + 1;
	request.setAttribute("getNum", getNum);
	request.setAttribute("date", sdf.format(calendar.getTime()));
	request.setAttribute("id", id);
	request.setAttribute("num", inputRelevel);
	request.setAttribute("recnt", recnts);

	/* StringBuffer sb = new StringBuffer();
	for(int i = 0; i <= Integer.parseInt(relevel); i++) {
		sb.append("-");
	}

	if(sb.length() > 0) {
		sb.append(">");
	} else{
	} */
	%>
	<!-- form을 통해 정보를 취합하여 전해주기 때문에 post사용 -->
	<div style="margin: auto; margin-top: 100px;">
	<div><h1>답글</h1></div>
	<form method="post" action="viewInsertedOne.jsp" name="fr"
		onsubmit="return join()" enctype="multipart/form-data">
		<table>
			<tr>
				<th>번호</th>
				<td><input type="hidden" value="${getNum}" name="id" />답글신규</td>
			</tr>
			<tr>
				<th>작성자</th>
				<td><input type="text" style="width: 200px;" name="writer"></td>
			</tr>
			<tr>
			<tr>
				<th>제목</th>
				<td><input type="text" style="width: 350px;" name="title"></td>
			</tr>
			<tr>
				<th>일자</th>
				<td><input type="hidden" value="${date}" name="date" />${date}</td>
			</tr>
			<tr>
				<th>내용</th>
				<td><textarea cols="132" rows="13" style="overflow: auto;"
						name="content"></textarea></td>
			</tr>
		</table>
		<input type="hidden" value="<%=rootid%>" name="rootid" /> <input
			type="hidden" value="${num}" name="relevel" /> <input type="hidden"
			value="${recnt}" name="recnt" /> <br>
		<div style="height: 350px;">
			<a href="mainReview.jsp"><input type="button" value="취소" id="button" /></a> <input
				type="submit" id="button" value="쓰기" />
		</div>
	</form>
	</div>
<footer id="footer"></footer>
</body>
</html>
<!-- 
<div style="overflow: auto; width: 500px; height: 500px;"></div> -->