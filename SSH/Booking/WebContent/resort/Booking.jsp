<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ page import="DAO.*"%>
<!DOCTYPE>
<html>
<head>
<meta charset=UTF-8>
<title></title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
	
</script>
<link
	href="https://fonts.googleapis.com/css2?family=Caveat:wght@700&display=swap"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@900&display=swap"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" href="style2.css">

</head>

<script type="text/javascript" src="script.js"></script>

<body>
	<%
		String Year = request.getParameter("year");
	String RoomNum = "";
	String paramSeason = "";

	if (request.getParameter("room") == null) {
		RoomNum = "다시 선택중";
	} else {
		RoomNum = request.getParameter("room") + "호";
	}

	String season = "";

	String Month = "";
	if (request.getParameter("month").length() == 1) {
		Month = "0" + request.getParameter("month");
	} else {
		Month = request.getParameter("month");
	}

	String day = "";
	if (request.getParameter("day") != null) {
		if (request.getParameter("day").length() == 1) {
			day = "0" + request.getParameter("day");
		} else {
			day = request.getParameter("day");
		}
	}
	//선택된 날짜
	String bookDate = "";
	if (request.getParameter("day") != null) {
		bookDate = Year + "-" + Month + "-" + day;
	} else {
		bookDate = "다시 선택중<br>";
	}

	if (request.getParameter("day") != null) {
		if ((Integer.parseInt(Month + day) >= 715) && (Integer.parseInt(Month + day) <= 815)) {
			season = "<b class='season'>성수기</b>요금이 적용 됩니다.";
			paramSeason = "성수기";
		} else {
			season = "<b class='season'>비수기</b>요금이 적용 됩니다.";
			paramSeason = "비수기";
		}
	}
	//현재 날짜 정보 
	Calendar cr = Calendar.getInstance();
	int year = cr.get(Calendar.YEAR);
	int month = cr.get(Calendar.MONTH);
	int date = cr.get(Calendar.DATE);

	String todayNum = Integer.toString(year) + Integer.toString(month + 1) + Integer.toString(date);

	//오늘 날짜
	String today = year + "년" + (month + 1) + "월" + date + "일";

	//선택한 연도 / 월
	String input_year = request.getParameter("year");
	String input_month = request.getParameter("month");

	if (input_month != null) {
		month = Integer.parseInt(input_month) - 1;
	}
	if (input_year != null) {
		year = Integer.parseInt(input_year);
	}
	// 1일부터 시작하는 달력을 만들기 위해 오늘의 연도,월을 셋팅하고 일부분은 1을 셋팅한다.
	cr.set(year, month, 1);

	// 셋팅한 날짜로 부터 아래 내용을 구함

	// 해당 월의 첫날를 구함
	int startDate = cr.getMinimum(Calendar.DATE);

	// 해당 월의 마지막 날을 구함
	int endDate = cr.getActualMaximum(Calendar.DATE);

	// 1일의 요일을 구함
	int startDay = cr.get(Calendar.DAY_OF_WEEK);

	int count = 0;
	%>
	<header id="header"></header>
	<div class="bookingFlex">
		<div class="bookingCalender">
			<form method="post" action="Booking.jsp" name="change">
				<table width="400" cellpadding="2" cellspacing="0" border="0"
					align="center">
					<tr>
						<td width="140" align="right"><input type="button" value="◁"
							onClick="monthDown(this.form)"></td>
						<td width="120" align="center"><select name="year"
							onchange="selectCheck(this.form)">
								<%
									RoomDAO.connect();
								for (int i = year - 10; i < year + 10; i++) {
									String selected = (i == year) ? "selected" : "";
									String color = (i == year) ? "#CCCCCC" : "#FFFFFF";
									out.print("<option value=" + i + " " + selected + " style=background:" + color + ">" + i + "</option>");
								}
								%>
						</select> <select name="month" onchange="selectCheck(this.form)">
								<%
									for (int i = 1; i <= 12; i++) {
									String selected = (i == month + 1) ? "selected" : "";
									String color = (i == month + 1) ? "#CCCCCC" : "#FFFFFF";
									out.print("<option value=" + i + " " + selected + " style=background:" + color + ">" + i + "</option>");
								}
								%>
						</select></td>
						<td width="140"><input type="button" value="▷"
							onClick="monthUp(this.form)"></td>
					</tr>
					<tr>
						<td align="center" colspan="3"><font size="2">선택된 날짜 :
								<%=bookDate%></font></td>
					</tr>
				</table>
			</form>
			<table width="700" cellpadding="2" cellspacing="0" border="1"
				align="center">
				<tr height="30">
					<td><font size="2">일</font></td>
					<td><font size="2">월</font></td>
					<td><font size="2">화</font></td>
					<td><font size="2">수</font></td>
					<td><font size="2">목</font></td>
					<td><font size="2">금</font></td>
					<td><font size="2">토</font></td>
				</tr>
				<tr height="30">
					<%
						for (int i = 1; i < startDay; i++) {
						count++;
					%>
					<td>&nbsp;</td>
					<%
						}
					for (int i = startDate; i <= endDate; i++) {
					String bgcolor = "";
					if (i < 10) {
						bgcolor = (bookDate.equals(Year + "-" + Month + "-0" + i)) ? "#CCCCCC" : "#FFFFFF";

					} else {
						bgcolor = (bookDate.equals(Year + "-" + Month + "-" + i)) ? "#CCCCCC" : "#FFFFFF";
					}
					String color = (count % 7 == 0 || count % 7 == 6) ? "red" : "black";
					count++;
					String resistDate = year + "-" + (month + 1) + "-" + i;
					String TD = "";
					if (i < 10) {
						TD = Integer.toString(year) + Integer.toString(month + 1) + "0" + Integer.toString(i);
					} else {
						TD = Integer.toString(year) + Integer.toString(month + 1) + Integer.toString(i);
					}
					%>
					<td bgcolor="<%=bgcolor%>"><font size="2" color=<%=color%>><%=i%>일<br>
							<br> <span class="select"> <%
					 	for (int j = 101; j < 104; j++) {
					 		if (Integer.parseInt(todayNum) <= Integer.parseInt(TD)) {
					 			if (RoomDAO.roomCheck(resistDate, j) == 0) {
									 %> <a class="select"
								href="Booking.jsp?room=<%=j%>&year=<%=year%>&month=<%=month + 1%>&day=<%=i%>"><%=j%>호</a><br>
								<%
								} else {
								%> <br> <%
							 	}
 							} else {
								 %> <br> <%
						 	}
						 }
						 %>
						</span></font></td>

					<%
						if (count % 7 == 0 && i < endDate) {
					%>
				</tr>
				<tr height="30">
					<%
						}
					}
					while (count % 7 != 0) {
					%>
					<td>&nbsp;</td>
					<%
						count++;
					}
					RoomDAO.disconnect();
					%>
				</tr>
			</table>
		</div>
		<div style="margin: auto;">
			<div>
				<div style="padding-top: 100px;">
					<table border="1" width="700">
						<tr>
							<td style="padding: 10px;" align="center" colspan="2"><span
								style="font-size: 25px;">선택일<br><%=bookDate%><br></span><%=season%></td>
						</tr>

						<tr>
							<td align="center">주소</td>
							<td style="padding: 10px;">서울시 강동구 성내로3가길<br>서울시 강동구
								성내동
							</td>
						</tr>
						<tr>
							<td align="center" style="padding: 10px;">전화번호</td>
							<td style="padding: 10px;">010-1123-2334</td>
						</tr>
						<tr>
							<td align="center" style="padding: 10px;">결제방법</td>
							<td style="padding: 10px;">무통장</td>
						</tr>
						<tr>
							<td align="center" style="padding: 10px;">기간안내</td>
							<td style="padding: 10px;">성수기 : 2020.07.15 ~ 2020.08.15<br>비수기
								: 그 외
							</td>
						</tr>
					</table>
					<input type="hidden" name="bookDate" value="<%=bookDate%>" /> <input
						type="hidden" name="RoomNum" value="<%=RoomNum%>" />
				</div>
			</div>
			<div style="width: 700px; float: left; padding-bottom: 200px;"
				align="center">
				<div>
					<h1>예약하기</h1>
				</div>
				<form action="InsertResult.jsp" method="post" name="check" onsubmit="return checked()">
					<input type="hidden" name="roomNum" value="<%=RoomNum%>"><input
						type="hidden" name="date" value="<%=bookDate%>">
					<table border="1" width="700">
						<tr>
							<td class="book" align="center">방 번호</td>
							<td style="padding: 20px;"><%=RoomNum%></td>
						</tr>
						<tr>
							<td class="book" align="center">날짜</td>
							<td style="padding: 20px;"><%=bookDate%></td>
						</tr>
						<tr>
							<td class="book" align="center">예약자</td>
							<td style="padding: 20px;"><input placeholder="성함"
								type="text" name="name" value="" id="name"/></td>
						</tr>
						<tr>
							<td class="book" align="center">인원</td>
							<td style="padding: 20px;"><select name="adult" id="adult"><option
										disabled="disabled" selected>성인</option>
									<option>1명</option>
									<option>2명</option></select> <select name="teen" id="teen"><option
										disabled="disabled" selected>청소년</option>
									<option>0명</option>
									<option>1명</option></select> <select name="child" id="child"><option
										disabled="disabled" selected>유아</option>
									<option>0명</option>
									<option>1명</option></select></td>
						</tr>
						<tr>
							<td colspan="2" class="book" align="center"><input
								type="submit" value="확인" id="bookNow"/></td>
						</tr>
					</table>
					<input type="hidden" value="<%=paramSeason%>" name="season">
				</form>
			</div>
		</div>
	</div>
	<footer id="footer"></footer>
		<script type="text/javascript">
		function checked() {
			var re2 = document.check.name.value;
			var newtitle = re2.replace(/(\s*)/g, "");	
			var re3 = document.check.adult.value;
			var newpasswd1 = re3.replace("성인", "");
			var re4 = document.check.teen.value;
			var newpasswd2 = re4.replace("청소년", "");
			var re5 = document.check.child.value;
			var newpasswd3 = re5.replace("유아", "");
			
			if(newtitle.length == 0) {
				alert("성함을 입력하세요");
				document.check.name.focus();
				return false;
			} else

			if(newpasswd1.length == 0) {
				alert("선택하지 않으셨습니다")
				document.check.adult.focus();
				return false;
			}

			if(newpasswd2.length == 0) {
				alert("선택하지 않으셨습니다")
				document.check.teen.focus();
				return false;
			}

			if(newpasswd3.length == 0) {
				alert("선택하지 않으셨습니다")
				document.check.child.focus();
				return false;
			}
		}
	</script>
</body>
</html>