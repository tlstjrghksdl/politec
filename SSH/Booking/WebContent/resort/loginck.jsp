<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*, javax.sql.*, java.net.*, java.io.*"%>
<%
	request.setCharacterEncoding("utf-8");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
	String jump = request.getParameter("jump");
	String id = request.getParameter("id");
	String passwd = request.getParameter("passwd");
	
	boolean bPassChk = false;
	
	if(id.replaceAll(" ", "").equals("admin") && passwd.replaceAll(" ", "").equals("admin")) {
		bPassChk = true;
	} else {
		bPassChk = false;
	}
	
	
	if(bPassChk) {
		session.setAttribute("login_ok", "yes");
		session.setAttribute("login_id", id);
		System.out.println(session);
		System.out.println(session.getCreationTime());
		response.sendRedirect(jump);
	} else {
		out.println("<h2>아이디 또는 패스워드 오류</h2>");
		out.println("<input type='button' value='로그인' onclick=\"location.href='login.jsp?jump=" + jump + "'\">");
	}
%>

</body>
</html>