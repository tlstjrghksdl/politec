function change(val) {
		var a = document.getElementById("money");	
		var b = Number(a.value);
		var c = Number(val);

		a.value = b + c;
	}

	function change1(val) {
		var a = document.getElementById("rate")
		var b = Number(a.value);
		var c = Number(val);

		a.value = c;
	}

	function change2(val) {
		var a = document.getElementById("term")
		var b = Number(a.value);
		var c = Number(val);

		a.value = c * 12;
	}

	function change3(val) {
		var a = document.getElementById("term1")
		var b = Number(a.value);
		var c = Number(val);

		a.value = c * 12;
	}
	
	function comma(num) {
		var len, point, str;
		num = num + "";
		point = num.length % 3;
		len = num.length;

		str = num.substring(0, point);
		while (point < len) {
			if (str != "")
				str += ",";
			str += num.substring(point, point + 3);
			point += 3;
		}
		return str;
	}
	
	
	let goBack = document.getElementById("goBack");
	goBack.onclick = function() {
		document.getElementById("money").value = 0;
        document.getElementById("rate").value = 0;
		document.getElementById("term").value = 0;
        document.getElementById("term1").value = 0; 
	}
	
	
	
	let home = document.getElementById("home");
	let a = document.getElementById("a");
	home.onclick = function() {
		a.innerHTML = "<div>";                                                   
			a.innerHTML += "<div id='ganTitle' class='backColor'>No</div>";
			a.innerHTML += "<div id='ganTitle' class='backColor'>상환금</div>";
			a.innerHTML += "<div id='ganTitle' class='backColor'>납임원금</div";
			a.innerHTML += "<div id='ganTitle' class='backColor'>이자</div>";
			a.innerHTML += "<div id='ganTitle' class='backColor'>납입원금계</div>";
			a.innerHTML += "<div id='ganTitle' class='backColor'>잔금</div></div>";
		
		var x = document.getElementById("inputBox");
		var z = document.getElementById("outter");
		if (x.style.display === "none") {
		  x.style.display = "block";
		  z.style.display = "none";
		} else {
		  x.style.display = "none";
		  z.style.display = "block";
		}
	}
		
	
	
	var calc = document.getElementById("calc");
	calc.onclick = function() {
		var x = document.getElementById("inputBox");
		var z = document.getElementById("outter");
		if (x.style.display === "none") {
		  x.style.display = "block";
		  z.style.display = "none";
		} else {
		  x.style.display = "none";
		  z.style.display = "block";
		}
		
		
		// 고객이 선택한 최종 대출금, 대출금리 등의 값들
        let loan = document.getElementById("money").value;
		console.log(loan);
        let rate = document.getElementById("rate").value;
        console.log(rate);
		let loanPeriod = document.getElementById("term").value;
        let defPeriod = document.getElementById("term1").value;
        let priRepayInterest = document.getElementById("data_text").innerHTML; 
		
           
        document.getElementById("loanValue").innerHTML=loan + "원";
        document.getElementById("rateValue").innerHTML=rate + "%"; 
        document.getElementById("loanPeriodValue").innerHTML=loanPeriod + "개월";
        document.getElementById("defPeriodValue").innerHTML=defPeriod + "개월";
        document.getElementById("priRepayInterestValue").innerHTML= '원리금균등상환';

		var amount_of_loans = parseInt(loan); //총 대출액 (원)
		console.log(amount_of_loans);

		var lending_rate = parseInt(rate) / 100;
		console.log(lending_rate);

		var monthly_installment_plan = parseInt(loanPeriod); //대출기간 (개월)
		console.log(monthly_installment_plan);

		var term2 = parseInt(defPeriod); //대출기간 (개월)
		console.log(monthly_installment_plan);

		var leeJa = 0;
		var power = Math.pow(1 + lending_rate / 12, monthly_installment_plan);
		var repayments = amount_of_loans * lending_rate
		/12*power/(power - 1);
		//document.getElementById("span__repayments").innerHTML = "상환금:"+Math.round(repayments)+"원";

		var interest = amount_of_loans * lending_rate / 12;
		//document.getElementById("span__interest").innerHTML = "이자:"+Math.round(interest)+"원";

		var principal = repayments - interest;
		//document.getElementById("span__principal").innerHTML = "원금:"+Math.round(principal)+"원";

		var balance = amount_of_loans - principal;
		//document.getElementById("span__balance").innerHTML = "잔금(잔액):"+Math.round(balance)+"원";
		var my_amount_of_loans = amount_of_loans;
		var tag = "";
		var nab = 0;
		for (var i = 1; i < term2 + 1; i++) {
			tag += "<div>";
			tag += "<div id='gan' class='backGround'>" + i + "</div>";
			tag += "<div id='gan' class='backGround'>" + comma(Math.round(interest)) + "</div>";
			tag += "<div id='gan' class='backGround'>" + 0 + "</div>";
			tag += "<div id='gan' class='backGround'>" + comma(Math.round(interest)) + "</div>";
			leeJa = leeJa + Math.round(interest);
			tag += "<div id='gan' class='backGround'>" + 0 + "</div>";
			tag += "<div id='gan' class='backGround'>" + amount_of_loans + "</div>";
			tag += "</div><br>";
		}
		for (var i = term2 + 1; i < monthly_installment_plan + 1 + term2; i++) {
			console.log("------------   " + Number(i) + "개월   ------------");
			var power = Math.pow(1 + lending_rate / 12,
					monthly_installment_plan);
			var repayments = amount_of_loans * lending_rate
			/12*power/(power - 1);

			var interest = my_amount_of_loans * lending_rate / 12;
			leeJa = leeJa + Math.round(interest);
			var principal = repayments - interest;
			nab = nab + Math.round(principal);
			var balance = my_amount_of_loans - principal;

			
			my_amount_of_loans = balance;
			tag += "<div>";
			tag += "<div id='gan' class='backGround'>" + i + "</div>";
			tag += "<div id='gan' class='backGround'>" + comma(Math.round(repayments))
					+ "</div>";
			tag += "<div id='gan' class='backGround'>" + comma(Math.round(principal))
					+ "</div>";
			tag += "<div id='gan' class='backGround'>" + comma(Math.round(interest)) + "</div>";
			tag += "<div id='gan' class='backGround'>" + comma(nab) + "</div>";
			tag += "<div id='gan' class='backGround'>" + comma(Math.round(balance)) + "</div>";
			tag += "</div><br>";
		}
		var a = document.getElementById("a");
		a.innerHTML = a.innerHTML + tag;
		document.getElementById("can").innerHTML = comma(leeJa) + " 원";
    }

	


	