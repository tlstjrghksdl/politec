<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="style.css" type="text/css" rel="stylesheet" />
</head>
<style>
</style>
<body>
	<%
		int money = Integer.parseInt(request.getParameter("money")); // 총대출액
	double rate = Integer.parseInt(request.getParameter("rate")) / 100; // 이자 %
	int term = Integer.parseInt(request.getParameter("term")); // 대출기간
	int year = term / 12;
	int term1 = Integer.parseInt(request.getParameter("term1")); // 거치기간
	int year1 = term1 / 12;
	DecimalFormat df = new DecimalFormat("###,###,###,###");
	%>
	<input id="InputBox_data" type="hidden" value="<%=money%>" />
	<div id="outter">
		<div id="innerd">
			<div style="">
				<div id="loan_InputBox_Title" class="backColor">대출금</div>
				<div id="InputBox_data" class="backGround" style=""><%=df.format(money)%>
				</div>
			</div>
			<div>
				<div id="loan_Rate_Title" class="backColor">대출금리</div>
				<div id="Rate_data" class="backGround"><%=request.getParameter("rate")%>%
				</div>
			</div>
			<div>
				<div id="loan_Duration_Title" class="backColor">대출기간</div>
				<div id="Duration_data" class="backGround"><%=term%>개월
				</div>
			</div>
			<div>
				<div id="loan_Period_Title" class="backColor">거치기간</div>
				<div id="Period_data" class="backGround"><%=term1%>
					개월 (<%=year1%>년)
				</div>
			</div>
			<div>
				<div id="loan_payType_Title" class="backColor">상환방법</div>
				<div id="payType_data" class="backGround">원리금균등상환</div>
			</div>
			<div>
				<div id="loan_payType_Title" class="backColor">총이자</div>
				<div id="can" class="backGround"><%=money%></div>
			</div>
		</div>
		<div id="a">
			<div>
				<div class="ganTitle">No</div>
				<div class="ganTitle">상환금</div>
				<div class="ganTitle">납임원금</div>
				<div class="ganTitle">이자</div>
				<div class="ganTitle">납입원금계</div>
				<div class="ganTitle">잔금</div>
			</div>
			<br>
		</div>
	</div>

	<br>
	<br>
	<br>
	<br>
	<br>

	<script type="text/javascript" src="calc.js">
		
	</script>
</body>
</html>
