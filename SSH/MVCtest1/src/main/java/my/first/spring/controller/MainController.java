package my.first.spring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import my.first.spring.controller.model.dto.PointDTO;


@Controller
public class MainController {

	
	@RequestMapping("/")
	public String home(Model model) {

		model.addAttribute("name", "신석환");
		model.addAttribute("message", "MVC Practice");
		return "/include/main";
	}
	@RequestMapping("gugu.do")
	public String gugu(Model model) {

		return "/include/gugudan";
	}
	
	@RequestMapping("Calculate.do")
	public String gugudan(@RequestParam(defaultValue = "") int number1, Model model) {
		String result="";
		
		for (int i = 1; i <= 9; i++) {
			result += number1 + " X " + i + " = " + number1*i + "<br>";
		}
		model.addAttribute("result", result);
		return "/include/resultGugu";
	}
	
	@RequestMapping("point.do")
	public String point(Model model) {

		return "/include/point";
	}
	
	
	@RequestMapping("point_result.do")
	public String pointResult(@ModelAttribute PointDTO dto, Model model) {
		dto.setTotal(dto.getKor() + dto.getEng() + dto.getMat());
		String average = String.format("%.2f", dto.getTotal()/3.0);
		dto.setAverage(Double.parseDouble(average));
		model.addAttribute("dto", dto);
		return "/include/point_result";
	}
	
	
	
	@RequestMapping("cal4.do")
	public String gugudan(@RequestParam String number1,@RequestParam String operator,@RequestParam String number2, Model model) {
		String result = "";
		
		if(operator.equals("+")) {
			result = Integer.toString(Integer.parseInt(number1) + Integer.parseInt(number2));
		} else if(operator.equals("-")) {
			result = Integer.toString(Integer.parseInt(number1) - Integer.parseInt(number2));
		} else if(operator.equals("*")) {
			result = Integer.toString(Integer.parseInt(number1) * Integer.parseInt(number2));
		} else {
			result = Integer.toString(Integer.parseInt(number1) / Integer.parseInt(number2));
		} 
		model.addAttribute("result", result);
		return "/include/resultGugu";
	}
	
	
}
