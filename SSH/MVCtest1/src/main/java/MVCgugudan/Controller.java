package MVCgugudan;

public class Controller {
	private GUGUDAN gugudan;
	private viewGUGU view;
	
	public Controller(GUGUDAN gugudan, viewGUGU view) {
		this.gugudan = gugudan;
		this.view = view;
	}
	
	public void setGugudanNum(int Num) {
		gugudan.setNum(Num);
	}
	
	public int getGugudanNum() {
		return gugudan.getNum();
	}
	
	public void updateView() {
		view.calculate(gugudan.getNum());
	}
}
