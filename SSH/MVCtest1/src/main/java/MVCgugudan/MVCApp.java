package MVCgugudan;

public class MVCApp {
	public static void main(String[] args) {
		GUGUDAN gugudan = retrieveGUGUDAN();
		viewGUGU view = new viewGUGU();
		
		Controller controller = new Controller(gugudan, view);
		controller.updateView();
	}

	public static GUGUDAN retrieveGUGUDAN() {
		GUGUDAN gugudan = new GUGUDAN();
		gugudan.setNum(7);
		return gugudan;
	}
}
