package my.kopo.exam;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import my.kopo.dto.Exam;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model) {
		logger.info("Welcome home!");
		
		
		model.addAttribute("name", "신석환");
		model.addAttribute("addr", "강동구");
		
		Exam exam = new Exam("제니", 209901, 90, 100, 100);
		model.addAttribute("exam", exam);
		
		List<Exam> exams = new ArrayList<Exam>();
		exams.add(new Exam("나연", 209901 ,90 ,90, 90));
		exams.add(new Exam("정연", 209902 ,90 ,90, 90));
		exams.add(new Exam("모모", 209903 ,90 ,90, 90));
		exams.add(new Exam("사나", 209904 ,90 ,90, 90));
		exams.add(new Exam("지효", 209905 ,90 ,90, 90));
		exams.add(new Exam("미나", 209906 ,90 ,90, 90));
		exams.add(new Exam("다현", 209907 ,90 ,90, 90));
		exams.add(new Exam("채영", 209908 ,90 ,90, 90));
		exams.add(new Exam("쯔위", 209909 ,90 ,90, 90));
		model.addAttribute("exams", exams);
		return "home";
	}
	
}