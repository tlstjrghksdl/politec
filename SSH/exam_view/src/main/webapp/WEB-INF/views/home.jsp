<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<% request.setCharacterEncoding("utf-8"); %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta charset="UTF-8">
	<title>Home</title>
</head>
<body>
이름 : ${name} <br>
주소 : ${addr} <br>

<hr>
	<table cellkspacing=1 width="600" border=1>
	<c:choose>
		<c:when test="${empty exam}">
			<tr>
				<td colspan=3>
					exam없다
				</td>
			</tr>
		</c:when>
		<c:otherwise>
			<tr>
				<td width="50"><p align="center">${exam.name}</p></td>
				<td width="50"><p align="center">${exam.studentid}</p></td>
				<td width="50"><p align="center">${exam.kor}</p></td>
				<td width="50"><p align="center">${exam.eng}</p></td>
				<td width="50"><p align="center">${exam.mat}</p></td>
			</tr>
		</c:otherwise>
	</c:choose>
	</table>
	<hr>
	<table cellkspacing=1 width="600" border=1>
	<c:choose>
		<c:when test="${empty exams}">
			<tr>
				<td colspan=3>
					exams 없다
				</td>
			</tr>
		</c:when>
		<c:otherwise>
				<c:forEach var="e" items="${exams}">
			<tr>
				<td width="50"><p align="center">${e.name}</p></td>
				<td width="50"><p align="center">${e.studentid}</p></td>
				<td width="50"><p align="center">${e.kor}</p></td>
				<td width="50"><p align="center">${e.eng}</p></td>
				<td width="50"><p align="center">${e.mat}</p></td>
			</tr>
			</c:forEach>
		</c:otherwise>
	</c:choose>
	</table></body>
</html>
