package service;

import java.util.List;

import kopo.domain.ExamSIO;

public interface ExamService {
	void createDB();
	void dropDB();
	// 서비스같은 것
	void allsetDB();
	//Read
	ExamSIO selectOne(int id);
	List<ExamSIO> selectAll();
	List<ExamSIO> selectAllByName(String name);
	//update
	int insert(ExamSIO examScore);
	int update(int id, ExamSIO examScore);
	int update(ExamSIO examScore);
	//Delete
	int delete(int id);
	int delete(ExamSIO examScore);
} 
