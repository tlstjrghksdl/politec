<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Welcome To Searching</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link
	href="https://fonts.googleapis.com/css2?family=Rowdies:wght@700&display=swap"
	rel="stylesheet">

<style type="text/css">
		h1 {
			font-family: 'Rowdies', cursive;
		}
</style>
</head>
<body style="background-color: black;">
	<div class="container">
		<div class="row">
			<div class="col">
				<h1 style="color: white;">Music Search</h1>
				<form action="result.jsp">
				<input type="text" name="songName" class="btn btn-light">
				<button type="submit" class="btn btn-light">SEARCH</button>
				</form>
			</div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>
</html>