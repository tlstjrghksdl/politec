package my.kopo.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import my.kopo.dto.Exam;

@Repository
public class ExamRepoImpl implements ExamRepo {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public void createDB() throws Exception {
		// TODO Auto-generated method stub
		String query = "create table examtable(" + "name varchar(20), studentid int not null primary key,"
				+ "kor int, eng int, mat int)";
		jdbcTemplate.execute(query);

	}

	@Override
	public void dropDB() throws Exception {
		// TODO Auto-generated method stub
		String query = "drop table examtable";
		jdbcTemplate.execute(query);
	}

	@Override
	public int save(Exam u) throws Exception {
		// TODO Auto-generated method stub
		String query = "insert into examtable(name, studentid, kor, eng, mat) values(?, ?, ?, ?, ?)";
		return jdbcTemplate.update(query, u.getName(), u.getStudentid(), u.getKor(), u.getEng(), u.getMat());
	}

	@Override
	public void update(Exam u) throws Exception {
		// TODO Auto-generated method stub
		String query = "update examtable set name=?, studentid=?, kor=?, eng=?, mat=? where studentid=?;";
		jdbcTemplate.update(query, u.getName(), u.getStudentid(), u.getKor(), u.getEng(), u.getMat(), u.getStudentid());
	}

	@Override
	public void delete(Exam u) throws Exception {
		// TODO Auto-generated method stub
		String query = "delete from examtable where studentid=?";
		jdbcTemplate.update(query, u.getStudentid());
	}

	@Override
	public List<Exam> getAllRecords() throws Exception {
		// TODO Auto-generated method stub\
		List<Exam> results = jdbcTemplate.query("select * from examtable", new RowMapper<Exam>() {
			@Override
			public Exam mapRow(ResultSet rs, int rowNum) throws SQLException {
				Exam u = new Exam();
				u.setName(rs.getNString("name"));
				u.setStudentid(rs.getInt("studentid"));
				u.setKor(rs.getInt("kor"));
				u.setEng(rs.getInt("eng"));
				u.setMat(rs.getInt("mat"));

				return u;
			}
		});
		return results;
	}

	@Override
	public Exam getRecordById(int id) throws Exception {
		// TODO Auto-generated method stub
		List<Exam> results = jdbcTemplate.query("select * from examtable where studentid=?", new RowMapper<Exam>() {
			@Override
			public Exam mapRow(ResultSet rs, int rowNum) throws SQLException {
				Exam u = new Exam();
				u.setName(rs.getNString("name"));
				u.setStudentid(rs.getInt("studentid"));
				u.setKor(rs.getInt("kor"));
				u.setEng(rs.getInt("eng"));
				u.setMat(rs.getInt("mat"));

				return u;
			}
		}, id);
		return results.isEmpty() ? null : results.get(0);
	}

}
