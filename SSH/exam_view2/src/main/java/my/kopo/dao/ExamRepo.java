package my.kopo.dao;

import java.util.List;

import my.kopo.dto.Exam;

public interface ExamRepo {
public void createDB() throws Exception;
	
	public void dropDB() throws Exception;
	
	public int save(Exam u) throws Exception;
	public void update(Exam u) throws Exception;
	public void delete(Exam u) throws Exception;
	public List<Exam> getAllRecords() throws Exception;
	public Exam getRecordById(int id) throws Exception;
	
}
