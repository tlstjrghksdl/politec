package my.kopo.service;

import java.util.List;

import my.kopo.dto.Exam;
import my.kopo.dto.ExamScore;

public interface ExamService {

	
	public void createDB() throws Exception;
	public void dropDB() throws Exception;
	public void allsetDB() throws Exception;
	
	public void insert(ExamScore examScore) throws Exception;
	
	public Exam selectOne(int id) throws Exception;
	public List<ExamScore> selectAll() throws Exception;
	public List<Exam> selectAllByName(String name) throws Exception;
	
	public void update(ExamScore u) throws Exception;
	public void update(int id, ExamScore u) throws Exception;
	
	
	public void delete(ExamScore u) throws Exception;
	public void delete(int id) throws Exception;
}
