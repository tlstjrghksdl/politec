package my.kopo.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import my.kopo.dao.ExamRepo;
import my.kopo.dto.Exam;
import my.kopo.dto.ExamScore;

@Service
public class ExamServiceImpl implements ExamService {

	@Inject
	private ExamRepo ER;

	@Override
	public void createDB() throws Exception {
		// TODO Auto-generated method stub
		ER.createDB();
	}

	@Override
	public void dropDB() throws Exception {
		// TODO Auto-generated method stub
		ER.dropDB();
	}

	@Override
	public void allsetDB() throws Exception {
		// TODO Auto-generated method stub
		this.insert(new ExamScore("나연", 209901, 91, 100, 95));
		this.insert(new ExamScore("정연", 209902, 92, 100, 95));
		this.insert(new ExamScore("모모", 209903, 93, 100, 95));
		this.insert(new ExamScore("사나", 209904, 94, 100, 95));
		this.insert(new ExamScore("지효", 209905, 80, 100, 95));
		this.insert(new ExamScore("미나", 209906, 96, 100, 95));
		this.insert(new ExamScore("다현", 209907, 97, 100, 95));
		this.insert(new ExamScore("채영", 209908, 98, 100, 95));
		this.insert(new ExamScore("쯔위", 209909, 99, 100, 95));
	}

	@Override
	public void insert(ExamScore u) throws Exception {
		// TODO Auto-generated method stub
		try {
			ER.save(new Exam(u.getName(), u.getStudentid(), u.getKor(), u.getEng(), u.getMat()));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public Exam selectOne(int id) throws Exception {
		// TODO Auto-generated method stub
		Exam exam = null;
		try {
			exam = ER.getRecordById(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return new Exam(exam.getName(), exam.getStudentid(), exam.getKor(), exam.getEng(), exam.getMat());
	}

	@Override
	public List<ExamScore> selectAll() throws Exception {
		// TODO Auto-generated method stub
		List<Exam> exams = null;
		try {
			exams = ER.getAllRecords();
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		List<ExamScore> examScores = new ArrayList<ExamScore>();
		for (int i = 0; i < exams.size(); i++) {
			examScores.add(new ExamScore(
					exams.get(i).getName(), 
					exams.get(i).getStudentid(), 
					exams.get(i).getKor(),
					exams.get(i).getEng(), 
					exams.get(i).getMat()

			));
		}
		return examScores;
	}

	@Override
	public List<Exam> selectAllByName(String name) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(ExamScore u) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(int id, ExamScore u) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(ExamScore u) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(int id) throws Exception {
		// TODO Auto-generated method stub

	}

}
