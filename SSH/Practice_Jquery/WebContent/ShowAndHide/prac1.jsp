<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
</script>
<script>
	$(document).ready(function() {
		if ($('p').hide()) {
			$('button').click(function() {
				$('p').show();
			})
		} else {
			$('button').click(function() {
				$('p').hide();
			})
		}
	})
</script>
<body>
	<p>안녕하세요~ 나는 사라질 놈입니다.</p>
	<button>버튼이다</button>
</body>