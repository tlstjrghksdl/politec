<%@page import="com.mysql.cj.util.DnsSrv.SrvRecord"%>

<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Arrays"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="my.first.dao.*" %>
<%@page import="my.first.dto.*"%>
<%@page import="my.first.service.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
 	request.setCharacterEncoding("utf-8");
 %>



<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>
<!-- 스타일을 정해둔다. -->
<style type="text/css">
	table {
		width: 1000px;
		border-top: 1px solid #444444;
		border-collapse: collapse;
		margin-bottom: 10px;
	}
	
	th, td {
		border-bottom: 1px solid #444444;
		padding: 5px;
	}
	div {
		width: 1000px;	
	}
	
	#none {
		border-bottom: none;
		border-top: none;
	}
	
	#button {
		background-color: #FF337B;
		color: white;
		width: 75px;
		height: 30px;
	}
</style>

<script type="text/javascript">
//onclick함수를 만든다. 답변으로 가는 것과 제거로 가는 함수이다.

function goDelete() {
	var del = document.forms['inputform'];
	del.action = '/delete';
	del.submit();
}
</script>
</head>
<body>
	
	<div style="padding-top: 20px;"> 
		<table style="border-color: #FF337B">
			<tr>
				<td style="border-bottom-color: #FF337B; font-size: 20px; color: #FF337B "><b>웹서버프로그래밍</b></td>
				<td style="border-bottom-color: #FF337B" align="right">
				<select style="height: 25px">
				<option value="none" selected="selected" disabled>2020년 1학기</option>
				</select>
				<select style="height: 25px">
				<option value="none" selected="selected" disabled>웹서버프로그래밍</option>
				</select></td>
			</tr>
		</table>
	</div>
	<!-- form을 통해 정보를 취합하여 전해주기 때문에 post사용 -->
	<form method="post" action="/update" id="inputform" onsubmit="return check()" >
	<table>
		<colgroup>
			<col width="15%">	
			<col width="25%">	
			<col width="15%">	
			<col width="25%">	
			<col width="12%">	
		</colgroup>
		<!-- 불러온 데이터를 UI에 표시하기위한 것 -->
		<tr>
			<th>년도/학기</th><td colspan="5">2020년도 1학기</td>
		</tr>
		<tr>
			<th>강의</th><td colspan="5">Spring 프로그래밍</td>
		</tr>
		<tr>
			<th>제목</th>
			<td  colspan="5">${noticeOne.title}</td>
		</tr>
		<tr>
			<th>작성일</th>
			<td>${noticeOne.date}</td>
			<th>작성자</th>
			<td>${noticeOne.writer}</td>
		</tr>
		<tr>
			<td colspan="6" height="250px;">${noticeOne.content}</td>
		</tr>
		<!-- UI에는 안보이지만 파라미터로 넘겨줄 값들을 hidden으로 숨겨둔다. -->
	</table>
	<input type="hidden" value="${noticeOne.date}" name="date">
	<input type="hidden" value="${noticeOne.writer}" name="writer">
	<input type="hidden" value="${noticeOne.content}" name="content">
	<input type="hidden" value="${noticeOne.title}" name="title">
	<input type="hidden" value="${noticeOne.id}" name="id">
	
		<div align="right">
		<!-- 목록을 눌렀을때, 목록을 보여주는 jsp로 이동한다. -->
		<a href="/"><input type="button" id="button" style="background-color: gray;" value="목록"/></a>
		<!-- 수정을 누르면 submit이 동작하면서 수정 jsp로 이동한다. -->
		<input type="submit" id="button" value="수정"/>
		<!-- 삭제를 클릭하면 삭제 jsp로 이동한다. -->
		<input type="button" onclick="goDelete();" id="button" value="삭제"/>
		</div>
	</form> 
</body>
</html><!-- 
<div style="overflow: auto; width: 500px; height: 500px;"></div> -->