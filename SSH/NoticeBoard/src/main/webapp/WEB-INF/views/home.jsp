<%@page import="com.mysql.cj.util.DnsSrv.SrvRecord"%>

<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Arrays"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="my.first.dao.*" %>
<%@page import="my.first.dto.*"%>
<%@page import="my.first.service.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
 	request.setCharacterEncoding("utf-8");
 %>


<!DOCTYPE html>
<html>
<head>

<meta charset="utf-8">
<title></title>

<!-- 스타일을 미리 정해준다. -->
<style type="text/css">
	table {
		width: 1000px;
		border-top: 1px solid #444444;
		border-bottom: 1px solid #444444;
		border-collapse: collapse;
		margin-bottom: 10px;
	}
	
	th, td {
		border-bottom: 1px solid #444444;
		padding: 5px;
	}
	div {
		width: 1000px;	
	}
	
	#none {
		border-bottom: none;
		border-top: none;
	}
	
	#button {
		background-color: #FF337B;
		color: white;
		width: 75px;
		height: 30px;
	}
</style>
<script type="text/javascript">

// 버튼을 클릭했을때 onclick="goSearch()시 mainNotice.jsp로 간다."
function goSearch() {
	var del = document.forms['inputform'];
	del.action = 'delete';
	del.submit();
}

</script>
</head>
<body>
		<div style="padding-top: 20px;">
			<table style="border: none;">
				<tr>
					<td><font size="5">자유게시판</font></td>
					<td align="right">Home > 강의실 > 학습지원실 > 자유게시판</td>
				</tr>
			</table>
		</div>
	
		<div style="padding-top: 20px;">
			<table style="border-color: #FF337B">
				<tr>
					<td style="border-bottom-color: #FF337B; font-size: 20px; color: #FF337B "><b>웹서버프로그래밍</b></td>
					<td style="border-bottom-color: #FF337B" align="right">
					<select style="height: 25px">
					<option value="none" selected="selected" disabled>2020년 1학기</option>
					</select>
					<select style="height: 25px">
					<option value="none" selected="selected" disabled>Spring 프로그래밍</option>
					</select></td>
				</tr>
			</table>
		</div>
		<form action="insert" method="get"  name="inputform">
		<table>
		<colgroup>
			<col width="5%">
			<col width="60%">
			<col width="15%">
			<col width="10%">
			<col width="10%">
		</colgroup>
			<tr align="center" bgcolor=gray >
				<th>번호</th>
				<th>제목</th>
				<th>등록일</th>
				<th>작성자</th>
			</tr>
			<c:forEach var="notice" items="${notices}">
			<tr>
			<td>${notice.id}</td>
			<td><a href="selectOne/?id=${notice.id}" >${notice.title}</a></td>
			<td>${notice.date}</td>
			<td>${notice.writer}</td>
			</tr>
			</c:forEach>
			<tr>
			<td><a href="/"><input type="button" id="button" value="글전체"/></a></td>
			<td colspan="5" align="right"><a href="insert"><input type="button" id="button" value="신규"/></a></td>
			</tr>
			</table>
			<input type="hidden" name="${notices}">
			</form>
			
</body>
</html>