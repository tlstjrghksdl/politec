package my.first.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import my.first.dto.Notice;

@Repository
public class NoticeDaoImpl implements NoticeDao {

	@Inject
	SqlSession sqlSession;
	private static final String Namespace = "my.first.mapper.noticeMapper";
	
	
	@Override
	public List<Notice> selectAll() {
		// TODO Auto-generated method stub
		return sqlSession.selectList(Namespace + ".select");
	}

	@Override
	public Notice selectOne(String id) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne(Namespace + ".selectOne", id);
	}

	@Override
	public void deleteNotice(String id) {
		// TODO Auto-generated method stub
		sqlSession.delete(Namespace + ".delete", id);
	}

	@Override
	public int insertOrUpdate(Notice notice) {
		// TODO Auto-generated method stub
	 return sqlSession.insert(Namespace + ".insertOrUpdate", notice);
	}

	@Override
	public int getNum() {
		// TODO Auto-generated method stub
		return sqlSession.selectOne(Namespace + ".getNum");
	}
	
}
