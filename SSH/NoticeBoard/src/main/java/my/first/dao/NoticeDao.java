package my.first.dao;

import java.util.List;

import my.first.dto.Notice;

public interface NoticeDao {
	public List<Notice> selectAll();
	public Notice selectOne(String id);
	public void deleteNotice(String id);
	public int insertOrUpdate(Notice notice);
	public int getNum();
}
