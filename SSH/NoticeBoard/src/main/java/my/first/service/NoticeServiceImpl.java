package my.first.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import my.first.dao.NoticeDao;
import my.first.dto.Notice;

@Service
public class NoticeServiceImpl implements NoticeService {

	@Inject
	NoticeDao dao;
	
	@Override
	public List<Notice> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public Notice selectOne(String id) {
		// TODO Auto-generated method stub
		return dao.selectOne(id);
	}

	@Override
	public void deleteNotice(String id) {
		// TODO Auto-generated method stub
		dao.deleteNotice(id);
	}

	@Override
	public int insertOrUpdate(Notice notice) {
		// TODO Auto-generated method stub
		return dao.insertOrUpdate(notice);
	}

	@Override
	public int getNum() {
		// TODO Auto-generated method stub
		return dao.getNum();
	}
	
}
