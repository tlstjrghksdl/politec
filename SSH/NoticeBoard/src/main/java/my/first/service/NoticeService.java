package my.first.service;

import java.util.List;

import my.first.dto.Notice;

public interface NoticeService {
	public List<Notice> selectAll();
	public Notice selectOne(String id);
	public void deleteNotice(String id);
	public int insertOrUpdate(Notice notice);
	public int getNum();
}
