package my.first.notice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import my.first.dto.Notice;
import my.first.service.NoticeService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	@Inject
	NoticeService service;

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model) {
		List<Notice> notices = new ArrayList<Notice>();
		notices = service.selectAll();
		
		model.addAttribute("notices", notices);
		return "home";
	}
	
	@RequestMapping(value = "insert", method = RequestMethod.GET)
	public String insert(Notice notice, Model model) {
		// 날짜를 입력하기 위한 simpledateformat, calendar
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		// 몇번째 글인지 num을 구한다.
		
		int getNum = 0;
		try {
			getNum = service.getNum() + 1;
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		model.addAttribute("getNum", getNum);
		model.addAttribute("date", sdf.format(date));
		return "insertNotice";
	}
	
	@RequestMapping(value ="selectOne", method = RequestMethod.GET)
	public String selectOne(@RequestParam("id") String id, Model model) {

		Notice noticeOne = service.selectOne(id);	
	
		model.addAttribute("noticeOne", noticeOne);

		return "selectOne";
	}
	
	@RequestMapping(value = "update", method = RequestMethod.POST)
	public String update(@ModelAttribute Notice noticeOne ,Model model) {
		System.out.println(noticeOne.getTitle());
		
		model.addAttribute("notice", noticeOne);
		
		return "update";
	}
	
	@RequestMapping(value = "insertOrUpdate", method = RequestMethod.POST)
	public String insertresult(@ModelAttribute Notice notice, Model model) {
		System.out.println(notice.getTitle());
		
		int result = service.insertOrUpdate(notice);
		
		if(result == 1) {
			model.addAttribute("result", "입력완료");
		} else {
			model.addAttribute("result", "입력이 안됐거나 수정됨");
		}
		
		return "Result";
	}
	
	@RequestMapping(value = "delete", method = RequestMethod.POST)
	public String delete(@ModelAttribute Notice noticeOne ,Model model) {
		System.out.println(noticeOne.getTitle());
		service.deleteNotice(noticeOne.getId());
		model.addAttribute("result", "삭제됐습니다");
		
		return "Result";
	}
}
